!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Driver for the halo updates
!>  @author Wayne Gaudin
!>  @details Invokes the kernels for the internal and external halo cells for
!>  the fields specified.

MODULE update_halo_module

CONTAINS

SUBROUTINE update_halo(fields,depth)

  USE clover_module
  USE update_tile_halo_module
  USE update_halo_kernel_module

  IMPLICIT NONE

  INTEGER :: tile,fields(NUM_FIELDS),depth

  ! Top/bottom exchange first
  CALL update_tile_halo(fields,depth,.FALSE.)
  CALL clover_exchange(fields,depth,.FALSE.)

  IF(chunk%chunk_neighbours(CHUNK_BOTTOM) .EQ. EXTERNAL_FACE) THEN
!$OMN PARALLEL
!$OMN DO
    DO tile=1,tiles_per_chunk
      IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.EXTERNAL_TILE) THEN
        CALL update_halo_kernel_b(chunk%tiles(tile)%t_xmin,                      &
                                  chunk%tiles(tile)%t_xmax,                      &
                                  chunk%tiles(tile)%t_ymin,                      &
                                  chunk%tiles(tile)%t_ymax,                      &
                                  chunk%tiles(tile)%field%total_mmcs0,           &
                                  chunk%tiles(tile)%field%total_components0,     &
                                  chunk%tiles(tile)%field%total_mmcs1,           &
                                  chunk%tiles(tile)%field%total_components1,     &
                                  chunk%tiles(tile)%field%total_mmcsmax,         &
                                  chunk%tiles(tile)%field%total_componentsmax,   &
                                  chunk%tiles(tile)%field%total_mmcs_halo,       &
                                  chunk%tiles(tile)%field%total_components_halo, &
                                  number_of_threads,                             &
                                  chunk%tiles(tile)%field%mmc_counter_next,      &
                                  chunk%tiles(tile)%field%component_counter_next,&
                                  chunk%tiles(tile)%field%density0,              &
                                  chunk%tiles(tile)%field%energy0,               &
                                  chunk%tiles(tile)%field%pressure,              &
                                  chunk%tiles(tile)%field%viscosity,             &
                                  chunk%tiles(tile)%field%soundspeed,            &
                                  chunk%tiles(tile)%field%density1,              &
                                  chunk%tiles(tile)%field%energy1,               &
                                  chunk%tiles(tile)%field%xvel0,                 &
                                  chunk%tiles(tile)%field%yvel0,                 &
                                  chunk%tiles(tile)%field%xvel1,                 &
                                  chunk%tiles(tile)%field%yvel1,                 &
                                  chunk%tiles(tile)%field%vol_flux_x,            &
                                  chunk%tiles(tile)%field%vol_flux_y,            &
                                  chunk%tiles(tile)%field%mass_flux_x,           &
                                  chunk%tiles(tile)%field%mass_flux_y,           &
                                  chunk%tiles(tile)%field%material0,             &
                                  chunk%tiles(tile)%field%mmc_index0,            &
                                  chunk%tiles(tile)%field%mmc_j0,                &
                                  chunk%tiles(tile)%field%mmc_k0,                &
                                  chunk%tiles(tile)%field%mmc_component0,        &
                                  chunk%tiles(tile)%field%mmc_material0,         &
                                  chunk%tiles(tile)%field%mmc_density0,          &
                                  chunk%tiles(tile)%field%mmc_energy0,           &
                                  chunk%tiles(tile)%field%mmc_pressure,          &
                                  chunk%tiles(tile)%field%mmc_soundspeed,        &
                                  chunk%tiles(tile)%field%mmc_viscosity,         &
                                  chunk%tiles(tile)%field%mmc_volume0,           &
                                  chunk%tiles(tile)%field%material1,             &
                                  chunk%tiles(tile)%field%mmc_index1,            &
                                  chunk%tiles(tile)%field%mmc_j1,                &
                                  chunk%tiles(tile)%field%mmc_k1,                &
                                  chunk%tiles(tile)%field%mmc_component1,        &
                                  chunk%tiles(tile)%field%mmc_material1,         &
                                  chunk%tiles(tile)%field%mmc_density1,          &
                                  chunk%tiles(tile)%field%mmc_energy1,           &
                                  chunk%tiles(tile)%field%mmc_volume1,           &
                                  fields,                                        &
                                  depth                                          )
      ENDIF
    ENDDO
!$OMN END DO
!$OMN END PARALLEL
  ENDIF

  IF(chunk%chunk_neighbours(CHUNK_TOP) .EQ. EXTERNAL_FACE) THEN
!$OMN PARALLEL
!$OMN DO
    DO tile=1,tiles_per_chunk
      IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.EXTERNAL_TILE) THEN
        CALL update_halo_kernel_t(chunk%tiles(tile)%t_xmin,                      &
                                  chunk%tiles(tile)%t_xmax,                      &
                                  chunk%tiles(tile)%t_ymin,                      &
                                  chunk%tiles(tile)%t_ymax,                      &
                                  chunk%tiles(tile)%field%total_mmcs0,           &
                                  chunk%tiles(tile)%field%total_components0,     &
                                  chunk%tiles(tile)%field%total_mmcs1,           &
                                  chunk%tiles(tile)%field%total_components1,     &
                                  chunk%tiles(tile)%field%total_mmcsmax,         &
                                  chunk%tiles(tile)%field%total_componentsmax,   &
                                  chunk%tiles(tile)%field%total_mmcs_halo,       &
                                  chunk%tiles(tile)%field%total_components_halo, &
                                  number_of_threads,                             &
                                  chunk%tiles(tile)%field%mmc_counter_next,      &
                                  chunk%tiles(tile)%field%component_counter_next,&
                                  chunk%tiles(tile)%field%density0,              &
                                  chunk%tiles(tile)%field%energy0,               &
                                  chunk%tiles(tile)%field%pressure,              &
                                  chunk%tiles(tile)%field%viscosity,             &
                                  chunk%tiles(tile)%field%soundspeed,            &
                                  chunk%tiles(tile)%field%density1,              &
                                  chunk%tiles(tile)%field%energy1,               &
                                  chunk%tiles(tile)%field%xvel0,                 &
                                  chunk%tiles(tile)%field%yvel0,                 &
                                  chunk%tiles(tile)%field%xvel1,                 &
                                  chunk%tiles(tile)%field%yvel1,                 &
                                  chunk%tiles(tile)%field%vol_flux_x,            &
                                  chunk%tiles(tile)%field%vol_flux_y,            &
                                  chunk%tiles(tile)%field%mass_flux_x,           &
                                  chunk%tiles(tile)%field%mass_flux_y,           &
                                  chunk%tiles(tile)%field%material0,             &
                                  chunk%tiles(tile)%field%mmc_index0,            &
                                  chunk%tiles(tile)%field%mmc_j0,                &
                                  chunk%tiles(tile)%field%mmc_k0,                &
                                  chunk%tiles(tile)%field%mmc_component0,        &
                                  chunk%tiles(tile)%field%mmc_material0,         &
                                  chunk%tiles(tile)%field%mmc_density0,          &
                                  chunk%tiles(tile)%field%mmc_energy0,           &
                                  chunk%tiles(tile)%field%mmc_pressure,          &
                                  chunk%tiles(tile)%field%mmc_soundspeed,        &
                                  chunk%tiles(tile)%field%mmc_viscosity,         &
                                  chunk%tiles(tile)%field%mmc_volume0,           &
                                  chunk%tiles(tile)%field%material1,             &
                                  chunk%tiles(tile)%field%mmc_index1,            &
                                  chunk%tiles(tile)%field%mmc_j1,                &
                                  chunk%tiles(tile)%field%mmc_k1,                &
                                  chunk%tiles(tile)%field%mmc_component1,        &
                                  chunk%tiles(tile)%field%mmc_material1,         &
                                  chunk%tiles(tile)%field%mmc_density1,          &
                                  chunk%tiles(tile)%field%mmc_energy1,           &
                                  chunk%tiles(tile)%field%mmc_volume1,           &
                                  fields,                                        &
                                  depth                                          )
      ENDIF
    ENDDO
!$OMN END DO
!$OMN END PARALLEL
  ENDIF

  ! Now left/right exchange
  CALL update_tile_halo(fields,depth,.TRUE.)
  CALL clover_exchange(fields,depth,.TRUE.)

  IF(chunk%chunk_neighbours(CHUNK_LEFT) .EQ. EXTERNAL_FACE) THEN
!$OMN PARALLEL
!$OMN DO
    DO tile=1,tiles_per_chunk
      IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.EXTERNAL_TILE) THEN
        CALL update_halo_kernel_l(chunk%tiles(tile)%t_xmin,                      &
                                  chunk%tiles(tile)%t_xmax,                      &
                                  chunk%tiles(tile)%t_ymin,                      &
                                  chunk%tiles(tile)%t_ymax,                      &
                                  chunk%tiles(tile)%field%total_mmcs0,           &
                                  chunk%tiles(tile)%field%total_components0,     &
                                  chunk%tiles(tile)%field%total_mmcs1,           &
                                  chunk%tiles(tile)%field%total_components1,     &
                                  chunk%tiles(tile)%field%total_mmcsmax,         &
                                  chunk%tiles(tile)%field%total_componentsmax,   &
                                  chunk%tiles(tile)%field%total_mmcs_halo,       &
                                  chunk%tiles(tile)%field%total_components_halo, &
                                  number_of_threads,                             &
                                  chunk%tiles(tile)%field%mmc_counter_next,      &
                                  chunk%tiles(tile)%field%component_counter_next,&
                                  chunk%tiles(tile)%field%density0,              &
                                  chunk%tiles(tile)%field%energy0,               &
                                  chunk%tiles(tile)%field%pressure,              &
                                  chunk%tiles(tile)%field%viscosity,             &
                                  chunk%tiles(tile)%field%soundspeed,            &
                                  chunk%tiles(tile)%field%density1,              &
                                  chunk%tiles(tile)%field%energy1,               &
                                  chunk%tiles(tile)%field%xvel0,                 &
                                  chunk%tiles(tile)%field%yvel0,                 &
                                  chunk%tiles(tile)%field%xvel1,                 &
                                  chunk%tiles(tile)%field%yvel1,                 &
                                  chunk%tiles(tile)%field%vol_flux_x,            &
                                  chunk%tiles(tile)%field%vol_flux_y,            &
                                  chunk%tiles(tile)%field%mass_flux_x,           &
                                  chunk%tiles(tile)%field%mass_flux_y,           &
                                  chunk%tiles(tile)%field%material0,             &
                                  chunk%tiles(tile)%field%mmc_index0,            &
                                  chunk%tiles(tile)%field%mmc_j0,                &
                                  chunk%tiles(tile)%field%mmc_k0,                &
                                  chunk%tiles(tile)%field%mmc_component0,        &
                                  chunk%tiles(tile)%field%mmc_material0,         &
                                  chunk%tiles(tile)%field%mmc_density0,          &
                                  chunk%tiles(tile)%field%mmc_energy0,           &
                                  chunk%tiles(tile)%field%mmc_pressure,          &
                                  chunk%tiles(tile)%field%mmc_soundspeed,        &
                                  chunk%tiles(tile)%field%mmc_viscosity,         &
                                  chunk%tiles(tile)%field%mmc_volume0,           &
                                  chunk%tiles(tile)%field%material1,             &
                                  chunk%tiles(tile)%field%mmc_index1,            &
                                  chunk%tiles(tile)%field%mmc_j1,                &
                                  chunk%tiles(tile)%field%mmc_k1,                &
                                  chunk%tiles(tile)%field%mmc_component1,        &
                                  chunk%tiles(tile)%field%mmc_material1,         &
                                  chunk%tiles(tile)%field%mmc_density1,          &
                                  chunk%tiles(tile)%field%mmc_energy1,           &
                                  chunk%tiles(tile)%field%mmc_volume1,           &
                                  fields,                                        &
                                  depth                                          )
      ENDIF
    ENDDO
!$OMN END DO
!$OMN END PARALLEL
  ENDIF

  IF(chunk%chunk_neighbours(CHUNK_RIGHT) .EQ. EXTERNAL_FACE) THEN
!$OMN PARALLEL
!$OMN DO
    DO tile=1,tiles_per_chunk
      IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.EXTERNAL_TILE) THEN
        CALL update_halo_kernel_r(chunk%tiles(tile)%t_xmin,                      &
                                  chunk%tiles(tile)%t_xmax,                      &
                                  chunk%tiles(tile)%t_ymin,                      &
                                  chunk%tiles(tile)%t_ymax,                      &
                                  chunk%tiles(tile)%field%total_mmcs0,           &
                                  chunk%tiles(tile)%field%total_components0,     &
                                  chunk%tiles(tile)%field%total_mmcs1,           &
                                  chunk%tiles(tile)%field%total_components1,     &
                                  chunk%tiles(tile)%field%total_mmcsmax,         &
                                  chunk%tiles(tile)%field%total_componentsmax,   &
                                  chunk%tiles(tile)%field%total_mmcs_halo,       &
                                  chunk%tiles(tile)%field%total_components_halo, &
                                  number_of_threads,                             &
                                  chunk%tiles(tile)%field%mmc_counter_next,      &
                                  chunk%tiles(tile)%field%component_counter_next,&
                                  chunk%tiles(tile)%field%density0,              &
                                  chunk%tiles(tile)%field%energy0,               &
                                  chunk%tiles(tile)%field%pressure,              &
                                  chunk%tiles(tile)%field%viscosity,             &
                                  chunk%tiles(tile)%field%soundspeed,            &
                                  chunk%tiles(tile)%field%density1,              &
                                  chunk%tiles(tile)%field%energy1,               &
                                  chunk%tiles(tile)%field%xvel0,                 &
                                  chunk%tiles(tile)%field%yvel0,                 &
                                  chunk%tiles(tile)%field%xvel1,                 &
                                  chunk%tiles(tile)%field%yvel1,                 &
                                  chunk%tiles(tile)%field%vol_flux_x,            &
                                  chunk%tiles(tile)%field%vol_flux_y,            &
                                  chunk%tiles(tile)%field%mass_flux_x,           &
                                  chunk%tiles(tile)%field%mass_flux_y,           &
                                  chunk%tiles(tile)%field%material0,             &
                                  chunk%tiles(tile)%field%mmc_index0,            &
                                  chunk%tiles(tile)%field%mmc_j0,                &
                                  chunk%tiles(tile)%field%mmc_k0,                &
                                  chunk%tiles(tile)%field%mmc_component0,        &
                                  chunk%tiles(tile)%field%mmc_material0,         &
                                  chunk%tiles(tile)%field%mmc_density0,          &
                                  chunk%tiles(tile)%field%mmc_energy0,           &
                                  chunk%tiles(tile)%field%mmc_pressure,          &
                                  chunk%tiles(tile)%field%mmc_soundspeed,        &
                                  chunk%tiles(tile)%field%mmc_viscosity,         &
                                  chunk%tiles(tile)%field%mmc_volume0,           &
                                  chunk%tiles(tile)%field%material1,             &
                                  chunk%tiles(tile)%field%mmc_index1,            &
                                  chunk%tiles(tile)%field%mmc_j1,                &
                                  chunk%tiles(tile)%field%mmc_k1,                &
                                  chunk%tiles(tile)%field%mmc_component1,        &
                                  chunk%tiles(tile)%field%mmc_material1,         &
                                  chunk%tiles(tile)%field%mmc_density1,          &
                                  chunk%tiles(tile)%field%mmc_energy1,           &
                                  chunk%tiles(tile)%field%mmc_volume1,           &
                                  fields,                                        &
                                  depth                                          )
      ENDIF
    ENDDO
!$OMN END DO
!$OMN END PARALLEL
  ENDIF

  ! Reset MMC pointers to real cell values
  DO tile=1,tiles_per_chunk
    chunk%tiles(tile)%field%total_mmcs_halo=chunk%tiles(tile)%field%mmc_counter_next(1)-1
    chunk%tiles(tile)%field%total_components_halo=chunk%tiles(tile)%field%component_counter_next(1)-1
    chunk%tiles(tile)%field%mmc_counter_next=MAX(chunk%tiles(tile)%field%total_mmcs1+1,1)
    chunk%tiles(tile)%field%component_counter_next=MAX(chunk%tiles(tile)%field%total_components1+1,1)
  ENDDO

END SUBROUTINE update_halo

END MODULE update_halo_module
