!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran kernel to update the external halo cells in a chunk.
!>  @author Wayne Gaudin
!>  @details Updates halo cells for the required fields at the required depth
!>  for any halo cells that lie on an external boundary. The location and type
!>  of data governs how this is carried out. External boundaries are always
!>  reflective.

MODULE update_mmc_halo_kernel_module

  USE data_module 

CONTAINS

  SUBROUTINE update_mmc_halo_kernel(x_min,x_max,y_min,y_max,                            &
                                    total_mmcs0,                                        &
                                    total_mmcs1,                                        &
                                    total_components0,                                  &
                                    total_components1,                                  &
                                    total_mmcsmax,                                      &
                                    total_componentsmax,                                &
                                    chunk_neighbours,                                   &
                                    tile_neighbours,                                    &
                                    material0,                                          &
                                    material1,                                          &
                                    mmc_index0,                                         &
                                    mmc_index1,                                         &
                                    mmc_component0,                                     &
                                    mmc_component1,                                     &
                                    mmc_j0,                                             &
                                    mmc_j1,                                             &
                                    mmc_k0,                                             &
                                    mmc_k1,                                             &
                                    mmc_material0,                                      &
                                    mmc_material1,                                      &
                                    mmc_volume0,                                        &
                                    mmc_volume1,                                        &
                                    mmc_density0,                                       &
                                    mmc_density1,                                       &
                                    density0,                                           &
                                    energy0,                                            &
                                    pressure,                                           &
                                    viscosity,                                          &
                                    soundspeed,                                         &
                                    density1,                                           &
                                    energy1,                                            &
                                    xvel0,                                              &
                                    yvel0,                                              &
                                    xvel1,                                              &
                                    yvel1,                                              &
                                    vol_flux_x,                                         &
                                    vol_flux_y,                                         &
                                    mass_flux_x,                                        &
                                    mass_flux_y,                                        &
                                    fields,                                             &
                                    depth                                               )
  IMPLICIT NONE

  INTEGER                                                  :: x_min,x_max,y_min,y_max
  INTEGER                                                  :: total_mmcs0,total_mmcs1,total_components0,total_components1
  INTEGER                                                  :: total_mmcsmax,total_componentsmax
  INTEGER,      DIMENSION(4)                               :: tile_neighbours, chunk_neighbours, depth_mask
  INTEGER,      DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0
  INTEGER,      DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material1
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_j0
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_j1
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_k0
  INTEGER,      DIMENSION(1:total_mmcsmax)                 :: mmc_k1
  INTEGER,      DIMENSION(1:total_componentsmax)           :: mmc_material0
  INTEGER,      DIMENSION(1:total_componentsmax)           :: mmc_material1
  INTEGER,      DIMENSION(1:total_componentsmax)           :: mmc_volume0
  INTEGER,      DIMENSION(1:total_componentsmax)           :: mmc_volume1
  INTEGER,      DIMENSION(1:total_componentsmax)           :: mmc_density0
  INTEGER,      DIMENSION(1:total_componentsmax)           :: mmc_density1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER :: fields(:),depth

  INTEGER :: j,k,component,mmc,mmc_new,component_index,component_index_new,current_mmc_index,current_component_index

  ! Note that at the moment unset set data is copied and then overwritten. With mmcs, this might cause crashes so
  ! logic might be needed that prevents this from happening if the MPI comms hasn't filled those cells in already.

  ! At the moment, 1 means pure, 2 means pure and mixed. Could switch to bit masks to allow both options in isolation

  ! Store the first free index for mmc and component
  current_mmc_index=total_mmcs0+1
  current_component_index=total_components0+1

  IF(fields(FIELD_MATERIAL0).GE.1) THEN
    IF( (chunk_neighbours(CHUNK_BOTTOM).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_BOTTOM).EQ.EXTERNAL_TILE) ) THEN
      DO j=x_min-depth,x_max+depth ! Should this have depth?
        DO k=1,depth
          material0(j,1-k)=material0(j,0+k)
          IF(material0(j,1-k).LT.1) THEN
            ! If copying a mmc then a new index value is needed. Should be atomic/critical section
            mmc=-1*material0(j,k)
            material0(j,1-k)=current_mmc_index
            mmc_new=material0(j,1-k)
            mmc_index0(mmc_new)=current_component_index
! Need an atomic/critical section here with OMP
            current_mmc_index=current_mmc_index+1
            current_component_index=current_component_index+mmc_component0(mmc_new)
            mmc_j0(mmc_new)=j
            mmc_k0(mmc_new)=1-k
            mmc_component0(mmc_new)=mmc_component0(mmc)
            ! Loop over components of the donor cell and update the halo data
            component_index=mmc_index0(mmc)
            component_index_new=mmc_index0(mmc_new)
            DO component=1,mmc_component0(mmc)
              mmc_material0(component_index_new+component-1)=mmc_material0(component_index+component-1)
              mmc_volume0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
            ENDDO
          ENDIF
        ENDDO
      ENDDO
    ENDIF
    IF( (chunk_neighbours(CHUNK_TOP).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_TOP).EQ.EXTERNAL_TILE) ) THEN
      DO j=x_min-depth,x_max+depth ! Should this have depth?
        DO k=1,depth
          material0(j,y_max+k)=material0(j,y_max+1-k)
          IF(material0(j,y_max+k).LT.1) THEN
            ! If copying a mmc then a new index value is needed. Should be atomic/critical section
            mmc=-1*material0(j,y_max+1-k)
            material0(j,y_max+k)=current_mmc_index
            mmc_new=material0(j,y_max+k)
            mmc_index0(mmc_new)=current_component_index
            current_mmc_index=current_mmc_index+1
            current_component_index=current_component_index+mmc_component0(mmc_new)
            mmc_j0(mmc_new)=j
            mmc_k0(mmc_new)=y_max+k
            mmc_component0(mmc_new)=mmc_component0(mmc)
            ! Loop over components of the donor cell and update the halo data
            component_index=mmc_index0(mmc)
            component_index_new=mmc_index0(mmc_new)
            DO component=1,mmc_component0(mmc)
              mmc_material0(component_index_new+component-1)=mmc_material0(component_index+component-1)
              mmc_volume0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
            ENDDO
          ENDIF
        ENDDO
      ENDDO
    ENDIF
    IF( (chunk_neighbours(CHUNK_LEFT).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_LEFT).EQ.EXTERNAL_TILE) ) THEN
      DO k=y_min-depth,y_max+depth ! This should have depth
        DO j=1,depth
          material0(1-j,k)=material0(0+j,k)
          IF(material0(1-j,k).LT.1) THEN
            ! If copying a mmc then a new index value is needed. Should be atomic/critical section
            mmc=-1*material0(j,k)
            material0(1-j,k)=current_mmc_index
            mmc_new=material0(1-j,k)
            mmc_index0(mmc_new)=current_component_index
            current_mmc_index=current_mmc_index+1
            current_component_index=current_component_index+mmc_component0(mmc_new)
            mmc_j0(mmc_new)=1-j
            mmc_k0(mmc_new)=k
            mmc_component0(mmc_new)=mmc_component0(mmc)
            ! Loop over components of the donor cell and update the halo data
            component_index=mmc_index0(mmc)
            component_index_new=mmc_index0(mmc_new)
            DO component=1,mmc_component0(mmc)
              mmc_material0(component_index_new+component-1)=mmc_material0(component_index+component-1)
              mmc_volume0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
            ENDDO
          ENDIF
        ENDDO
      ENDDO
    ENDIF
    IF( (chunk_neighbours(CHUNK_RIGHT).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_RIGHT).EQ.EXTERNAL_TILE) ) THEN
      DO k=y_min-depth,y_max+depth ! This should have depth
        DO j=1,depth
          material0(x_max+j,k)=material0(x_max+1-j,k)
          IF(material0(x_max+j,k).LT.1) THEN
            ! If copying a mmc then a new index value is needed. Should be atomic/critical section
            mmc=-1*material0(x_max+1-j,k)
            material0(x_max+j,k)=current_mmc_index
            mmc_new=material0(x_max+1-j,k)
            mmc_index0(mmc_new)=current_component_index
            current_mmc_index=current_mmc_index+1
            current_component_index=current_component_index+mmc_component0(mmc_new)
            mmc_j0(mmc_new)=x_max+1+j
            mmc_k0(mmc_new)=k
            mmc_component0(mmc_new)=mmc_component0(mmc)
            ! Loop over components of the donor cell and update the halo data
            component_index=mmc_index0(mmc)
            component_index_new=mmc_index0(mmc_new)
            DO component=1,mmc_component0(mmc)
              mmc_material0(component_index_new+component-1)=mmc_material0(component_index+component-1)
              mmc_volume0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
            ENDDO
          ENDIF
        ENDDO
      ENDDO
    ENDIF
  ENDIF

  ! Test what happens with performance if I pass data through to a subroutine rather than explicitly code all fields up

!!$OMP PARALLEL PRIVATE(j)

  ! Update values in external halo cells based on depth and fields requested
  ! Even though half of these loops look the wrong way around, it should be noted
  ! that depth is either 1 or 2 so that it is more efficient to always thread
  ! loop along the mesh edge.
  IF(fields(FIELD_DENSITY0).GE.1) THEN
    IF( (chunk_neighbours(CHUNK_BOTTOM).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_BOTTOM).EQ.EXTERNAL_TILE) ) THEN
!!$OMP DO
      DO j=x_min-depth,x_max+depth
        DO k=1,depth
          density0(j,1-k)=density0(j,0+k)
        ENDDO
      ENDDO
!!$OMP END DO
      IF(fields(FIELD_DENSITY0).GE.2) THEN
        mmc=-1*material0(j,k)
        mmc_new=material0(j,1-k)
        mmc_component0(mmc_new)=mmc_component0(mmc)
        component_index=mmc_index0(mmc)
        component_index_new=mmc_index0(mmc_new)
        DO component=1,mmc_component0(mmc)
          mmc_density0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
        ENDDO
      ENDIF
    ENDIF
    IF( (chunk_neighbours(CHUNK_TOP).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_TOP).EQ.EXTERNAL_TILE) ) THEN
!!$OMP DO
      DO j=x_min-depth,x_max+depth
        DO k=1,depth
          density0(j,y_max+k)=density0(j,y_max+1-k)
        ENDDO
      ENDDO
!!$OMP END DO
      IF(fields(FIELD_DENSITY0).GE.2) THEN
        mmc=-1*material0(j,y_max+1-k)
        mmc_new=material0(j,y_max+k)
        mmc_component0(mmc_new)=mmc_component0(mmc)
        component_index=mmc_index0(mmc)
        component_index_new=mmc_index0(mmc_new)
        DO component=1,mmc_component0(mmc)
          mmc_density0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
        ENDDO
      ENDIF
    ENDIF
    IF( (chunk_neighbours(CHUNK_LEFT).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_LEFT).EQ.EXTERNAL_TILE) ) THEN
!!$OMP DO
      DO k=y_min-depth,y_max+depth
        DO j=1,depth
          density0(1-j,k)=density0(0+j,k)
        ENDDO
      ENDDO
!!$OMP END DO
      IF(fields(FIELD_DENSITY0).GE.2) THEN
        mmc=-1*material0(j,k)
        mmc_new=material0(1-j,k)
        mmc_component0(mmc_new)=mmc_component0(mmc)
        component_index=mmc_index0(mmc)
        component_index_new=mmc_index0(mmc_new)
        DO component=1,mmc_component0(mmc)
          mmc_density0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
        ENDDO
      ENDIF
    ENDIF
    IF( (chunk_neighbours(CHUNK_RIGHT).EQ.EXTERNAL_FACE) .AND. (tile_neighbours(TILE_RIGHT).EQ.EXTERNAL_TILE) ) THEN
!!$OMP DO
      DO k=y_min-depth,y_max+depth
        DO j=1,depth
          density0(x_max+j,k)=density0(x_max+1-j,k)
        ENDDO
      ENDDO
!!$OMP END DO
      IF(fields(FIELD_DENSITY0).GE.2) THEN
        mmc=-1*material0(x_max+1-j,k)
        mmc_new=material0(x_max+j,k)
        mmc_component0(mmc_new)=mmc_component0(mmc)
        component_index=mmc_index0(mmc)
        component_index_new=mmc_index0(mmc_new)
        DO component=1,mmc_component0(mmc)
          mmc_density0(component_index_new+component-1)=mmc_volume0(component_index+component-1)
        ENDDO
      ENDIF
    ENDIF
  ENDIF

!$OMP END PARALLEL

END SUBROUTINE update_mmc_halo_kernel

END  MODULE update_mmc_halo_kernel_module
