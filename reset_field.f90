!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Reset field driver
!>  @author Wayne Gaudin
!>  @details Invokes the user specified field reset kernel.

MODULE reset_field_module

CONTAINS

SUBROUTINE reset_field()

  USE clover_module
  USE reset_field_kernel_module

  IMPLICIT NONE

  INTEGER :: tile

  REAL(KIND=8) :: kernel_time,timer

  IF(profiler_on) kernel_time=timer()
  
!$OMN PARALLEL
!$OMN DO
  DO tile=1,tiles_per_chunk
    CALL reset_field_kernel(chunk%tiles(tile)%t_xmin,                   &
                            chunk%tiles(tile)%t_xmax,                   &
                            chunk%tiles(tile)%t_ymin,                   &
                            chunk%tiles(tile)%t_ymax,                   &
                            chunk%tiles(tile)%field%total_mmcs0,        &
                            chunk%tiles(tile)%field%total_components0,  &
                            chunk%tiles(tile)%field%total_mmcs1,        &
                            chunk%tiles(tile)%field%total_components1,  &
                            chunk%tiles(tile)%field%total_mmcsmax,      &
                            chunk%tiles(tile)%field%total_componentsmax,&
                            chunk%tiles(tile)%field%density0,           &
                            chunk%tiles(tile)%field%density1,           &
                            chunk%tiles(tile)%field%energy0,            &
                            chunk%tiles(tile)%field%energy1,            &
                            chunk%tiles(tile)%field%xvel0,              &
                            chunk%tiles(tile)%field%xvel1,              &
                            chunk%tiles(tile)%field%yvel0,              &
                            chunk%tiles(tile)%field%yvel1,              &
                            chunk%tiles(tile)%field%material0,          &
                            chunk%tiles(tile)%field%material1,          &
                            chunk%tiles(tile)%field%mmc_material0,      &
                            chunk%tiles(tile)%field%mmc_material1,      &
                            chunk%tiles(tile)%field%mmc_index0,         &
                            chunk%tiles(tile)%field%mmc_index1,         &
                            chunk%tiles(tile)%field%mmc_j0,             &
                            chunk%tiles(tile)%field%mmc_k0,             &
                            chunk%tiles(tile)%field%mmc_j1,             &
                            chunk%tiles(tile)%field%mmc_k1,             &
                            chunk%tiles(tile)%field%mmc_component0,     &
                            chunk%tiles(tile)%field%mmc_component1,     &
                            chunk%tiles(tile)%field%mmc_density0,       &
                            chunk%tiles(tile)%field%mmc_density1,       &
                            chunk%tiles(tile)%field%mmc_energy0,        &
                            chunk%tiles(tile)%field%mmc_energy1,        &
                            chunk%tiles(tile)%field%mmc_volume0,        &
                            chunk%tiles(tile)%field%mmc_volume1         )
  ENDDO
!$OMN END DO
!$OMN END PARALLEL
  IF(profiler_on) profiler%reset=profiler%reset+(timer()-kernel_time)

END SUBROUTINE reset_field

END MODULE reset_field_module
