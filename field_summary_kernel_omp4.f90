!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran field summary kernel
!>  @author Wayne Gaudin
!>  @details The total mass, internal energy, kinetic energy and volume weighted
!>  pressure for the chunk is calculated.

MODULE field_summary_kernel_module

CONTAINS

SUBROUTINE field_summary_kernel(x_min,x_max,y_min,y_max, &
                                total_mmcs0,             &
                                total_components0,       &
                                total_mmcsmax,           &
                                total_componentsmax,     &
                                number_of_materials,     &
                                volume,                  &
                                density0,                &
                                energy0,                 &
                                pressure,                &
                                xvel0,                   &
                                yvel0,                   &
                                material0,               &
                                mmc_component0,          &
                                mmc_index0,              &
                                mmc_material0,           &
                                mmc_volume0,             &
                                mmc_density0,            &
                                mmc_energy0,             &
                                mmc_pressure,            &
                                vol,mass,ie,ke,press     )

  IMPLICIT NONE

  INTEGER(KIND=4)                                             :: x_min,x_max,y_min,y_max
  INTEGER                                                     :: total_mmcs0,total_components0
  INTEGER                                                     :: total_mmcsmax,total_componentsmax
  INTEGER(KIND=4)                                             :: number_of_materials
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: volume
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: pressure
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)    :: xvel0,yvel0
  INTEGER(KIND=4), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER(KIND=4), DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_volume0

  REAL(KIND=8), DIMENSION(number_of_materials)                :: vol,mass,ie,ke,press

  INTEGER      :: j,k,jv,kv,mat,mmc,component
  REAL(KIND=8) :: vsqrd,cell_vol,cell_mass

  LOGICAL          :: multi_material=.FALSE.

! The CRAY compiler does not yet allow array reductions so ATOMICS have to be
!  used instead in this case

  vol=0.0
  mass=0.0
  ie=0.0
  ke=0.0
  press=0.0

  IF (total_mmcs0.NE.0) multi_material=.TRUE.

!$OMP TARGET ENTER DATA MAP(TO:volume,density0,energy0,pressure                   )
!$OMP TARGET ENTER DATA MAP(TO:material0,mmc_index0,mmc_component0,mmc_material0  )
!$OMP TARGET ENTER DATA MAP(TO:mmc_density0,mmc_energy0,mmc_pressure,mmc_volume0  )
!$OMP TARGET ENTER DATA MAP(TO:xvel0,yvel0                                        )
!$OMP TARGET ENTER DATA MAP(TO:vol,mass,ie,ke,press                               )
!$OMP TARGET TEAMS DISTRIBUTE                                              &
!$OMP              PARALLEL DO                                             &
!$OMP              PRIVATE(j,k,vsqrd,cell_vol,cell_mass,mat,mmc,component) &
!$OMP              REDUCTION(+ : vol,mass,press,ie,ke)                     &
!$OMP              COLLAPSE(2)
  DO k=y_min,y_max
    DO j=x_min,x_max
      vsqrd=0.0
      DO kv=k,k+1
        DO jv=j,j+1
          vsqrd=vsqrd+0.25_8*(xvel0(jv,kv)**2+yvel0(jv,kv)**2)
        ENDDO
      ENDDO
      IF(material0(j,k).GT.0) THEN
        mat=material0(j,k)
        cell_vol=volume(j,k)
        cell_mass=cell_vol*density0(j,k)
!!$OMP ATOMIC WRITE
        vol(mat)=vol(mat)+cell_vol
!!$OMP ATOMIC WRITE
        mass(mat)=mass(mat)+cell_mass
!!$OMP ATOMIC WRITE
        ie(mat)=ie(mat)+cell_mass*energy0(j,k)
!!$OMP ATOMIC WRITE
        ke(mat)=ke(mat)+cell_mass*0.5_8*vsqrd
!!$OMP ATOMIC WRITE
        press(mat)=press(mat)+cell_vol*pressure(j,k)
      ELSE ! Could be placed after pure loop
        mmc=-1*material0(j,k)
        DO component=mmc_index0(mmc),mmc_index0(mmc)+mmc_component0(mmc)-1
          mat=mmc_material0(component)
!!$OMP ATOMIC WRITE
          cell_vol=volume(j,k)*mmc_volume0(component)
!!$OMP ATOMIC WRITE
          vol(mat)=vol(mat)+cell_vol
!!$OMP ATOMIC WRITE
          cell_mass=cell_vol*mmc_density0(component)
!!$OMP ATOMIC WRITE
          mass(mat)=mass(mat)+cell_mass
!!$OMP ATOMIC WRITE
          ie(mat)=ie(mat)+cell_mass*mmc_energy0(component)
!!$OMP ATOMIC WRITE
          ke(mat)=ke(mat)+cell_mass*0.5_8*vsqrd
!!$OMP ATOMIC WRITE
          press(mat)=press(mat)+cell_vol*mmc_pressure(component)
        ENDDO
      ENDIF
    ENDDO
  ENDDO
!$OMP END                         &
!$OMP TARGET TEAMS DISTRIBUTE     &
!$OMP PARALLEL DO
!$OMP TARGET EXIT DATA MAP (FROM:vol,mass,ie,ke,press)

END SUBROUTINE field_summary_kernel

END MODULE field_summary_kernel_module
