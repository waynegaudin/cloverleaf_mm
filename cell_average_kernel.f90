!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran Cell average kernel.
!>  @author Wayne Gaudin
!>  @details Calculates cell averages in multi-material cells

MODULE cell_average_kernel_module

CONTAINS

SUBROUTINE cell_average_kernel(x_min,x_max,y_min,y_max,                         &
                               total_mmcs,                                      &
                               total_components,                                &
                               density,                                         &
                               energy,                                          &
                               pressure,                                        &
                               soundspeed,                                      &
                               material,                                        &
                               mmc_index,                                       &
                               mmc_j,                                           &
                               mmc_k,                                           &
                               mmc_component,                                   &
                               mmc_density,                                     &
                               mmc_energy,                                      &
                               mmc_pressure,                                    &
                               mmc_soundspeed,                                  &
                               mmc_volume                                       )
  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max,total_mmcs,total_components

  REAL(KIND=8),OPTIONAL, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: density
  REAL(KIND=8),OPTIONAL, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: energy
  REAL(KIND=8),OPTIONAL, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: pressure
  REAL(KIND=8),OPTIONAL, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: soundspeed
  INTEGER(KIND=4),OPTIONAL, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material
  INTEGER(KIND=4),OPTIONAL, DIMENSION(1:total_mmcs)                    :: mmc_index
  INTEGER(KIND=4),OPTIONAL, DIMENSION(1:total_mmcs)                    :: mmc_component
  INTEGER(KIND=4),OPTIONAL, DIMENSION(1:total_mmcs)                    :: mmc_j,mmc_k
  REAL(KIND=8),OPTIONAL, DIMENSION(1:total_components)                 :: mmc_density
  REAL(KIND=8),OPTIONAL, DIMENSION(1:total_components)                 :: mmc_energy
  REAL(KIND=8),OPTIONAL, DIMENSION(1:total_components)                 :: mmc_pressure
  REAL(KIND=8),OPTIONAL, DIMENSION(1:total_components)                 :: mmc_soundspeed
  REAL(KIND=8),OPTIONAL, DIMENSION(1:total_components)                 :: mmc_volume

  INTEGER :: mmc,component
  REAL(KIND=8)                                                :: mmc_average,mass

  ! If I loop over mm cells, then I need to map the value back to the j,k cell
  ! Cell averages still need writing back, but I need the index arrays to do
  ! this correctly

  ! I coudld have an array of length total_mmcs that holds cell averages
  ! that I could write back in a seperate loop or use mapping array in the loop
  ! itself. Try both?

  IF(PRESENT(mmc_density)) THEN
!!$OMP PARALLEL DO
    DO mmc=1,total_mmcs
      mmc_average=0.0
      DO component=mmc_index(mmc),mmc_index(mmc)+mmc_component(mmc)-1
        mmc_average=mmc_average+mmc_density(component)*mmc_volume(component)
      ENDDO
      density(mmc_j(mmc),mmc_k(mmc))=mmc_average
    ENDDO
!!$OMP END PARALLEL DO
  ENDIF

  IF(PRESENT(mmc_energy)) THEN
!!$OMP PARALLEL DO
    DO mmc=1,total_mmcs
      mmc_average=0.0
      mass=0.0
      DO component=mmc_index(mmc),mmc_index(mmc)+mmc_component(mmc)-1
        mmc_average=mmc_average+mmc_energy(component)*mmc_volume(component)*mmc_density(component)
        mass=mass+mmc_density(component)*mmc_volume(component)
      ENDDO
      mmc_average=mmc_average/mass
      energy(mmc_j(mmc),mmc_k(mmc))=mmc_average
    ENDDO
!!$OMP END PARALLEL DO
  ENDIF

  IF(PRESENT(mmc_pressure)) THEN
!!$OMP PARALLEL DO
    DO mmc=1,total_mmcs
      mmc_average=0.0
      DO component=mmc_index(mmc),mmc_index(mmc)+mmc_component(mmc)-1
        mmc_average=mmc_average+mmc_pressure(component)*mmc_volume(component)
      ENDDO
      pressure(mmc_j(mmc),mmc_k(mmc))=mmc_average
    ENDDO
!!$OMP END PARALLEL DO
  ENDIF

  IF(PRESENT(mmc_soundspeed)) THEN
!!$OMP PARALLEL DO
    DO mmc=1,total_mmcs
      mmc_average=0.0
      DO component=mmc_index(mmc),mmc_index(mmc)+mmc_component(mmc)-1
        mmc_average=mmc_average+mmc_soundspeed(component)*mmc_volume(component)
      ENDDO
      soundspeed(mmc_j(mmc),mmc_k(mmc))=mmc_average
    ENDDO
!!$OMP END PARALLEL DO
  ENDIF

END SUBROUTINE cell_average_kernel

END MODULE cell_average_kernel_module

