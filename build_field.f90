!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief  Allocates the data for each mesh chunk
!>  @author Wayne Gaudin
!>  @details The data fields for the mesh chunk are allocated based on the mesh
!>  size.

SUBROUTINE build_field(tile)

   USE clover_module

   IMPLICIT NONE

   INTEGER :: tile,j,k,total_mmcsmax,total_componentsmax,thread

   REAL(KIND=8) :: mmc_delta,component_delta

   ALLOCATE(chunk%tiles(tile)%field%material0 (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%material1 (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%materialnew(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%density0  (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%density1  (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%densitynew(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%energy0   (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%energy1   (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%energynew (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%pressure  (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%viscosity (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%soundspeed(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                   chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))

   ALLOCATE(chunk%tiles(tile)%field%xvel0(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                      chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%xvel1(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                      chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%yvel0(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                      chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%yvel1(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                      chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))

   ALLOCATE(chunk%tiles(tile)%field%vol_flux_x (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%mass_flux_x(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%vol_flux_y (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%mass_flux_y(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))

   ALLOCATE(chunk%tiles(tile)%field%work_array1(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%work_array2(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%work_array3(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%work_array4(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%work_array5(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%work_array6(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%work_array7(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                            chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))

   ALLOCATE(chunk%tiles(tile)%field%cellx   (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2))
   ALLOCATE(chunk%tiles(tile)%field%celly   (chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%vertexx (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3))
   ALLOCATE(chunk%tiles(tile)%field%vertexy (chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%celldx  (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2))
   ALLOCATE(chunk%tiles(tile)%field%celldy  (chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%vertexdx(chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3))
   ALLOCATE(chunk%tiles(tile)%field%vertexdy(chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))
   ALLOCATE(chunk%tiles(tile)%field%volume  (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                         chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%xarea   (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+3, &
                                         chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
   ALLOCATE(chunk%tiles(tile)%field%yarea   (chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                         chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+3))

   chunk%tiles(tile)%field%total_mmcs0=0
   chunk%tiles(tile)%field%total_components0=0
   chunk%tiles(tile)%field%total_mmcs1=0
   chunk%tiles(tile)%field%total_components1=0
   ! Allocate some storage for mmc cells, with a guess based on mesh size
   total_mmcsmax=MAX(200,3*(chunk%tiles(tile)%t_xmax+chunk%tiles(tile)%t_ymax))
   total_componentsmax=2.5*total_mmcsmax
   chunk%tiles(tile)%field%total_mmcsmax=total_mmcsmax
   chunk%tiles(tile)%field%total_componentsmax=total_componentsmax
   ALLOCATE(chunk%tiles(tile)%field%mmc_j0(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_k0(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_j1(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_k1(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_jnew(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_knew(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_index0(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_index1(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_indexnew(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_component0(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_component1(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_componentnew(1:total_mmcsmax))

   ALLOCATE(chunk%tiles(tile)%field%mmc_volume0(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_volume1(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_volumenew(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_material0(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_material1(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_materialnew(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_density0(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_density1(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_densitynew(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_energy0(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_energy1(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_energynew(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_pressure(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_soundspeed(1:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_viscosity(1:total_componentsmax))

   ALLOCATE(chunk%tiles(tile)%field%mmc_counter_next(1:number_of_threads))
   ALLOCATE(chunk%tiles(tile)%field%component_counter_next(1:number_of_threads))

   ALLOCATE(chunk%tiles(tile)%field%mmc_start_index(0:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%mmc_index(0:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%comp_start_index(0:total_componentsmax))
   ALLOCATE(chunk%tiles(tile)%field%comp_index(0:total_componentsmax))

   ! Set some default thread starting locations for MMC storage

   mmc_delta=REAL(total_mmcsmax,8)/REAL(number_of_threads,8)
   component_delta=REAL(total_componentsmax,8)/REAL(number_of_threads,8)

   chunk%tiles(tile)%field%mmc_index(0)=1
   chunk%tiles(tile)%field%mmc_start_index(0)=1
   chunk%tiles(tile)%field%comp_index(0)=1
   chunk%tiles(tile)%field%comp_start_index(0)=1
 
   DO thread=1,number_of_threads
     chunk%tiles(tile)%field%mmc_index(thread)=1+(thread-1)*mmc_delta
     chunk%tiles(tile)%field%mmc_start_index(thread)=1+(thread-1)*mmc_delta
     chunk%tiles(tile)%field%comp_index(thread)=1+(thread-1)*component_delta
     chunk%tiles(tile)%field%comp_start_index(thread)=1+(thread-1)*component_delta
   ENDDO

   ALLOCATE(chunk%tiles(tile)%field%left_index_snd_comp_list(1:total_mmcsmax))     ! These can be smaller
   ALLOCATE(chunk%tiles(tile)%field%left_component_snd_comp_list(1:total_mmcsmax)) ! as the only need to hold

   ALLOCATE(chunk%tiles(tile)%field%right_index_snd_comp_list(1:total_mmcsmax))    ! data in a two cell layer
   ALLOCATE(chunk%tiles(tile)%field%right_component_snd_comp_list(1:total_mmcsmax))

   ALLOCATE(chunk%tiles(tile)%field%bottom_component_snd_comp_list(1:total_mmcsmax,2)) ! 2 is halo depth
   ALLOCATE(chunk%tiles(tile)%field%bottom_component_snd_comp_count(1:total_mmcsmax,2))

   ALLOCATE(chunk%tiles(tile)%field%top_component_snd_comp_list(1:total_mmcsmax,2))
   ALLOCATE(chunk%tiles(tile)%field%top_component_snd_comp_count(1:total_mmcsmax,2))

   ALLOCATE(chunk%tiles(tile)%field%left_index_rcv_comp_list(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%right_index_rcv_comp_list(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%bottom_index_rcv_comp_list(1:total_mmcsmax))
   ALLOCATE(chunk%tiles(tile)%field%top_index_rcv_comp_list(1:total_mmcsmax))

   !ALLOCATE(chunk%tiles(tile)%field%top_component_rcv_comp_list(1:total_mmcsmax))
   !ALLOCATE(chunk%tiles(tile)%field%bottom_component_rcv_comp_list(1:total_mmcsmax))

   !CALL associate_pointer(chunk%tiles(tile)%t_xmin,         &
   !                       chunk%tiles(tile)%t_xmax,         &
   !                       chunk%tiles(tile)%t_ymin,         &
   !                       chunk%tiles(tile)%t_ymax,         &
   !                       chunk%tiles(tile)%field%density0,  &
   !                       chunk%tiles(tile)%field%field_list(FIELD_DENSITY0)%smc_data)
   !CALL associate_pointer(chunk%tiles(tile)%field%field_list(FIELD_DENSITY0),chunk%tiles(tile)%field%density0)
   !CALL associate_pointer(chunk%tiles(tile)%field%field_list(FIELD_DENSITY1),chunk%tiles(tile)%field%density1)
   !CALL associate_pointer(chunk%tiles(tile)%field%field_list(FIELD_ENERGY0),chunk%tiles(tile)%field%energy0)
   !CALL associate_pointer(chunk%tiles(tile)%field%field_list(FIELD_ENERGY1),chunk%tiles(tile)%field%energy1)

   !DO k=1,1 !NUM_FIELDS
   !  write(*,*) chunk%tiles(tile)%field%field_list(k)%smc_data(1,1)
   !ENDDO

   ! Zeroing isn't strictly neccessary but it ensures physical pages
   ! are allocated. This prevents first touch overheads in the main code
   ! cycle which can skew timings in the first step

!$OMP PARALLEL
!$OMP DO 
   DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+3
     DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+3
       chunk%tiles(tile)%field%work_array1(j,k)=0.0
       chunk%tiles(tile)%field%work_array2(j,k)=0.0
       chunk%tiles(tile)%field%work_array3(j,k)=0.0
       chunk%tiles(tile)%field%work_array4(j,k)=0.0
       chunk%tiles(tile)%field%work_array5(j,k)=0.0
       chunk%tiles(tile)%field%work_array6(j,k)=0.0
       chunk%tiles(tile)%field%work_array7(j,k)=0.0

       chunk%tiles(tile)%field%xvel0(j,k)=0.0
       chunk%tiles(tile)%field%xvel1(j,k)=0.0
       chunk%tiles(tile)%field%yvel0(j,k)=0.0
       chunk%tiles(tile)%field%yvel1(j,k)=0.0
     ENDDO
   ENDDO
!$OMP END DO

!$OMP DO 
   DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+2
     DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+2
       chunk%tiles(tile)%field%material0(j,k)=0
       chunk%tiles(tile)%field%material1(j,k)=0
       chunk%tiles(tile)%field%density0(j,k)=0.0
       chunk%tiles(tile)%field%density1(j,k)=0.0
       chunk%tiles(tile)%field%energy0(j,k)=0.0
       chunk%tiles(tile)%field%energy1(j,k)=0.0
       chunk%tiles(tile)%field%pressure(j,k)=0.0
       chunk%tiles(tile)%field%viscosity(j,k)=0.0
       chunk%tiles(tile)%field%soundspeed(j,k)=0.0
       chunk%tiles(tile)%field%volume(j,k)=0.0
     ENDDO
   ENDDO
!$OMP END DO

!$OMP DO 
   DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+2  
     DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+3  
       chunk%tiles(tile)%field%vol_flux_x(j,k)=0.0
       chunk%tiles(tile)%field%mass_flux_x(j,k)=0.0
       chunk%tiles(tile)%field%xarea(j,k)=0.0
     ENDDO
   ENDDO
!$OMP END DO
!$OMP DO 
   DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+3
     DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+2
       chunk%tiles(tile)%field%vol_flux_y(j,k)=0.0
       chunk%tiles(tile)%field%mass_flux_y(j,k)=0.0
       chunk%tiles(tile)%field%yarea(j,k)=0.0
     ENDDO
   ENDDO
!$OMP END DO

!$OMP DO 
    DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+2
   chunk%tiles(tile)%field%cellx(j)=0.0
   chunk%tiles(tile)%field%celldx(j)=0.0
    ENDDO
!$OMP END DO
!$OMP DO 
    DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+2
   chunk%tiles(tile)%field%celly(k)=0.0
   chunk%tiles(tile)%field%celldy(k)=0.0
    ENDDO
!$OMP END DO

!$OMP DO 
    DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+3
     chunk%tiles(tile)%field%vertexx(j)=0.0
     chunk%tiles(tile)%field%vertexdx(j)=0.0
    ENDDO
!$OMP END DO
!$OMP DO 
    DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+3
     chunk%tiles(tile)%field%vertexy(k)=0.0
     chunk%tiles(tile)%field%vertexdy(k)=0.0
    ENDDO
!$OMP END DO

   chunk%tiles(tile)%field%mmc_counter_next(:)=1
   chunk%tiles(tile)%field%component_counter_next(:)=1

!$OMP END PARALLEL
 
END SUBROUTINE build_field

!SUBROUTINE associate_pointer(field_list_temp,real_data)
SUBROUTINE associate_pointer(x_min,x_max,y_min,y_max,smc_data,smc_data_pointer)

  !USE definitions_module

  IMPLICIT NONE

  ! A cheeky wave to get a list of pointers to allocatable arrays inside structs without the compiler moaning

  !TYPE(field_list) :: field_list_temp
!  REAL(KIND=8)  :: real_data(-1:,-1:)
  INTEGER :: x_min,x_max,y_min,y_max
  REAL(KIND=8), TARGET , DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: smc_data

  REAL(KIND=8), POINTER :: smc_data_pointer(:,:)

  
!write(0,*) smc_data(1,1)
  !field_list_temp%smc_data=>real_data
smc_data_pointer=>smc_data

end subroutine associate_pointer
