!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Communication Utilities
!>  @author Wayne Gaudin
!>  @details Contains all utilities required to run CloverLeaf in a distributed
!>  environment, including initialisation, mesh decompostion, reductions and
!>  halo exchange using explicit buffers.
!>
!>  Note the halo exchange is currently coded as simply as possible and no 
!>  optimisations have been implemented, such as post receives before sends or packing
!>  buffers with multiple data fields. This is intentional so the effect of these
!>  optimisations can be measured on large systems, as and when they are added.
!>
!>  Even without these modifications CloverLeaf weak scales well on moderately sized
!>  systems of the order of 10K cores.

MODULE clover_module

  USE data_module
  USE definitions_module
  USE MPI

  IMPLICIT NONE

CONTAINS

SUBROUTINE clover_barrier

  INTEGER :: err

  CALL MPI_BARRIER(MPI_COMM_WORLD,err)

END SUBROUTINE clover_barrier

SUBROUTINE clover_abort

  INTEGER :: ierr,err

  CALL MPI_ABORT(MPI_COMM_WORLD,ierr,err)

END SUBROUTINE clover_abort

SUBROUTINE clover_finalize

  INTEGER :: err

  CLOSE(g_out)
  CALL FLUSH(0)
  CALL FLUSH(6)
  CALL FLUSH(g_out)
  CALL MPI_FINALIZE(err)

END SUBROUTINE clover_finalize

SUBROUTINE clover_init_comms

  IMPLICIT NONE

  INTEGER :: err,rank,size

  rank=0
  size=1

  CALL MPI_INIT(err) 

  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,err) 
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,err) 

  parallel%parallel=.TRUE.
  parallel%task=rank

  IF(rank.EQ.0) THEN
    parallel%boss=.TRUE.
  ENDIF

  parallel%boss_task=0
  parallel%max_task=size

END SUBROUTINE clover_init_comms

SUBROUTINE clover_get_num_chunks(count)

  IMPLICIT NONE

  INTEGER :: count

! Should be changed so there can be more than one chunk per mpi task

  count=parallel%max_task

END SUBROUTINE clover_get_num_chunks

SUBROUTINE clover_decompose(x_cells,y_cells,left,right,bottom,top)

  ! This decomposes the mesh into a number of chunks.
  ! The number of chunks may be a multiple of the number of mpi tasks
  ! Doesn't always return the best split if there are few factors
  ! All factors need to be stored and the best picked. But its ok for now

  IMPLICIT NONE

  INTEGER :: x_cells,y_cells,left,right,top,bottom
  INTEGER :: c,delta_x,delta_y

  REAL(KIND=8) :: mesh_ratio,factor_x,factor_y
  INTEGER  :: chunk_x,chunk_y,mod_x,mod_y,split_found

  INTEGER  :: cx,cy,cnk,add_x,add_y,add_x_prev,add_y_prev

  ! 2D Decomposition of the mesh

  mesh_ratio=REAL(x_cells)/REAL(y_cells)

  chunk_x=number_of_chunks
  chunk_y=1

  split_found=0 ! Used to detect 1D decomposition
  DO c=1,number_of_chunks
    IF (MOD(number_of_chunks,c).EQ.0) THEN
      factor_x=number_of_chunks/REAL(c)
      factor_y=c
      !Compare the factor ratio with the mesh ratio
      IF(factor_x/factor_y.LE.mesh_ratio) THEN
        chunk_y=c
        chunk_x=number_of_chunks/c
        split_found=1
        EXIT
      ENDIF
    ENDIF
  ENDDO

  IF(split_found.EQ.0.OR.chunk_y.EQ.number_of_chunks) THEN ! Prime number or 1D decomp detected
    IF(mesh_ratio.GE.1.0) THEN
      chunk_x=number_of_chunks
      chunk_y=1
    ELSE
      chunk_x=1
      chunk_y=number_of_chunks
    ENDIF
  ENDIF

  delta_x=x_cells/chunk_x
  delta_y=y_cells/chunk_y
  mod_x=MOD(x_cells,chunk_x)
  mod_y=MOD(y_cells,chunk_y)

  ! Set up chunk mesh ranges and chunk connectivity

  add_x_prev=0
  add_y_prev=0
  cnk=1
  DO cy=1,chunk_y
    DO cx=1,chunk_x
      add_x=0
      add_y=0
      IF(cx.LE.mod_x)add_x=1
      IF(cy.LE.mod_y)add_y=1

      IF (cnk .EQ. parallel%task+1) THEN
        left   = (cx-1)*delta_x+1+add_x_prev
        right  = left+delta_x-1+add_x
        bottom = (cy-1)*delta_y+1+add_y_prev
        top    = bottom+delta_y-1+add_y

        chunk%chunk_neighbours(CHUNK_LEFT)=chunk_x*(cy-1)+cx-1
        chunk%chunk_neighbours(CHUNK_RIGHT)=chunk_x*(cy-1)+cx+1
        chunk%chunk_neighbours(CHUNK_BOTTOM)=chunk_x*(cy-2)+cx
        chunk%chunk_neighbours(CHUNK_TOP)=chunk_x*(cy)+cx

        IF(cx.EQ.1)       chunk%chunk_neighbours(CHUNK_LEFT)=EXTERNAL_FACE
        IF(cx.EQ.chunk_x) chunk%chunk_neighbours(CHUNK_RIGHT)=EXTERNAL_FACE
        IF(cy.EQ.1)       chunk%chunk_neighbours(CHUNK_BOTTOM)=EXTERNAL_FACE
        IF(cy.EQ.chunk_y) chunk%chunk_neighbours(CHUNK_TOP)=EXTERNAL_FACE
      ENDIF

      IF(cx.LE.mod_x)add_x_prev=add_x_prev+1
      cnk=cnk+1
    ENDDO
    add_x_prev=0
    IF(cy.LE.mod_y)add_y_prev=add_y_prev+1
  ENDDO

  IF(parallel%boss)THEN
    WRITE(g_out,*)
    WRITE(g_out,*)"Mesh ratio of ",mesh_ratio
    WRITE(g_out,*)"Decomposing the mesh into ",chunk_x," by ",chunk_y," chunks"
    WRITE(g_out,*)
  ENDIF

END SUBROUTINE clover_decompose

SUBROUTINE clover_tile_decompose(chunk_x_cells, chunk_y_cells)

  IMPLICIT NONE

  INTEGER :: chunk_x_cells, chunk_y_cells

  INTEGER :: tile_x, tile_y, split_found, t
  REAL(KIND=8) :: chunk_mesh_ratio,factor_x,factor_y,factor_ratio
  INTEGER :: chunk_delta_x, chunk_delta_y,  chunk_mod_x,  chunk_mod_y
  INTEGER :: add_x_prev, add_y_prev, tile, tx, ty, add_x, add_y, left, right, top, bottom

  chunk_mesh_ratio=REAL(chunk_x_cells)/REAL(chunk_y_cells)
  factor_ratio=9999.9

  tile_x=tiles_per_chunk
  tile_y=1

  split_found=0 ! Used to detect 1D decomposition
  DO t=1,tiles_per_chunk-1 ! debug bodge
    IF (MOD(tiles_per_chunk,t).EQ.0) THEN
      factor_x=tiles_per_chunk/REAL(t)
      factor_y=t
      !Compare the factor ratio with the mesh ratio
      IF(factor_x/factor_y.LE.factor_ratio) THEN
        tile_y=t
        tile_x=tiles_per_chunk/t
        split_found=1
        factor_ratio=factor_x/factor_y
      ENDIF
    ENDIF
  ENDDO

  IF(split_found.EQ.0.OR.tile_y.EQ.tiles_per_chunk) THEN ! Prime number or 1D decomp detected
    IF(chunk_mesh_ratio.GE.1.0) THEN
      tile_x=tiles_per_chunk
      tile_y=1
    ELSE
      tile_x=1
      tile_y=tiles_per_chunk
    ENDIF
  ENDIF

  chunk_delta_x=chunk_x_cells/tile_x
  chunk_delta_y=chunk_y_cells/tile_y
  chunk_mod_x=MOD(chunk_x_cells,tile_x)
  chunk_mod_y=MOD(chunk_y_cells,tile_y)

  add_x_prev=0
  add_y_prev=0
  tile=1
  DO ty=1,tile_y
    DO tx=1,tile_x
      add_x=0
      add_y=0
      IF(tx.LE.chunk_mod_x)add_x=1
      IF(ty.LE.chunk_mod_y)add_y=1

      left   = chunk%left+(tx-1)*chunk_delta_x+add_x_prev
      right  = left+chunk_delta_x-1+add_x
      bottom = chunk%bottom+(ty-1)*chunk_delta_y+add_y_prev
      top    = bottom+chunk_delta_y-1+add_y

      chunk%tiles(tile)%tile_neighbours(TILE_LEFT)=tile_x*(ty-1)+tx-1
      chunk%tiles(tile)%tile_neighbours(TILE_RIGHT)=tile_x*(ty-1)+tx+1
      chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM)=tile_x*(ty-2)+tx
      chunk%tiles(tile)%tile_neighbours(TILE_TOP)=tile_x*(ty)+tx

      IF(tx.EQ.1) THEN      
        IF(chunk%chunk_neighbours(CHUNK_LEFT).NE.EXTERNAL_FACE) THEN
          chunk%tiles(tile)%tile_neighbours(TILE_LEFT)=MPI_TILE
        ELSE
          chunk%tiles(tile)%tile_neighbours(TILE_LEFT)=EXTERNAL_TILE
        ENDIF
      ENDIF
      IF(tx.EQ.tile_x) THEN 
        IF(chunk%chunk_neighbours(CHUNK_RIGHT).NE.EXTERNAL_FACE) THEN
          chunk%tiles(tile)%tile_neighbours(TILE_RIGHT)=MPI_TILE
        ELSE
          chunk%tiles(tile)%tile_neighbours(TILE_RIGHT)=EXTERNAL_TILE
        ENDIF
      ENDIF
      IF(ty.EQ.1) THEN      
        IF(chunk%chunk_neighbours(CHUNK_BOTTOM).NE.EXTERNAL_FACE) THEN
          chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM)=MPI_TILE
        ELSE
          chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM)=EXTERNAL_TILE
        ENDIF
      ENDIF
      IF(ty.EQ.tile_y) THEN 
        IF(chunk%chunk_neighbours(CHUNK_TOP).NE.EXTERNAL_FACE) THEN
          chunk%tiles(tile)%tile_neighbours(TILE_TOP)=MPI_TILE
        ELSE
          chunk%tiles(tile)%tile_neighbours(TILE_TOP)=EXTERNAL_TILE
        ENDIF
      ENDIF

      IF(tx.LE.chunk_mod_x)add_x_prev=add_x_prev+1

      chunk%tiles(tile)%t_xmin = 1
      chunk%tiles(tile)%t_xmax = right - left + 1
      chunk%tiles(tile)%t_ymin = 1
      chunk%tiles(tile)%t_ymax = top - bottom + 1

      ! Global extent of the tile on the task chunk
      chunk%tiles(tile)%t_left = left
      chunk%tiles(tile)%t_right = right
      chunk%tiles(tile)%t_top = top
      chunk%tiles(tile)%t_bottom = bottom

      tile=tile+1
    ENDDO
    add_x_prev=0
    IF(ty.LE.chunk_mod_y)add_y_prev=add_y_prev+1
  ENDDO

END SUBROUTINE clover_tile_decompose

SUBROUTINE clover_allocate_buffers()

  IMPLICIT NONE

  ! Buffer sizes will need to either be expnaded or new buffers for mmc data added
  
  ! Unallocated buffers for external boundaries caused issues on some systems so they are now
  !  all allocated
  IF(parallel%task.EQ.chunk%task)THEN
    chunk%left_buffer_size=10*4*(chunk%y_max+5)
    ALLOCATE(chunk%left_snd_buffer(chunk%left_buffer_size))
    ALLOCATE(chunk%left_rcv_buffer(chunk%left_buffer_size))
    ALLOCATE(chunk%integer_left_snd_buffer(chunk%left_buffer_size))
    ALLOCATE(chunk%integer_left_rcv_buffer(chunk%left_buffer_size))
    chunk%right_buffer_size=10*4*(chunk%y_max+5)
    ALLOCATE(chunk%right_snd_buffer(chunk%right_buffer_size))
    ALLOCATE(chunk%right_rcv_buffer(chunk%right_buffer_size))
    ALLOCATE(chunk%integer_right_snd_buffer(chunk%right_buffer_size))
    ALLOCATE(chunk%integer_right_rcv_buffer(chunk%right_buffer_size))
    chunk%bottom_buffer_size=120*4*(chunk%x_max+5)
    ALLOCATE(chunk%bottom_snd_buffer(chunk%bottom_buffer_size))
    ALLOCATE(chunk%bottom_rcv_buffer(chunk%bottom_buffer_size))
    ALLOCATE(chunk%integer_bottom_snd_buffer(chunk%bottom_buffer_size))
    ALLOCATE(chunk%integer_bottom_rcv_buffer(chunk%bottom_buffer_size))
    chunk%top_buffer_size=120*4*(chunk%x_max+5)
    ALLOCATE(chunk%top_snd_buffer(chunk%top_buffer_size))
    ALLOCATE(chunk%top_rcv_buffer(chunk%top_buffer_size))
    ALLOCATE(chunk%integer_top_snd_buffer(chunk%top_buffer_size))
    ALLOCATE(chunk%integer_top_rcv_buffer(chunk%top_buffer_size))
  ENDIF

END SUBROUTINE clover_allocate_buffers

SUBROUTINE clover_exchange(fields,depth,left_right)

  IMPLICIT NONE

  LOGICAL      :: left_right
  INTEGER      :: fields(:),depth, tile
  !Send data
  INTEGER      :: left_offset(REAL_FIELDS,tiles_per_chunk),bottom_offset(REAL_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_left_offset(REAL_FIELDS,tiles_per_chunk)
  INTEGER      :: right_offset(REAL_FIELDS,tiles_per_chunk),top_offset(REAL_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_right_offset(REAL_FIELDS,tiles_per_chunk)
  INTEGER      :: left_offset_integer(INT_FIELDS,tiles_per_chunk),bottom_offset_integer(INT_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_left_offset_integer(INT_FIELDS,tiles_per_chunk),mmc_bottom_offset_integer(INT_FIELDS)
  INTEGER      :: right_offset_integer(INT_FIELDS,tiles_per_chunk),top_offset_integer(INT_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_right_offset_integer(INT_FIELDS,tiles_per_chunk)
  !Receive data
  INTEGER      :: left_rcv_offset(REAL_FIELDS,tiles_per_chunk),bottom_rcv_offset(REAL_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_left_rcv_offset(REAL_FIELDS,tiles_per_chunk),mmc_bottom_rcv_offset(REAL_FIELDS)
  INTEGER      :: right_rcv_offset(REAL_FIELDS,tiles_per_chunk),top_rcv_offset(REAL_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_right_rcv_offset(REAL_FIELDS,tiles_per_chunk),mmc_top_rcv_offset(REAL_FIELDS)
  INTEGER      :: left_rcv_offset_integer(INT_FIELDS,tiles_per_chunk),bottom_rcv_offset_integer(INT_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_left_rcv_offset_integer(INT_FIELDS,tiles_per_chunk),mmc_bottom_rcv_offset_integer(INT_FIELDS)
  INTEGER      :: right_rcv_offset_integer(INT_FIELDS,tiles_per_chunk),top_rcv_offset_integer(INT_FIELDS,tiles_per_chunk)
  INTEGER      :: mmc_right_rcv_offset_integer(INT_FIELDS,tiles_per_chunk),mmc_top_rcv_offset_integer(INT_FIELDS)

  INTEGER      :: request(8)
  INTEGER      :: message_count,err
  INTEGER      :: status(MPI_STATUS_SIZE,8)
  INTEGER      :: field,component_counter,component_counter_tile(tiles_per_chunk,2) ! Depth
  INTEGER      :: end_pack_index_snd_left,end_pack_index_snd_right,end_pack_index_snd_top,end_pack_index_snd_bottom
  INTEGER      :: end_pack_index_rcv_left,end_pack_index_rcv_right,end_pack_index_rcv_top,end_pack_index_rcv_bottom
  INTEGER      :: end_pack_index_snd_left_integer,end_pack_index_snd_right_integer,end_pack_index_snd_bottom_integer,end_pack_index_snd_top_integer
  INTEGER      :: end_pack_index_rcv_left_integer,end_pack_index_rcv_right_integer,end_pack_index_rcv_bottom_integer,end_pack_index_rcv_top_integer 

  INTEGER      :: mmc_offset_bottom,mmc_offset_top

  LOGICAL      :: integer_comm,mmc_comm

  INTEGER :: left_task,right_task,bottom_task,top_task
  INTEGER :: bottom_mmc_offset(tiles_per_chunk),total_bottom_mmc_offset

  ! These are at the MPI chunk level, and per halo depth
  INTEGER :: mmc_left_rcv(2),mmc_right_rcv(2),mmc_bottom_rcv(2),mmc_top_rcv(2)
  INTEGER :: mmc_left_snd(2),mmc_right_snd(2),mmc_bottom_snd(2),mmc_top_snd(2)
  INTEGER :: component_left_rcv(2),component_right_rcv(2),component_bottom_rcv(2),component_top_rcv(2)
  INTEGER :: component_left_snd(2),component_right_snd(2),component_bottom_snd(2),component_top_snd(2)

  INTEGER :: component_top_offset(0:tiles_per_chunk,NUM_FIELDS,2)
  INTEGER :: component_integer_top_offset(0:tiles_per_chunk,2)
  INTEGER :: mmc_count_top(tiles_per_chunk,2)

  INTEGER :: component_bottom_offset(0:tiles_per_chunk,NUM_FIELDS,2)
  INTEGER :: component_integer_bottom_offset(0:tiles_per_chunk,2)
  INTEGER :: mmc_count_bottom(tiles_per_chunk,2)

  INTEGER :: smc_top_offset,smc_bottom_offset

  INTEGER :: x_inc

  ! Assuming 1 patch per task, this will be changed

  ! fields=1 means cell communication
  ! fields=2 means mmc communication but the structure is unchanged (material not required in this case)
  ! fields=3 means mmc communication but with a structure change

  left_task =chunk%chunk_neighbours(CHUNK_LEFT) - 1
  right_task =chunk%chunk_neighbours(CHUNK_RIGHT) - 1
  bottom_task=chunk%chunk_neighbours(CHUNK_BOTTOM) - 1
  top_task=chunk%chunk_neighbours(CHUNK_TOP) - 1

  integer_comm=.FALSE.
  IF(left_right) THEN
    IF(fields(FIELD_MATERIAL1).EQ.3) THEN
      message_count=0
      integer_comm=.TRUE.
      mmc_left_snd=0
      component_left_snd=0
      mmc_right_snd=0
      component_right_snd=0
      DO tile=1,tiles_per_chunk
        ! Tot up contributions from each tile to the total MMC counts for the MPI task that are being sent, not per tile
        IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
          mmc_left_snd=mmc_left_snd+chunk%tiles(tile)%field%mmc_left_snd
          component_left_snd=component_left_snd+chunk%tiles(tile)%field%component_left_snd
        ENDIF
        IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
          mmc_right_snd=mmc_right_snd+chunk%tiles(tile)%field%mmc_right_snd
          component_right_snd=component_right_snd+chunk%tiles(tile)%field%component_right_snd
        ENDIF
      ENDDO
      IF(chunk%chunk_neighbours(CHUNK_LEFT).NE.EXTERNAL_FACE) THEN
        CALL MPI_ISEND(mmc_left_snd,2,MPI_INTEGER,left_task,2,MPI_COMM_WORLD,request(message_count+1),err)
        CALL MPI_IRECV(mmc_left_rcv,2,MPI_INTEGER,left_task,1,MPI_COMM_WORLD,request(message_count+2),err)
        CALL MPI_ISEND(component_left_snd,2,MPI_INTEGER,left_task,12,MPI_COMM_WORLD,request(message_count+3),err)
        CALL MPI_IRECV(component_left_rcv,2,MPI_INTEGER,left_task,11,MPI_COMM_WORLD,request(message_count+4),err)
        message_count=message_count+4
      ENDIF
      IF(chunk%chunk_neighbours(CHUNK_RIGHT).NE.EXTERNAL_FACE) THEN
        CALL MPI_ISEND(mmc_right_snd,2,MPI_INTEGER,right_task,1,MPI_COMM_WORLD,request(message_count+1),err)
        CALL MPI_IRECV(mmc_right_rcv,2,MPI_INTEGER,right_task,2,MPI_COMM_WORLD,request(message_count+2),err)
        CALL MPI_ISEND(component_right_snd,2,MPI_INTEGER,right_task,11,MPI_COMM_WORLD,request(message_count+3),err)
        CALL MPI_IRECV(component_right_rcv,2,MPI_INTEGER,right_task,12,MPI_COMM_WORLD,request(message_count+4),err)
        message_count=message_count+4
      ENDIF
      IF(message_count.GT.0) THEN
        CALL MPI_WAITALL(message_count,request,status,err)
      ENDIF
    ENDIF

    mmc_comm=.FALSE.

    request=0
    message_count=0

    ! Calculate buffer offsets for packed messages, taking into account whether MMC data is included or not

    ! I have not implemented code to deal with MMC comms only. Not sure if that functionlity is required

    ! Cell/Node data
    end_pack_index_snd_left=0 ! These are the total buffer sizes being sent
    end_pack_index_snd_right=0
    DO tile=1,tiles_per_chunk
      DO field=1,REAL_FIELDS
        IF(fields(field).GT.0) THEN
          left_offset(field,tile)=end_pack_index_snd_left
          right_offset(field,tile)=end_pack_index_snd_right
          ! Note the +5 in an upper bound for vertex variables.
          ! Perhaps it should match the actual upper bounds of the mesh variable to avoid
          ! the odd gap in the buffers. 
          IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
            end_pack_index_snd_left=end_pack_index_snd_left+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
          IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
            end_pack_index_snd_right=end_pack_index_snd_right+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
        ENDIF
      ENDDO
    ENDDO

    ! MMC data
    DO tile=1,tiles_per_chunk
      DO field=1,REAL_FIELDS
        IF(fields(field).GT.0) THEN
          mmc_left_offset(field,tile)=end_pack_index_snd_left
          mmc_right_offset(field,tile)=end_pack_index_snd_right
          IF(fields(field).GT.1)mmc_comm=.TRUE.
          IF(mmc_comm) THEN
            ! Offsets for MMC real data
            IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
              end_pack_index_snd_left=end_pack_index_snd_left                                    &
                                 +SUM(chunk%tiles(tile)%field%component_left_snd(1:depth))
            ENDIF
            IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
              end_pack_index_snd_right=end_pack_index_snd_right                                  &
                                 +SUM(chunk%tiles(tile)%field%component_right_snd(1:depth))
            ENDIF
          ENDIF
        ENDIF
      ENDDO
    ENDDO

    ! Integer data
    end_pack_index_snd_left_integer=0
    end_pack_index_snd_right_integer=0
    DO tile=1,tiles_per_chunk
      DO field=REAL_FIELDS+1,REAL_FIELDS+INT_FIELDS
        IF(fields(field).EQ.3) THEN
          integer_comm=.TRUE.
          left_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_snd_left_integer
          right_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_snd_right_integer
          IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
            end_pack_index_snd_left_integer=end_pack_index_snd_left_integer+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
          IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
            end_pack_index_snd_right_integer=end_pack_index_snd_right_integer+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
        ENDIF
      ENDDO
    ENDDO

    ! Integer MMC data
    DO tile=1,tiles_per_chunk
      DO field=REAL_FIELDS+1,REAL_FIELDS+INT_FIELDS
        IF(fields(field).EQ.3) THEN
          integer_comm=.TRUE.
          mmc_left_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_snd_left_integer
          mmc_right_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_snd_right_integer
          ! Offsets for MMC integer data
          IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
            end_pack_index_snd_left_integer=end_pack_index_snd_left_integer                    &
                               +SUM(chunk%tiles(tile)%field%mmc_left_snd(1:depth)      &
                               +chunk%tiles(tile)%field%component_left_snd(1:depth))
          ENDIF
          IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
            end_pack_index_snd_right_integer=end_pack_index_snd_right_integer                  &
                               +SUM(chunk%tiles(tile)%field%mmc_right_snd(1:depth)     &
                               +chunk%tiles(tile)%field%component_right_snd(1:depth))
          ENDIF
        ENDIF
      ENDDO
    ENDDO

! I CAN CALCULATE THE SIZE OF THE RECEIVE BUFFER AND AVERAGE OFFSETS BUT NOT THE MMC OFFSETS YET

    end_pack_index_rcv_left=0
    end_pack_index_rcv_right=0
    DO tile=1,tiles_per_chunk
      DO field=1,REAL_FIELDS
        IF(fields(field).GT.0) THEN
          left_rcv_offset(field,tile)=end_pack_index_rcv_left
          right_rcv_offset(field,tile)=end_pack_index_rcv_right
          ! Note the +5 in an upper bound for vertex variables.
          ! Perhaps it should match the actual upper bounds to avoid
          ! the odd gap in the buffers. 
          IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
            end_pack_index_rcv_left=end_pack_index_rcv_left+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
          IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
            end_pack_index_rcv_right=end_pack_index_rcv_right+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
          !IF(fields(field).GT.1)mmc_comm=.TRUE.
          !IF(mmc_comm) THEN
          !  ! Offsets for MMC real data, these need to be added to the total offset correctly
          !  end_pack_index_rcv_left=end_pack_index_rcv_left                            &
          !                     +SUM(component_left_rcv(1:depth))
          !  end_pack_index_rcv_right=end_pack_index_rcv_right                          &
          !                     +SUM(component_right_rcv(1:depth))
          !ENDIF
        ENDIF
      ENDDO
    ENDDO

    end_pack_index_rcv_left_integer=0
    end_pack_index_rcv_right_integer=0
    DO tile=1,tiles_per_chunk
      DO field=REAL_FIELDS+1,REAL_FIELDS+INT_FIELDS
        IF(fields(field).EQ.3) THEN
          integer_comm=.TRUE.
          left_rcv_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_rcv_left_integer
          right_rcv_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_rcv_right_integer
          IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
            end_pack_index_rcv_left_integer=end_pack_index_rcv_left_integer+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
          IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
            end_pack_index_rcv_right_integer=end_pack_index_rcv_right_integer+depth*(chunk%tiles(tile)%t_ymax+5)
          ENDIF
          ! Offsets for MMC integer data
          !end_pack_index_rcv_left_integer=end_pack_index_rcv_left_integer            &
          !                   +SUM(mmc_left_rcv(1:depth)                              &
          !                   +component_left_rcv(1:depth))
          !end_pack_index_rcv_right_integer=end_pack_index_rcv_right_integer          &
          !                   +SUM(mmc_right_rcv(1:depth)                             &
          !                   +component_right_rcv(1:depth))
        ENDIF
      ENDDO
    ENDDO

    ! Check buffer sizes are still adequate for messages
    IF(end_pack_index_snd_left.GT.chunk%left_buffer_size.OR.             &
      end_pack_index_rcv_left.GT.chunk%left_buffer_size.OR.         &
      end_pack_index_snd_left_integer.GT.chunk%left_buffer_size.OR.     &
      end_pack_index_rcv_left_integer.GT.chunk%left_buffer_size)THEN
      chunk%left_buffer_size=1.2*MAX(end_pack_index_snd_left,end_pack_index_rcv_left)
      DEALLOCATE(chunk%left_snd_buffer)
      DEALLOCATE(chunk%left_rcv_buffer)
      DEALLOCATE(chunk%integer_left_snd_buffer)
      DEALLOCATE(chunk%integer_left_rcv_buffer)
      ALLOCATE(chunk%left_snd_buffer(chunk%left_buffer_size))
      ALLOCATE(chunk%left_rcv_buffer(chunk%left_buffer_size))
      ALLOCATE(chunk%integer_left_snd_buffer(chunk%left_buffer_size))
      ALLOCATE(chunk%integer_left_rcv_buffer(chunk%left_buffer_size))
    ENDIF

    IF(end_pack_index_snd_right.GT.chunk%right_buffer_size.OR.            &
      end_pack_index_rcv_right.GT.chunk%right_buffer_size.OR.        &
      end_pack_index_snd_right_integer.GT.chunk%left_buffer_size.OR.     &
      end_pack_index_rcv_right_integer.GT.chunk%left_buffer_size)THEN
      chunk%right_buffer_size=1.2*MAX(end_pack_index_snd_right,end_pack_index_rcv_right)
      DEALLOCATE(chunk%right_snd_buffer)
      DEALLOCATE(chunk%right_rcv_buffer)
      DEALLOCATE(chunk%integer_right_snd_buffer)
      DEALLOCATE(chunk%integer_right_rcv_buffer)
      ALLOCATE(chunk%right_snd_buffer(chunk%right_buffer_size))
      ALLOCATE(chunk%right_rcv_buffer(chunk%right_buffer_size))
      ALLOCATE(chunk%integer_right_snd_buffer(chunk%right_buffer_size))
      ALLOCATE(chunk%integer_right_rcv_buffer(chunk%right_buffer_size))
    ENDIF

!!!!!!!!!!! THE MMC_LEFT_OFFSET etc HAVE NOT BEEN SET YET !!!!!!!!!!!!!!!!!!!!

    IF(chunk%chunk_neighbours(CHUNK_LEFT).NE.EXTERNAL_FACE) THEN
      ! do left exchanges
      ! Find left hand tiles
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
          CALL clover_pack_left(tile, fields, depth, left_offset(:,tile),mmc_left_offset(:,tile),mmc_comm)
        ENDIF
      ENDDO
!$OMN END PARALLEL DO

      !send and recv messagse to the left
      ! These are total sizes for the MPI chunk. Tile info is gone by now.
      CALL clover_send_recv_message_left(chunk%left_snd_buffer,                              &
                                         chunk%left_rcv_buffer,                              &
                                         end_pack_index_snd_left,                            &
                                         end_pack_index_rcv_left,                            &
                                         1, 2,                                               &
                                         request(message_count+1), request(message_count+2))
      message_count = message_count + 2

! If I do this before the reals I will know the MMC structure.
      IF(integer_comm) THEN
!$OMN   PARALLEL DO
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
            CALL clover_pack_left_integer(tile, fields, depth, left_offset_integer(:,tile),mmc_left_offset_integer(:,tile))
          ENDIF
        ENDDO
!$OMN   END PARALLEL DO  
      ENDIF

      !send integer message downwards
      IF(integer_comm) THEN
        CALL clover_send_recv_message_left_integer(chunk%integer_left_snd_buffer,                     &
                                                   chunk%integer_left_rcv_buffer,                     &
                                                   end_pack_index_snd_left_integer,                   &
                                                   end_pack_index_rcv_left_integer,                   &
                                                   11, 12,                                            &
                                                   request(message_count+1), request(message_count+2))
        message_count = message_count + 2
      ENDIF

    ENDIF

    IF(chunk%chunk_neighbours(CHUNK_RIGHT).NE.EXTERNAL_FACE) THEN
      ! do right exchanges
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
          CALL clover_pack_right(tile,fields, depth,right_offset(:,tile),mmc_right_offset(:,tile),mmc_comm)
        ENDIF
      ENDDO
!$OMN END PARALLEL DO

      !send message to the right
      CALL clover_send_recv_message_right(chunk%right_snd_buffer,                             &
                                          chunk%right_rcv_buffer,                             &
                                          end_pack_index_snd_right,                           &
                                          end_pack_index_rcv_right,                           &
                                          2, 1,                                               &
                                          request(message_count+1), request(message_count+2))
      message_count = message_count + 2

      IF(integer_comm) THEN
!$OMN   PARALLEL DO
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
            CALL clover_pack_right_integer(tile, fields, depth, right_offset_integer(:,tile),mmc_right_offset(:,tile))
          ENDIF
        ENDDO
!$OMN   END PARALLEL DO  
      ENDIF

      !send integer message upwards
      IF(integer_comm) THEN
        CALL clover_send_recv_message_right_integer(chunk%integer_right_snd_buffer,                        &
                                                    chunk%integer_right_rcv_buffer,                        &
                                                    end_pack_index_snd_right_integer,                      &
                                                    end_pack_index_rcv_right_integer,                      &
                                                    12, 11,                                                &
                                                    request(message_count+1), request(message_count+2))
        message_count = message_count + 2
      ENDIF

    ENDIF

    CALL MPI_WAITALL(message_count,request,status,err)

    !unpack in left direction
    IF(chunk%chunk_neighbours(CHUNK_LEFT).NE.EXTERNAL_FACE) THEN
      IF(integer_comm) THEN
!$OMN PARALLEL DO
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
            CALL clover_unpack_left_integer(tile,fields,depth,left_rcv_offset_integer(:,tile),mmc_left_rcv_offset_integer(:,tile))
          ENDIF
        ENDDO
!$OMN END PARALLEL DO  
      !CALL clover_calculate_rcv_offsets(left) or do it in the clover_unpack_left_integer, will set mmc_left_rcv_offsets
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_LEFT).EQ.MPI_TILE) THEN
          CALL clover_unpack_left(fields, tile, depth,mmc_comm,left_rcv_offset(:,tile),mmc_left_rcv_offset(:,tile))
        ENDIF
      ENDDO 
!$OMN END PARALLEL DO
      ENDIF
    ENDIF

    !unpack in right direction
    IF(chunk%chunk_neighbours(CHUNK_RIGHT).NE.EXTERNAL_FACE) THEN
      IF(integer_comm) THEN
!$OMN PARALLEL DO
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
            CALL clover_unpack_right_integer(tile,fields,depth,right_rcv_offset_integer(:,tile),mmc_right_rcv_offset_integer(:,tile))
          ENDIF
        ENDDO
!$OMN END PARALLEL DO  
      ENDIF
      !CALL clover_calculate_rcv_offsets(right) or do it in the clover_unpack_right_integer, will set mmc_right_rcv_offsets
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_RIGHT).EQ.MPI_TILE) THEN
          CALL clover_unpack_right(fields, tile, depth,mmc_comm,right_rcv_offset(:,tile),mmc_right_rcv_offset(:,tile))
        ENDIF
      ENDDO
!$OMN END PARALLEL DO    
    ENDIF
  ENDIF

  IF(.NOT.left_right) THEN
    IF(fields(FIELD_MATERIAL1).EQ.3) THEN
      ! Calculate MMC offsets and sizes
      message_count=0
      integer_comm=.TRUE.
      mmc_top_snd=0
      component_top_snd=0
      mmc_bottom_snd=0
      component_bottom_snd=0

      IF(chunk%chunk_neighbours(CHUNK_TOP).NE.EXTERNAL_FACE) THEN
        ! Calculate the offsets for the MMC data per tile, per field, per halo
        CALL clover_calc_top_mmc_snd_offsets(fields,depth,component_top_offset,  &
                                             component_integer_top_offset,       &
                                             mmc_count_top,                      &
                                             component_top_snd                   )
      ENDIF

      IF(chunk%chunk_neighbours(CHUNK_BOTTOM).NE.EXTERNAL_FACE) THEN
        ! Calculate the offsets for the MMC data per tile, per field, per halo
        CALL clover_calc_bottom_mmc_snd_offsets(fields,depth,component_bottom_offset, &
                                                component_integer_bottom_offset,      &
                                                mmc_count_bottom,                     &
                                                component_bottom_snd                  )
      ENDIF

! DO I NEED MMC COUNTS? SURELY JUST TOTAL COMPONENT COUNT IS ENOUGH?
      IF(chunk%chunk_neighbours(CHUNK_BOTTOM).NE.EXTERNAL_FACE) THEN
        CALL MPI_ISEND(mmc_bottom_snd,2,MPI_INTEGER,bottom_task,4,MPI_COMM_WORLD,request(message_count+1),err)
        CALL MPI_IRECV(mmc_bottom_rcv,2,MPI_INTEGER,bottom_task,3,MPI_COMM_WORLD,request(message_count+2),err)
        CALL MPI_ISEND(component_bottom_snd,2,MPI_INTEGER,bottom_task,14,MPI_COMM_WORLD,request(message_count+3),err)
        CALL MPI_IRECV(component_bottom_rcv,2,MPI_INTEGER,bottom_task,13,MPI_COMM_WORLD,request(message_count+4),err)
        message_count=message_count+4
      ENDIF
      IF(chunk%chunk_neighbours(CHUNK_TOP).NE.EXTERNAL_FACE) THEN
        CALL MPI_ISEND(mmc_top_snd,2,MPI_INTEGER,top_task,3,MPI_COMM_WORLD,request(message_count+1),err)
        CALL MPI_IRECV(mmc_top_rcv,2,MPI_INTEGER,top_task,4,MPI_COMM_WORLD,request(message_count+2),err)
        CALL MPI_ISEND(component_top_snd,2,MPI_INTEGER,top_task,13,MPI_COMM_WORLD,request(message_count+3),err)
        CALL MPI_IRECV(component_top_rcv,2,MPI_INTEGER,top_task,14,MPI_COMM_WORLD,request(message_count+4),err)
        message_count=message_count+4
      ENDIF
      IF(message_count.GT.0) THEN
        CALL MPI_WAITALL(message_count,request,status,err)
      ENDIF
    ENDIF ! End of MMC SIZES

    mmc_comm=.FALSE.

    ! SMC data offsets
    bottom_offset=0
    top_offset=0
    end_pack_index_snd_bottom=0
    end_pack_index_rcv_bottom=0
    end_pack_index_snd_top=0
    end_pack_index_rcv_top=0
    DO tile=1,tiles_per_chunk
      IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
        end_pack_index_snd_bottom=chunk%tiles(tile)%t_left-1
        end_pack_index_rcv_bottom=chunk%tiles(tile)%t_left-1
        DO field=1,REAL_FIELDS
          IF(fields(field).GT.0) THEN
            bottom_offset(field,tile)=end_pack_index_snd_bottom
            bottom_rcv_offset(field,tile)=end_pack_index_rcv_bottom
            x_inc=0
            IF(field_centering(field).EQ.VERTEX_DATA) x_inc=1
            IF(field_centering(field).EQ.X_FACE_DATA) x_inc=1
            end_pack_index_snd_bottom=end_pack_index_snd_bottom+(depth)*(chunk%x_max+x_inc)
            end_pack_index_rcv_bottom=end_pack_index_rcv_bottom+(depth)*(chunk%x_max+x_inc)
          ENDIF
        ENDDO
        DO field=1,REAL_FIELDS
          IF(fields(field).GT.1) THEN
            mmc_comm=.TRUE.
            end_pack_index_snd_bottom=end_pack_index_snd_bottom+SUM(mmc_bottom_snd(1:depth)+component_bottom_snd(1:depth))
            end_pack_index_rcv_bottom=end_pack_index_rcv_bottom+SUM(mmc_bottom_rcv(1:depth)+component_bottom_rcv(1:depth))
          ENDIF
        ENDDO
      ENDIF
      IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
        end_pack_index_snd_top=chunk%tiles(tile)%t_left-1
        end_pack_index_rcv_top=chunk%tiles(tile)%t_left-1
        DO field=1,REAL_FIELDS
          IF(fields(field).GT.0) THEN
            top_offset(field,tile)=end_pack_index_snd_top
            top_rcv_offset(field,tile)=end_pack_index_rcv_top
            x_inc=0
            IF(field_centering(field).EQ.VERTEX_DATA) x_inc=1
            IF(field_centering(field).EQ.X_FACE_DATA) x_inc=1
            end_pack_index_snd_top=end_pack_index_snd_top+(depth)*(chunk%x_max+x_inc)
            end_pack_index_rcv_top=end_pack_index_rcv_top+(depth)*(chunk%x_max+x_inc)
          ENDIF
        ENDDO
        DO field=1,REAL_FIELDS
          IF(fields(field).GT.1) THEN
            mmc_comm=.TRUE.
            end_pack_index_snd_top=end_pack_index_snd_top+SUM(mmc_top_snd(1:depth)+component_top_snd(1:depth))
            end_pack_index_rcv_top=end_pack_index_rcv_top+SUM(mmc_top_rcv(1:depth)+component_top_rcv(1:depth))
          ENDIF
        ENDDO
      ENDIF
    ENDDO

    ! Integer data
    end_pack_index_snd_bottom_integer=0
    end_pack_index_snd_top_integer=0
    end_pack_index_rcv_bottom_integer=0
    end_pack_index_rcv_top_integer=0
    DO tile=1,tiles_per_chunk
      IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
        end_pack_index_snd_bottom_integer=chunk%tiles(tile)%t_left-1
        end_pack_index_rcv_bottom_integer=chunk%tiles(tile)%t_left-1
        DO field=REAL_FIELDS+1,REAL_FIELDS+INT_FIELDS
          IF(fields(field).GT.0) THEN
            integer_comm=.TRUE.
            bottom_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_snd_bottom_integer
            bottom_rcv_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_rcv_bottom_integer
            end_pack_index_snd_bottom_integer=end_pack_index_snd_bottom_integer+depth*(chunk%x_max)
            end_pack_index_rcv_bottom_integer=end_pack_index_rcv_bottom_integer+depth*(chunk%x_max)
          ENDIF
        ENDDO
        ! Offsets for MMC integer data
        DO field=REAL_FIELDS+1,REAL_FIELDS+INT_FIELDS
          IF(fields(field).GT.0) THEN
            mmc_comm=.TRUE.
            end_pack_index_snd_bottom_integer=end_pack_index_snd_bottom_integer+SUM(mmc_bottom_snd(1:depth)+component_bottom_snd(1:depth))
            end_pack_index_rcv_bottom_integer=end_pack_index_rcv_bottom_integer+SUM(mmc_bottom_rcv(1:depth)+component_bottom_rcv(1:depth))
          ENDIF
        ENDDO
      ENDIF
      IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
        end_pack_index_snd_top_integer=chunk%tiles(tile)%t_left-1
        end_pack_index_rcv_top_integer=chunk%tiles(tile)%t_left-1
        DO field=REAL_FIELDS+1,REAL_FIELDS+INT_FIELDS
          IF(fields(field).GT.0) THEN
            integer_comm=.TRUE.
            top_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_snd_top_integer
            top_rcv_offset_integer(field-REAL_FIELDS,tile)=end_pack_index_rcv_top_integer
            end_pack_index_snd_top_integer=end_pack_index_snd_top_integer+depth*(chunk%x_max)
            end_pack_index_rcv_top_integer=end_pack_index_rcv_top_integer+depth*(chunk%x_max)
          ENDIF
        ENDDO
        ! Offsets for MMC integer data
        DO field=REAL_FIELDS+1,REAL_FIELDS+INT_FIELDS
          IF(fields(field).GT.0) THEN
            mmc_comm=.TRUE.
            end_pack_index_snd_top_integer=end_pack_index_snd_top_integer+SUM(mmc_top_snd(1:depth)+component_top_snd(1:depth))
            end_pack_index_rcv_top_integer=end_pack_index_rcv_top_integer+SUM(mmc_top_rcv(1:depth)+component_top_rcv(1:depth))
          ENDIF
        ENDDO
      ENDIF
    ENDDO

    ! Check buffer sizes are still adequate for messages
    IF(end_pack_index_snd_top.GT.chunk%top_buffer_size.OR.            &
       end_pack_index_rcv_top.GT.chunk%top_buffer_size.OR.        &
       end_pack_index_snd_top_integer.GT.chunk%top_buffer_size.OR.    &
       end_pack_index_rcv_top_integer.GT.chunk%top_buffer_size)THEN
      chunk%top_buffer_size=1.2*MAX(end_pack_index_snd_top,end_pack_index_rcv_top)
      DEALLOCATE(chunk%top_snd_buffer)
      DEALLOCATE(chunk%top_rcv_buffer)
      DEALLOCATE(chunk%integer_top_snd_buffer)
      DEALLOCATE(chunk%integer_top_rcv_buffer)
      ALLOCATE(chunk%top_snd_buffer(chunk%top_buffer_size))
      ALLOCATE(chunk%top_rcv_buffer(chunk%top_buffer_size))
      ALLOCATE(chunk%integer_top_snd_buffer(chunk%top_buffer_size))
      ALLOCATE(chunk%integer_top_rcv_buffer(chunk%top_buffer_size))
    ENDIF

    IF(end_pack_index_snd_bottom.GT.chunk%bottom_buffer_size.OR.         &
       end_pack_index_rcv_bottom.GT.chunk%bottom_buffer_size.OR.     &
       end_pack_index_snd_bottom_integer.GT.chunk%top_buffer_size.OR.    &
       end_pack_index_rcv_bottom_integer.GT.chunk%top_buffer_size)THEN
      chunk%bottom_buffer_size=1.2*MAX(end_pack_index_snd_bottom,end_pack_index_rcv_bottom)
      DEALLOCATE(chunk%bottom_snd_buffer)
      DEALLOCATE(chunk%bottom_rcv_buffer)
      DEALLOCATE(chunk%integer_bottom_snd_buffer)
      DEALLOCATE(chunk%integer_bottom_rcv_buffer)
      ALLOCATE(chunk%bottom_snd_buffer(chunk%bottom_buffer_size))
      ALLOCATE(chunk%bottom_rcv_buffer(chunk%bottom_buffer_size))
      ALLOCATE(chunk%integer_bottom_snd_buffer(chunk%bottom_buffer_size))
      ALLOCATE(chunk%integer_bottom_rcv_buffer(chunk%bottom_buffer_size))
    ENDIF

    message_count = 0
    request = 0

    ! All data sizes and offsets have now been calculated. It is time to pack and exchange the halos
    ! First check if each face in the MPI mesh chunk is on a comms boundary
    ! If it is then find the tiles that contribute to the MPI buffer and pack the data
    IF(chunk%chunk_neighbours(CHUNK_BOTTOM).NE.EXTERNAL_FACE) THEN
      ! Do bottom exchanges
      IF(integer_comm) THEN
!$OMN   PARALLEL DO
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
            CALL clover_pack_bottom_integer(tile,                                  &
                                            fields,                                &
                                            depth,                                 &
                                            bottom_offset_integer,                 &
                                            component_integer_bottom_offset,       &
                                            mmc_count_bottom                       )
          ENDIF
        ENDDO
!$OMN   END PARALLEL DO  
        ! Send integer message downwards
        CALL clover_send_recv_message_bottom_integer(chunk%integer_bottom_snd_buffer,                     &
                                                     chunk%integer_bottom_rcv_buffer,                     &
                                                     end_pack_index_snd_bottom_integer,                   &
                                                     end_pack_index_rcv_bottom_integer,                   &
                                                     13, 14,                                              &
                                                     request(message_count+1), request(message_count+2))
        message_count = message_count + 2
      ENDIF
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
          CALL clover_pack_bottom(tile,                           &
                                  fields,                         &
                                  depth,                          &
                                  bottom_offset,                  &
                                  component_bottom_offset,        &
                                  mmc_count_bottom,               &
                                  mmc_comm                        )
        ENDIF
      ENDDO
!$OMN END PARALLEL DO  
      ! Send message downwards
      CALL clover_send_recv_message_bottom(chunk%bottom_snd_buffer,                             &
                                           chunk%bottom_rcv_buffer,                             &
                                           end_pack_index_snd_bottom,                           &
                                           end_pack_index_rcv_bottom,                           &
                                           3, 4,                                                &
                                           request(message_count+1), request(message_count+2))
      message_count = message_count + 2
    ENDIF ! End CHUNK_BOTTOM

    IF(chunk%chunk_neighbours(CHUNK_TOP).NE.EXTERNAL_FACE) THEN
      ! Do top exchanges
      IF(integer_comm) THEN
!$OMN   PARALLEL DO
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
            CALL clover_pack_top_integer(tile,                               &
                                         fields,                             &
                                         depth,                              &
                                         top_offset_integer,                 &
                                         component_integer_top_offset,       &
                                         mmc_count_top                       )
          ENDIF
        ENDDO
!$OMN   END PARALLEL DO  
        ! Send integer message upwards
        CALL clover_send_recv_message_top_integer(chunk%integer_top_snd_buffer,                        &
                                                  chunk%integer_top_rcv_buffer,                        &
                                                  end_pack_index_snd_top_integer,                      &
                                                  end_pack_index_rcv_top_integer,                      &
                                                  14, 13,                                              &
                                                  request(message_count+1), request(message_count+2))
        message_count = message_count + 2
      ENDIF
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
          CALL clover_pack_top(tile,                        &
                               fields,                      &
                               depth,                       &
                               top_offset,                  &
                               component_top_offset,        &
                               mmc_count_top,               &        
                               mmc_comm                     )

        ENDIF
      ENDDO
!$OMN END PARALLEL DO  ! Send message upwards
      CALL clover_send_recv_message_top(chunk%top_snd_buffer,                             &
                                        chunk%top_rcv_buffer,                             &
                                        end_pack_index_snd_top,                           &
                                        end_pack_index_rcv_top,                           &
                                        4, 3,                                             &
                                        request(message_count+1), request(message_count+2))
      message_count = message_count + 2
    ENDIF ! End TOP_CHUNK

    ! Do the bottom/top exchange
    CALL MPI_WAITALL(message_count,request,status,err)

    ! Now unpack the data into the correct tile halos
    ! Unpack in bottom direction
    IF(chunk%chunk_neighbours(CHUNK_BOTTOM).NE.EXTERNAL_FACE) THEN
      IF(integer_comm) THEN
        CALL clover_calc_bottom_mmc_rcv_offsets(fields,depth,chunk%integer_bottom_rcv_buffer,smc_bottom_offset, &
                                                component_counter,component_counter_tile)
      ENDIF
      mmc_offset_bottom=0 ! This variable is not currently thread safe if I wantted to thread over packing tiles
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
          CALL clover_unpack_bottom(fields, tile, depth,bottom_rcv_offset,smc_bottom_offset, &
                                    mmc_offset_bottom,component_counter,component_counter_tile,mmc_comm)
          mmc_offset_bottom=SUM(component_counter_tile(tile,:))
        ENDIF
      ENDDO
!$OMN END PARALLEL DO  
    ENDIF

    ! Unpack in top direction
    IF( chunk%chunk_neighbours(CHUNK_TOP).NE.EXTERNAL_FACE) THEN
      IF(integer_comm) THEN
        ! Analyse the incoming buffer and set up MMC read (from buffer) and write lists (to tile)
        ! I actually unpack the integer data as well now
        CALL clover_calc_top_mmc_rcv_offsets(fields,depth,chunk%integer_top_rcv_buffer,smc_top_offset, &
                                             component_counter,component_counter_tile)
      ENDIF
      mmc_offset_top=0 ! This variable is not currently thread safe if I wantted to thread over packing tiles
!$OMN PARALLEL DO
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
          CALL clover_unpack_top(fields, tile, depth,top_rcv_offset,smc_top_offset, &
                                 mmc_offset_top,component_counter,component_counter_tile,mmc_comm)
          mmc_offset_top=SUM(component_counter_tile(tile,:))
        ENDIF
      ENDDO
!$OMN END PARALLEL DO  
    ENDIF

  ENDIF

END SUBROUTINE clover_exchange

SUBROUTINE clover_pack_left(tile,fields,depth,left_offset,mmc_left_offset,mmc_comm)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER      :: fields(:),depth,tile
  INTEGER      :: left_offset(:),mmc_left_offset(:)
  LOGICAL      :: mmc_comm

  INTEGER      :: t_offset,mmc_t_offset
  
  t_offset=left_offset(tile)
  mmc_t_offset=mmc_left_offset(tile)
    
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%density0,               &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, field_centering(FIELD_DENSITY0),         &
                                  left_offset(FIELD_DENSITY0)+t_offset,           &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_DENSITY0)+mmc_t_offset,   &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_density0,           &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%density1,               &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_DENSITY1)+t_offset,           &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_DENSITY1)+mmc_t_offset,   &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_density1,           &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%energy0,                &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_ENERGY0)+t_offset,            &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_ENERGY0)+mmc_t_offset,    &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_energy0,            &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%energy1,                &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_ENERGY1)+t_offset,            &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_ENERGY1)+mmc_t_offset,    &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_energy1,            &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%pressure,               &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_PRESSURE)+t_offset,           &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_PRESSURE)+mmc_t_offset,   &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_pressure,           &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%viscosity,              &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_VISCOSITY)+t_offset,          &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_VISCOSITY)+mmc_t_offset,  &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_viscosity,          &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%soundspeed,             &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_SOUNDSPEED)+t_offset,         &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_SOUNDSPEED)+mmc_t_offset, &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_soundspeed,         &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN ! Don't send if no MMC
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%volume,                 &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_VOLUME0)+t_offset,            &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_VOLUME0)+mmc_t_offset,    &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_volume0,            &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN ! Don't send if no MMC
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                       &
                                  chunk%tiles(tile)%t_xmax,                       &
                                  chunk%tiles(tile)%t_ymin,                       &
                                  chunk%tiles(tile)%t_ymax,                       &
                                  chunk%tiles(tile)%field%volume,                 &
                                  chunk%left_snd_buffer,                          &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,  &
                                  depth, CELL_DATA,                               &
                                  left_offset(FIELD_VOLUME1)+t_offset,            &
                                  mmc_comm,                                       &
                                  mmc_left_offset(FIELD_VOLUME1)+mmc_t_offset,    &
                                  chunk%tiles(tile)%field%mmc_left_snd,           &
                                  chunk%tiles(tile)%field%component_left_snd,     &
                                  chunk%tiles(tile)%field%mmc_volume1,            &
                                  chunk%tiles(tile)%field%left_index_snd_comp_list,   &
                                  chunk%tiles(tile)%field%left_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%xvel0,                &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, VERTEX_DATA,                           &
                                  left_offset(FIELD_XVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%xvel1,                &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, VERTEX_DATA,                           &
                                  left_offset(FIELD_XVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%yvel0,                &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, VERTEX_DATA,                           &
                                  left_offset(FIELD_YVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%yvel1,                &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, VERTEX_DATA,                           &
                                  left_offset(FIELD_YVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%vol_flux_x,           &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, X_FACE_DATA,                           &
                                  left_offset(FIELD_VOL_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%vol_flux_y,           &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, Y_FACE_DATA,                           &
                                  left_offset(FIELD_VOL_FLUX_Y)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%mass_flux_x,          &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, X_FACE_DATA,                           &
                                  left_offset(FIELD_MASS_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                  chunk%tiles(tile)%t_xmax,                     &
                                  chunk%tiles(tile)%t_ymin,                     &
                                  chunk%tiles(tile)%t_ymax,                     &
                                  chunk%tiles(tile)%field%mass_flux_y,          &
                                  chunk%left_snd_buffer,                        &
                                  CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                  depth, Y_FACE_DATA,                           &
                                  left_offset(FIELD_MASS_FLUX_Y)+t_offset)
  ENDIF
  
END SUBROUTINE clover_pack_left

SUBROUTINE clover_send_recv_message_left(left_snd_buffer,left_rcv_buffer,      &
                                         total_size,                           &
                                         total_size_rcv,                       &
                                         tag_send, tag_recv,                   &
                                         req_send, req_recv)

  IMPLICIT NONE

  REAL(KIND=8)    :: left_snd_buffer(:),left_rcv_buffer(:)
  INTEGER         :: left_task
  INTEGER         :: total_size,total_size_rcv,tag_send,tag_recv,err
  INTEGER         :: req_send, req_recv

  left_task =chunk%chunk_neighbours(CHUNK_LEFT) - 1

  CALL MPI_ISEND(left_snd_buffer,total_size,MPI_DOUBLE_PRECISION,left_task,tag_send &
                ,MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(left_rcv_buffer,total_size_rcv,MPI_DOUBLE_PRECISION,left_task,tag_recv &
                ,MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_left

SUBROUTINE clover_send_recv_message_left_integer(integer_left_snd_buffer,integer_left_rcv_buffer,  &
                                                 total_size,                                       &
                                                 total_size_rcv,                                   &
                                                 tag_send, tag_recv,                               &
                                                 req_send, req_recv)

  IMPLICIT NONE

  INTEGER      :: integer_left_snd_buffer(:),integer_left_rcv_buffer(:)
  INTEGER      :: left_task
  INTEGER      :: total_size,total_size_rcv,tag_send,tag_recv,err
  INTEGER      :: req_send,req_recv

  left_task=chunk%chunk_neighbours(CHUNK_LEFT)-1

  CALL MPI_ISEND(integer_left_snd_buffer,total_size,MPI_INTEGER,left_task,tag_send &
                ,MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(integer_left_rcv_buffer,total_size_rcv,MPI_INTEGER,left_task,tag_recv &
                ,MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_left_integer

SUBROUTINE clover_unpack_left(fields, tile, depth,mmc_comm,left_offset,mmc_left_offset)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: fields(:),tile,depth,t_offset,mmc_t_offset,left_offset(:),mmc_left_offset(:)
  LOGICAL        :: mmc_comm

  t_offset = (chunk%tiles(tile)%t_bottom - chunk%bottom)*depth
  mmc_t_offset = 1 ! Needs calcualting

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%density0,                   &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_DENSITY0)+t_offset,               &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_DENSITY0)+mmc_t_offset,       &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_density0,               &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%density1,                   &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_DENSITY1)+t_offset,               &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_DENSITY1)+mmc_t_offset,       &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_density1,               &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%energy0,                    &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_ENERGY0)+t_offset,                &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_ENERGY0)+mmc_t_offset,        &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_energy0,                &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%energy1,                    &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_ENERGY1)+t_offset,                &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_ENERGY1)+mmc_t_offset,        &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_energy1,                &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%pressure,                   &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_PRESSURE)+t_offset,               &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_PRESSURE)+mmc_t_offset,       &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_pressure,               &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%viscosity,                  &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_VISCOSITY)+t_offset,              &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_VISCOSITY)+mmc_t_offset,      &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_viscosity,              &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%soundspeed,                 &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_SOUNDSPEED)+t_offset,             &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_SOUNDSPEED)+mmc_t_offset,     &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_soundspeed,             &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%volume,                     &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_VOLUME0)+t_offset,                &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_VOLUME0)+mmc_t_offset,        &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_volume0,                &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                           &
                                    chunk%tiles(tile)%t_xmax,                           &
                                    chunk%tiles(tile)%t_ymin,                           &
                                    chunk%tiles(tile)%t_ymax,                           &
                                    chunk%tiles(tile)%field%volume,                     &
                                    chunk%left_rcv_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                    depth, CELL_DATA,                                   &
                                    left_offset(FIELD_VOLUME1)+t_offset,                &
                                    mmc_comm,                                           &
                                    mmc_left_offset(FIELD_VOLUME1)+mmc_t_offset,        &
                                    chunk%tiles(tile)%field%mmc_left_rcv,               &
                                    chunk%tiles(tile)%field%component_left_rcv,         &
                                    chunk%tiles(tile)%field%mmc_volume1,                &
                                    chunk%tiles(tile)%field%left_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%xvel0,                &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, VERTEX_DATA,                           &
                                    left_offset(FIELD_XVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%xvel1,                &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, VERTEX_DATA,                           &
                                    left_offset(FIELD_XVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%yvel0,                &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, VERTEX_DATA,                           &
                                    left_offset(FIELD_YVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%yvel1,                &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, VERTEX_DATA,                           &
                                    left_offset(FIELD_YVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%vol_flux_x,           &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, X_FACE_DATA,                           &
                                    left_offset(FIELD_VOL_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%vol_flux_y,           &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, Y_FACE_DATA,                           &
                                    left_offset(FIELD_VOL_FLUX_Y)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%mass_flux_x,          &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, X_FACE_DATA,                           &
                                    left_offset(FIELD_MASS_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_left(chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%tiles(tile)%field%mass_flux_y,          &
                                    chunk%left_rcv_buffer,                        &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth, Y_FACE_DATA,                           &
                                    left_offset(FIELD_MASS_FLUX_Y)+t_offset)
  ENDIF

END SUBROUTINE clover_unpack_left

SUBROUTINE clover_pack_right(tile,fields,depth,right_offset,mmc_right_offset,mmc_comm)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER      :: fields(:),depth,tile
  INTEGER      :: right_offset(:),mmc_right_offset(:)
  LOGICAL      :: mmc_comm

  INTEGER      :: t_offset,mmc_t_offset
  
  t_offset=right_offset(tile)
  mmc_t_offset=mmc_right_offset(tile)
  
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%density0,                &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_DENSITY0)+t_offset,           &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_DENSITY0)+mmc_t_offset,   &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_density0,            &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%density1,                &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_DENSITY1)+t_offset,           &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_DENSITY1)+mmc_t_offset,   &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_density1,            &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%energy0,                 &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_ENERGY0)+t_offset,            &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_ENERGY0)+mmc_t_offset,    &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_energy0,             &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%energy1,                 &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_ENERGY1)+t_offset,            &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_ENERGY1)+mmc_t_offset,    &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_energy1,             &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%pressure,                &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_PRESSURE)+t_offset,           &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_PRESSURE)+mmc_t_offset,   &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_pressure,            &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%viscosity,               &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_VISCOSITY)+t_offset,          &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_VISCOSITY)+mmc_t_offset,  &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_viscosity,           &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%soundspeed,              &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_SOUNDSPEED)+t_offset,         &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_SOUNDSPEED)+mmc_t_offset, &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_soundspeed,          &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%volume,                  &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_VOLUME0)+t_offset,            &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_VOLUME0)+mmc_t_offset,    &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_volume0,             &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                        &
                                   chunk%tiles(tile)%t_xmax,                        &
                                   chunk%tiles(tile)%t_ymin,                        &
                                   chunk%tiles(tile)%t_ymax,                        &
                                   chunk%tiles(tile)%field%volume,                  &
                                   chunk%right_snd_buffer,                          &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,   &
                                   depth, CELL_DATA,                                &
                                   right_offset(FIELD_VOLUME1)+t_offset,            &
                                   mmc_comm,                                        &
                                   mmc_right_offset(FIELD_VOLUME1)+mmc_t_offset,    &
                                   chunk%tiles(tile)%field%mmc_right_snd,           &
                                   chunk%tiles(tile)%field%component_right_snd,     &
                                   chunk%tiles(tile)%field%mmc_volume1,             &
                                   chunk%tiles(tile)%field%right_index_snd_comp_list,   &
                                   chunk%tiles(tile)%field%right_component_snd_comp_list)
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%xvel0,                &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   right_offset(FIELD_XVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%xvel1,                &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   right_offset(FIELD_XVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%yvel0,                &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   right_offset(FIELD_YVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%yvel1,                &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   right_offset(FIELD_YVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%vol_flux_x,           &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, X_FACE_DATA,                           &
                                   right_offset(FIELD_VOL_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%vol_flux_y,           &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, Y_FACE_DATA,                           &
                                   right_offset(FIELD_VOL_FLUX_Y)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%mass_flux_x,          &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, X_FACE_DATA,                           &
                                   right_offset(FIELD_MASS_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%tiles(tile)%field%mass_flux_y,          &
                                   chunk%right_snd_buffer,                       &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, Y_FACE_DATA,                           &
                                   right_offset(FIELD_MASS_FLUX_Y)+t_offset)
  ENDIF
  
END SUBROUTINE clover_pack_right

SUBROUTINE clover_send_recv_message_right(right_snd_buffer, right_rcv_buffer,   &
                                          total_size,                           &
                                          total_size_rcv,                       &
                                          tag_send, tag_recv,                   &
                                          req_send, req_recv)

  IMPLICIT NONE

  REAL(KIND=8) :: right_snd_buffer(:),right_rcv_buffer(:)
  INTEGER      :: right_task
  INTEGER      :: total_size, total_size_rcv, tag_send, tag_recv, err
  INTEGER      :: req_send, req_recv

  right_task=chunk%chunk_neighbours(CHUNK_RIGHT)-1

  CALL MPI_ISEND(right_snd_buffer,total_size,MPI_DOUBLE_PRECISION,right_task,tag_send, &
                 MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(right_rcv_buffer,total_size_rcv,MPI_DOUBLE_PRECISION,right_task,tag_recv, &
                 MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_right

SUBROUTINE clover_send_recv_message_right_integer(integer_right_snd_buffer,integer_right_rcv_buffer,&
                                                  total_size,                                       &
                                                  total_size_rcv,                                   &
                                                  tag_send, tag_recv,                               &
                                                  req_send, req_recv)

  IMPLICIT NONE

  INTEGER      :: integer_right_snd_buffer(:),integer_right_rcv_buffer(:)
  INTEGER      :: right_task
  INTEGER      :: total_size,total_size_rcv,tag_send,tag_recv,err
  INTEGER      :: req_send, req_recv

  right_task=chunk%chunk_neighbours(CHUNK_RIGHT)-1

  CALL MPI_ISEND(integer_right_snd_buffer,total_size,MPI_INTEGER,right_task,tag_send &
                ,MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(integer_right_rcv_buffer,total_size_rcv,MPI_INTEGER,right_task,tag_recv &
                ,MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_right_integer

SUBROUTINE clover_unpack_right(fields,tile,depth,mmc_comm,right_offset,mmc_right_offset)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: fields(:),tile,depth,right_offset(:),mmc_right_offset(:)
  LOGICAL        :: mmc_comm

  INTEGER        :: t_offset,mmc_t_offset
  
  t_offset=right_offset(tile)
  mmc_t_offset=mmc_right_offset(tile)
  
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%density0,                    &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_DENSITY0)+t_offset,               &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_DENSITY0)+mmc_t_offset,       &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_density0,                &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%density1,                    &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_DENSITY1)+t_offset,               &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_DENSITY1)+mmc_t_offset,       &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_density1,                &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%energy0,                     &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_ENERGY0)+t_offset,                &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_ENERGY0)+mmc_t_offset,        &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_energy0,                 &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%energy1,                     &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_ENERGY1)+t_offset,                &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_ENERGY1)+mmc_t_offset,        &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_energy1,                 &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%pressure,                    &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_PRESSURE)+t_offset,               &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_PRESSURE)+mmc_t_offset,       &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_pressure,                &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%viscosity,                   &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_VISCOSITY)+t_offset,              &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_VISCOSITY)+mmc_t_offset,      &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_viscosity,               &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%soundspeed,                  &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_SOUNDSPEED)+t_offset,             &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_SOUNDSPEED)+mmc_t_offset,     &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_soundspeed,              &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%volume,                      &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_VOLUME0)+t_offset,                &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_VOLUME0)+mmc_t_offset,        &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_volume0,                 &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                            &
                                     chunk%tiles(tile)%t_xmax,                            &
                                     chunk%tiles(tile)%t_ymin,                            &
                                     chunk%tiles(tile)%t_ymax,                            &
                                     chunk%tiles(tile)%field%volume,                      &
                                     chunk%right_rcv_buffer,                              &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,       &
                                     depth, CELL_DATA,                                    &
                                     right_offset(FIELD_VOLUME1)+t_offset,                &
                                     mmc_comm,                                            &
                                     mmc_right_offset(FIELD_VOLUME1)+mmc_t_offset,        &
                                     chunk%tiles(tile)%field%mmc_right_rcv,               &
                                     chunk%tiles(tile)%field%component_right_rcv,         &
                                     chunk%tiles(tile)%field%mmc_volume1,                 &
                                     chunk%tiles(tile)%field%right_component_rcv_comp_list)
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%xvel0,                &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, VERTEX_DATA,                           &
                                     right_offset(FIELD_XVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%xvel1,                &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, VERTEX_DATA,                           &
                                     right_offset(FIELD_XVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%yvel0,                &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, VERTEX_DATA,                           &
                                     right_offset(FIELD_YVEL0)+t_offset)
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%yvel1,                &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, VERTEX_DATA,                           &
                                     right_offset(FIELD_YVEL1)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%vol_flux_x,           &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, X_FACE_DATA,                           &
                                     right_offset(FIELD_VOL_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%vol_flux_y,           &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, Y_FACE_DATA,                           &
                                     right_offset(FIELD_VOL_FLUX_Y)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%mass_flux_x,          &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, X_FACE_DATA,                           &
                                     right_offset(FIELD_MASS_FLUX_X)+t_offset)
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_right(chunk%tiles(tile)%t_xmin,                     &
                                     chunk%tiles(tile)%t_xmax,                     &
                                     chunk%tiles(tile)%t_ymin,                     &
                                     chunk%tiles(tile)%t_ymax,                     &
                                     chunk%tiles(tile)%field%mass_flux_y,          &
                                     chunk%right_rcv_buffer,                       &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth, Y_FACE_DATA,                           &
                                     right_offset(FIELD_MASS_FLUX_Y)+t_offset)
  ENDIF

END SUBROUTINE clover_unpack_right

SUBROUTINE clover_pack_top(tile,                        &
                           fields,                      &
                           depth,                       &
                           top_offset,                  &
                           component_top_offset,        &
                           mmc_count_top,               &        
                           mmc_comm                     )

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,top_offset(:,:),component_top_offset(:,:,:),mmc_count_top(:,:)
  INTEGER        :: t_offset
  LOGICAL        :: mmc_comm

  
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%density0,                   &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_DENSITY0,CELL_DATA,                     &
                                 top_offset(FIELD_DENSITY0,tile),                    &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  & ! Not a typo
                                 chunk%tiles(tile)%field%mmc_index1,                 & ! Not a typo
                                 chunk%tiles(tile)%field%mmc_component1,             & ! Not a typo
                                 chunk%tiles(tile)%field%mmc_density0,               &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%density1,                   &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_DENSITY1,CELL_DATA,                     &
                                 top_offset(FIELD_DENSITY1,tile),                    &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_density1,               &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%energy0,                    &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_ENERGY0,CELL_DATA,                      &
                                 top_offset(FIELD_ENERGY0,tile),                     &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_energy0,                &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%energy1,                    &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_ENERGY1,CELL_DATA,                      &
                                 top_offset(FIELD_ENERGY1,tile),                     &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_energy1,                &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%pressure,                   &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_PRESSURE,CELL_DATA,                     &
                                 top_offset(FIELD_PRESSURE,tile),                    &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_pressure,               &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%viscosity,                  &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_VISCOSITY,CELL_DATA,                    &
                                 top_offset(FIELD_VISCOSITY,tile),                   &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_viscosity,              &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%soundspeed,                 &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_SOUNDSPEED,CELL_DATA,                   &
                                 top_offset(FIELD_SOUNDSPEED,tile),                  &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_viscosity,              &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%volume,                     &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_VOLUME0,CELL_DATA,                      &
                                 top_offset(FIELD_VOLUME0,tile),                     &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_volume0,                &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN
    CALL clover_pack_message_top(tile,                                               &
                                 chunk%tiles(tile)%t_xmin,                           &
                                 chunk%tiles(tile)%t_xmax,                           &
                                 chunk%tiles(tile)%t_ymin,                           &
                                 chunk%tiles(tile)%t_ymax,                           &
                                 chunk%x_min,                                        &
                                 chunk%x_max,                                        &
                                 chunk%tiles(tile)%field%volume,                     &
                                 chunk%top_snd_buffer,                               &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                 depth,FIELD_VOLUME1,CELL_DATA,                      &
                                 top_offset(FIELD_VOLUME1,tile),                     &
                                 mmc_comm,                                           &
                                 chunk%tiles(tile)%field%material1,                  &
                                 chunk%tiles(tile)%field%mmc_index1,                 &
                                 chunk%tiles(tile)%field%mmc_component1,             &
                                 chunk%tiles(tile)%field%mmc_volume1,                &
                                 component_top_offset,                               &
                                 mmc_count_top,                                      &
                                 chunk%tiles(tile)%field%top_component_snd_comp_list,&
                                 chunk%tiles(tile)%field%top_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%xvel0,                &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_XVEL0,VERTEX_DATA,                &
                                 top_offset(FIELD_XVEL0,tile))
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%xvel1,                &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_XVEL1,VERTEX_DATA,                &
                                 top_offset(FIELD_XVEL1,tile))
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%yvel0,                &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_YVEL0,VERTEX_DATA,                &
                                 top_offset(FIELD_YVEL0,tile))
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%yvel1,                &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_YVEL1,VERTEX_DATA,                &
                                 top_offset(FIELD_YVEL1,tile))
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%vol_flux_x,           &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_VOL_FLUX_X,X_FACE_DATA,           &
                                 top_offset(FIELD_VOL_FLUX_X,tile))
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%vol_flux_y,           &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_VOL_FLUX_Y,Y_FACE_DATA,           &
                                 top_offset(FIELD_VOL_FLUX_Y,tile))
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%mass_flux_x,          &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_MASS_FLUX_X,X_FACE_DATA,          &
                                 top_offset(FIELD_MASS_FLUX_X,tile))
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_top(tile,                                         &
                                 chunk%tiles(tile)%t_xmin,                     &
                                 chunk%tiles(tile)%t_xmax,                     &
                                 chunk%tiles(tile)%t_ymin,                     &
                                 chunk%tiles(tile)%t_ymax,                     &
                                 chunk%x_min,                                  &
                                 chunk%x_max,                                  &
                                 chunk%tiles(tile)%field%mass_flux_y,          &
                                 chunk%top_snd_buffer,                         &
                                 CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                 depth,FIELD_MASS_FLUX_Y,Y_FACE_DATA,          &
                                 top_offset(FIELD_MASS_FLUX_Y,tile))
  ENDIF
END SUBROUTINE clover_pack_top

SUBROUTINE clover_send_recv_message_top(top_snd_buffer, top_rcv_buffer,     &
                                        total_size,                         &
                                        total_size_rcv,                     &
                                        tag_send, tag_recv,                 &
                                        req_send, req_recv)

  IMPLICIT NONE

  REAL(KIND=8) :: top_snd_buffer(:),top_rcv_buffer(:)
  INTEGER      :: top_task
  INTEGER      :: total_size,total_size_rcv,tag_send,tag_recv,err
  INTEGER      :: req_send,req_recv

  top_task=chunk%chunk_neighbours(CHUNK_TOP)-1

  CALL MPI_ISEND(top_snd_buffer,total_size,MPI_DOUBLE_PRECISION,top_task,tag_send, &
                 MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(top_rcv_buffer,total_size_rcv,MPI_DOUBLE_PRECISION,top_task,tag_recv, &
                 MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_top

SUBROUTINE clover_send_recv_message_top_integer(integer_top_snd_buffer,integer_top_rcv_buffer,  &
                                                total_size,                                     &
                                                total_size_rcv,                                 &
                                                tag_send, tag_recv,                             &
                                                req_send, req_recv)

  IMPLICIT NONE

  INTEGER      :: integer_top_snd_buffer(:),integer_top_rcv_buffer(:)
  INTEGER      :: top_task
  INTEGER      :: total_size,total_size_rcv,tag_send,tag_recv,err
  INTEGER      :: req_send, req_recv

  top_task=chunk%chunk_neighbours(CHUNK_TOP)-1

  CALL MPI_ISEND(integer_top_snd_buffer,total_size,MPI_INTEGER,top_task,tag_send &
                ,MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(integer_top_rcv_buffer,total_size_rcv,MPI_INTEGER,top_task,tag_recv &
                ,MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_top_integer

SUBROUTINE clover_unpack_top(fields,tile,depth,top_offset,smc_offset, &
                             mmc_offset_top,component_counter,component_counter_tile,mmc_comm)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: fields(:),tile,depth,top_offset(:,:),field
  LOGICAL        :: mmc_comm

  INTEGER        :: smc_offset,mmc_offset,component_counter,mmc_offset_top,component_counter_tile(:,:)
  INTEGER        :: mmc_halo_offset

  mmc_offset=mmc_offset_top
  mmc_halo_offset=component_counter_tile(tile,2)

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%density0,                   &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_DENSITY0,tile),                    &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         & ! Carried into each unpack
                                   mmc_halo_offset,                                    & ! Carried into each unpack
                                   component_counter,                                  & ! Carried into each unpack
                                   chunk%tiles(tile)%field%material1,                  & ! Not a typo
                                   chunk%tiles(tile)%field%mmc_index1,                 & ! Not a typo
                                   chunk%tiles(tile)%field%mmc_component1,             & ! Not a typo
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_density0,               &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list ) ! Needed?
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%density1,                   &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_DENSITY1,tile),                    &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_density1,               &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%energy0,                    &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_ENERGY0,tile),                     &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_energy0,                &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%energy1,                    &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_ENERGY1,tile),                     &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_energy1,                &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%pressure,                   &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_PRESSURE,tile),                    &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_pressure,               &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%viscosity,                  &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_VISCOSITY,tile),                   &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_pressure,               &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%soundspeed,                 &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_VISCOSITY,tile),                   &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_soundspeed,             &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%volume,                     &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_VOLUME0,tile),                     &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_volume0,                &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                           &
                                   chunk%tiles(tile)%t_xmax,                           &
                                   chunk%tiles(tile)%t_ymin,                           &
                                   chunk%tiles(tile)%t_ymax,                           &
                                   chunk%x_min,                                        &
                                   chunk%x_max,                                        &
                                   chunk%tiles(tile)%field%volume,                     &
                                   chunk%top_rcv_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,      &
                                   depth, CELL_DATA,                                   &
                                   top_offset(FIELD_VOLUME1,tile),                     &
                                   mmc_comm,                                           &
                                   smc_offset,                                         &
                                   mmc_offset,                                         &
                                   mmc_halo_offset,                                    &
                                   component_counter,                                  &
                                   chunk%tiles(tile)%field%material1,                  &
                                   chunk%tiles(tile)%field%mmc_index1,                 &
                                   chunk%tiles(tile)%field%mmc_component1,             &
                                   chunk%tiles(tile)%field%component_top_rcv,          &
                                   chunk%tiles(tile)%field%mmc_volume1,                &
                                   chunk%tiles(tile)%field%top_component_rcv_comp_list )
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%xvel0,                &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   top_offset(FIELD_XVEL0,tile)                  )
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%xvel1,                &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   top_offset(FIELD_XVEL1,tile)                  )
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%yvel0,                &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   top_offset(FIELD_YVEL0,tile)                  )
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%yvel1,                &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, VERTEX_DATA,                           &
                                   top_offset(FIELD_YVEL1,tile)                  )
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%vol_flux_x,           &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, X_FACE_DATA,                           &
                                   top_offset(FIELD_VOL_FLUX_X,tile)             )
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%vol_flux_y,           &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, Y_FACE_DATA,                           &
                                   top_offset(FIELD_VOL_FLUX_Y,tile)             )
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%mass_flux_x,          &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, X_FACE_DATA,                           &
                                   top_offset(FIELD_MASS_FLUX_X,tile)            )
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_top(chunk%tiles(tile)%t_xmin,                     &
                                   chunk%tiles(tile)%t_xmax,                     &
                                   chunk%tiles(tile)%t_ymin,                     &
                                   chunk%tiles(tile)%t_ymax,                     &
                                   chunk%x_min,                                  &
                                   chunk%x_max,                                  &
                                   chunk%tiles(tile)%field%mass_flux_y,          &
                                   chunk%top_rcv_buffer,                         &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth, Y_FACE_DATA,                           &
                                   top_offset(FIELD_MASS_FLUX_Y,tile)            )
  ENDIF

END SUBROUTINE clover_unpack_top

SUBROUTINE clover_pack_bottom(tile,                           &
                              fields,                         &
                              depth,                          &
                              bottom_offset,                  &
                              component_bottom_offset,        &
                              mmc_count_bottom,               &        
                              mmc_comm                        )

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,bottom_offset(:,:),component_bottom_offset(:,:,:),mmc_count_bottom(:,:)
  LOGICAL        :: mmc_comm

  INTEGER        :: smc_offset

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%density0,                      &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_DENSITY0,CELL_DATA,                        &
                                    bottom_offset(FIELD_DENSITY0,tile),                    &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_density0,                  &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%density1,                      &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_DENSITY1,CELL_DATA,                        &
                                    bottom_offset(FIELD_DENSITY1,tile),                    &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_density1,                  &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%energy0,                       &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_ENERGY0,CELL_DATA,                         &
                                    bottom_offset(FIELD_ENERGY0,tile),                     &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_energy0,                   &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%energy1,                       &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_ENERGY1,CELL_DATA,                         &
                                    bottom_offset(FIELD_ENERGY1,tile),                     &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%component_bottom_snd,          &
                                    chunk%tiles(tile)%field%mmc_energy1,                   &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%pressure,                      &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_PRESSURE,CELL_DATA,                        &
                                    bottom_offset(FIELD_PRESSURE,tile),                    &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_pressure,                  &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%viscosity,                     &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_VISCOSITY,CELL_DATA,                       &
                                    bottom_offset(FIELD_VISCOSITY,tile),                   &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_viscosity,                 &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%soundspeed,                    &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_SOUNDSPEED,CELL_DATA,                      &
                                    bottom_offset(FIELD_SOUNDSPEED,tile),                  &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_soundspeed,                &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%volume,                        &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_VOLUME0,CELL_DATA,                         &
                                    bottom_offset(FIELD_VOLUME0,tile),                     &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_volume0,                   &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN
    CALL clover_pack_message_bottom(tile,                                                  &
                                    chunk%tiles(tile)%t_xmin,                              &
                                    chunk%tiles(tile)%t_xmax,                              &
                                    chunk%tiles(tile)%t_ymin,                              &
                                    chunk%tiles(tile)%t_ymax,                              &
                                    chunk%x_min,                                           &
                                    chunk%x_max,                                           &
                                    chunk%tiles(tile)%field%volume,                        &
                                    chunk%bottom_snd_buffer,                               &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,         &
                                    depth,FIELD_VOLUME1,CELL_DATA,                         &
                                    bottom_offset(FIELD_VOLUME1,tile),                     &
                                    mmc_comm,                                              &
                                    chunk%tiles(tile)%field%material1,                     &
                                    chunk%tiles(tile)%field%mmc_index1,                    &
                                    chunk%tiles(tile)%field%mmc_component1,                &
                                    chunk%tiles(tile)%field%mmc_volume1,                   &
                                    component_bottom_offset,                               &
                                    mmc_count_bottom,                                      &
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_list,&
                                    chunk%tiles(tile)%field%bottom_component_snd_comp_count)
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%xvel0,                &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_XVEL0,VERTEX_DATA,                &
                                    bottom_offset(FIELD_XVEL0,tile))
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%xvel1,                &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_XVEL1,VERTEX_DATA,                &
                                    bottom_offset(FIELD_XVEL1,tile))
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%yvel0,                &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_YVEL0,VERTEX_DATA,                &
                                    bottom_offset(FIELD_YVEL0,tile))
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%yvel1,                &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_YVEL1,VERTEX_DATA,                &
                                    bottom_offset(FIELD_YVEL1,tile))
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%vol_flux_x,           &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_VOL_FLUX_X,X_FACE_DATA,           &
                                    bottom_offset(FIELD_VOL_FLUX_X,tile))
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%vol_flux_y,           &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_VOL_FLUX_Y,Y_FACE_DATA,           &
                                    bottom_offset(FIELD_VOL_FLUX_Y,tile))
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%mass_flux_x,          &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_MASS_FLUX_X,X_FACE_DATA,          &
                                    bottom_offset(FIELD_MASS_FLUX_X,tile))
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_pack_message_bottom(tile,                                         &
                                    chunk%tiles(tile)%t_xmin,                     &
                                    chunk%tiles(tile)%t_xmax,                     &
                                    chunk%tiles(tile)%t_ymin,                     &
                                    chunk%tiles(tile)%t_ymax,                     &
                                    chunk%x_min,                                  &
                                    chunk%x_max,                                  &
                                    chunk%tiles(tile)%field%mass_flux_y,          &
                                    chunk%bottom_snd_buffer,                      &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,FIELD_MASS_FLUX_Y,Y_FACE_DATA,          &
                                    bottom_offset(FIELD_MASS_FLUX_Y,tile))
  ENDIF
  
END SUBROUTINE clover_pack_bottom

SUBROUTINE clover_pack_left_integer(tile,fields,depth,left_offset_integer,mmc_left_offset_integer)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,left_offset_integer(:),mmc_left_offset_integer(:)

  INTEGER        :: t_offset,mmc_t_offset

  t_offset=left_offset_integer(tile)
  mmc_t_offset=mmc_left_offset_integer(tile)
  
  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    CALL clover_pack_message_left_integer(chunk%tiles(tile)%t_xmin,                                          &
                                          chunk%tiles(tile)%t_xmax,                                          &
                                          chunk%tiles(tile)%t_ymin,                                          &
                                          chunk%tiles(tile)%t_ymax,                                          &
                                          chunk%tiles(tile)%field%material0,                                 &
                                          chunk%tiles(tile)%field%mmc_material0,                             &
                                          chunk%tiles(tile)%field%mmc_component0,                            &
                                          chunk%integer_left_snd_buffer,                                     &
                                          depth,                                                             &
                                          chunk%tiles(tile)%field%mmc_left_snd,                              &
                                          left_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+t_offset,         &
                                          mmc_left_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+mmc_t_offset, &
                                          chunk%tiles(tile)%field%left_index_snd_comp_list,                      &
                                          chunk%tiles(tile)%field%left_component_snd_comp_list                   )
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    CALL clover_pack_message_left_integer(chunk%tiles(tile)%t_xmin,                                          &
                                          chunk%tiles(tile)%t_xmax,                                          &
                                          chunk%tiles(tile)%t_ymin,                                          &
                                          chunk%tiles(tile)%t_ymax,                                          &
                                          chunk%tiles(tile)%field%material1,                                 &
                                          chunk%tiles(tile)%field%mmc_material1,                             &
                                          chunk%tiles(tile)%field%mmc_component1,                            &
                                          chunk%integer_left_snd_buffer,                                     &
                                          depth,                                                             &
                                          chunk%tiles(tile)%field%mmc_left_snd,                              &
                                          left_offset_integer(FIELD_MATERIAL1-REAL_FIELDS)+t_offset,         &
                                          mmc_left_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+mmc_t_offset, &
                                          chunk%tiles(tile)%field%left_index_snd_comp_list,                      &
                                          chunk%tiles(tile)%field%left_component_snd_comp_list                   )
  ENDIF

END SUBROUTINE clover_pack_left_integer

SUBROUTINE clover_unpack_left_integer(tile,fields,depth,left_offset_integer,mmc_left_offset_integer)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,left_offset_integer(:),mmc_left_offset_integer(:)

  INTEGER        :: t_offset,mmc_t_offset

  t_offset=left_offset_integer(tile)
  mmc_t_offset=mmc_left_offset_integer(tile)

! SO CALCULATE MMC RCV OFFSETS SOMEWHERE HERE.
! I MAY HAVE TO SPLIT THE UNPACKING INTO CELL AND MMC

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    CALL clover_unpack_message_left_integer(chunk%tiles(tile)%t_xmin,                                          &
                                            chunk%tiles(tile)%t_xmax,                                          &
                                            chunk%tiles(tile)%t_ymin,                                          &
                                            chunk%tiles(tile)%t_ymax,                                          &
                                            chunk%tiles(tile)%field%material0,                                 &
                                            chunk%integer_left_rcv_buffer,                                     &
                                            depth,                                                             &
                                            chunk%tiles(tile)%field%mmc_left_rcv,                              &
                                            chunk%tiles(tile)%field%component_left_rcv,                        &
                                            left_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+t_offset,         &
                                            mmc_left_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+mmc_t_offset, &
                                            chunk%tiles(tile)%field%mmc_material0,                             &
                                            chunk%tiles(tile)%field%mmc_index0,                                &
                                            chunk%tiles(tile)%field%mmc_component0,                            &
                                            chunk%tiles(tile)%field%mmc_j0,                                    &
                                            chunk%tiles(tile)%field%mmc_k0,                                    &
                                            chunk%tiles(tile)%field%left_index_rcv_comp_list,                  &
                                            chunk%tiles(tile)%field%left_component_rcv_comp_list               )
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    CALL clover_unpack_message_left_integer(chunk%tiles(tile)%t_xmin,                                          &
                                            chunk%tiles(tile)%t_xmax,                                          &
                                            chunk%tiles(tile)%t_ymin,                                          &
                                            chunk%tiles(tile)%t_ymax,                                          &
                                            chunk%tiles(tile)%field%material1,                                 &
                                            chunk%integer_left_rcv_buffer,                                     &
                                            depth,                                                             &
                                            chunk%tiles(tile)%field%mmc_left_rcv,                              &
                                            chunk%tiles(tile)%field%component_left_rcv,                        &
                                            left_offset_integer(FIELD_MATERIAL1-REAL_FIELDS)+t_offset,         &
                                            mmc_left_offset_integer(FIELD_MATERIAL1-REAL_FIELDS)+mmc_t_offset, &
                                            chunk%tiles(tile)%field%mmc_material1,                             &
                                            chunk%tiles(tile)%field%mmc_index1,                                &
                                            chunk%tiles(tile)%field%mmc_component1,                            &
                                            chunk%tiles(tile)%field%mmc_j1,                                    &
                                            chunk%tiles(tile)%field%mmc_k1,                                    &
                                            chunk%tiles(tile)%field%left_index_rcv_comp_list,                  &
                                            chunk%tiles(tile)%field%left_component_rcv_comp_list               )
  ENDIF

END SUBROUTINE clover_unpack_left_integer

SUBROUTINE clover_pack_right_integer(tile, fields, depth, right_offset_integer,mmc_right_offset_integer)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,right_offset_integer(:),mmc_right_offset_integer(:)

  INTEGER        :: t_offset,mmc_t_offset

  t_offset=right_offset_integer(tile)
  mmc_t_offset=mmc_right_offset_integer(tile)
  
  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    CALL clover_pack_message_right_integer(chunk%tiles(tile)%t_xmin,                                          &
                                           chunk%tiles(tile)%t_xmax,                                          &
                                           chunk%tiles(tile)%t_ymin,                                          &
                                           chunk%tiles(tile)%t_ymax,                                          &
                                           chunk%tiles(tile)%field%material0,                                 &
                                           chunk%tiles(tile)%field%mmc_material0,                             &
                                           chunk%tiles(tile)%field%mmc_component0,                            &
                                           chunk%integer_right_snd_buffer,                                    &
                                           depth,                                                             &
                                           chunk%tiles(tile)%field%mmc_right_snd,                             &
                                           right_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+t_offset,        &
                                           mmc_right_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+mmc_t_offset,&
                                           chunk%tiles(tile)%field%right_index_snd_comp_list,                     &
                                           chunk%tiles(tile)%field%right_component_snd_comp_list                  )
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    CALL clover_pack_message_right_integer(chunk%tiles(tile)%t_xmin,                                          &
                                           chunk%tiles(tile)%t_xmax,                                          &
                                           chunk%tiles(tile)%t_ymin,                                          &
                                           chunk%tiles(tile)%t_ymax,                                          &
                                           chunk%tiles(tile)%field%material1,                                 &
                                           chunk%tiles(tile)%field%mmc_material1,                             &
                                           chunk%tiles(tile)%field%mmc_component1,                            &
                                           chunk%integer_right_snd_buffer,                                    &
                                           depth,                                                             &
                                           chunk%tiles(tile)%field%mmc_right_snd,                             &
                                           right_offset_integer(FIELD_MATERIAL1-REAL_FIELDS)+t_offset,        &
                                           mmc_right_offset_integer(FIELD_MATERIAL1-REAL_FIELDS)+mmc_t_offset,&
                                           chunk%tiles(tile)%field%right_index_snd_comp_list,                     &
                                           chunk%tiles(tile)%field%right_component_snd_comp_list                  )
  ENDIF

END SUBROUTINE clover_pack_right_integer

SUBROUTINE clover_unpack_right_integer(tile,fields,depth,right_offset_integer,mmc_right_offset_integer)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,right_offset_integer(:),mmc_right_offset_integer(:)

  INTEGER        :: t_offset,mmc_t_offset

  t_offset=right_offset_integer(tile)
  mmc_t_offset=mmc_right_offset_integer(tile)
  
  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    CALL clover_unpack_message_right_integer(chunk%tiles(tile)%t_xmin,                                          &
                                             chunk%tiles(tile)%t_xmax,                                          &
                                             chunk%tiles(tile)%t_ymin,                                          &
                                             chunk%tiles(tile)%t_ymax,                                          &
                                             chunk%tiles(tile)%field%material0,                                 &
                                             chunk%integer_right_rcv_buffer,                                    &
                                             depth,                                                             &
                                             chunk%tiles(tile)%field%mmc_right_rcv,                             &
                                             chunk%tiles(tile)%field%component_right_rcv,                       &
                                             right_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+t_offset,        &
                                             mmc_right_offset_integer(FIELD_MATERIAL0-REAL_FIELDS)+mmc_t_offset,&
                                             chunk%tiles(tile)%field%mmc_material0,                             &
                                             chunk%tiles(tile)%field%mmc_index0,                                &
                                             chunk%tiles(tile)%field%mmc_component0,                            &
                                             chunk%tiles(tile)%field%mmc_j0,                                    &
                                             chunk%tiles(tile)%field%mmc_k0,                                    &
                                             chunk%tiles(tile)%field%right_index_rcv_comp_list,                 &
                                             chunk%tiles(tile)%field%right_component_rcv_comp_list              )
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    CALL clover_unpack_message_right_integer(chunk%tiles(tile)%t_xmin,                                          &
                                             chunk%tiles(tile)%t_xmax,                                          &
                                             chunk%tiles(tile)%t_ymin,                                          &
                                             chunk%tiles(tile)%t_ymax,                                          &
                                             chunk%tiles(tile)%field%material1,                                 &
                                             chunk%integer_right_rcv_buffer,                                    &
                                             depth,                                                             &
                                             chunk%tiles(tile)%field%mmc_right_rcv,                             &
                                             chunk%tiles(tile)%field%component_right_rcv,                       &
                                             right_offset_integer(FIELD_MATERIAL1-REAL_FIELDS)+t_offset,        &
                                             mmc_right_offset_integer(FIELD_MATERIAL1-REAL_FIELDS)+mmc_t_offset,&
                                             chunk%tiles(tile)%field%mmc_material1,                             &
                                             chunk%tiles(tile)%field%mmc_index1,                                &
                                             chunk%tiles(tile)%field%mmc_component1,                            &
                                             chunk%tiles(tile)%field%mmc_j1,                                    &
                                             chunk%tiles(tile)%field%mmc_k1,                                    &
                                             chunk%tiles(tile)%field%right_index_rcv_comp_list,                 &
                                             chunk%tiles(tile)%field%right_component_rcv_comp_list              )
  ENDIF

END SUBROUTINE clover_unpack_right_integer

SUBROUTINE clover_pack_bottom_integer(tile,                            &
                                      fields,                          &
                                      depth,                           &
                                      bottom_offset_integer,           &
                                      component_integer_bottom_offset, &
                                      mmc_count_bottom                 )

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,bottom_offset_integer(:,:),component_integer_bottom_offset(:,:)
  INTEGER        :: mmc_count_bottom(:,:)

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    CALL clover_pack_message_bottom_integer(tile,                                                       &
                                            chunk%tiles(tile)%t_xmin,                                   &
                                            chunk%tiles(tile)%t_xmax,                                   &
                                            chunk%tiles(tile)%t_ymin,                                   &
                                            chunk%tiles(tile)%t_ymax,                                   &
                                            chunk%x_min,                                                &
                                            chunk%x_max,                                                &
                                            chunk%tiles(tile)%field%material0,                          &
                                            chunk%tiles(tile)%field%mmc_material0,                      &
                                            chunk%tiles(tile)%field%mmc_component0,                     &
                                            chunk%integer_bottom_snd_buffer,                            &
                                            depth,                                                      &
                                            mmc_count_bottom,                                           &
                                            bottom_offset_integer(FIELD_MATERIAL0-REAL_FIELDS,tile),    &
                                            component_integer_bottom_offset,                            &
                                            chunk%tiles(tile)%field%bottom_component_snd_comp_list,     &
                                            chunk%tiles(tile)%field%bottom_component_snd_comp_count     )
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    CALL clover_pack_message_bottom_integer(tile,                                                       &
                                            chunk%tiles(tile)%t_xmin,                                   &
                                            chunk%tiles(tile)%t_xmax,                                   &
                                            chunk%tiles(tile)%t_ymin,                                   &
                                            chunk%tiles(tile)%t_ymax,                                   &
                                            chunk%x_min,                                                &
                                            chunk%x_max,                                                &
                                            chunk%tiles(tile)%field%material1,                          &
                                            chunk%tiles(tile)%field%mmc_material1,                      &
                                            chunk%tiles(tile)%field%mmc_component1,                     &
                                            chunk%integer_bottom_snd_buffer,                            &
                                            depth,                                                      &
                                            mmc_count_bottom,                                           &
                                            bottom_offset_integer(FIELD_MATERIAL1-REAL_FIELDS,tile),    &
                                            component_integer_bottom_offset,                            &
                                            chunk%tiles(tile)%field%bottom_component_snd_comp_list,     &
                                            chunk%tiles(tile)%field%bottom_component_snd_comp_count     )
  ENDIF

END SUBROUTINE clover_pack_bottom_integer

SUBROUTINE clover_pack_top_integer(tile,                            &
                                   fields,                          &
                                   depth,                           &
                                   top_offset_integer,              &
                                   component_integer_top_offset,    &
                                   mmc_count_top                    )

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER        :: tile,fields(:),depth,top_offset_integer(:,:),component_integer_top_offset(:,:)
  INTEGER        :: mmc_count_top(:,:)

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    CALL clover_pack_message_top_integer(tile,                                                &
                                         chunk%tiles(tile)%t_xmin,                            &
                                         chunk%tiles(tile)%t_xmax,                            &
                                         chunk%tiles(tile)%t_ymin,                            &
                                         chunk%tiles(tile)%t_ymax,                            &
                                         chunk%x_min,                                         &
                                         chunk%x_max,                                         &
                                         chunk%tiles(tile)%field%material0,                   &
                                         chunk%tiles(tile)%field%mmc_material0,               &
                                         chunk%tiles(tile)%field%mmc_component0,              &
                                         chunk%integer_top_snd_buffer,                        &
                                         depth,                                               &
                                         mmc_count_top,                                       &
                                         top_offset_integer(FIELD_MATERIAL0-REAL_FIELDS,tile),&
                                         component_integer_top_offset,                        &
                                         chunk%tiles(tile)%field%top_component_snd_comp_list, &
                                         chunk%tiles(tile)%field%top_component_snd_comp_count )
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    CALL clover_pack_message_top_integer(tile,                                                &
                                         chunk%tiles(tile)%t_xmin,                            &
                                         chunk%tiles(tile)%t_xmax,                            &
                                         chunk%tiles(tile)%t_ymin,                            &
                                         chunk%tiles(tile)%t_ymax,                            &
                                         chunk%x_min,                                         &
                                         chunk%x_max,                                         &
                                         chunk%tiles(tile)%field%material1,                   &
                                         chunk%tiles(tile)%field%mmc_material1,               &
                                         chunk%tiles(tile)%field%mmc_component1,              &
                                         chunk%integer_top_snd_buffer,                        &
                                         depth,                                               &
                                         mmc_count_top,                                       &
                                         top_offset_integer(FIELD_MATERIAL1-REAL_FIELDS,tile),&
                                         component_integer_top_offset,                        &
                                         chunk%tiles(tile)%field%top_component_snd_comp_list, &
                                         chunk%tiles(tile)%field%top_component_snd_comp_count )
  ENDIF

END SUBROUTINE clover_pack_top_integer

SUBROUTINE clover_send_recv_message_bottom(bottom_snd_buffer,bottom_rcv_buffer, &
                                           total_size,                          &
                                           total_size_rcv,                      &
                                           tag_send, tag_recv,                  &
                                           req_send, req_recv                   )

  IMPLICIT NONE

  REAL(KIND=8) :: bottom_snd_buffer(:),bottom_rcv_buffer(:)
  INTEGER      :: bottom_task
  INTEGER      :: total_size,total_size_rcv,tag_send,tag_recv, err
  INTEGER      :: req_send, req_recv

  bottom_task=chunk%chunk_neighbours(CHUNK_BOTTOM)-1

  CALL MPI_ISEND(bottom_snd_buffer,total_size,MPI_DOUBLE_PRECISION,bottom_task,tag_send &
                ,MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(bottom_rcv_buffer,total_size_rcv,MPI_DOUBLE_PRECISION,bottom_task,tag_recv &
                ,MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_bottom

SUBROUTINE clover_send_recv_message_bottom_integer(integer_bottom_snd_buffer,integer_bottom_rcv_buffer,  &
                                                   total_size,                                           &
                                                   total_size_rcv,                                       &
                                                   tag_send, tag_recv,                                   &
                                                   req_send, req_recv)

  IMPLICIT NONE

  INTEGER      :: integer_bottom_snd_buffer(:), integer_bottom_rcv_buffer(:)
  INTEGER      :: bottom_task
  INTEGER      :: total_size,total_size_rcv, tag_send, tag_recv, err
  INTEGER      :: req_send, req_recv

  bottom_task=chunk%chunk_neighbours(CHUNK_BOTTOM)-1

  CALL MPI_ISEND(integer_bottom_snd_buffer,total_size,MPI_INTEGER,bottom_task,tag_send &
                ,MPI_COMM_WORLD,req_send,err)

  CALL MPI_IRECV(integer_bottom_rcv_buffer,total_size_rcv,MPI_INTEGER,bottom_task,tag_recv &
                ,MPI_COMM_WORLD,req_recv,err)

END SUBROUTINE clover_send_recv_message_bottom_integer

SUBROUTINE clover_unpack_bottom(fields,tile,depth,bottom_offset,smc_offset,                        &
                                mmc_offset_bottom,component_counter,component_counter_tile,mmc_comm)

  USE pack_kernel_module

  IMPLICIT NONE

  INTEGER         :: fields(:),tile,depth,bottom_offset(:,:)
  LOGICAL         :: mmc_comm

  INTEGER         :: smc_offset,mmc_offset_bottom,component_counter,mmc_offset,component_counter_tile(:,:)
  INTEGER         :: mmc_halo_offset

  mmc_offset=mmc_offset_bottom
  mmc_halo_offset=component_counter_tile(tile,2)

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%density0,                     &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_DENSITY0,tile),                   &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_density0,                 &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)  ! Needed?
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%density1,                     &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_DENSITY1,tile),                   &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_density1,                 &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%energy0,                      &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_ENERGY0,tile),                    &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_energy0,                  &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%energy1,                      &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_ENERGY1,tile),                    &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_energy1,                  &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%pressure,                     &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_PRESSURE,tile),                   &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_pressure,                 &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%viscosity,                    &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_VISCOSITY,tile),                  &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_viscosity,                &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%soundspeed,                   &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_SOUNDSPEED,tile),                 &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_soundspeed,               &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_VOLUME0).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%volume,                       &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_VOLUME0,tile),                    &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_volume0,                  &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_VOLUME1).GT.0) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                             &
                                      chunk%tiles(tile)%t_xmax,                             &
                                      chunk%tiles(tile)%t_ymin,                             &
                                      chunk%tiles(tile)%t_ymax,                             &
                                      chunk%x_min,                                          &
                                      chunk%x_max,                                          &
                                      chunk%tiles(tile)%field%volume,                       &
                                      chunk%bottom_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,        &
                                      depth, CELL_DATA,                                     &
                                      bottom_offset(FIELD_VOLUME1,tile),                    &
                                      mmc_comm,                                             &
                                      smc_offset,                                           &
                                      mmc_offset,                                           &
                                      mmc_halo_offset,                                      &
                                      component_counter,                                    &
                                      chunk%tiles(tile)%field%material1,                    &
                                      chunk%tiles(tile)%field%mmc_index1,                   &
                                      chunk%tiles(tile)%field%mmc_component1,               &
                                      chunk%tiles(tile)%field%component_bottom_rcv,         &
                                      chunk%tiles(tile)%field%mmc_volume1,                  &
                                      chunk%tiles(tile)%field%bottom_component_rcv_comp_list)
    mmc_offset=mmc_offset+component_counter
  ENDIF
  IF(fields(FIELD_XVEL0).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%xvel0,                &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, VERTEX_DATA,                           &
                                      bottom_offset(FIELD_XVEL0,tile)               )
  ENDIF
  IF(fields(FIELD_XVEL1).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%xvel1,                &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, VERTEX_DATA,                           &
                                      bottom_offset(FIELD_XVEL1,tile)               )
  ENDIF
  IF(fields(FIELD_YVEL0).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%yvel0,                &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, VERTEX_DATA,                           &
                                      bottom_offset(FIELD_YVEL0,tile)               )
  ENDIF
  IF(fields(FIELD_YVEL1).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%yvel1,                &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, VERTEX_DATA,                           &
                                      bottom_offset(FIELD_YVEL1,tile)               )
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%vol_flux_x,           &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, X_FACE_DATA,                           &
                                      bottom_offset(FIELD_VOL_FLUX_X,tile)          )
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%vol_flux_y,           &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, Y_FACE_DATA,                           &
                                      bottom_offset(FIELD_VOL_FLUX_Y,tile)          )
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%mass_flux_x,          &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, X_FACE_DATA,                           &
                                      bottom_offset(FIELD_MASS_FLUX_X,tile)         )
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
    CALL clover_unpack_message_bottom(chunk%tiles(tile)%t_xmin,                     &
                                      chunk%tiles(tile)%t_xmax,                     &
                                      chunk%tiles(tile)%t_ymin,                     &
                                      chunk%tiles(tile)%t_ymax,                     &
                                      chunk%x_min,                                  &
                                      chunk%x_max,                                  &
                                      chunk%tiles(tile)%field%mass_flux_y,          &
                                      chunk%bottom_rcv_buffer,                      &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth, Y_FACE_DATA,                           &
                                      bottom_offset(FIELD_MASS_FLUX_Y,tile)         )
  ENDIF
  
END SUBROUTINE clover_unpack_bottom

SUBROUTINE clover_calc_top_mmc_rcv_offsets(fields,depth,integer_top_rcv_buffer,smc_top_offset, &
                                           component_counter,component_counter_tile)


  ! Set up rcv lists for MMCs and unpack the integer data

  IMPLICIT NONE

  INTEGER :: fields(:),depth,integer_top_rcv_buffer(chunk%top_buffer_size)

  INTEGER :: mmc_next(tiles_per_chunk),component_next(tiles_per_chunk)
  INTEGER :: mmc_count(tiles_per_chunk),t_offset(tiles_per_chunk)

  INTEGER :: mmc,comps,component,halo
  INTEGER :: component_counter,component_counter_tile(:,:)
  INTEGER :: tile_list(tiles_per_chunk),tile_list_counter
  INTEGER :: tile_found(tiles_per_chunk)

  INTEGER :: tile,buffer_index,j,k,comp,buffer_offset,acceptor_tile,j_tile,k_tile
  INTEGER :: smc_integer_offset,smc_top_offset,mmc_offset,field,cell_smc_offset,vertex_smc_offset,facex_smc_offset,facey_smc_offset

  ! Loop over the MPI buffer and check and store ownership of MMC data for tiles on the chunk
  buffer_index=0
  mmc_count=0 ! Could just be a diagnostic or I can use it to make sure storage isn't exceeded
  component_counter=0
  component_counter_tile=0
  tile_list=0
  tile_found=0
  tile_list_counter=1

  ! Record initial free index for each tile
  DO tile=1,tiles_per_chunk
    mmc_next(tile)=chunk%tiles(tile)%field%mmc_counter_next(1) ! Remember (1) is thread number
    component_next(tile)=chunk%tiles(tile)%field%component_counter_next(1)
  ENDDO

  ! There is where the SMC data finishes in the MPI buffer
  ! This allows me to jump ahead to the MMC data
  ! Calculate the total offset of SMC data. Do I need to store this per field? I think no.
  mmc_count=0
  smc_top_offset=0
  mmc_offset=0
  cell_smc_offset=(chunk%x_max-chunk%x_min+1)*depth
  vertex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facey_smc_offset=(chunk%x_max-chunk%x_min+1)*depth
  ! Calculate the total offset of SMC data. Do I need to store this per field? I think no.
  smc_top_offset=0
  DO field=1,REAL_FIELDS
    IF(fields(field).GT.0) THEN
      IF(field_centering(field).EQ.CELL_DATA)   smc_top_offset=smc_top_offset+cell_smc_offset
      IF(field_centering(field).EQ.VERTEX_DATA) smc_top_offset=smc_top_offset+vertex_smc_offset
      IF(field_centering(field).EQ.X_FACE_DATA) smc_top_offset=smc_top_offset+facex_smc_offset
      IF(field_centering(field).EQ.Y_FACE_DATA) smc_top_offset=smc_top_offset+facey_smc_offset
    ENDIF 
  ENDDO
  smc_integer_offset=cell_smc_offset

  ! Loop over the cells that will be unpacked from the MPI task chunk buffer
  halo=0
  DO k=chunk%top+1,chunk%top+depth
    halo=halo+1
    DO j=chunk%x_min,chunk%x_max
      buffer_index=buffer_index+1
      acceptor_tile=0
      DO tile=1,tiles_per_chunk
        ! Which tile owns this data?
        IF(j.GE.chunk%tiles(tile)%t_left    .AND. &
           j.LE.chunk%tiles(tile)%t_right   .AND. &
           k.GE.chunk%tiles(tile)%t_bottom+2.AND. &
           k.LE.chunk%tiles(tile)%t_top+2)   THEN 
           j_tile=j-chunk%tiles(tile)%t_left+1
           k_tile=k-chunk%tiles(tile)%t_bottom+1
           acceptor_tile=tile
           IF(tile_found(tile).EQ.0) THEN
             tile_list(tile_list_counter)=tile
             tile_list_counter=tile_list_counter+1
             tile_found(tile)=1
           ENDIF
           EXIT
        ENDIF
      ENDDO

      IF(integer_top_rcv_buffer(buffer_index).GT.0) THEN
        ! Single material cell (SMC)
        chunk%tiles(acceptor_tile)%field%material1(j_tile,k_tile)=integer_top_rcv_buffer(buffer_index)
      ELSE
        ! Found a Multi material cell (MMC)
        ! Keep a running total of the total component count in the buffer so I can use this to point
        ! to tile specific data
        comps=-integer_top_rcv_buffer(buffer_index)
        ! Write the data into the acceptor halo cells that I can at this point
        mmc=mmc_next(acceptor_tile)
        component=component_next(acceptor_tile)
        chunk%tiles(acceptor_tile)%field%material1(j_tile,k_tile)=-mmc
        chunk%tiles(acceptor_tile)%field%mmc_component1(mmc)=comps
        chunk%tiles(acceptor_tile)%field%mmc_j1(mmc)=j_tile
        chunk%tiles(acceptor_tile)%field%mmc_k1(mmc)=k_tile
        chunk%tiles(acceptor_tile)%field%mmc_index1(mmc)=component

        ! Update the write locations pointers for MMC and component data
        mmc_count(acceptor_tile)=mmc_count(acceptor_tile)+1
        ! This needs be an array with buffer indices that the tile owns, but I have update the index1 var, so probably not
        ! I may be able to delete these or just initialise them to the starting pointer (but index1 is correct now so I think deleting is an option)
        ! So store the current component counter into the receive list for each MMC
        chunk%tiles(acceptor_tile)%field%top_index_rcv_comp_list(1)=mmc_next(acceptor_tile)
        chunk%tiles(acceptor_tile)%field%top_component_rcv_comp_list=component_next(acceptor_tile)

        buffer_offset=component_counter+smc_integer_offset
        ! Can I use it now to set material components and save it for other component data?
        DO comp=1,comps
          chunk%tiles(acceptor_tile)%field%mmc_material1(component_next(acceptor_tile)+comp-1)=integer_top_rcv_buffer(buffer_offset+comp)
        ENDDO

        mmc_next(acceptor_tile)=mmc_next(acceptor_tile)+1
        component_next(acceptor_tile)=component_next(acceptor_tile)+ABS(integer_top_rcv_buffer(buffer_index))

        ! I need to store this so I can increment the buffer offset
        component_counter=component_counter+comps
        component_counter_tile(acceptor_tile,halo)=component_counter_tile(acceptor_tile,halo)+comps
      ENDIF
    ENDDO
  ENDDO

  ! Update the free index for use with the next communication
  DO tile=1,tiles_per_chunk
    chunk%tiles(tile)%field%mmc_counter_next(1)=mmc_next(tile)
    chunk%tiles(tile)%field%component_counter_next(1)=component_next(tile)
  ENDDO

END SUBROUTINE clover_calc_top_mmc_rcv_offsets

SUBROUTINE clover_calc_bottom_mmc_rcv_offsets(fields,depth,integer_bottom_rcv_buffer,smc_bottom_offset, &
                                              component_counter,component_counter_tile)

  ! Set up rcv lists for MMCs and unpack the integer data

  IMPLICIT NONE

  INTEGER :: fields(:),depth,integer_bottom_rcv_buffer(chunk%bottom_buffer_size)
  INTEGER :: component_counter,component_counter_tile(:,:)

  INTEGER :: mmc_next(tiles_per_chunk),component_next(tiles_per_chunk)
  INTEGER :: mmc_count(tiles_per_chunk),t_offset(tiles_per_chunk)
  INTEGER :: tile_list(tiles_per_chunk),tile_list_counter
  INTEGER :: tile_found(tiles_per_chunk)

  INTEGER :: mmc,comps,component,halo

  INTEGER :: tile,buffer_index,j,k,comp,buffer_offset,j_tile,k_tile,acceptor_tile
  INTEGER :: smc_bottom_offset,smc_integer_offset,mmc_offset,field,cell_smc_offset,vertex_smc_offset,facex_smc_offset,facey_smc_offset

  ! Loop over the MPI buffer and check and store ownership of MMC data for tiles on the chunk
  buffer_index=0
  mmc_count=0 ! Could just be a diagnostic or I can use it to make sure storage isn't exceeded
  component_counter=0
  component_counter_tile=0

  ! Record initial free index for each tile
  DO tile=1,tiles_per_chunk
    mmc_next(tile)=chunk%tiles(tile)%field%mmc_counter_next(1) ! Remember (1) is thread number
    component_next(tile)=chunk%tiles(tile)%field%component_counter_next(1)
  ENDDO

  ! There is where the SMC data finishes in the MPI buffer
  ! This allows me to jump ahead to the MMC data
  ! Calculate the total offset of SMC data. Do I need to store this per field? I think no.
  mmc_count=0
  smc_bottom_offset=0
  mmc_offset=0
  tile_list=0
  tile_found=0
  tile_list_counter=1
  cell_smc_offset=(chunk%x_max-chunk%x_min+1)*depth
  vertex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facey_smc_offset=(chunk%x_max-chunk%x_min+1)*depth
  ! Calculate the total offset of SMC data. Do I need to store this per field? I think no.
  smc_bottom_offset=0
  DO field=1,REAL_FIELDS
    IF(fields(field).GT.0) THEN
      IF(field_centering(field).EQ.CELL_DATA)   smc_bottom_offset=smc_bottom_offset+cell_smc_offset
      IF(field_centering(field).EQ.VERTEX_DATA) smc_bottom_offset=smc_bottom_offset+vertex_smc_offset
      IF(field_centering(field).EQ.X_FACE_DATA) smc_bottom_offset=smc_bottom_offset+facex_smc_offset
      IF(field_centering(field).EQ.Y_FACE_DATA) smc_bottom_offset=smc_bottom_offset+facey_smc_offset
    ENDIF 
  ENDDO
  smc_integer_offset=cell_smc_offset
  halo=0

  ! Loop over global coords to find acceptor tile
  DO k=chunk%bottom-1,chunk%bottom-depth,-1 ! Unpack first halo first
    halo=halo+1
    DO j=chunk%left,chunk%right
      buffer_index=buffer_index+1
      acceptor_tile=0
      DO tile=1,tiles_per_chunk
        ! Which tile owns this data?
        IF(j.GE.chunk%tiles(tile)%t_left    .AND. &
           j.LE.chunk%tiles(tile)%t_right   .AND. &
           k.GE.chunk%tiles(tile)%t_bottom-2.AND. &
           k.LE.chunk%tiles(tile)%t_top-2)   THEN 
           j_tile=j-chunk%tiles(tile)%t_left+1
           k_tile=k-chunk%tiles(tile)%t_bottom+1
           acceptor_tile=tile
           IF(tile_found(tile).EQ.0) THEN
             tile_list(tile_list_counter)=tile
             tile_list_counter=tile_list_counter+1
             tile_found(tile)=1
           ENDIF
           EXIT
        ENDIF
      ENDDO

      IF(integer_bottom_rcv_buffer(buffer_index).GT.0) THEN
        ! Single material cell (SMC)
        chunk%tiles(acceptor_tile)%field%material1(j_tile,k_tile)=integer_bottom_rcv_buffer(buffer_index)
      ELSE
        ! Found a Multi material cell (MMC)
        ! Keep a running total of the total component count in the buffer so I can use this to point
        ! to tile specific data
        comps=-integer_bottom_rcv_buffer(buffer_index)
        ! Write the data into the acceptor halo cells that I can at this point
        mmc=mmc_next(acceptor_tile)
        component=component_next(acceptor_tile)
        chunk%tiles(acceptor_tile)%field%material1(j_tile,k_tile)=-mmc
        chunk%tiles(acceptor_tile)%field%mmc_component1(mmc)=comps
        chunk%tiles(acceptor_tile)%field%mmc_j1(mmc)=j_tile
        chunk%tiles(acceptor_tile)%field%mmc_k1(mmc)=k_tile
        chunk%tiles(acceptor_tile)%field%mmc_index1(mmc)=component

        ! Update the write locations pointers for MMC and component data
        mmc_count(acceptor_tile)=mmc_count(acceptor_tile)+1
        ! This needs be an array with buffer indices that the tile owns, but I have update the index1 var, so probably not
        ! I may be able to delete these or just initialise them to the starting pointer (but index1 is correct now so I think deleting is an option)
        ! So store the current component counter into the receive list for each MMC
        chunk%tiles(acceptor_tile)%field%bottom_index_rcv_comp_list(1)=mmc_next(acceptor_tile)
        chunk%tiles(acceptor_tile)%field%bottom_component_rcv_comp_list=component_next(acceptor_tile)

        buffer_offset=component_counter+smc_integer_offset
        ! Can I use it now to set material components and save it for other component data?
        DO comp=1,comps
          chunk%tiles(acceptor_tile)%field%mmc_material1(component_next(acceptor_tile)+comp-1)=integer_bottom_rcv_buffer(buffer_offset+comp)
        ENDDO

        mmc_next(acceptor_tile)=mmc_next(acceptor_tile)+1
        component_next(acceptor_tile)=component_next(acceptor_tile)+ABS(integer_bottom_rcv_buffer(buffer_index))

        ! I need to store this so I can increment the buffer index
        component_counter=component_counter+comps
        component_counter_tile(acceptor_tile,halo)=component_counter_tile(acceptor_tile,halo)+comps
      ENDIF
    ENDDO
  ENDDO

  ! Update the free index for use with the next communication
  DO tile=1,tiles_per_chunk
    chunk%tiles(tile)%field%mmc_counter_next(1)=mmc_next(tile)
    chunk%tiles(tile)%field%component_counter_next(1)=component_next(tile)
  ENDDO

END SUBROUTINE clover_calc_bottom_mmc_rcv_offsets

SUBROUTINE clover_calc_top_mmc_snd_offsets(fields,                      &
                                           depth,                       &
                                           component_top_offset,        &
                                           component_integer_top_offset,&
                                           mmc_count_top,               &
                                           component_top_snd            )

  ! Set up snd lists for MMC data. Doesn't pack here (yet)

  IMPLICIT NONE

  INTEGER      :: fields(:),depth
  INTEGER      :: component_top_offset(0:tiles_per_chunk,NUM_FIELDS,2) ! 2 is depth, starts at 0 so I can have an index of (tile-1)
  INTEGER      :: component_top_snd(1:depth)
  INTEGER      :: component_integer_top_offset(0:tiles_per_chunk,2)

  INTEGER      :: smc_offset,cell_smc_offset,vertex_smc_offset,facex_smc_offset,facey_smc_offset
  INTEGER      :: smc_integer_offset
  INTEGER      :: tile,j,k,comp,offset,j_tile,k_tile,donor_tile,halo_depth
  INTEGER      :: mmc,comps,component_index,field,prev_offset
  INTEGER      :: mmc_count_top(tiles_per_chunk,2) ! 2 is depth
  INTEGER      :: comp_count(tiles_per_chunk,2) ! 2 is depth

  ! Loop over the donor mesh and check and store SMC and MMC offsets for each tile that will write
  ! to the send buffer and then pack it

  ! Count the number of fields in this phase.

  ! There is where the SMC data finishes in the MPI buffer for one field
  ! This allows me to jump ahead to the MMC data
  cell_smc_offset=(chunk%x_max-chunk%x_min+1)*depth
  vertex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facey_smc_offset=(chunk%x_max-chunk%x_min+1)*depth

  ! Calculate the total offset of SMC data.
  smc_offset=0
  DO field=1,REAL_FIELDS
    IF(fields(field).GT.0) THEN
      IF(field_centering(field).EQ.CELL_DATA)   smc_offset=smc_offset+cell_smc_offset
      IF(field_centering(field).EQ.VERTEX_DATA) smc_offset=smc_offset+vertex_smc_offset
      IF(field_centering(field).EQ.X_FACE_DATA) smc_offset=smc_offset+facex_smc_offset
      IF(field_centering(field).EQ.Y_FACE_DATA) smc_offset=smc_offset+facey_smc_offset
    ENDIF 
  ENDDO

  ! Only one material field is communicated
  smc_integer_offset=cell_smc_offset

  ! IT WILL BE MORE EFFICIENT TO LOOP OVER TILES AND THOSE LIE ON THE COMMS FACE
  ! WILL SEND ALL DATA ALONG AN EDGE TO THE BUFFER
  ! Loop over the cells that will be packed into the MPI task chunk buffer
  comp_count=0
  mmc_count_top=0
  halo_depth=0
  ! Loop over the cells that will be packed into the MPI task chunk buffer
  DO k=chunk%top,chunk%top-depth+1,-1 ! Pack depth 1 halo first
    halo_depth=halo_depth+1
    DO j=chunk%left,chunk%right

      ! Find the tile that owns this cell
      donor_tile=0
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
          IF(j.GE.chunk%tiles(tile)%t_left  .AND. &
             j.LE.chunk%tiles(tile)%t_right       ) THEN
            ! This is the global j,k. I need to convert to the local tile indices now so I
            ! can access the tile data correctly.
            j_tile=j-chunk%tiles(tile)%t_left+1
            k_tile=k-chunk%tiles(tile)%t_bottom+1
            donor_tile=tile
            EXIT
          ENDIF
        ENDIF
      ENDDO

      IF(donor_tile.NE.0) THEN
        IF(chunk%tiles(donor_tile)%field%material1(j_tile,k_tile).GT.0) THEN
          ! Single material cell (SMC)
          !top_snd_buffer()=blah ! I can't do this yet until I decide how to loop over fields
        ELSE
          ! Found a Multi material cell (MMC)
          ! Set up the lists for each tile so they can write to the MPI independently
          mmc=-chunk%tiles(donor_tile)%field%material1(j_tile,k_tile)
          comps=chunk%tiles(donor_tile)%field%mmc_component1(mmc)
          component_index=chunk%tiles(donor_tile)%field%mmc_index1(mmc)
          mmc_count_top(donor_tile,halo_depth)=mmc_count_top(donor_tile,halo_depth)+1
          comp_count(donor_tile,halo_depth)=comp_count(donor_tile,halo_depth)+comps
          chunk%tiles(donor_tile)%field%top_component_snd_comp_list(mmc_count_top(donor_tile,halo_depth),halo_depth)=component_index
          chunk%tiles(donor_tile)%field%top_component_snd_comp_count(mmc_count_top(donor_tile,halo_depth),halo_depth)=comps
        ENDIF
      ENDIF

    ENDDO
  ENDDO

  ! Calculate offsets per halo, per tile for MMC integer data
  prev_offset=smc_integer_offset
  component_integer_top_offset=0
  IF(fields(FIELD_MATERIAL1).GT.1) THEN
    DO halo_depth=1,depth
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
          component_integer_top_offset(tile,halo_depth)=prev_offset
          prev_offset=component_integer_top_offset(tile,halo_depth)+comp_count(tile,halo_depth)
        ENDIF
      ENDDO
    ENDDO
  ENDIF

  ! Calculate offsets per halo, per field, per tile for MMC data
  prev_offset=smc_offset
  component_top_offset=0
  component_top_snd=0
  ! These loops are in stride one in MPI buffer order. This is what makes the MPI buffer tile agnostic
  DO field=1,REAL_FIELDS
    IF(fields(field).GT.1) THEN
      DO halo_depth=1,depth
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_TOP).EQ.MPI_TILE) THEN
            component_top_offset(tile,field,halo_depth)=prev_offset
            prev_offset=component_top_offset(tile,field,halo_depth)+comp_count(tile,halo_depth)
            component_top_snd(halo_depth)=component_top_snd(halo_depth)+comp_count(tile,halo_depth)
          ENDIF
        ENDDO
      ENDDO
    ENDIF
  ENDDO

END SUBROUTINE clover_calc_top_mmc_snd_offsets

SUBROUTINE clover_calc_bottom_mmc_snd_offsets(fields,                         &
                                              depth,                          &
                                              component_bottom_offset,        &
                                              component_integer_bottom_offset,&
                                              mmc_count_bottom,               &
                                              component_bottom_snd            )

  ! Set up snd lists for MMCs for real data. Doesn't pack here (yet)

  IMPLICIT NONE

  INTEGER      :: fields(:),depth
  INTEGER      :: component_bottom_offset(0:tiles_per_chunk,NUM_FIELDS,2) ! 2 is depth, starts at 0 so I can have an index of (tile-1)
  INTEGER      :: component_bottom_snd(1:depth)
  INTEGER      :: component_integer_bottom_offset(0:tiles_per_chunk,2)

  INTEGER      :: smc_offset,cell_smc_offset,vertex_smc_offset,facex_smc_offset,facey_smc_offset
  INTEGER      :: smc_integer_offset
  INTEGER      :: tile,j,k,comp,offset,j_tile,k_tile,donor_tile,halo_depth
  INTEGER      :: mmc,comps,component_index,field,prev_offset
  INTEGER      :: mmc_count_bottom(tiles_per_chunk,2) ! 2 is depth
  INTEGER      :: comp_count(tiles_per_chunk,2) ! 2 is depth

  ! Loop over the donor mesh and check and store SMC and MMC offsets for each tile that will write
  ! to the send buffer

  ! Count the number of fields in this phase.

  ! There is where the SMC data finishes in the MPI buffer
  ! This allows me to jump ahead to the MMC data
  cell_smc_offset=(chunk%x_max-chunk%x_min+1)*depth
  vertex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facex_smc_offset=(chunk%x_max-chunk%x_min+2)*depth
  facey_smc_offset=(chunk%x_max-chunk%x_min+1)*depth

  ! Calculate the total offset of SMC data. Do I need to store this per field? I think no.
  smc_offset=0
  DO field=1,REAL_FIELDS
    IF(fields(field).GT.0) THEN
      IF(field_centering(field).EQ.CELL_DATA)   smc_offset=smc_offset+cell_smc_offset
      IF(field_centering(field).EQ.VERTEX_DATA) smc_offset=smc_offset+vertex_smc_offset
      IF(field_centering(field).EQ.X_FACE_DATA) smc_offset=smc_offset+facex_smc_offset
      IF(field_centering(field).EQ.Y_FACE_DATA) smc_offset=smc_offset+facey_smc_offset
    ENDIF 
  ENDDO

  ! Only one material field is communicated
  smc_integer_offset=cell_smc_offset

  ! IT WILL BE MORE EFFICIENT TO LOOP OVER TILES AND THOSE LIE ON THE COMMS FACE
  ! WILL SEND ALL DATA ALONG AN EDGE TO THE BUFFER
  ! Loop over the cells that will be packed into the MPI task chunk buffer
  comp_count=0
  mmc_count_bottom=0
  halo_depth=0
  ! Loop over the cells that will be packed into the MPI task chunk buffer
  DO k=chunk%bottom,chunk%bottom+depth-1
    halo_depth=halo_depth+1
    DO j=chunk%x_min,chunk%x_max

      ! Find the tile that owns this cell
      donor_tile=0
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
          IF(j.GE.chunk%tiles(tile)%t_left  .AND. &
             j.LE.chunk%tiles(tile)%t_right       ) THEN
            ! This is the global j,k. I need to convert to the local tile indices now so I
            ! can access the tile data correctly.
            j_tile=j-chunk%tiles(tile)%t_left+1
            k_tile=k-chunk%tiles(tile)%t_bottom+1
            donor_tile=tile
            EXIT
          ENDIF
        ENDIF
      ENDDO

      IF(donor_tile.NE.0) THEN
        IF(chunk%tiles(donor_tile)%field%material1(j_tile,k_tile).GT.0) THEN
          ! Single material cell (SMC)
          ! At the moment packed in the pack routine. COULD BE MOVED TO HERE PERHAPS
        ELSE
          ! Found a Multi material cell (MMC)
          ! Set up the lists for each tile so they can write to the MPI independently
          mmc=-chunk%tiles(donor_tile)%field%material1(j_tile,k_tile)
          comps=chunk%tiles(donor_tile)%field%mmc_component1(mmc)
          component_index=chunk%tiles(donor_tile)%field%mmc_index1(mmc)
          mmc_count_bottom(donor_tile,halo_depth)=mmc_count_bottom(donor_tile,halo_depth)+1
          comp_count(donor_tile,halo_depth)=comp_count(donor_tile,halo_depth)+comps
          chunk%tiles(donor_tile)%field%bottom_component_snd_comp_list(mmc_count_bottom(donor_tile,halo_depth),halo_depth)=component_index
          chunk%tiles(donor_tile)%field%bottom_component_snd_comp_count(mmc_count_bottom(donor_tile,halo_depth),halo_depth)=comps
        ENDIF
      ENDIF

    ENDDO
  ENDDO

  ! Calculate offsets per halo, per tile for MMC integer data
  prev_offset=smc_integer_offset
  component_integer_bottom_offset=0
  IF(fields(FIELD_MATERIAL1).GT.1) THEN
    DO halo_depth=1,depth
      DO tile=1,tiles_per_chunk
        IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
          component_integer_bottom_offset(tile,halo_depth)=prev_offset
          prev_offset=component_integer_bottom_offset(tile,halo_depth)+comp_count(tile,halo_depth)
        ENDIF
      ENDDO
    ENDDO
  ENDIF

  ! Calculate offsets per halo, per field, per tile for MMC real data
  prev_offset=smc_offset
  component_bottom_offset=0
  component_bottom_snd=0
  ! These loops are in stride one in MPI buffer order. This is what makes the MPI buffer tile agnostic
  DO field=1,REAL_FIELDS
    IF(fields(field).GT.1) THEN
      DO halo_depth=1,depth
        DO tile=1,tiles_per_chunk
          IF(chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM).EQ.MPI_TILE) THEN
            component_bottom_offset(tile,field,halo_depth)=prev_offset
            prev_offset=component_bottom_offset(tile,field,halo_depth)+comp_count(tile,halo_depth)
            component_bottom_snd(halo_depth)=component_bottom_snd(halo_depth)+comp_count(tile,halo_depth)
          ENDIF
        ENDDO
      ENDDO
    ENDIF
  ENDDO

END SUBROUTINE clover_calc_bottom_mmc_snd_offsets

SUBROUTINE clover_sum(value)

  ! Only sums to the master

  IMPLICIT NONE

  REAL(KIND=8) :: value

  REAL(KIND=8) :: total

  INTEGER :: err

  total=value

  CALL MPI_REDUCE(value,total,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,err)

  value=total

END SUBROUTINE clover_sum

SUBROUTINE clover_min(value)

  IMPLICIT NONE

  REAL(KIND=8) :: value

  REAL(KIND=8) :: minimum

  INTEGER :: err

  minimum=value

  CALL MPI_ALLREDUCE(value,minimum,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,err)

  value=minimum

END SUBROUTINE clover_min

SUBROUTINE clover_max(value)

  IMPLICIT NONE

  REAL(KIND=8) :: value

  REAL(KIND=8) :: maximum

  INTEGER :: err

  maximum=value

  CALL MPI_ALLREDUCE(value,maximum,1,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD,err)

  value=maximum

END SUBROUTINE clover_max

SUBROUTINE clover_allgather(value,values)

  IMPLICIT NONE

  REAL(KIND=8) :: value

  REAL(KIND=8) :: values(parallel%max_task)

  INTEGER :: err

  values(1)=value ! Just to ensure it will work in serial

  CALL MPI_ALLGATHER(value,1,MPI_DOUBLE_PRECISION,values,1,MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,err)

END SUBROUTINE clover_allgather

SUBROUTINE clover_check_error(error)

  IMPLICIT NONE

  INTEGER :: error

  INTEGER :: maximum

  INTEGER :: err

  maximum=error

  CALL MPI_ALLREDUCE(error,maximum,1,MPI_INTEGER,MPI_MAX,MPI_COMM_WORLD,err)

  error=maximum

END SUBROUTINE clover_check_error

END MODULE clover_module
