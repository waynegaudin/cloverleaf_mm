!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran reset field kernel.
!>  @author Wayne Gaudin
!>  @details Copies all of the final end of step filed data to the begining of
!>  step data, ready for the next timestep.

MODULE reset_field_kernel_module

CONTAINS

SUBROUTINE reset_field_kernel(x_min,x_max,y_min,y_max,      &
                              total_mmcs0,                  &
                              total_components0,            &
                              total_mmcs1,                  &
                              total_components1,            &
                              total_mmcsmax,                &
                              total_componentsmax,          &
                              density0,                     &
                              density1,                     &
                              energy0,                      &
                              energy1,                      &
                              xvel0,                        &
                              xvel1,                        &
                              yvel0,                        &
                              yvel1,                        &
                              material0,                    &
                              material1,                    &
                              mmc_material0,                &
                              mmc_material1,                &
                              mmc_index0,                   &
                              mmc_index1,                   &
                              mmc_j0,                       &
                              mmc_k0,                       &
                              mmc_j1,                       &
                              mmc_k1,                       &
                              mmc_component0,               &
                              mmc_component1,               &
                              mmc_density0,                 &
                              mmc_density1,                 &
                              mmc_energy0,                  &
                              mmc_energy1,                  &
                              mmc_volume0,                  &
                              mmc_volume1                   )


  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)     :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)     :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)     :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)     :: xvel1,yvel1
  INTEGER(KIND=4), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)  :: material0
  INTEGER(KIND=4), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)  :: material1
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_index0
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_index1
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_component0
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_component1
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_j0
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_k0
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_j1
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                  :: mmc_k1
  INTEGER(KIND=4), DIMENSION(1:total_componentsmax)            :: mmc_material0
  INTEGER(KIND=4), DIMENSION(1:total_componentsmax)            :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)               :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)               :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)               :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)               :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)               :: mmc_volume0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)               :: mmc_volume1

  INTEGER :: j,k,mmc,component

  LOGICAL :: multi_material=.FALSE.

  ! Try and avoid MM updates if none on the chunk
  IF (total_mmcs1.NE.0) multi_material=.TRUE.

!$OMP              PARALLEL DO             &
!$OMP              PRIVATE(j,k)            &
!$OMP              COLLAPSE(2)
  DO k=y_min,y_max
    DO j=x_min,x_max
      density0(j,k)=density1(j,k)
      energy0(j,k)=energy1(j,k)
    ENDDO
  ENDDO
!$OMP END                         &
!$OMP PARALLEL DO

!$OMP              PARALLEL DO          &
!$OMP              PRIVATE(j,k)         &
!$OMP              COLLAPSE(2)
  DO k=y_min,y_max+1
    DO j=x_min,x_max+1
      xvel0(j,k)=xvel1(j,k)
      yvel0(j,k)=yvel1(j,k)
    ENDDO
  ENDDO
!$OMP END                         &
!$OMP PARALLEL DO

  IF(multi_material) THEN
!$OMP              PARALLEL DO          &
!$OMP              PRIVATE(j,k)         &
!$OMP              COLLAPSE(2)
    DO k=y_min,y_max
      DO j=x_min,x_max
        material0(j,k)=material1(j,k)
      ENDDO
    ENDDO
!$OMP END                         &
!$OMP PARALLEL DO
!$OMP        PARALLEL DO          &
!$OMP        PRIVATE(mmc)
    DO mmc=1,total_mmcs1
      mmc_index0(mmc)=mmc_index1(mmc)
      mmc_component0(mmc)=mmc_component1(mmc)
      mmc_j0(mmc)=mmc_j1(mmc)
      mmc_k0(mmc)=mmc_k1(mmc)
    ENDDO
!$OMP END                         &
!$OMP PARALLEL DO
!$OMP        PARALLEL DO          &
!$OMP        PRIVATE(mmc)
    DO component=1,total_components1
      mmc_material0(component)=mmc_material1(component)
      mmc_density0(component)=mmc_density1(component)
      mmc_energy0(component)=mmc_energy1(component)
      mmc_volume0(component)=mmc_volume1(component)
    ENDDO
!$OMP END                         &
!$OMP PARALLEL DO
  ENDIF

  total_mmcs0=total_mmcs1
  total_components0=total_components1

END SUBROUTINE reset_field_kernel

END MODULE reset_field_kernel_module
