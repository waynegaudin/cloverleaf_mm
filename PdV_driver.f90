!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief standalone driver for the PdV kernels
!>  @author Wayne Gaudin
!>  @details Calls user requested kernel in standalone mode

PROGRAM PdV_driver

  USE set_data_module
  USE PdV_kernel_module
  USE PdV_kernel_single_mat_module
  USE cell_average_kernel_module

  IMPLICIT NONE

!$ INTEGER :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM

  INTEGER :: numargs,iargc,i
  CHARACTER (LEN=20)  :: command_line,temp

  INTEGER :: x_size,y_size

  REAL(KIND=8) :: kernel_time,timer,PdV_time

  LOGICAL                     :: predict,multi_material
  INTEGER                     :: x_min,x_max,y_min,y_max,its,iteration,mm
  REAL(KIND=8)                :: dt
  REAL(KIND=8),   ALLOCATABLE :: xarea(:,:),yarea(:,:),volume(:,:)
  REAL(KIND=8),   ALLOCATABLE :: celldx(:),celldy(:)
  REAL(KIND=8),   ALLOCATABLE :: density0(:,:),density1(:,:),energy0(:,:),energy1(:,:)
  REAL(KIND=8),   ALLOCATABLE :: mmc_density0(:),mmc_density1(:),mmc_energy0(:),mmc_energy1(:),mmc_volume0(:),mmc_volume1(:)
  REAL(KIND=8),   ALLOCATABLE :: pressure(:,:),soundspeed(:,:),viscosity(:,:)
  REAL(KIND=8),   ALLOCATABLE :: mmc_pressure(:),mmc_viscosity(:),mmc_soundspeed(:)
  REAL(KIND=8),   ALLOCATABLE :: xvel0(:,:),yvel0(:,:),xvel1(:,:),yvel1(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: material0(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_component0(:),mmc_material0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_j0(:),mmc_k0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_index0(:)
  INTEGER(KIND=4)             :: total_mmcs0,total_components0

!$OMP PARALLEL
!$  IF(OMP_GET_THREAD_NUM().EQ.0) THEN
!$    WRITE(*,'(a15,i5)') 'Thread Count: ',OMP_GET_NUM_THREADS()
!$  ENDIF
!$OMP END PARALLEL

  x_size=100
  y_size=100
  its=1
  predict=.TRUE.
  !predict=.FALSE.
  multi_material=.FALSE.
  mm=0

  numargs = iargc()

  DO i=1,numargs,2
    CALL GETARG(i,command_line)
    SELECT CASE (command_line)
      CASE("-help")
        WRITE(*,*) "Usage -nx 100 -ny 100 -its 10i -multimat 0|1"
        STOP
      CASE("-nx")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") x_size
      CASE("-ny")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") y_size
      CASE("-its")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") its
      CASE("-multimat")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") mm
    END SELECT
  ENDDO

  x_min=1
  y_min=1
  x_max=x_size
  y_max=y_size

  IF(mm.NE.0)multi_material=.TRUE.

  WRITE(*,*) "PdV Kernel"
  WRITE(*,*) "Mesh size ",x_size,y_size
  WRITE(*,*) "Iterations ",its
  IF(multi_material) WRITE(*,*)"Multi material"
  IF(.NOT.multi_material) WRITE(*,*)"Single material"

  kernel_time=timer()

  CALL set_data(x_min,x_max,y_min,y_max,            &
                multi_material,                     &
                xarea=xarea,                        &
                yarea=yarea,                        &
                celldx=celldx,                      &
                celldy=celldy,                      &
                volume=volume,                      &
                density0=density0,                  &
                density1=density1,                  &
                energy0=energy0,                    &
                energy1=energy1,                    &
                pressure=pressure,                  &
                soundspeed=soundspeed,              &
                viscosity=viscosity,                &
                xvel0=xvel0,                        &
                xvel1=xvel1,                        &
                yvel0=yvel0,                        &
                yvel1=yvel1,                        &
                material0=material0,                &
                mmc_component0=mmc_component0,      &
                mmc_index0=mmc_index0,              &
                mmc_j0=mmc_j0,                      &
                mmc_k0=mmc_k0,                      &
                mmc_material0=mmc_material0,        &
                mmc_density0=mmc_density0,          &
                mmc_density1=mmc_density1,          &
                mmc_energy0=mmc_energy0,            &
                mmc_energy1=mmc_energy1,            &
                mmc_pressure=mmc_pressure,          &
                mmc_viscosity=mmc_viscosity,        &
                mmc_soundspeed=mmc_soundspeed,      &
                mmc_volume0=mmc_volume0,            &
                mmc_volume1=mmc_volume1,            &
                total_mmcs0=total_mmcs0,            &
                total_components0=total_components0,&
                dt=dt                               )

  WRITE(*,*) "Setup time ",timer()-kernel_time

  WRITE(*,*) "Data initialised"

  kernel_time=timer()

  DO iteration=1,its
!   CALL cell_average_kernel(x_min,x_max,y_min,y_max,                          &
!                            total_mmcs0,                                      &
!                            total_components0,                                &
!                            density0,                                         &
!                            density1,                                         &
!                            energy0,                                          &
!                            energy1,                                          &
!                            pressure,                                         &
!                            volume,                                           &
!                            material0,                                        &
!                            mmc_index0,                                       &
!                            mmc_component0,                                   &
!                            mmc_density0,                                     &
!                            mmc_density1,                                     &
!                            mmc_energy0,                                      &
!                            mmc_energy1,                                      &
!                            mmc_pressure,                                     &
!                            mmc_volume0,                                      &
!                            mmc_volume1                                       )
    CALL PdV_kernel(predict,          &
                    x_min,            &
                    x_max,            &
                    y_min,            &
                    y_max,            &
                    dt,               &
                    total_mmcs0,      &
                    total_components0,&
                    xarea,            &
                    yarea,            &
                    volume ,          &
                    density0,         &
                    density1,         &
                    energy0,          &
                    energy1,          &
                    pressure,         &
                    viscosity,        &
                    xvel0,            &
                    xvel1,            &
                    yvel0,            &
                    yvel1,            &
                    material0,        &
                    mmc_index0,       &
                    mmc_component0,   &
                    mmc_density0,     &
                    mmc_density1,     &
                    mmc_energy0,      &
                    mmc_energy1,      &
                    mmc_pressure,     &
                    mmc_viscosity,    &
                    mmc_volume0,      &
                    mmc_volume1       )
!   CALL cell_average_kernel(x_min,x_max,y_min,y_max,                          &
!                            total_mmcs0,                                      &
!                            total_components0,                                &
!                            density0,                                         &
!                            density1,                                         &
!                            energy0,                                          &
!                            energy1,                                          &
!                            pressure,                                         &
!                            volume,                                           &
!                            material0,                                        &
!                            mmc_index0,                                       &
!                            mmc_component0,                                   &
!                            mmc_density0,                                     &
!                            mmc_density1,                                     &
!                            mmc_energy0,                                      &
!                            mmc_energy1,                                      &
!                            mmc_pressure,                                     &
!                            mmc_volume0,                                      &
!                            mmc_volume1                                       )
  ENDDO

  PdV_time=(timer()-kernel_time)

  WRITE(*,*)
  WRITE(*,*) "Multi material PdV time  ",PdV_time 
  WRITE(*,*) "Density                  ",SUM(density1)
  WRITE(*,*) "Energy                   ",SUM(energy1)

  kernel_time=timer()

  DO iteration=1,its

    CALL PdV_kernel_single_mat(predict,          &
                               x_min,            &
                               x_max,            &
                               y_min,            &
                               y_max,            &
                               dt,               &
                               xarea,            &
                               yarea,            &
                               volume ,          &
                               density0,         &
                               density1,         &
                               energy0,          &
                               energy1,          &
                               pressure,         &
                               viscosity,        &
                               xvel0,            &
                               xvel1,            &
                               yvel0,            &
                               yvel1             )

  ENDDO

  PdV_time=(timer()-kernel_time)

  WRITE(*,*)
  WRITE(*,*) "Single material PdV time ",PdV_time 
  WRITE(*,*) "Density                  ",SUM(density1)
  WRITE(*,*) "Energy                   ",SUM(energy1)

  DEALLOCATE(xarea)
  DEALLOCATE(yarea)
  DEALLOCATE(celldx)
  DEALLOCATE(celldy)
  DEALLOCATE(volume)
  DEALLOCATE(density0)
  DEALLOCATE(density1)
  DEALLOCATE(energy0)
  DEALLOCATE(energy1)
  DEALLOCATE(pressure)
  DEALLOCATE(soundspeed)
  DEALLOCATE(viscosity)
  DEALLOCATE(xvel0)
  DEALLOCATE(yvel0)
  DEALLOCATE(xvel1)
  DEALLOCATE(yvel1)
  DEALLOCATE(material0)
  DEALLOCATE(mmc_component0)
  DEALLOCATE(mmc_index0)
  DEALLOCATE(mmc_material0)
  DEALLOCATE(mmc_density0)
  DEALLOCATE(mmc_density1)
  DEALLOCATE(mmc_energy0)
  DEALLOCATE(mmc_energy1)
  DEALLOCATE(mmc_pressure)
  DEALLOCATE(mmc_viscosity)
  DEALLOCATE(mmc_soundspeed)

END PROGRAM PdV_driver
