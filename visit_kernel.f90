!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Generates graphics output files.
!>  @author Wayne Gaudin
!>  @details The field data over all mesh chunks is written to a .vtk files and
!>  the .visit file is written that defines the time for each set of vtk files.
!>  The ideal gas and viscosity routines are invoked to make sure this data is
!>  up to data with the current energy, density and velocity.

SUBROUTINE visit_kernel(step,                             &
                        x_min,                            &
                        x_max,                            &
                        y_min,                            &
                        y_max,                            &
                        vertexx,                          &
                        vertexy,                          &
                        material,                         &
                        density,                          &
                        energy,                           &
                        pressure,                         &
                        viscosity,                        &
                        xvel,                             &
                        yvel                              )

  IMPLICIT NONE

  INTEGER :: step
  INTEGER :: x_min,x_max,y_min,y_max
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3)                     :: vertexx
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3)                     :: vertexy
  INTEGER(KIND=4), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)  :: material
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)     :: density
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)     :: energy
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)     :: pressure
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)     :: viscosity
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)     :: xvel
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)     :: yvel

  INTEGER :: j,k,c,err,u,dummy
  INTEGER :: nxc,nyc,nxv,nyv,nblocks
  REAL(KIND=8)    :: temp_var

  CHARACTER(len=80)           :: name
  CHARACTER(len=10)           :: chunk_name,tile_name,step_name
  CHARACTER(len=90)           :: filename

  LOGICAL, SAVE :: first_call=.TRUE.

  name = 'clover'

  IF(first_call) THEN

    nblocks=1
    filename = "clover.visit"
    u=201
    OPEN(UNIT=u,FILE=filename,STATUS='UNKNOWN',IOSTAT=err)
    WRITE(u,'(a,i5)')'!NBLOCKS ',nblocks
    CLOSE(u)

    first_call=.FALSE.

  ENDIF

  filename = "clover.visit"
  u=202
  OPEN(UNIT=u,FILE=filename,STATUS='UNKNOWN',POSITION='APPEND',IOSTAT=err)
  WRITE(chunk_name, '(i6)') 100000
  chunk_name(1:1) = "."
  WRITE(tile_name, '(i6)') 100000
  tile_name(1:1) = "."
  WRITE(step_name, '(i6)') step+100000
  step_name(1:1) = "."
  filename = trim(trim(name)//trim(chunk_name)//trim(tile_name)//trim(step_name))//".vtk"
  WRITE(u,'(a)')TRIM(filename)
  CLOSE(u)

  nxc=x_max-x_min+1
  nyc=y_max-y_min+1
  nxv=nxc+1
  nyv=nyc+1
  
  WRITE(chunk_name, '(i6)') 100000
  chunk_name(1:1) = "."
  WRITE(tile_name, '(i6)') 100000
  tile_name(1:1) = "."
  WRITE(step_name, '(i6)') step+100000
  step_name(1:1) = "."
  filename = trim(trim(name) //trim(chunk_name)//trim(tile_name)//trim(step_name))//".vtk"
  u=203
  OPEN(UNIT=u,FILE=filename,STATUS='UNKNOWN',IOSTAT=err)
  WRITE(u,'(a)')'# vtk DataFile Version 3.0'
  WRITE(u,'(a)')'vtk output'
  WRITE(u,'(a)')'ASCII'
  WRITE(u,'(a)')'DATASET RECTILINEAR_GRID'
  WRITE(u,'(a,2i12,a)')'DIMENSIONS',nxv,nyv,' 1'
  WRITE(u,'(a,i5,a)')'X_COORDINATES ',nxv,' double'
  DO j=x_min,x_max+1
    WRITE(u,'(e12.4)')vertexx(j)
  ENDDO
  WRITE(u,'(a,i5,a)')'Y_COORDINATES ',nyv,' double'
  DO k=y_min,y_max+1
    WRITE(u,'(e12.4)')vertexy(k)
  ENDDO
  WRITE(u,'(a)')'Z_COORDINATES 1 double'
  WRITE(u,'(a)')'0'
  WRITE(u,'(a,i20)')'CELL_DATA ',nxc*nyc
  WRITE(u,'(a)')'FIELD FieldData 5'
  WRITE(u,'(a,i20,a)')'material 1 ',nxc*nyc,' double'
  ! Add this an average value of the materials in the cell
  DO k=y_min,y_max
    WRITE(u,'(i12)')(material(j,k),j=x_min,x_max)
  ENDDO
  WRITE(u,'(a,i20,a)')'density 1 ',nxc*nyc,' double'
  DO k=y_min,y_max
    WRITE(u,'(e12.4)')(density(j,k),j=x_min,x_max)
  ENDDO
  WRITE(u,'(a,i20,a)')'energy 1 ',nxc*nyc,' double'
  DO k=y_min,y_max
    WRITE(u,'(e12.4)')(energy(j,k),j=x_min,x_max)
  ENDDO
  WRITE(u,'(a,i20,a)')'pressure 1 ',nxc*nyc,' double'
  DO k=y_min,y_max
    WRITE(u,'(e12.4)')(pressure(j,k),j=x_min,x_max)
  ENDDO
  WRITE(u,'(a,i20,a)')'viscosity 1 ',nxc*nyc,' double'
  DO k=y_min,y_max
    DO j=x_min,x_max
      temp_var=0.0
      IF(viscosity(j,k).GT.0.00000001) temp_var=viscosity(j,k)
      WRITE(u,'(e12.4)') temp_var
    ENDDO
  ENDDO
  WRITE(u,'(a,i20)')'POINT_DATA ',nxv*nyv
  WRITE(u,'(a)')'FIELD FieldData 3'
  WRITE(u,'(a,i20,a)')'x_vel 1 ',nxv*nyv,' double'
  DO k=y_min,y_max+1
    DO j=x_min,x_max+1
      temp_var=0.0
      IF(ABS(xvel(j,k)).GT.0.00000001) temp_var=xvel(j,k)
      WRITE(u,'(e12.4)') temp_var
    ENDDO
  ENDDO
  WRITE(u,'(a,i20,a)')'y_vel 1 ',nxv*nyv,' double'
  DO k=y_min,y_max+1
    DO j=x_min,x_max+1
      temp_var=0.0
      IF(ABS(yvel(j,k)).GT.0.00000001) temp_var=yvel(j,k)
      WRITE(u,'(e12.4)') temp_var
    ENDDO
  ENDDO
  WRITE(u,'(a,i20,a)')'speed 1 ',nxv*nyv,' double'
  DO k=y_min,y_max+1
    DO j=x_min,x_max+1
      temp_var=0.0
      IF(SQRT(yvel(j,k)**2.0_8+xvel(j,k)**2.0_8).GT.0.00000001) temp_var=SQRT(yvel(j,k)**2.0_8+xvel(j,k)**2.0_8)
      WRITE(u,'(e12.4)') temp_var
    ENDDO
  ENDDO
  CLOSE(u)

END SUBROUTINE visit_kernel
