/*Crown Copyright 2012 AWE.
*
* This file is part of CloverLeaf.
*
* CloverLeaf is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or (at your option)
* any later version.
*
* CloverLeaf is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* CloverLeaf. If not, see http://www.gnu.org/licenses/. */

/**
 *  @brief C revert kernel.
 *  @author Wayne Gaudin
 *  @details Takes the half step field data used in the predictor and reverts
 *  it to the start of step data, ready for the corrector.
 *  Note that this does not seem necessary in this proxy-app but should be
 *  left in to remain relevant to the full method.
 */

#include <stdio.h>
#include <stdlib.h>
#include "ftocmacros.h"
#include <math.h>

void revert_kernel_c_(int *xmin,int *xmax,int *ymin,int *ymax,
                      int *total_components0, int *total_componentsmax,
                      double *density0,
                      double *density1,
                      double *energy0,
                      double *energy1,
                      double *mmc_density0,
                      double *mmc_density1,
                      double *mmc_energy0,
                      double *mmc_energy1)

{
  int x_min=*xmin;
  int x_max=*xmax;
  int y_min=*ymin;
  int y_max=*ymax;
  int total_components=*total_components0;
  int total_componentsmx=*total_componentsmax;

  int j,k,component;
  
#pragma omp target enter data map(to:density0[0:(x_max+2)*(y_max+2)] \
                         ,density1[0:(x_max+2)*(y_max+2)]        \
                         ,energy0[0:(x_max+2)*(y_max+2)]         \
                         ,energy1[0:(x_max+2)*(y_max+2)]          \
                         ,mmc_density0[0:total_componentsmx]     \
                         ,mmc_density1[0:total_componentsmx]     \
                         ,mmc_energy0[0:total_componentsmx]      \
                         ,mmc_energy1[0:total_componentsmx])

#pragma omp target teams distribute parallel for private(j,k) collapse(2)
  for (k=y_min;k<=y_max;k++) {
#pragma ivdep
    for (j=x_min;j<=x_max;j++) {
      density1[FTNREF2D(j  ,k  ,x_max+4,x_min-2,y_min-2)]=density0[FTNREF2D(j  ,k  ,x_max+4,x_min-2,y_min-2)];
      energy1[FTNREF2D(j  ,k  ,x_max+4,x_min-2,y_min-2)]=energy0[FTNREF2D(j  ,k  ,x_max+4,x_min-2,y_min-2)];
    }
  }

#pragma omp target teams distribute parallel for private(component) 
  for (component=1;component<=total_components;component++) {
    mmc_density1[FTNREF1D(component,1)]=mmc_density0[FTNREF1D(component,1)];
    mmc_energy1[FTNREF1D(component,1)]=mmc_energy0[FTNREF1D(component,1)];
  }

#pragma omp target exit data map (from: density0[0:(x_max+2)*(y_max+2)] \
                         ,density1[0:(x_max+2)*(y_max+2)]        \
                         ,energy0[0:(x_max+2)*(y_max+2)]         \
                         ,energy1[0:(x_max+2)*(y_max+2)]          \
                         ,mmc_density0[0:total_componentsmx]     \
                         ,mmc_density1[0:total_componentsmx]     \
                         ,mmc_energy0[0:total_componentsmx]      \
                         ,mmc_energy1[0:total_componentsmx])

}

