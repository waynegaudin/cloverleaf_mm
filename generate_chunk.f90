!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Mesh chunk generation driver
!>  @author Wayne Gaudin
!>  @details Invoked the users specified chunk generator.

SUBROUTINE generate_chunk(tile)

  USE clover_module
  USE generate_chunk_kernel_module

  IMPLICIT NONE

  INTEGER         :: tile,j,k

  INTEGER         :: state,mat
  REAL(KIND=8), DIMENSION(number_of_states) :: state_density,state_energy,state_xvel,state_yvel
  REAL(KIND=8), DIMENSION(number_of_states) :: state_xmin,state_xmax,state_ymin,state_ymax,state_radius
  INTEGER,      DIMENSION(number_of_states) :: state_geometry,state_material
  INTEGER,ALLOCATABLE, DIMENSION(:)         :: material
  LOGICAL,ALLOCATABLE, DIMENSION(:)         :: unique_material

  LOGICAL :: auto_on

  auto_on=.FALSE.

  ALLOCATE(unique_material(MAXVAL(states(:)%material)))
  ALLOCATE(material(MAXVAL(states(:)%material)))

  DO state=1,number_of_states 
   state_density(state)=states(state)%density
   state_energy(state)=states(state)%energy
   state_xvel(state)=states(state)%xvel
   state_yvel(state)=states(state)%yvel
   state_xmin(state)=states(state)%xmin
   state_xmax(state)=states(state)%xmax
   state_ymin(state)=states(state)%ymin
   state_ymax(state)=states(state)%ymax
   state_radius(state)=states(state)%radius
   state_material(state)=states(state)%material
   state_geometry(state)=states(state)%geometry
   material(states(state)%material)=states(state)%materiaL
  ENDDO

  ! Generate driver kernel test problem, ignoring other input data
  IF(test_problem.EQ.-1) auto_on=.TRUE.

  CALL generate_chunk_kernel(chunk%tiles(tile)%t_xmin,                    &
                             chunk%tiles(tile)%t_xmax,                    &
                             chunk%tiles(tile)%t_ymin,                    &
                             chunk%tiles(tile)%t_ymax,                    &
                             chunk%tiles(tile)%field%vertexx,             &
                             chunk%tiles(tile)%field%vertexy,             &
                             chunk%tiles(tile)%field%cellx,               &
                             chunk%tiles(tile)%field%celly,               &
                             chunk%tiles(tile)%field%density0,            &
                             chunk%tiles(tile)%field%energy0,             &
                             chunk%tiles(tile)%field%material0,           &
                             chunk%tiles(tile)%field%material1,           &
                             chunk%tiles(tile)%field%xvel0,               &
                             chunk%tiles(tile)%field%yvel0,               &
                             number_of_states,                            &
                             state_density,                               &
                             state_energy,                                &
                             state_material,                              &
                             state_xvel,                                  &
                             state_yvel,                                  &
                             state_xmin,                                  &
                             state_xmax,                                  &
                             state_ymin,                                  &
                             state_ymax,                                  &
                             state_radius,                                &
                             state_geometry,                              &
                             g_rect,                                      &
                             g_circ,                                      &
                             g_point,                                     &
                             auto_on,                                     &
                             chunk%tiles(tile)%field%total_mmcs0,         &
                             chunk%tiles(tile)%field%total_components0,   &
                             chunk%tiles(tile)%field%total_mmcs1,         &
                             chunk%tiles(tile)%field%total_components1,   &
                             chunk%tiles(tile)%field%mmc_volume0,         &
                             chunk%tiles(tile)%field%mmc_volume1,         &
                             chunk%tiles(tile)%field%mmc_density0,        &
                             chunk%tiles(tile)%field%mmc_density1,        &
                             chunk%tiles(tile)%field%mmc_energy0,         &
                             chunk%tiles(tile)%field%mmc_energy1,         &
                             chunk%tiles(tile)%field%mmc_j0,              &
                             chunk%tiles(tile)%field%mmc_k0,              &
                             chunk%tiles(tile)%field%mmc_j1,              &
                             chunk%tiles(tile)%field%mmc_k1,              &
                             chunk%tiles(tile)%field%mmc_component0,      &
                             chunk%tiles(tile)%field%mmc_material0,       &
                             chunk%tiles(tile)%field%mmc_index0,          &
                             chunk%tiles(tile)%field%mmc_component1,      &
                             chunk%tiles(tile)%field%mmc_material1,       &
                             chunk%tiles(tile)%field%mmc_index1           )


  ! This should be done after a halo exchange to make sure all possible materials are included
  ! It should also include MMC data. But none at this point.

  unique_material=.FALSE.
  IF(.NOT.auto_on) THEN
    DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+2
      DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+2
        unique_material(chunk%tiles(tile)%field%material0(j,k))=.TRUE.
      ENDDO
    ENDDO
  ELSE
    chunk%tiles(tile)%field%max_mats=2
    unique_material(1:2)=.TRUE.
    chunk%tiles(tile)%field%total_components_halo=0
    chunk%tiles(tile)%field%total_mmcs_halo=0
  ENDIF

  chunk%tiles(tile)%field%max_mats=COUNT(unique_material)

  ! max_mats changes after each advection so these arrays should be updated
  ! In fact they should be deallocated if possible
  ! Also, once MMCs exist, they need to be check as well
  ! And it also should include internal halo cells after an exchange takes place

  ! These arrays should be above the tile level so they are used in loops over tiles. In this case
  ! they need to be over number of threads as well
  ALLOCATE(chunk%tiles(tile)%field%dv(chunk%tiles(tile)%field%max_mats,                      &
                                      chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                      chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
  ALLOCATE(chunk%tiles(tile)%field%dm(chunk%tiles(tile)%field%max_mats,                      &
                                      chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                      chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
  ALLOCATE(chunk%tiles(tile)%field%de(chunk%tiles(tile)%field%max_mats,                      &
                                      chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                      chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
  ALLOCATE(chunk%tiles(tile)%field%mat_list(chunk%tiles(tile)%field%max_mats))
  chunk%tiles(tile)%field%mat_list=PACK(material,unique_material)
  chunk%tiles(tile)%field%max_mat_number=MAXVAL(chunk%tiles(tile)%field%mat_list)
  ALLOCATE(chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%max_mat_number))
  chunk%tiles(tile)%field%reverse_mat_list=0
  DO mat=1,chunk%tiles(tile)%field%max_mats
    chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%mat_list(mat))=mat
  ENDDO
  DEALLOCATE(unique_material)
  DEALLOCATE(material)

END SUBROUTINE generate_chunk
