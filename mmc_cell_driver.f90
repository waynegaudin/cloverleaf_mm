!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief standalone driver for the cell multi-material advection kernels
!>  @author Wayne Gaudin
!>  @details Calls user requested kernel in standalone mode

PROGRAM mmc_cell_driver

  USE set_data_module
  USE advec_mmc_x_cell_module
  USE advec_mmc_y_cell_module
  USE advec_cell_kernel_module
  USE mmc_pack_kernel_module

  IMPLICIT NONE

!$ INTEGER :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM

  INTEGER :: numargs,iargc,i
  CHARACTER (LEN=20)  :: command_line,temp

  INTEGER :: x_size,y_size,mm
  LOGICAL :: multi_material

  LOGICAL :: reallocation_needed

  REAL(KIND=8) :: kernel_time,timer,cell_time

  INTEGER :: x_min,x_max,y_min,y_max,its,iteration,direction,sweep_number
  REAL(KIND=8) :: dt
  REAL(KIND=8),ALLOCATABLE :: vertexdx(:),vertexdy(:)
  REAL(KIND=8),ALLOCATABLE :: celldx(:),celldy(:)
  REAL(KIND=8),ALLOCATABLE :: xarea(:,:),yarea(:,:)
  REAL(KIND=8),ALLOCATABLE :: volume(:,:)
  REAL(KIND=8),ALLOCATABLE :: density0(:,:),energy0(:,:)
  REAL(KIND=8),ALLOCATABLE :: density1(:,:),energy1(:,:)
  REAL(KIND=8),ALLOCATABLE :: densitynew(:,:),energynew(:,:)
  REAL(KIND=8),ALLOCATABLE :: pressure(:,:),soundspeed(:,:)
  REAL(KIND=8),ALLOCATABLE :: mmc_density0(:),mmc_energy0(:),mmc_volume0(:)
  REAL(KIND=8),ALLOCATABLE :: mmc_density1(:),mmc_energy1(:),mmc_volume1(:)
  REAL(KIND=8),ALLOCATABLE :: mmc_densitynew(:),mmc_energynew(:),mmc_volumenew(:)
  REAL(KIND=8),ALLOCATABLE :: mmc_pressure(:),mmc_soundspeed(:)
  REAL(KIND=8),ALLOCATABLE :: xvel0(:,:),yvel0(:,:),xvel1(:,:),yvel1(:,:)
  REAL(KIND=8),ALLOCATABLE :: vol_flux_x(:,:),vol_flux_y(:,:),mass_flux_x(:,:),mass_flux_y(:,:)
  REAL(KIND=8),ALLOCATABLE :: pre_vol(:,:),post_vol(:,:)
  REAL(KIND=8),ALLOCATABLE :: ener_flux(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: material0(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: material1(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: materialnew(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_component0(:),mmc_material0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_component1(:),mmc_material1(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_componentnew(:),mmc_materialnew(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_j0(:),mmc_k0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_j1(:),mmc_k1(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_jnew(:),mmc_knew(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_index0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_index1(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_indexnew(:)
  INTEGER(KIND=4)             :: total_mmcs0,total_components0
  INTEGER(KIND=4)             :: total_mmcs1,total_components1
  INTEGER(KIND=4)             :: total_mmcsnew,total_componentsnew
  INTEGER(KIND=4)             :: total_mmcsmax,total_componentsmax
  INTEGER(KIND=4)             :: max_mats,max_comps
  INTEGER(KIND=4)             :: x_first

!$OMP PARALLEL
!$  IF(OMP_GET_THREAD_NUM().EQ.0) THEN
!$    WRITE(*,'(a15,i5)') 'Thread Count: ',OMP_GET_NUM_THREADS()
!$  ENDIF
!$OMP END PARALLEL

  x_size=100
  y_size=100
  its=1
  multi_material=.FALSE.
  mm=0

  numargs = iargc()

  DO i=1,numargs,2
    CALL GETARG(i,command_line)
    SELECT CASE (command_line)
      CASE("-help")
        WRITE(*,*) "Usage -nx 100 -ny 100 -its 10 -multimat 0|1"
        STOP
      CASE("-nx")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") x_size
      CASE("-ny")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") y_size
      CASE("-its")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") its
      CASE("-multimat")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") mm
    END SELECT
  ENDDO

  x_min=1
  y_min=1
  x_max=x_size
  y_max=y_size

  IF(mm.NE.0) THEN
    multi_material=.TRUE.
    max_mats=2
  ELSE
    max_mats=1
  ENDIF

  WRITE(*,*) "Advec MMC Cell Kernel"
  WRITE(*,*) "Mesh size ",x_size,y_size
  WRITE(*,*) "Iterations ",its
  IF(multi_material) WRITE(*,*)"Multi material"
  IF(.NOT.multi_material) WRITE(*,*)"Single material"

  kernel_time=timer()

  CALL set_data(x_min,x_max,y_min,y_max,                &
                multi_material,                         &
                vertexdx=vertexdx,                      &
                vertexdy=vertexdy,                      &
                celldx=celldx,                          &
                celldy=celldy,                          &
                xarea=xarea,                            &
                yarea=yarea,                            &
                volume=volume,                          &
                energy0=energy0,                        &
                energy1=energy1,                        &
                energynew=energynew,                    &
                density0=density0,                      &
                density1=density1,                      &
                densitynew=densitynew,                  &
                pressure=pressure,                      &
                soundspeed=soundspeed,                  &
                xvel0=xvel0,                            &
                xvel1=xvel1,                            &
                yvel0=yvel0,                            &
                yvel1=yvel1,                            &
                vol_flux_x=vol_flux_x,                  &
                vol_flux_y=vol_flux_y,                  &
                mass_flux_x=mass_flux_x,                &
                mass_flux_y=mass_flux_y,                &
                work_array1=pre_vol,                    &
                work_array2=post_vol,                   &
                work_array7=ener_flux,                  &
                material0=material0,                    &
                material1=material1,                    &
                materialnew=materialnew,                &
                mmc_component0=mmc_component0,          &
                mmc_component1=mmc_component1,          &
                mmc_componentnew=mmc_componentnew,      &
                mmc_index0=mmc_index0,                  &
                mmc_index1=mmc_index1,                  &
                mmc_indexnew=mmc_indexnew,              &
                mmc_j0=mmc_j0,                          &
                mmc_k0=mmc_k0,                          &
                mmc_j1=mmc_j1,                          &
                mmc_k1=mmc_k1,                          &
                mmc_jnew=mmc_jnew,                      &
                mmc_knew=mmc_knew,                      &
                mmc_material0=mmc_material0,            &
                mmc_material1=mmc_material1,            &
                mmc_materialnew=mmc_materialnew,        &
                mmc_density0=mmc_density0,              &
                mmc_density1=mmc_density1,              &
                mmc_densitynew=mmc_densitynew,          &
                mmc_energy0=mmc_energy0,                &
                mmc_energy1=mmc_energy1,                &
                mmc_energynew=mmc_energynew,            &
                mmc_pressure=mmc_pressure,              &
                mmc_soundspeed=mmc_soundspeed,          &
                mmc_volume0=mmc_volume0,                &
                mmc_volume1=mmc_volume1,                &
                mmc_volumenew=mmc_volumenew,            &
                total_mmcs0=total_mmcs0,                &
                total_components0=total_components0,    &
                total_mmcs1=total_mmcs1,                &
                total_components1=total_components1,    &
                total_mmcsmax=total_mmcsmax,            &
                total_componentsmax=total_componentsmax,&
                dt=dt                                   )

  WRITE(*,*) "Setup time ",timer()-kernel_time

  WRITE(*,*) "Data initialised"

  !Bodge advection problem
  !material1=1
  !material1(x_min+3:x_min+3,y_min+3:y_min+3)=2
  !energy1=1.0_8
  !energy1(x_min+3:x_min+3,y_min+3:y_min+3)=2.0_8
  !density1=1.0_8
  !density1(x_min+3:x_min+3,y_min+3:y_min+3)=2.0_8
  vol_flux_x=0.1_8
  vol_flux_y=0.1_8
!
  !material0=material1
  !energy0=energy1
  !density0=density1
  !mmc_material1=0
  !mmc_index1=0
  !mmc_component1=0
  !mmc_density1=0.0
  !mmc_energy1=0.0
  !mmc_volume1=0.0
  !mmc_j1=0
  !mmc_k1=0
  !total_mmcs1=0
  !total_components1=0

  kernel_time=timer()

  WRITE(*,*) "Density                         ",SUM(density1)
  WRITE(*,*) "Energy                          ",SUM(energy1)
write(0,*)"total_mmcs1 ",total_mmcs1
write(0,*)"total_components1 ",total_components1
  DO iteration=1,its
    x_first=MOD(iteration,2)
    WRITE(0,*)"Iteration count ", iteration, x_first

    IF( x_first.EQ.1) THEN
      WRITE(0,*)"X advection"
      direction=1
      sweep_number=1
      CALL advec_mmc_x_cell_kernel(x_min,x_max,y_min,y_max,    &
                                   direction,                  &
                                   sweep_number,               &
                                   max_mats,                   &
                                   total_mmcs1,                &
                                   total_components1,          &
                                   total_mmcsmax,              &
                                   total_componentsmax,        &
                                   total_mmcsnew,              &
                                   total_componentsnew,        &
                                   max_comps,                  &
                                   celldx,                     &
                                   celldy,                     &
                                   vertexdx,                   &
                                   vertexdy,                   &
                                   volume,                     &
                                   density1,                   &
                                   densitynew,                 &
                                   energy1,                    &
                                   energynew,                  &
                                   mass_flux_x,                &
                                   vol_flux_x,                 &
                                   mass_flux_y,                &
                                   vol_flux_y,                 &
                                   pre_vol,                    &
                                   post_vol,                   &
                                   ener_flux,                  &
                                   material1,                  &
                                   materialnew,                &
                                   mmc_material1,              &
                                   mmc_materialnew,            &
                                   mmc_index1,                 &
                                   mmc_indexnew,               &
                                   mmc_j1,                     &
                                   mmc_k1,                     &
                                   mmc_jnew,                   &
                                   mmc_knew,                   &
                                   mmc_component1,             &
                                   mmc_componentnew,           &
                                   mmc_density1,               &
                                   mmc_densitynew,             &
                                   mmc_energy1,                &
                                   mmc_energynew,              &
                                   mmc_volume1,                &
                                   mmc_volumenew,              &
                                   reallocation_needed         )
 write(0,*)"total_mmcs1 ",total_mmcsnew
 write(0,*)"total_components1 ",total_componentsnew
      IF(reallocation_needed) THEN
        ! Allocate new arrays
        DEALLOCATE(mmc_jnew)
        DEALLOCATE(mmc_knew)
        DEALLOCATE(mmc_indexnew)
        DEALLOCATE(mmc_componentnew)
        DEALLOCATE(mmc_volumenew)
        DEALLOCATE(mmc_materialnew)
        DEALLOCATE(mmc_densitynew)
        DEALLOCATE(mmc_energynew)
        DEALLOCATE(mmc_pressure)
        DEALLOCATE(mmc_soundspeed)
        ALLOCATE(mmc_jnew(1:total_mmcsmax))
        ALLOCATE(mmc_knew(1:total_mmcsmax))
        ALLOCATE(mmc_indexnew(1:total_mmcsmax))
        ALLOCATE(mmc_componentnew(1:total_mmcsmax))
        ALLOCATE(mmc_volumenew(1:total_componentsmax))
        ALLOCATE(mmc_materialnew(1:total_componentsmax))
        ALLOCATE(mmc_densitynew(1:total_componentsmax))
        ALLOCATE(mmc_energynew(1:total_componentsmax))
        ALLOCATE(mmc_pressure(1:total_componentsmax))
        ALLOCATE(mmc_soundspeed(1:total_componentsmax))
        ! Copy data
        mmc_jnew(1:total_mmcsnew)=mmc_j1(1:total_mmcsnew)
        mmc_knew(1:total_mmcsnew)=mmc_k1(1:total_mmcsnew)
        mmc_indexnew(1:total_mmcsnew)=mmc_index1(1:total_mmcsnew)
        mmc_componentnew(1:total_mmcsnew)=mmc_component1(1:total_mmcsnew)
        mmc_volumenew(1:total_componentsnew)=mmc_volume1(1:total_componentsnew)
        mmc_materialnew(1:total_componentsnew)=mmc_material1(1:total_componentsnew)
        mmc_densitynew(1:total_componentsnew)=mmc_density1(1:total_componentsnew)
        mmc_energynew(1:total_componentsnew)=mmc_energy1(1:total_componentsnew)
        ! Deallocate old arrays and allocate with new storage limits
        DEALLOCATE(mmc_j1)
        DEALLOCATE(mmc_k1)
        DEALLOCATE(mmc_index1)
        DEALLOCATE(mmc_component1)
        DEALLOCATE(mmc_volume1)
        DEALLOCATE(mmc_material1)
        DEALLOCATE(mmc_density1)
        DEALLOCATE(mmc_energy1)
        ALLOCATE(mmc_j1(total_mmcsmax))
        ALLOCATE(mmc_k1(total_mmcsmax))
        ALLOCATE(mmc_index1(total_mmcsmax))
        ALLOCATE(mmc_component1(total_mmcsmax))
        ALLOCATE(mmc_volume1(1:total_componentsmax))
        ALLOCATE(mmc_material1(1:total_componentsmax))
        ALLOCATE(mmc_density1(1:total_componentsmax))
        ALLOCATE(mmc_energy1(1:total_componentsmax))
      ENDIF
      direction=2
      sweep_number=2
      WRITE(0,*)"Y advection"
!   Note that the 2nd sweep uses *new as the initial data and *1 and as the final data, to prevent excessive copies
      CALL advec_mmc_y_cell_kernel(x_min,x_max,y_min,y_max,    &
                                   direction,                  &
                                   sweep_number,               &
                                   max_mats,                   &
                                   total_mmcs1,                &
                                   total_components1,          &
                                   total_mmcsmax,              &
                                   total_componentsmax,        &
                                   total_mmcsnew,              &
                                   total_componentsnew,        &
                                   max_comps,                  &
                                   celldx,                     &
                                   celldy,                     &
                                   vertexdx,                   &
                                   vertexdy,                   &
                                   volume,                     &
                                   densitynew,                 &
                                   density1,                   &
                                   energynew,                  &
                                   energy1,                    &
                                   mass_flux_x,                &
                                   vol_flux_x,                 &
                                   mass_flux_y,                &
                                   vol_flux_y,                 &
                                   pre_vol,                    &
                                   post_vol,                   &
                                   ener_flux,                  &
                                   materialnew,                &
                                   material1,                  &
                                   mmc_materialnew,            &
                                   mmc_material1,              &
                                   mmc_indexnew,               &
                                   mmc_index1,                 &
                                   mmc_jnew,                   &
                                   mmc_knew,                   &
                                   mmc_j1,                     &
                                   mmc_k1,                     &
                                   mmc_componentnew,           &
                                   mmc_component1,             &
                                   mmc_densitynew,             &
                                   mmc_density1,               &
                                   mmc_energynew,              &
                                   mmc_energy1,                &
                                   mmc_volumenew,              &
                                   mmc_volume1,                &
                                   reallocation_needed         )
write(0,*)"total_mmcs1 ",total_mmcsnew
write(0,*)"total_components1 ",total_componentsnew
      IF(reallocation_needed) THEN
        ! Allocate new arrays
        DEALLOCATE(mmc_j1)
        DEALLOCATE(mmc_k1)
        DEALLOCATE(mmc_index1)
        DEALLOCATE(mmc_component1)
        DEALLOCATE(mmc_volume1)
        DEALLOCATE(mmc_material1)
        DEALLOCATE(mmc_density1)
        DEALLOCATE(mmc_energy1)
        DEALLOCATE(mmc_pressure)
        DEALLOCATE(mmc_soundspeed)
        ALLOCATE(mmc_j1(1:total_mmcsmax))
        ALLOCATE(mmc_k1(1:total_mmcsmax))
        ALLOCATE(mmc_index1(1:total_mmcsmax))
        ALLOCATE(mmc_component1(1:total_mmcsmax))
        ALLOCATE(mmc_volume1(1:total_componentsmax))
        ALLOCATE(mmc_material1(1:total_componentsmax))
        ALLOCATE(mmc_density1(1:total_componentsmax))
        ALLOCATE(mmc_energy1(1:total_componentsmax))
        ALLOCATE(mmc_pressure(1:total_componentsmax))
        ALLOCATE(mmc_soundspeed(1:total_componentsmax))
        ! Copy data
        mmc_j1(1:total_mmcsnew)=mmc_jnew(1:total_mmcsnew)
        mmc_k1(1:total_mmcsnew)=mmc_knew(1:total_mmcsnew)
        mmc_index1(1:total_mmcsnew)=mmc_indexnew(1:total_mmcsnew)
        mmc_component1(1:total_mmcsnew)=mmc_componentnew(1:total_mmcsnew)
        mmc_volume1(1:total_componentsnew)=mmc_volumenew(1:total_componentsnew)
        mmc_material1(1:total_componentsnew)=mmc_materialnew(1:total_componentsnew)
        mmc_density1(1:total_componentsnew)=mmc_densitynew(1:total_componentsnew)
        mmc_energy1(1:total_componentsnew)=mmc_energynew(1:total_componentsnew)
        ! Deallocate old arrays and allocate with new storage limits
        DEALLOCATE(mmc_jnew)
        DEALLOCATE(mmc_knew)
        DEALLOCATE(mmc_indexnew)
        DEALLOCATE(mmc_componentnew)
        DEALLOCATE(mmc_materialnew)
        DEALLOCATE(mmc_volumenew)
        DEALLOCATE(mmc_densitynew)
        DEALLOCATE(mmc_energynew)
        ALLOCATE(mmc_jnew(total_mmcsmax))
        ALLOCATE(mmc_knew(total_mmcsmax))
        ALLOCATE(mmc_indexnew(total_mmcsmax))
        ALLOCATE(mmc_componentnew(total_mmcsmax))
        ALLOCATE(mmc_volumenew(1:total_componentsmax))
        ALLOCATE(mmc_materialnew(1:total_componentsmax))
        ALLOCATE(mmc_densitynew(1:total_componentsmax))
        ALLOCATE(mmc_energynew(1:total_componentsmax))
      ENDIF
    ELSE
      WRITE(0,*)"Y advection"
      direction=2
      sweep_number=1
      CALL advec_mmc_y_cell_kernel(x_min,x_max,y_min,y_max,    &
                                   direction,                  &
                                   sweep_number,               &
                                   max_mats,                   &
                                   total_mmcs1,                &
                                   total_components1,          &
                                   total_mmcsmax,              &
                                   total_componentsmax,        &
                                   total_mmcsnew,              &
                                   total_componentsnew,        &
                                   max_comps,                  &
                                   celldx,                     &
                                   celldy,                     &
                                   vertexdx,                   &
                                   vertexdy,                   &
                                   volume,                     &
                                   density1,                   &
                                   densitynew,                 &
                                   energy1,                    &
                                   energynew,                  &
                                   mass_flux_x,                &
                                   vol_flux_x,                 &
                                   mass_flux_y,                &
                                   vol_flux_y,                 &
                                   pre_vol,                    &
                                   post_vol,                   &
                                   ener_flux,                  &
                                   material1,                  &
                                   materialnew,                &
                                   mmc_material1,              &
                                   mmc_materialnew,            &
                                   mmc_index1,                 &
                                   mmc_indexnew,               &
                                   mmc_j1,                     &
                                   mmc_k1,                     &
                                   mmc_jnew,                   &
                                   mmc_knew,                   &
                                   mmc_component1,             &
                                   mmc_componentnew,           &
                                   mmc_density1,               &
                                   mmc_densitynew,             &
                                   mmc_energy1,                &
                                   mmc_energynew,              &
                                   mmc_volume1,                &
                                   mmc_volumenew,              &
                                   reallocation_needed         )
 write(0,*)"total_mmcs1 ",total_mmcsnew
 write(0,*)"total_components1 ",total_componentsnew
      IF(reallocation_needed) THEN
        ! Allocate new arrays
        DEALLOCATE(mmc_jnew)
        DEALLOCATE(mmc_knew)
        DEALLOCATE(mmc_indexnew)
        DEALLOCATE(mmc_componentnew)
        DEALLOCATE(mmc_volumenew)
        DEALLOCATE(mmc_materialnew)
        DEALLOCATE(mmc_densitynew)
        DEALLOCATE(mmc_energynew)
        DEALLOCATE(mmc_pressure)
        DEALLOCATE(mmc_soundspeed)
        ALLOCATE(mmc_jnew(1:total_mmcsmax))
        ALLOCATE(mmc_knew(1:total_mmcsmax))
        ALLOCATE(mmc_indexnew(1:total_mmcsmax))
        ALLOCATE(mmc_componentnew(1:total_mmcsmax))
        ALLOCATE(mmc_volumenew(1:total_componentsmax))
        ALLOCATE(mmc_materialnew(1:total_componentsmax))
        ALLOCATE(mmc_densitynew(1:total_componentsmax))
        ALLOCATE(mmc_energynew(1:total_componentsmax))
        ALLOCATE(mmc_pressure(1:total_componentsmax))
        ALLOCATE(mmc_soundspeed(1:total_componentsmax))
        ! Copy data
        mmc_jnew(1:total_mmcsnew)=mmc_j1(1:total_mmcsnew)
        mmc_knew(1:total_mmcsnew)=mmc_k1(1:total_mmcsnew)
        mmc_indexnew(1:total_mmcsnew)=mmc_index1(1:total_mmcsnew)
        mmc_componentnew(1:total_mmcsnew)=mmc_component1(1:total_mmcsnew)
        mmc_volumenew(1:total_componentsnew)=mmc_volume1(1:total_componentsnew)
        mmc_materialnew(1:total_componentsnew)=mmc_material1(1:total_componentsnew)
        mmc_densitynew(1:total_componentsnew)=mmc_density1(1:total_componentsnew)
        mmc_energynew(1:total_componentsnew)=mmc_energy1(1:total_componentsnew)
        ! Deallocate old arrays and allocate with new storage limits
        DEALLOCATE(mmc_j1)
        DEALLOCATE(mmc_k1)
        DEALLOCATE(mmc_index1)
        DEALLOCATE(mmc_component1)
        DEALLOCATE(mmc_volume1)
        DEALLOCATE(mmc_material1)
        DEALLOCATE(mmc_density1)
        DEALLOCATE(mmc_energy1)
        ALLOCATE(mmc_j1(total_mmcsmax))
        ALLOCATE(mmc_k1(total_mmcsmax))
        ALLOCATE(mmc_index1(total_mmcsmax))
        ALLOCATE(mmc_component1(total_mmcsmax))
        ALLOCATE(mmc_volume1(1:total_componentsmax))
        ALLOCATE(mmc_material1(1:total_componentsmax))
        ALLOCATE(mmc_density1(1:total_componentsmax))
        ALLOCATE(mmc_energy1(1:total_componentsmax))
      ENDIF
      direction=1
      sweep_number=2
      WRITE(0,*)"X advection"
!   Note that the 2nd sweep uses *new as the initial data and *1 and as the final data, to prevent excessive copies
      CALL advec_mmc_x_cell_kernel(x_min,x_max,y_min,y_max,    &
                                   direction,                  &
                                   sweep_number,               &
                                   max_mats,                   &
                                   total_mmcs1,                &
                                   total_components1,          &
                                   total_mmcsmax,              &
                                   total_componentsmax,        &
                                   total_mmcsnew,              &
                                   total_componentsnew,        &
                                   max_comps,                  &
                                   celldx,                     &
                                   celldy,                     &
                                   vertexdx,                   &
                                   vertexdy,                   &
                                   volume,                     &
                                   densitynew,                 &
                                   density1,                   &
                                   energynew,                  &
                                   energy1,                    &
                                   mass_flux_x,                &
                                   vol_flux_x,                 &
                                   mass_flux_y,                &
                                   vol_flux_y,                 &
                                   pre_vol,                    &
                                   post_vol,                   &
                                   ener_flux,                  &
                                   materialnew,                &
                                   material1,                  &
                                   mmc_materialnew,            &
                                   mmc_material1,              &
                                   mmc_indexnew,               &
                                   mmc_index1,                 &
                                   mmc_jnew,                   &
                                   mmc_knew,                   &
                                   mmc_j1,                     &
                                   mmc_k1,                     &
                                   mmc_componentnew,           &
                                   mmc_component1,             &
                                   mmc_densitynew,             &
                                   mmc_density1,               &
                                   mmc_energynew,              &
                                   mmc_energy1,                &
                                   mmc_volumenew,              &
                                   mmc_volume1,                &
                                   reallocation_needed         )
write(0,*)"total_mmcs1 ",total_mmcsnew
write(0,*)"total_components1 ",total_componentsnew
      IF(reallocation_needed) THEN
        ! Allocate new arrays
        DEALLOCATE(mmc_j1)
        DEALLOCATE(mmc_k1)
        DEALLOCATE(mmc_index1)
        DEALLOCATE(mmc_component1)
        DEALLOCATE(mmc_volume1)
        DEALLOCATE(mmc_material1)
        DEALLOCATE(mmc_density1)
        DEALLOCATE(mmc_energy1)
        DEALLOCATE(mmc_pressure)
        DEALLOCATE(mmc_soundspeed)
        ALLOCATE(mmc_j1(1:total_mmcsmax))
        ALLOCATE(mmc_k1(1:total_mmcsmax))
        ALLOCATE(mmc_index1(1:total_mmcsmax))
        ALLOCATE(mmc_component1(1:total_mmcsmax))
        ALLOCATE(mmc_volume1(1:total_componentsmax))
        ALLOCATE(mmc_material1(1:total_componentsmax))
        ALLOCATE(mmc_density1(1:total_componentsmax))
        ALLOCATE(mmc_energy1(1:total_componentsmax))
        ALLOCATE(mmc_pressure(1:total_componentsmax))
        ALLOCATE(mmc_soundspeed(1:total_componentsmax))
        ! Copy data
        mmc_j1(1:total_mmcsnew)=mmc_jnew(1:total_mmcsnew)
        mmc_k1(1:total_mmcsnew)=mmc_knew(1:total_mmcsnew)
        mmc_index1(1:total_mmcsnew)=mmc_indexnew(1:total_mmcsnew)
        mmc_component1(1:total_mmcsnew)=mmc_componentnew(1:total_mmcsnew)
        mmc_volume1(1:total_componentsnew)=mmc_volumenew(1:total_componentsnew)
        mmc_material1(1:total_componentsnew)=mmc_materialnew(1:total_componentsnew)
        mmc_density1(1:total_componentsnew)=mmc_densitynew(1:total_componentsnew)
        mmc_energy1(1:total_componentsnew)=mmc_energynew(1:total_componentsnew)
        ! Deallocate old arrays and allocate with new storage limits
        DEALLOCATE(mmc_jnew)
        DEALLOCATE(mmc_knew)
        DEALLOCATE(mmc_indexnew)
        DEALLOCATE(mmc_componentnew)
        DEALLOCATE(mmc_materialnew)
        DEALLOCATE(mmc_volumenew)
        DEALLOCATE(mmc_densitynew)
        DEALLOCATE(mmc_energynew)
        ALLOCATE(mmc_jnew(total_mmcsmax))
        ALLOCATE(mmc_knew(total_mmcsmax))
        ALLOCATE(mmc_indexnew(total_mmcsmax))
        ALLOCATE(mmc_componentnew(total_mmcsmax))
        ALLOCATE(mmc_volumenew(1:total_componentsmax))
        ALLOCATE(mmc_materialnew(1:total_componentsmax))
        ALLOCATE(mmc_densitynew(1:total_componentsmax))
        ALLOCATE(mmc_energynew(1:total_componentsmax))
      ENDIF
    ENDIF

    ! This is where the final advected data will be pack for better memory access
    !CALL mmc_pack_kernel()

  ENDDO

  cell_time=(timer()-kernel_time)

  WRITE(*,*)
  WRITE(*,*) "Multi material advec cell time  ",cell_time
  WRITE(*,*) "Density                         ",SUM(density1)
  WRITE(*,*) "Energy                          ",SUM(energy1)

  ! Reset values for single material call
  density1=density0
  energy1=energy0
  material1=material0

  kernel_time=timer()

  DO iteration=1,its
    x_first=MOD(iteration,2)
    WRITE(0,*)"Iteration count ", iteration, x_first

    sweep_number=1
    IF( x_first.EQ.1) THEN
      WRITE(0,*)"X advection"
      direction=1
    ELSE
      WRITE(0,*)"Y advection"
      direction=2
    ENDIF
    CALL advec_cell_kernel(x_min,x_max,y_min,y_max,   &
                           direction,                 &
                           sweep_number,              &
                           vertexdx,                  &
                           vertexdy,                  &
                           volume,                    &
                           density1,                  &
                           energy1,                   &
                           mass_flux_x,               &
                           vol_flux_x,                &
                           mass_flux_y,               &
                           vol_flux_y,                &
                           pre_vol,                   &
                           post_vol,                  &
                           ener_flux                  )
    IF( x_first.NE.1) THEN
      WRITE(0,*)"X advection"
      direction=1
    ELSE
      WRITE(0,*)"Y advection"
      direction=2
    ENDIF
    sweep_number=2
    CALL advec_cell_kernel(x_min,x_max,y_min,y_max,   &
                           direction,                 &
                           sweep_number,              &
                           vertexdx,                  &
                           vertexdy,                  &
                           volume,                    &
                           density1,                  &
                           energy1,                   &
                           mass_flux_x,               &
                           vol_flux_x,                &
                           mass_flux_y,               &
                           vol_flux_y,                &
                           pre_vol,                   &
                           post_vol,                  &
                           ener_flux                  )
  ENDDO

  cell_time=(timer()-kernel_time)

  WRITE(*,*)
  WRITE(*,*) "Single material advec cell time ",cell_time
  WRITE(*,*) "Density                         ",SUM(density1)
  WRITE(*,*) "Energy                          ",SUM(energy1)

  DEALLOCATE(vertexdx)
  DEALLOCATE(vertexdy)
  DEALLOCATE(xarea)
  DEALLOCATE(yarea)
  DEALLOCATE(volume)
  DEALLOCATE(soundspeed)
  DEALLOCATE(density1)
  DEALLOCATE(xvel0)
  DEALLOCATE(xvel1)
  DEALLOCATE(yvel0)
  DEALLOCATE(yvel1)
  DEALLOCATE(vol_flux_x)
  DEALLOCATE(vol_flux_y)
  DEALLOCATE(mass_flux_x)
  DEALLOCATE(mass_flux_y)
  DEALLOCATE(pre_vol)
  DEALLOCATE(post_vol)
  DEALLOCATE(ener_flux)

END PROGRAM mmc_cell_driver
