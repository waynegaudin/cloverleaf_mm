!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran kernel to update the external halo cells in a chunk.
!>  @author Wayne Gaudin
!>  @details Updates halo cells for the required fields at the required depth
!>  for any halo cells that lie on an external boundary. The location and type
!>  of data governs how this is carried out. External boundaries are always
!>  reflective.

MODULE update_halo_kernel_module

  USE data_module 

CONTAINS

SUBROUTINE update_halo_kernel_t(x_min,x_max,y_min,y_max,                                    &
                                total_mmcs0,                                                &
                                total_components0,                                          &
                                total_mmcs1,                                                &
                                total_components1,                                          &
                                total_mmcsmax,                                              &
                                total_componentsmax,                                        &
                                total_mmcs_halo,                                            &
                                total_components_halo,                                      &
                                number_of_threads,                                          &
                                mmc_counter_next,                                           &
                                component_counter_next,                                     &
                                density0,                                                   &
                                energy0,                                                    &
                                pressure,                                                   &
                                viscosity,                                                  &
                                soundspeed,                                                 &
                                density1,                                                   &
                                energy1,                                                    &
                                xvel0,                                                      &
                                yvel0,                                                      &
                                xvel1,                                                      &
                                yvel1,                                                      &
                                vol_flux_x,                                                 &
                                vol_flux_y,                                                 &
                                mass_flux_x,                                                &
                                mass_flux_y,                                                &
                                material0,                                                  &
                                mmc_index0,                                                 &
                                mmc_j0,                                                     &
                                mmc_k0,                                                     &
                                mmc_component0,                                             &
                                mmc_material0,                                              &
                                mmc_density0,                                               &
                                mmc_energy0,                                                &
                                mmc_pressure,                                               &
                                mmc_soundspeed,                                             &
                                mmc_viscosity,                                              &
                                mmc_volume0,                                                &
                                material1,                                                  &
                                mmc_index1,                                                 &
                                mmc_j1,                                                     &
                                mmc_k1,                                                     &
                                mmc_component1,                                             &
                                mmc_material1,                                              &
                                mmc_density1,                                               &
                                mmc_energy1,                                                &
                                mmc_volume1,                                                &
                                fields,                                                     &
                                depth                                                       )

  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: total_mmcs_halo,total_components_halo
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER :: fields(:),depth

  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume0
  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume1

  INTEGER :: mmc,mmc_new,component,component_new,j,k

  INTEGER(KIND=4) :: thread_number=1

  LOGICAL :: multi_material, mmc_phase

!$ INTEGER :: omp_get_thread_num

  multi_material=.FALSE.
  IF(total_mmcs1.NE.0) multi_material=.TRUE.
  ! Maybe allow FIELD_MATERIAL1 to equal 3 if the MMC structure has changed, 2 if it hasn't but data has
  ! 1 if it is just cell value required, no material numbers

  ! Note that so variables are only ever one time level, like pressure, so they only have one
  ! array for mmc data. But they do not to use the correct structure.
  ! Fortunately, because material1 is a copy of material0 at the begining of the step, I can
  ! use material1 to do the updates without adding the extra code to cope with this.

  ! Initial bodge until thread counters are correct
  ! These need to be set after each advection sweep so I don't need to find the numbers again
  ! Probably needed for both time levels, but only level 1 is dynamically updated. Level 0
  ! is just a copy at the end of a step so I may not need to bother.
  ! Another issue is what to when a halo is already fixed. Might be ok as I am not using linked
  ! links or complex data structures and everything is redordered after the advection anyway
  ! Also, we packed the mixed data structure after the advection, but should we pack again?
  ! Probably too expensive and as the structure is only changed after an advection sweep, external
  ! boundaries should be updated before packing so it is done anyway?

  ! Update values in external halo cells based on depth and fields requested
  ! Even though half of these loops look the wrong way around, it should be noted
  ! that depth is either 1 or 2 so that it is more efficient to always thread
  ! loop along the mesh edge.

  ! TO CHECK/UNDERSTAND
  ! When do I need to update material? Do I call this only after the advection (twice per step)
  ! So I will call it with unpacked data? And then pack it afterwards?
  ! Once material is set, then, all the other variables are ready to be copied into the
  ! correct data locations.
  ! If the data is already packed, then this does not need doing again.
  ! What order do I do external/internal/tile comms.
  ! Probably only pack after all this is done, because they will all affect the material
  ! layout
  ! I NEED to set a value for mmc_counter_next and component_counter_next in the advection kernel
  ! If I have not packed the data, then I should still have values for each thread from
  ! the last material advection if I have stored them in a global location
  ! e.g. mmc_counter_next(1:number_of_threads), component_counter_next(1:number_of_threads)
  thread_number=1 ! Default for non-threaded case

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,thread_number,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
!$      thread_number=omp_get_thread_num()
        material0(j,y_max+k)=material0(j,y_max+1-k)
        IF(material0(j,y_max+1-k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*material0(j,y_max+1-k)
          ! Acceptor
! I need to detect if storage had been exhausted here
! CHANGE THIS TO AN ATOMIC UPDATE AND USE THE TOTAL_MMCS AND COMPS AS THE STARTING POINT
! OTHERWISE I WILL HAVE TO RECOMPACT ALL THE DATA. FOR THE LIMITED HALOS THIS MAY BE OK
! AN ALTERNATIVE I CAN CALCUATE A PRIORI FROM THE DONOR CELLS THE EXACT SIZE EACH THREAD NEEDS
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc)
!$OMP END ATOMIC
          mmc_component0(mmc_new)=mmc_component0(mmc)
          ! Set acceptor data
          material0(j,y_max+k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc)
            mmc_material0(mmc_index0(mmc_new)+component-1)=mmc_material0(mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=mmc_volume0(mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=j
          mmc_k0(mmc_new)=y_max+k
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,thread_number,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
!$      thread_number=omp_get_thread_num()
        material1(j,y_max+k)=material1(j,y_max+1-k)
        IF(material1(j,y_max+1-k).LT.1) THEN ! Check if MMC
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Where do we store the halo cell mmc data
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc)
!$OMP END ATOMIC
          mmc_component1(mmc_new)=mmc_component1(mmc)
          ! Set acceptor data
          material1(j,y_max+k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc)
            mmc_material1(mmc_index1(mmc_new)+component-1)=mmc_material1(mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=mmc_volume1(mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=j
          mmc_k1(mmc_new)=y_max+k
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF
  ! As long as material has been communicated previously, then this should work fine
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        density0(j,y_max+k)=density0(j,y_max+1-k)
        IF(mmc_phase.AND.material1(j,y_max+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density0(mmc_index1(mmc_new)+component-1)=mmc_density1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        density1(j,y_max+k)=density1(j,y_max+1-k)
        IF(mmc_phase.AND.material1(j,y_max+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density1(mmc_index1(mmc_new)+component-1)=mmc_density1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        energy0(j,y_max+k)=energy0(j,y_max+1-k)
        IF(mmc_phase.AND.material1(j,y_max+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=mmc_energy0(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        energy1(j,y_max+k)=energy1(j,y_max+1-k)
        IF(mmc_phase.AND.material1(j,y_max+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=mmc_energy1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        pressure(j,y_max+k)=pressure(j,y_max+1-k)
        IF(mmc_phase.AND.material1(j,y_max+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=mmc_pressure(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        viscosity(j,y_max+k)=viscosity(j,y_max+1-k)
        IF(mmc_phase.AND.material1(j,y_max+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=mmc_viscosity(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        soundspeed(j,y_max+k)=soundspeed(j,y_max+1-k)
        IF(mmc_phase.AND.material1(j,y_max+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,y_max+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=mmc_soundspeed(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        xvel0(j,y_max+1+k)=xvel0(j,y_max+1-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        xvel1(j,y_max+1+k)=xvel1(j,y_max+1-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        yvel0(j,y_max+1+k)=-yvel0(j,y_max+1-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        yvel1(j,y_max+1+k)=-yvel1(j,y_max+1-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        vol_flux_x(j,y_max+k)=vol_flux_x(j,y_max-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        mass_flux_x(j,y_max+k)=mass_flux_x(j,y_max-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        vol_flux_y(j,y_max+k+1)=-vol_flux_y(j,y_max+1-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        mass_flux_y(j,y_max+k+1)=-mass_flux_y(j,y_max+1-k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

END SUBROUTINE update_halo_kernel_t

SUBROUTINE update_halo_kernel_b(x_min,x_max,y_min,y_max,                                    &
                                total_mmcs0,                                                &
                                total_components0,                                          &
                                total_mmcs1,                                                &
                                total_components1,                                          &
                                total_mmcsmax,                                              &
                                total_componentsmax,                                        &
                                total_mmcs_halo,                                            &
                                total_components_halo,                                      &
                                number_of_threads,                                          &
                                mmc_counter_next,                                           &
                                component_counter_next,                                     &
                                density0,                                                   &
                                energy0,                                                    &
                                pressure,                                                   &
                                viscosity,                                                  &
                                soundspeed,                                                 &
                                density1,                                                   &
                                energy1,                                                    &
                                xvel0,                                                      &
                                yvel0,                                                      &
                                xvel1,                                                      &
                                yvel1,                                                      &
                                vol_flux_x,                                                 &
                                vol_flux_y,                                                 &
                                mass_flux_x,                                                &
                                mass_flux_y,                                                &
                                material0,                                                  &
                                mmc_index0,                                                 &
                                mmc_j0,                                                     &
                                mmc_k0,                                                     &
                                mmc_component0,                                             &
                                mmc_material0,                                              &
                                mmc_density0,                                               &
                                mmc_energy0,                                                &
                                mmc_pressure,                                               &
                                mmc_soundspeed,                                             &
                                mmc_viscosity,                                              &
                                mmc_volume0,                                                &
                                material1,                                                  &
                                mmc_index1,                                                 &
                                mmc_j1,                                                     &
                                mmc_k1,                                                     &
                                mmc_component1,                                             &
                                mmc_material1,                                              &
                                mmc_density1,                                               &
                                mmc_energy1,                                                &
                                mmc_volume1,                                                &
                                fields,                                                     &
                                depth                                                       )
  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: total_mmcs_halo,total_components_halo
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER :: fields(:),depth

  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume0
  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume1

  INTEGER :: mmc,mmc_new,component,component_new,j,k

  INTEGER(KIND=4) :: thread_number=1

  LOGICAL :: multi_material, mmc_phase

!$ INTEGER :: omp_get_thread_num

  multi_material=.FALSE.
  IF(total_mmcs1.NE.0) multi_material=.TRUE.

  thread_number=1 ! Default for non-threaded case
  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,thread_number,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
!$      thread_number=omp_get_thread_num()
        material0(j,1-k)=material0(j,0+k)
        ! Perhaps I should have the mmc stuff outside the pure loop?
        IF(material0(j,0+k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*material0(j,0+k)
          ! Acceptor
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc)
!$OMP END ATOMIC
          mmc_component0(mmc_new)=mmc_component0(mmc)
          ! Set acceptor data
          material0(j,1-k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc)
            mmc_material0(mmc_index0(mmc_new)+component-1)=mmc_material0(mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=mmc_volume0(mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=j
          mmc_k0(mmc_new)=1-k
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,thread_number,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
!$      thread_number=omp_get_thread_num()
        material1(j,1-k)=material1(j,0+k)
        ! Perhaps I should have the mmc stuff outside the pure loop?
        IF(material1(j,0+k).LT.1) THEN ! Check if MMC
          ! Donor location
          mmc=-1*material1(j,0+k)
          ! Where do we store the halo cell mmc data
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc)
!$OMP END ATOMIC
          mmc_component1(mmc_new)=mmc_component1(mmc)
          ! Set acceptor data
          material1(j,1-k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc)
            mmc_material1(mmc_index1(mmc_new)+component-1)=mmc_material1(mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=mmc_volume1(mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=j
          mmc_k1(mmc_new)=1-k
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF
  ! As long as material has been communicated previously, then this should work fine
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        density0(j,1-k)=density0(j,0+k)
        IF(mmc_phase.AND.material1(j,0+k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,0+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,1-k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density0(mmc_index1(mmc_new)+component-1)=mmc_density0(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        density1(j,1-k)=density1(j,0+k)
        IF(mmc_phase.AND.material1(j,0+k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,0+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,1-k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density1(mmc_index1(mmc_new)+component-1)=mmc_density1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        energy0(j,1-k)=energy0(j,0+k)
        IF(mmc_phase.AND.material1(j,0+k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,k)
          ! Set acceptor data
          mmc_new=-1*material1(j,1-k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=mmc_energy0(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        energy1(j,1-k)=energy1(j,0+k)
        IF(mmc_phase.AND.material1(j,0+k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,k)
          ! Set acceptor data
          mmc_new=-1*material1(j,1-k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=mmc_energy1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        pressure(j,1-k)=pressure(j,0+k)
        IF(mmc_phase.AND.material1(j,0+k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,k)
          ! Set acceptor data
          mmc_new=-1*material1(j,1-k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=mmc_pressure(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        viscosity(j,1-k)=viscosity(j,0+k)
        IF(mmc_phase.AND.material1(j,0+k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,k)
          ! Set acceptor data
          mmc_new=-1*material1(j,1-k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=mmc_viscosity(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        soundspeed(j,1-k)=soundspeed(j,0+k)
        IF(mmc_phase.AND.material1(j,0+k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(j,k)
          ! Set acceptor data
          mmc_new=-1*material1(j,1-k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=mmc_soundspeed(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        xvel0(j,1-k)=xvel0(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        xvel1(j,1-k)=xvel1(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        yvel0(j,1-k)=-yvel0(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        yvel1(j,1-k)=-yvel1(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        vol_flux_x(j,1-k)=vol_flux_x(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max+1
      DO k=1,depth
        mass_flux_x(j,1-k)=mass_flux_x(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        vol_flux_y(j,1-k)=-vol_flux_y(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO j=x_min,x_max
      DO k=1,depth
        mass_flux_y(j,1-k)=-mass_flux_y(j,1+k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

END SUBROUTINE update_halo_kernel_b

SUBROUTINE update_halo_kernel_l(x_min,x_max,y_min,y_max,                                    &
                                total_mmcs0,                                                &
                                total_components0,                                          &
                                total_mmcs1,                                                &
                                total_components1,                                          &
                                total_mmcsmax,                                              &
                                total_componentsmax,                                        &
                                total_mmcs_halo,                                            &
                                total_components_halo,                                      &
                                number_of_threads,                                          &
                                mmc_counter_next,                                           &
                                component_counter_next,                                     &
                                density0,                                                   &
                                energy0,                                                    &
                                pressure,                                                   &
                                viscosity,                                                  &
                                soundspeed,                                                 &
                                density1,                                                   &
                                energy1,                                                    &
                                xvel0,                                                      &
                                yvel0,                                                      &
                                xvel1,                                                      &
                                yvel1,                                                      &
                                vol_flux_x,                                                 &
                                vol_flux_y,                                                 &
                                mass_flux_x,                                                &
                                mass_flux_y,                                                &
                                material0,                                                  &
                                mmc_index0,                                                 &
                                mmc_j0,                                                     &
                                mmc_k0,                                                     &
                                mmc_component0,                                             &
                                mmc_material0,                                              &
                                mmc_density0,                                               &
                                mmc_energy0,                                                &
                                mmc_pressure,                                               &
                                mmc_soundspeed,                                             &
                                mmc_viscosity,                                              &
                                mmc_volume0,                                                &
                                material1,                                                  &
                                mmc_index1,                                                 &
                                mmc_j1,                                                     &
                                mmc_k1,                                                     &
                                mmc_component1,                                             &
                                mmc_material1,                                              &
                                mmc_density1,                                               &
                                mmc_energy1,                                                &
                                mmc_volume1,                                                &
                                fields,                                                     &
                                depth                                                       )
  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: total_mmcs_halo,total_components_halo
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER :: fields(:),depth

  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume0
  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume1

  INTEGER :: mmc,mmc_new,component,component_new,j,k

  INTEGER(KIND=4) :: thread_number=1

  LOGICAL :: multi_material, mmc_phase

!$ INTEGER :: omp_get_thread_num

  multi_material=.FALSE.
  IF(total_mmcs1.NE.0) multi_material=.TRUE.

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,thread_number,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
!$      thread_number=omp_get_thread_num()
        material0(1-j,k)=material0(0+j,k)
        IF(material0(0+j,k).LT.1) THEN ! Check if MMC
          ! Donor location
          mmc=-1*material0(0+j,k)
          ! Where do we store the halo cell mmc data
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc)
!$OMP END ATOMIC
          mmc_component0(mmc_new)=mmc_component0(mmc)
          ! Set acceptor data
          material0(1-j,k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc)
            mmc_material0(mmc_index0(mmc_new)+component-1)=mmc_material0(mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=mmc_volume0(mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=1-j
          mmc_k0(mmc_new)=k
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO

  ENDIF

  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,thread_number,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
!$      thread_number=omp_get_thread_num()
        material1(1-j,k)=material1(0+j,k)
        IF(material1(0+j,k).LT.1) THEN ! Check if MMC
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Where do we store the halo cell mmc data
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc)
!$OMP END ATOMIC
          mmc_component1(mmc_new)=mmc_component1(mmc)
          ! Set acceptor data
          material1(1-j,k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc)
            mmc_material1(mmc_index1(mmc_new)+component-1)=mmc_material1(mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=mmc_volume1(mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=1-j
          mmc_k1(mmc_new)=k
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF
  ! As long as material has been communicated previously, then this should work fine
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density0(1-j,k)=density0(0+j,k)
        IF(mmc_phase.AND.material1(0+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(1-j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density0(mmc_index1(mmc_new)+component-1)=mmc_density0(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density1(1-j,k)=density1(0+j,k)
        IF(mmc_phase.AND.material1(0+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(1-j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density1(mmc_index1(mmc_new)+component-1)=mmc_density1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        IF(mmc_phase.AND.material1(0+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(1-j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=mmc_energy0(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
        energy0(1-j,k)=energy0(0+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        energy1(1-j,k)=energy1(0+j,k)
        IF(mmc_phase.AND.material1(0+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(1-j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=mmc_energy1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        pressure(1-j,k)=pressure(0+j,k)
        IF(mmc_phase.AND.material1(0+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(1-j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=mmc_pressure(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        viscosity(1-j,k)=viscosity(0+j,k)
        IF(mmc_phase.AND.material1(0+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(1-j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=mmc_viscosity(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        soundspeed(1-j,k)=soundspeed(0+j,k)
        IF(mmc_phase.AND.material1(0+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(0+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(1-j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=mmc_soundspeed(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel0(1-j,k)=-xvel0(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel1(1-j,k)=-xvel1(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel0(1-j,k)=yvel0(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel1(1-j,k)=yvel1(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        vol_flux_x(1-j,k)=-vol_flux_x(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        mass_flux_x(1-j,k)=-mass_flux_x(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        vol_flux_y(1-j,k)=vol_flux_y(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        mass_flux_y(1-j,k)=mass_flux_y(1+j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF


END SUBROUTINE update_halo_kernel_l

SUBROUTINE update_halo_kernel_r(x_min,x_max,y_min,y_max,                                    &
                                total_mmcs0,                                                &
                                total_components0,                                          &
                                total_mmcs1,                                                &
                                total_components1,                                          &
                                total_mmcsmax,                                              &
                                total_componentsmax,                                        &
                                total_mmcs_halo,                                            &
                                total_components_halo,                                      &
                                number_of_threads,                                          &
                                mmc_counter_next,                                           &
                                component_counter_next,                                     &
                                density0,                                                   &
                                energy0,                                                    &
                                pressure,                                                   &
                                viscosity,                                                  &
                                soundspeed,                                                 &
                                density1,                                                   &
                                energy1,                                                    &
                                xvel0,                                                      &
                                yvel0,                                                      &
                                xvel1,                                                      &
                                yvel1,                                                      &
                                vol_flux_x,                                                 &
                                vol_flux_y,                                                 &
                                mass_flux_x,                                                &
                                mass_flux_y,                                                &
                                material0,                                                  &
                                mmc_index0,                                                 &
                                mmc_j0,                                                     &
                                mmc_k0,                                                     &
                                mmc_component0,                                             &
                                mmc_material0,                                              &
                                mmc_density0,                                               &
                                mmc_energy0,                                                &
                                mmc_pressure,                                               &
                                mmc_soundspeed,                                             &
                                mmc_viscosity,                                              &
                                mmc_volume0,                                                &
                                material1,                                                  &
                                mmc_index1,                                                 &
                                mmc_j1,                                                     &
                                mmc_k1,                                                     &
                                mmc_component1,                                             &
                                mmc_material1,                                              &
                                mmc_density1,                                               &
                                mmc_energy1,                                                &
                                mmc_volume1,                                                &
                                fields,                                                     &
                                depth                                                       )
  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: total_mmcs_halo,total_components_halo
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER :: fields(:),depth

  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume0
  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER, DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER, DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)      :: mmc_volume1

  INTEGER :: mmc,mmc_new,component,component_new,j,k

  INTEGER(KIND=4) :: thread_number=1

  LOGICAL :: multi_material, mmc_phase

!$ INTEGER :: omp_get_thread_num

  multi_material=.FALSE.
  IF(total_mmcs1.NE.0) multi_material=.TRUE.

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
!$      thread_number=omp_get_thread_num()
        material0(x_max+j,k)=material0(x_max+1-j,k)
        IF(material0(x_max+1-j,k).LT.1) THEN ! Check if MMC
          ! Donor location
          mmc=-1*material0(x_max+1-j,k)
          ! Where do we store the halo cell mmc data
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc)
!$OMP END ATOMIC
          mmc_component0(mmc_new)=mmc_component0(mmc)
          ! Set acceptor data
          material0(x_max+j,k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc)
            mmc_material0(mmc_index0(mmc_new)+component-1)=mmc_material0(mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=mmc_volume0(mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=x_max+j
          mmc_k0(mmc_new)=k
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        material1(x_max+j,k)=material1(x_max+1-j,k)
        IF(material1(x_max+1-j,k).LT.1) THEN ! Check if MMC
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
!$OMP ATOMIC CAPTURE
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
!$OMP END ATOMIC
!$OMP ATOMIC CAPTURE
          component_new=component_counter_next(thread_number)
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc)
!$OMP END ATOMIC
          mmc_component1(mmc_new)=mmc_component1(mmc)
          ! Set acceptor data
          material1(x_max+j,k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc)
            mmc_material1(mmc_index1(mmc_new)+component-1)=mmc_material1(mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=mmc_volume1(mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=x_max+j
          mmc_k1(mmc_new)=k
        ENDIF

      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  ! As long as material has been communicated previously, then this should work fine
  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density0(x_max+j,k)=density0(x_max+1-j,k)
        IF(mmc_phase.AND.material1(x_max+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density0(mmc_index1(mmc_new)+component-1)=mmc_density0(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density1(x_max+j,k)=density1(x_max+1-j,k)
        IF(mmc_phase.AND.material1(x_max+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_density1(mmc_index1(mmc_new)+component-1)=mmc_density1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        energy0(x_max+j,k)=energy0(x_max+1-j,k)
        IF(mmc_phase.AND.material1(x_max+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=mmc_energy0(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        energy1(x_max+j,k)=energy1(x_max+1-j,k)
        IF(mmc_phase.AND.material1(x_max+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=mmc_energy1(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        pressure(x_max+j,k)=pressure(x_max+1-j,k)
        IF(mmc_phase.AND.material1(x_max+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=mmc_pressure(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        viscosity(x_max+j,k)=viscosity(x_max+1-j,k)
        IF(mmc_phase.AND.material1(x_max+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=mmc_viscosity(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k,mmc,mmc_new,component,component_new) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        soundspeed(x_max+j,k)=soundspeed(x_max+1-j,k)
        IF(mmc_phase.AND.material1(x_max+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*material1(x_max+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          component_new=mmc_index1(mmc_new)
          DO component=1,mmc_component1(mmc)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=mmc_soundspeed(mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel0(x_max+1+j,k)=xvel0(x_max+1-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_XVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel1(x_max+1+j,k)=-xvel1(x_max+1-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL0).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel0(x_max+1+j,k)=yvel0(x_max+1-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_YVEL1).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel1(x_max+1+j,k)=yvel1(x_max+1-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        vol_flux_x(x_max+j+1,k)=-vol_flux_x(x_max+1-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_X).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        mass_flux_x(x_max+j+1,k)=-mass_flux_x(x_max+1-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_VOL_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        vol_flux_y(x_max+j,k)=vol_flux_y(x_max-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

  IF(fields(FIELD_MASS_FLUX_Y).EQ.1) THEN
!$OMP TARGET TEAMS DISTRIBUTE               &
!$OMP PARALLEL DO PRIVATE(j,k) COLLAPSE(2)
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        mass_flux_y(x_max+j,k)=mass_flux_y(x_max-j,k)
      ENDDO
    ENDDO
!$OMP END TARGET TEAMS DISTRIBUTE PARALLEL DO
  ENDIF

END SUBROUTINE update_halo_kernel_r

END  MODULE update_halo_kernel_module
