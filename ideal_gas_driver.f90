!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief standalone driver for the ideal gas kernels
!>  @author Wayne Gaudin
!>  @details Calls user requested kernel in standalone mode

PROGRAM ideal_gas_driver

  USE set_data_module
  USE ideal_gas_kernel_module
  USE ideal_gas_kernel_single_mat_module
  USE cell_average_kernel_module

  IMPLICIT NONE

!$ INTEGER :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM

  INTEGER :: numargs,iargc,i
  CHARACTER (LEN=20)  :: command_line,temp

  INTEGER :: x_size,y_size

  REAL(KIND=8) :: kernel_time,timer,ideal_gas_time

  LOGICAL :: use_fortran_kernels,multi_material
  INTEGER :: x_min,x_max,y_min,y_max,its,iteration,mm
  REAL(KIND=8),   ALLOCATABLE :: density0(:,:),energy0(:,:),pressure(:,:),soundspeed(:,:)
  REAL(KIND=8),   ALLOCATABLE :: mmc_density0(:),mmc_energy0(:),mmc_volume0(:)
  REAL(KIND=8),   ALLOCATABLE :: mmc_pressure(:),mmc_soundspeed(:)
  INTEGER(KIND=4),ALLOCATABLE :: material0(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_component0(:),mmc_material0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_j0(:),mmc_k0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_index0(:)
  INTEGER(KIND=4)             :: total_mmcs0,total_components0

  LOGICAL :: check_averages

!$OMP PARALLEL
!$  IF(OMP_GET_THREAD_NUM().EQ.0) THEN
!$    WRITE(*,'(a15,i5)') 'Thread Count: ',OMP_GET_NUM_THREADS()
!$  ENDIF
!$OMP END PARALLEL

  x_size=100
  y_size=100
  its=1
  use_fortran_kernels=.TRUE.
  multi_material=.FALSE.
  mm=0
  !check_averages=.FALSE.
  check_averages=.TRUE.

  numargs = iargc()

  DO i=1,numargs,2
    CALL GETARG(i,command_line)
    SELECT CASE (command_line)
      CASE("-help")
        WRITE(*,*) "Usage -nx 100 -ny 100 -its 10 -kernel fortran|c"
        stop
      CASE("-nx")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") x_size
      CASE("-ny")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") y_size
      CASE("-its")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") its
      CASE("-kernel")
        CALL GETARG(i+1,temp)
        IF(temp.EQ."fortran") THEN
          use_fortran_kernels=.TRUE.
        ENDIF
      CASE("-multimat")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") mm
    END SELECT
  ENDDO

  x_min=1
  y_min=1
  x_max=x_size
  y_max=y_size

  IF(mm.NE.0)multi_material=.TRUE.

  WRITE(*,*) "Ideal Gas Kernel"
  WRITE(*,*) "Mesh size ",x_size,y_size
  WRITE(*,*) "Iterations ",its
  IF(multi_material) WRITE(*,*)"Multi material"
  IF(.NOT.multi_material) WRITE(*,*)"Single material"

  kernel_time=timer()

  CALL set_data(x_min,x_max,y_min,y_max,            &
                multi_material,                     &
                density0=density0,                  &
                energy0=energy0,                    &
                pressure=pressure,                  &
                soundspeed=soundspeed,              &
                material0=material0,                &
                mmc_component0=mmc_component0,      &
                mmc_index0=mmc_index0,              &
                mmc_j0=mmc_j0,                      &
                mmc_k0=mmc_k0,                      &
                mmc_material0=mmc_material0,        &
                mmc_density0=mmc_density0,          &
                mmc_energy0=mmc_energy0,            &
                mmc_pressure=mmc_pressure,          &
                mmc_soundspeed=mmc_soundspeed,      &
                mmc_volume0=mmc_volume0,            &
                total_mmcs0=total_mmcs0,            &
                total_components0=total_components0 )

  WRITE(*,*) "Setup time ",timer()-kernel_time

  WRITE(*,*) "Data initialised"
  WRITE(*,*) "Density    Before: ",SUM(density0)
  WRITE(*,*) "Energy     Before: ",SUM(energy0)
  WRITE(*,*) "Pressure   Before: ",SUM(pressure)
  WRITE(*,*) "Soundspeed Before: ",SUM(soundspeed)

  kernel_time=timer()

  DO iteration=1,its
    CALL ideal_gas_kernel(x_min,               &
                          x_max,               &
                          y_min,               &
                          y_max,               &
                          total_mmcs0,         &
                          total_components0,   &
                          density0,            &
                          energy0,             &
                          pressure,            &
                          soundspeed,          &
                          material0,           &
                          mmc_component0,      &
                          mmc_index0,          &
                          mmc_material0,       &
                          mmc_density0,        &
                          mmc_energy0,         &
                          mmc_pressure,        &
                          mmc_soundspeed       )
  ENDDO

  ideal_gas_time=(timer()-kernel_time)
  WRITE(*,*)
  WRITE(*,*) "Multi material ideal gas time  ",ideal_gas_time 
  WRITE(*,*) "Density                        ",SUM(density0)
  WRITE(*,*) "Energy                         ",SUM(energy0)
  WRITE(*,*) "Pressure                       ",SUM(pressure)
  WRITE(*,*) "Soundspeed                     ",SUM(soundspeed)

  IF(check_averages) THEN
   CALL cell_average_kernel(x_min,x_max,y_min,y_max,      &
                            total_mmcs0,                  &
                            total_components0,            & 
                            density=density0,             &
                            energy=energy0,               &
                            pressure=pressure,            &
                            soundspeed=soundspeed,        &
                            material=material0,           &
                            mmc_index=mmc_index0,         &
                            mmc_j=mmc_j0,                 &
                            mmc_k=mmc_k0,                 &
                            mmc_component=mmc_component0, &
                            mmc_density=mmc_density0,     &
                            mmc_energy=mmc_energy0,       &
                            mmc_pressure=mmc_pressure,    &
                            mmc_soundspeed=mmc_soundspeed,&
                            mmc_volume=mmc_volume0        )
  ENDIF

  kernel_time=timer()

  DO iteration=1,its
    CALL ideal_gas_kernel_single_mat(x_min,     &
                                     x_max,     &
                                     y_min,     &
                                     y_max,     &
                                     density0,  &
                                     energy0,   &
                                     pressure,  &
                                     soundspeed )
  ENDDO

  ideal_gas_time=(timer()-kernel_time)

  WRITE(*,*)
  WRITE(*,*) "Single material ideal gas time ",ideal_gas_time 
  WRITE(*,*) "Density                        ",SUM(density0)
  WRITE(*,*) "Energy                         ",SUM(energy0)
  WRITE(*,*) "Pressure                       ",SUM(pressure)
  WRITE(*,*) "Soundspeed                     ",SUM(soundspeed)

  DEALLOCATE(density0)
  DEALLOCATE(energy0)
  DEALLOCATE(pressure)
  DEALLOCATE(soundspeed)

END PROGRAM ideal_gas_driver

