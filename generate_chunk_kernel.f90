!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran mesh chunk generator
!>  @author Wayne Gaudin
!>  @details Generates the field data on a mesh chunk based on the user specified
!>  input for the states.
!>
!>  Note that state one is always used as the background state, which is then
!>  overwritten by further state definitions.

MODULE generate_chunk_kernel_module

CONTAINS

SUBROUTINE generate_chunk_kernel(x_min,x_max,y_min,y_max, &
                                 vertexx,                 &
                                 vertexy,                 &
                                 cellx,                   &
                                 celly,                   &
                                 density0,                &
                                 energy0,                 &
                                 material0,               &
                                 material1,               &
                                 xvel0,                   &
                                 yvel0,                   &
                                 number_of_states,        &
                                 state_density,           &
                                 state_energy,            &
                                 state_material,          &
                                 state_xvel,              &
                                 state_yvel,              &
                                 state_xmin,              &
                                 state_xmax,              &
                                 state_ymin,              &
                                 state_ymax,              &
                                 state_radius,            &
                                 state_geometry,          &
                                 g_rect,                  &
                                 g_circ,                  &
                                 g_point,                 &
                                 auto,                    &
                                 total_mmcs0,             &
                                 total_components0,       &
                                 total_mmcs1,             &
                                 total_components1,       &
                                 mmc_volume0,             &
                                 mmc_volume1,             &
                                 mmc_density0,            &
                                 mmc_density1,            &
                                 mmc_energy0,             &
                                 mmc_energy1,             &
                                 mmc_j0,                  &
                                 mmc_k0,                  &
                                 mmc_j1,                  &
                                 mmc_k1,                  &
                                 mmc_component0,          &
                                 mmc_material0,           &
                                 mmc_index0,              &
                                 mmc_component1,          &
                                 mmc_material1,           &
                                 mmc_index1               )

  IMPLICIT NONE

  INTEGER      :: x_min,x_max,y_min,y_max
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3) :: vertexx
  REAL(KIND=8), DIMENSION(y_min-2:y_max+3) :: vertexy
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2) :: cellx
  REAL(KIND=8), DIMENSION(y_min-2:y_max+2) :: celly
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  INTEGER(KIND=4),DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0
  INTEGER(KIND=4),DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  INTEGER      :: number_of_states
  REAL(KIND=8), DIMENSION(number_of_states) :: state_density
  REAL(KIND=8), DIMENSION(number_of_states) :: state_energy
  INTEGER(KIND=4),DIMENSION(number_of_states):: state_material
  REAL(KIND=8), DIMENSION(number_of_states) :: state_xvel
  REAL(KIND=8), DIMENSION(number_of_states) :: state_yvel
  REAL(KIND=8), DIMENSION(number_of_states) :: state_xmin
  REAL(KIND=8), DIMENSION(number_of_states) :: state_xmax
  REAL(KIND=8), DIMENSION(number_of_states) :: state_ymin
  REAL(KIND=8), DIMENSION(number_of_states) :: state_ymax
  REAL(KIND=8), DIMENSION(number_of_states) :: state_radius
  INTEGER     , DIMENSION(number_of_states) :: state_geometry
  INTEGER      :: g_rect
  INTEGER      :: g_circ
  INTEGER      :: g_point
  LOGICAL        ,OPTIONAL :: auto
  INTEGER(KIND=4),OPTIONAL :: total_mmcs0,total_components0
  INTEGER(KIND=4),OPTIONAL :: total_mmcs1,total_components1
  REAL(KIND=8),   OPTIONAL :: mmc_volume0(:),mmc_volume1(:)
  REAL(KIND=8),   OPTIONAL :: mmc_density0(:),mmc_density1(:),mmc_energy0(:),mmc_energy1(:)
  INTEGER(KIND=4),OPTIONAL :: mmc_j0(:),mmc_k0(:),mmc_j1(:),mmc_k1(:)
  INTEGER(KIND=4),OPTIONAL :: mmc_component0(:),mmc_material0(:),mmc_index0(:)
  INTEGER(KIND=4),OPTIONAL :: mmc_component1(:),mmc_material1(:),mmc_index1(:)

  REAL(KIND=8) :: radius,x_cent,y_cent
  INTEGER      :: state

  INTEGER      :: j,k,jt,kt
  REAL(KIND=8) :: delta_h,dx,dy,x,y,mult,theta

  INTEGER      :: mmc_counter,component_counter
  LOGICAL :: auto_on

  auto_on=.FALSE.

  IF(PRESENT(auto)) auto_on=auto

  IF(.NOT.auto) THEN

  ! State 1 is always the background state

!$OMP PARALLEL SHARED(x_cent,y_cent)
!$OMP DO
  DO k=y_min-2,y_max+2
    DO j=x_min-2,x_max+2
      energy0(j,k)=state_energy(1)
    ENDDO
  ENDDO
!$OMP END DO
!$OMP DO
  DO k=y_min-2,y_max+2
    DO j=x_min-2,x_max+2
      density0(j,k)=state_density(1)
    ENDDO
  ENDDO
!$OMP END DO
!$OMP DO
  DO k=y_min-2,y_max+2
    DO j=x_min-2,x_max+2
      xvel0(j,k)=state_xvel(1)
    ENDDO
  ENDDO
!$OMP END DO
!$OMP DO
  DO k=y_min-2,y_max+2
    DO j=x_min-2,x_max+2
      yvel0(j,k)=state_yvel(1)
    ENDDO
  ENDDO
!$OMP END DO
!$OMP DO
  DO k=y_min-2,y_max+2
    DO j=x_min-2,x_max+2
      material0(j,k)=state_material(1)
      material1(j,k)=state_material(1)
    ENDDO
  ENDDO
!$OMP END DO

  DO state=2,number_of_states

! Could the velocity setting be thread unsafe?
    x_cent=state_xmin(state)
    y_cent=state_ymin(state)

!$OMP DO PRIVATE(radius,jt,kt)
    DO k=y_min-2,y_max+2
      DO j=x_min-2,x_max+2
        IF(state_geometry(state).EQ.g_rect ) THEN
          IF(vertexx(j+1).GE.state_xmin(state).AND.vertexx(j).LT.state_xmax(state)) THEN
            IF(vertexy(k+1).GE.state_ymin(state).AND.vertexy(k).LT.state_ymax(state)) THEN
              energy0(j,k)=state_energy(state)
              density0(j,k)=state_density(state)
              material0(j,k)=state_material(state)
              material1(j,k)=state_material(state)
              DO kt=k,k+1
                DO jt=j,j+1
                  xvel0(jt,kt)=state_xvel(state)
                  yvel0(jt,kt)=state_yvel(state)
                ENDDO
              ENDDO
            ENDIF
          ENDIF
        ELSEIF(state_geometry(state).EQ.g_circ ) THEN
          radius=SQRT((cellx(j)-x_cent)*(cellx(j)-x_cent)+(celly(k)-y_cent)*(celly(k)-y_cent))
          IF(radius.LE.state_radius(state))THEN
            energy0(j,k)=state_energy(state)
            density0(j,k)=state_density(state)
            material0(j,k)=state_material(state)
            material1(j,k)=state_material(state)
            DO kt=k,k+1
              DO jt=j,j+1
                xvel0(jt,kt)=state_xvel(state)
                yvel0(jt,kt)=state_yvel(state)
              ENDDO
            ENDDO
          ENDIF
        ELSEIF(state_geometry(state).EQ.g_point) THEN
          IF(state_xmin(state).GE.vertexx(j).AND.state_xmin(state).LT.vertexx(j+1)) THEN
            IF(state_ymin(state).GE.vertexy(k).AND.state_ymin(state).LT.vertexy(k+1)) THEN
              energy0(j,k)=state_energy(state)
              density0(j,k)=state_density(state)
              material0(j,k)=state_material(state)
              material1(j,k)=state_material(state)
              DO kt=k,k+1
                DO jt=j,j+1
                  xvel0(jt,kt)=state_xvel(state)
                  yvel0(jt,kt)=state_yvel(state)
                ENDDO
              ENDDO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
    ENDDO
!$OMP END DO

  ENDDO

!$OMP END PARALLEL

  ELSE ! Auto generate the problem used in the hydro driver set data routine

    dx=(10.0_8)/FLOAT(x_max-x_min+1)
    dy=(10.0_8)/FLOAT(y_max-y_min+1)
    delta_h=SQRT(dx**2.0_8+dy**2.0_8)
    component_counter=1
    mmc_counter=1

!$OMP PARALLEL
!$OMP DO PRIVATE(radius)
      DO k=y_min-2,y_max+2
        DO j=x_min-2,x_max+2
          radius=SQRT((FLOAT(j)*dx-5.0_8)**2.0_8+(FLOAT(k)*dy-5.0_8)**2.0_8)
          IF(radius.LE.2.5_8-delta_h) THEN
            material0(j,k)=1
            material1(j,k)=1
          ELSEIF(radius.GE.2.5_8+delta_h) THEN
            material0(j,k)=2
            material1(j,k)=2
          ELSE
            material0(j,k)=-2 ! Just a marker for now, will hold starting index
            material1(j,k)=-2 ! Just a marker for now, will hold starting index
          ENDIF
        ENDDO
      ENDDO
!$OMP ENDDO

!$OMP MASTER
    DO k=y_min-2,y_max+2
      DO j=x_min-2,x_max+2
        IF(material0(j,k).LT.0) THEN
          ! Two ways to access mixed cell data, depending on whether loop is over the mesh or mm cells
          material0(j,k)=-mmc_counter
          material1(j,k)=-mmc_counter
          mmc_j0(mmc_counter)=j
          mmc_k0(mmc_counter)=k
          mmc_j1(mmc_counter)=j
          mmc_k1(mmc_counter)=k
          mmc_index0(mmc_counter)=component_counter
          mmc_index1(mmc_counter)=component_counter
          mmc_component0(mmc_counter)=2
          mmc_component1(mmc_counter)=2
          mmc_material0(component_counter)=1
          mmc_material0(component_counter+1)=2
          mmc_material1(component_counter)=1
          mmc_material1(component_counter+1)=2
          mmc_density0(component_counter)=1.0_8
          mmc_density0(component_counter+1)=2.0_8
          mmc_density1(component_counter)=1.0_8
          mmc_density1(component_counter+1)=2.0_8
          mmc_energy0(component_counter)=2.0_8
          mmc_energy0(component_counter+1)=5.0_8
          mmc_energy1(component_counter)=2.0_8
          mmc_energy1(component_counter+1)=5.0_8
          mmc_counter=mmc_counter+1
          component_counter=component_counter+2
        ENDIF
      ENDDO
    ENDDO
    total_mmcs0=mmc_counter-1
    total_mmcs1=mmc_counter-1
    total_components0=component_counter-1
    total_components1=component_counter-1
    mmc_volume0=0.5_8
    mmc_volume1=0.5_8
!$OMP END MASTER

!$OMP DO PRIVATE(radius)
    DO k=y_min-2,y_max+2
      DO j=x_min-2,x_max+2
        radius=SQRT((FLOAT(j)*dx-5.0_8)**2.0_8+(FLOAT(k)*dy-5.0_8)**2.0_8)
        IF(radius.LE.2.5_8-delta_h) THEN
          density0(j,k)=2.0_8-(radius*2.0_8/10.0_8)
        ELSEIF(radius.GE.2.5_8+delta_h) THEN
          density0(j,k)=1.0_8-((radius-5.0_8)*1.0_8/20.0_8)
        ELSE
          density0(j,k)=1.5_8 ! Cell average assuming equal amounts of each material
        ENDIF
        IF(radius.GE.3.5_8+delta_h) THEN
          density0(j,k)=1.0_8
        ENDIF
      ENDDO
    ENDDO
!$OMP ENDDO

!$OMP DO PRIVATE(radius)
    DO k=y_min-2,y_max+2
      DO j=x_min-2,x_max+2
        radius=SQRT((FLOAT(j)*dx-5.0_8)**2.0_8+(FLOAT(k)*dy-5.0_8)**2.0_8)
        IF(radius.LE.2.5_8-delta_h) THEN
          energy0(j,k)=5.0_8-(radius*2.0_8/10.0_8)
        ELSEIF(radius.GE.2.5_8+delta_h) THEN
          energy0(j,k)=2.0_8-((radius-5.0_8)*1.0_8/20.0_8)
        ELSE
          energy0(j,k)=(0.5_8*5.0_8*2.0_8+0.5_8*2.0_8*1.0_8)/(1.5_8) ! This is mass weighted
        ENDIF
        IF(radius.GE.3.5_8+delta_h) THEN
          energy0(j,k)=2.0_8
        ENDIF
      ENDDO
    ENDDO
!$OMP ENDDO

!$OMP DO PRIVATE(radius,theta,x,y,mult)
    DO k=y_min-2,y_max+3
      DO j=x_min-2,x_max+3
        x=(FLOAT(j)*dx-5.0_8)
        y=(FLOAT(k)*dy-5.0_8)
        radius=SQRT(x**2.0_8+y**2.0_8)
        mult=1.0_8
        IF(x.LE.0.0_8.AND.y.LE.0.0_8) mult=-1.0_8
        IF(x.LE.0.0_8.AND.y.GT.0.0_8) mult=-1.0_8
        IF(ABS(x).GE.0.000000001_8) THEN
          theta=ATAN(y/x)
        ELSE
          theta=ATAN(y/(-1.0_8*0.000000001_8))
        ENDIF
        IF(radius.LE.2.5_8) THEN
          xvel0(j,k)=mult*(2.0_8-(radius*2.0_8/10.0_8))*COS(theta)
        ELSEIF(radius.LE.3.5_8) THEN
          xvel0(j,k)=mult*(1.0_8-((radius-5.0_8)*1.0_8/20.0_8))*COS(theta)
        ELSE
          xvel0(j,k)=0.0_8
        ENDIF
      ENDDO
    ENDDO
!$OMP ENDDO

!$OMP DO PRIVATE(radius,theta,x,y,mult)
    DO k=y_min-2,y_max+3
      DO j=x_min-2,x_max+3
        x=(FLOAT(j)*dx-5.0_8)
        y=(FLOAT(k)*dy-5.0_8)
        radius=SQRT(x**2.0_8+y**2.0_8)
        mult=1.0_8
        IF(x.LE.0.0_8.AND.y.LE.0.0_8) mult=-1.0_8
        IF(x.LE.0.0_8.AND.y.GT.0.0_8) mult=-1.0_8
        IF(ABS(x).GE.0.000000001_8) THEN
          theta=ATAN(y/x)
        ELSE
          theta=ATAN(y/(-1.0_8*0.000000001_8))
        ENDIF
        radius=SQRT((FLOAT(j)*dx-5.0_8)**2.0_8+(FLOAT(k)*dy-5.0_8)**2.0_8)
        IF(radius.LE.2.5_8) THEN
          yvel0(j,k)=mult*(2.0_8-(radius*2.0_8/10.0_8))*SIN(theta)
        ELSEIF(radius.LE.3.5_8) THEN
          yvel0(j,k)=mult*(1.0_8-((radius-5.0_8)*1.0_8/20.0_8))*SIN(theta)
        ELSE
          yvel0(j,k)=0.0_8
        ENDIF
      ENDDO
    ENDDO

!$OMP ENDDO
!$OMP END PARALLEL
  ENDIF


END SUBROUTINE generate_chunk_kernel

END MODULE generate_chunk_kernel_module
