!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran kernel to update the external halo cells in a chunk.
!>  @author Wayne Gaudin
!>  @details Updates halo cells for the required fields at the required depth
!>  for any halo cells that lie on an external boundary. The location and type
!>  of data governs how this is carried out. External boundaries are always
!>  reflective.

MODULE update_tile_halo_kernel_module

  USE data_module 

CONTAINS

SUBROUTINE update_tile_halo_l_kernel(x_min,x_max,y_min,y_max,                            &
                                     total_mmcs0,                                        &
                                     total_components0,                                  &
                                     total_mmcs1,                                        &
                                     total_components1,                                  &
                                     total_mmcsmax,                                      &
                                     total_componentsmax,                                &
                                     number_of_threads,                                  &
                                     mmc_counter_next,                                   &
                                     component_counter_next,                             &
                                     density0,                                           &
                                     energy0,                                            &
                                     pressure,                                           &
                                     viscosity,                                          &
                                     soundspeed,                                         &
                                     density1,                                           &
                                     energy1,                                            &
                                     xvel0,                                              &
                                     yvel0,                                              &
                                     xvel1,                                              &
                                     yvel1,                                              &
                                     vol_flux_x,                                         &
                                     vol_flux_y,                                         &
                                     mass_flux_x,                                        &
                                     mass_flux_y,                                        &
                                     material0,                                          &
                                     material1,                                          &
                                     mmc_index0,                                         &
                                     mmc_j0,                                             &
                                     mmc_k0,                                             &
                                     mmc_component0,                                     &
                                     mmc_material0,                                      &
                                     mmc_density0,                                       &
                                     mmc_energy0,                                        &
                                     mmc_pressure,                                       &
                                     mmc_soundspeed,                                     &
                                     mmc_viscosity,                                      &
                                     mmc_volume0,                                        &
                                     mmc_index1,                                         &
                                     mmc_j1,                                             &
                                     mmc_k1,                                             &
                                     mmc_component1,                                     &
                                     mmc_material1,                                      &
                                     mmc_density1,                                       &
                                     mmc_energy1,                                        &
                                     mmc_volume1,                                        &
                                     left_xmin, left_xmax, left_ymin, left_ymax,         &         
                                     left_total_mmcs0,                                   &
                                     left_total_components0,                             &
                                     left_total_mmcs1,                                   &
                                     left_total_components1,                             &
                                     left_total_mmcsmax,                                 &
                                     left_total_componentsmax,                           &
                                     left_density0,                                      &
                                     left_energy0,                                       &
                                     left_pressure,                                      &
                                     left_viscosity,                                     &
                                     left_soundspeed,                                    &
                                     left_density1,                                      &
                                     left_energy1,                                       &
                                     left_xvel0,                                         &
                                     left_yvel0,                                         &
                                     left_xvel1,                                         &
                                     left_yvel1,                                         &
                                     left_vol_flux_x,                                    &
                                     left_vol_flux_y,                                    &
                                     left_mass_flux_x,                                   &
                                     left_mass_flux_y,                                   &
                                     left_material0,                                     &
                                     left_material1,                                     &
                                     left_mmc_index0,                                    &
                                     left_mmc_j0,                                        &
                                     left_mmc_k0,                                        &
                                     left_mmc_component0,                                &
                                     left_mmc_material0,                                 &
                                     left_mmc_density0,                                  &
                                     left_mmc_energy0,                                   &
                                     left_mmc_pressure,                                  &
                                     left_mmc_soundspeed,                                &
                                     left_mmc_viscosity,                                 &
                                     left_mmc_volume0,                                   &
                                     left_mmc_index1,                                    &
                                     left_mmc_j1,                                        &
                                     left_mmc_k1,                                        &
                                     left_mmc_component1,                                &
                                     left_mmc_material1,                                 &
                                     left_mmc_density1,                                  &
                                     left_mmc_energy1,                                   &
                                     left_mmc_volume1,                                   &
                                     fields,                                             &
                                     depth                                               )

  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER     , DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0,material1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume1
  INTEGER :: left_xmin, left_xmax, left_ymin, left_ymax
  INTEGER :: left_total_mmcs0,left_total_components0
  INTEGER :: left_total_mmcs1,left_total_components1
  INTEGER :: left_total_mmcsmax,left_total_componentsmax
  REAL(KIND=8), DIMENSION(left_xmin-2:left_xmax+2,left_ymin-2:left_ymax+2) :: left_density0,left_energy0
  REAL(KIND=8), DIMENSION(left_xmin-2:left_xmax+2,left_ymin-2:left_ymax+2) :: left_pressure,left_viscosity,left_soundspeed
  REAL(KIND=8), DIMENSION(left_xmin-2:left_xmax+2,left_ymin-2:left_ymax+2) :: left_density1,left_energy1
  REAL(KIND=8), DIMENSION(left_xmin-2:left_xmax+3,left_ymin-2:left_ymax+3) :: left_xvel0,left_yvel0
  REAL(KIND=8), DIMENSION(left_xmin-2:left_xmax+3,left_ymin-2:left_ymax+3) :: left_xvel1,left_yvel1
  REAL(KIND=8), DIMENSION(left_xmin-2:left_xmax+3,left_ymin-2:left_ymax+2) :: left_vol_flux_x,left_mass_flux_x
  REAL(KIND=8), DIMENSION(left_xmin-2:left_xmax+2,left_ymin-2:left_ymax+3) :: left_vol_flux_y,left_mass_flux_y
  INTEGER     , DIMENSION(left_xmin-2:left_xmax+2,left_ymin-2:left_ymax+2) :: left_material0,left_material1
  INTEGER     , DIMENSION(1:left_total_mmcsmax)                            :: left_mmc_index0
  INTEGER     , DIMENSION(1:left_total_mmcsmax)                            :: left_mmc_component0
  INTEGER     , DIMENSION(1:left_total_mmcsmax)                            :: left_mmc_j0,left_mmc_k0
  INTEGER     , DIMENSION(1:left_total_componentsmax)                      :: left_mmc_material0
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_density0
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_energy0
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_pressure
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_viscosity
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_volume0
  INTEGER     , DIMENSION(1:left_total_mmcsmax)                            :: left_mmc_index1
  INTEGER     , DIMENSION(1:left_total_mmcsmax)                            :: left_mmc_component1
  INTEGER     , DIMENSION(1:left_total_mmcsmax)                            :: left_mmc_j1,left_mmc_k1
  INTEGER     , DIMENSION(1:left_total_componentsmax)                      :: left_mmc_material1
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_density1
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_energy1
  REAL(KIND=8), DIMENSION(1:left_total_componentsmax)                      :: left_mmc_volume1

  INTEGER :: fields(:),depth
  INTEGER :: j,k,mmc,mmc_new,component,component_new

  INTEGER :: thread_number=1

  LOGICAL :: mmc_phase

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        material0(x_min-j,k)=left_material0(left_xmax+1-j,k)
        ! Perhaps I should have the mmc stuff outside the pure loop?
        IF(left_material0(left_xmax+1-j,k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*left_material0(left_xmax+1-j,k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component0(mmc_new)=left_mmc_component0(mmc)
          ! Set acceptor data
          material0(x_min-j,k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc_new)
            mmc_material0(mmc_index0(mmc_new)+component-1)=left_mmc_material0(left_mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=left_mmc_volume0(left_mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=x_min-j
          mmc_k0(mmc_new)=k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        material1(x_min-j,k)=left_material1(left_xmax+1-j,k)
        IF(left_material1(left_xmax+1-j,k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*left_material1(left_xmax+1-j,k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component1(mmc_new)=left_mmc_component1(mmc)
          ! Set acceptor data
          material1(x_min-j,k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc_new)
            mmc_material1(mmc_index1(mmc_new)+component-1)=left_mmc_material1(left_mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=left_mmc_volume1(left_mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=x_min-j
          mmc_k1(mmc_new)=k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density0(x_min-j,k)=left_density0(left_xmax+1-j,k)
        IF(mmc_phase.AND.left_material1(left_xmax+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*left_material1(left_xmax+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_min-j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_density0(mmc_index1(mmc_new)+component-1)=left_mmc_density0(left_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density1(x_min-j,k)=left_density1(left_xmax+1-j,k)
        IF(mmc_phase.AND.left_material1(left_xmax+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*left_material1(left_xmax+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_min-j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_density1(mmc_index1(mmc_new)+component-1)=left_mmc_density1(left_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        energy0(x_min-j,k)=left_energy0(left_xmax+1-j,k)
        IF(mmc_phase.AND.left_material1(left_xmax+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*left_material0(left_xmax+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_min-j,k)
          DO component=1,mmc_component0(mmc_new)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=left_mmc_energy0(left_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        energy1(x_min-j,k)=left_energy1(left_xmax+1-j,k)
        IF(mmc_phase.AND.left_material1(left_xmax+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*left_material1(left_xmax+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_min-j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=left_mmc_energy1(left_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        pressure(x_min-j,k)=left_pressure(left_xmax+1-j,k)
        IF(mmc_phase.AND.left_material1(left_xmax+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*left_material1(left_xmax+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_min-j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=left_mmc_pressure(left_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        viscosity(x_min-j,k)=left_viscosity(left_xmax+1-j,k)
        IF(mmc_phase.AND.left_material1(left_xmax+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*left_material1(left_xmax+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_min-j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=left_mmc_viscosity(left_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        soundspeed(x_min-j,k)=left_soundspeed(left_xmax+1-j,k)
        IF(mmc_phase.AND.left_material1(left_xmax+1-j,k).LT.1) THEN
          ! Donor location
          mmc=-1*left_material1(left_xmax+1-j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_min-j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=left_mmc_soundspeed(left_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL0).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel0(x_min-j,k)=left_xvel0(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL1).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel1(x_min-j,k)=left_xvel1(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL0).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel0(x_min-j,k)=left_yvel0(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL1).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel1(x_min-j,k)=left_yvel1(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).GT.0) THEN
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        vol_flux_x(x_min-j,k)=left_vol_flux_x(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).GT.0) THEN
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        mass_flux_x(x_min-j,k)=left_mass_flux_x(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        vol_flux_y(x_min-j,k)=left_vol_flux_y(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        mass_flux_y(x_min-j,k)=left_mass_flux_y(left_xmax+1-j,k)
      ENDDO
    ENDDO
  ENDIF

END SUBROUTINE update_tile_halo_l_kernel

SUBROUTINE update_tile_halo_r_kernel(x_min,x_max,y_min,y_max,                            &
                                     total_mmcs0,                                        &
                                     total_components0,                                  &
                                     total_mmcs1,                                        &
                                     total_components1,                                  &
                                     total_mmcsmax,                                      &
                                     total_componentsmax,                                &
                                     number_of_threads,                                  &
                                     mmc_counter_next,                                   &
                                     component_counter_next,                             &
                                     density0,                                           &
                                     energy0,                                            &
                                     pressure,                                           &
                                     viscosity,                                          &
                                     soundspeed,                                         &
                                     density1,                                           &
                                     energy1,                                            &
                                     xvel0,                                              &
                                     yvel0,                                              &
                                     xvel1,                                              &
                                     yvel1,                                              &
                                     vol_flux_x,                                         &
                                     vol_flux_y,                                         &
                                     mass_flux_x,                                        &
                                     mass_flux_y,                                        &
                                     material0,                                          &
                                     material1,                                          &
                                     mmc_index0,                                         &
                                     mmc_j0,                                             &
                                     mmc_k0,                                             &
                                     mmc_component0,                                     &
                                     mmc_material0,                                      &
                                     mmc_density0,                                       &
                                     mmc_energy0,                                        &
                                     mmc_pressure,                                       &
                                     mmc_soundspeed,                                     &
                                     mmc_viscosity,                                      &
                                     mmc_volume0,                                        &
                                     mmc_index1,                                         &
                                     mmc_j1,                                             &
                                     mmc_k1,                                             &
                                     mmc_component1,                                     &
                                     mmc_material1,                                      &
                                     mmc_density1,                                       &
                                     mmc_energy1,                                        &
                                     mmc_volume1,                                        &
                                     right_xmin, right_xmax, right_ymin, right_ymax,     &         
                                     right_total_mmcs0,                                  &
                                     right_total_components0,                            &
                                     right_total_mmcs1,                                  &
                                     right_total_components1,                            &
                                     right_total_mmcsmax,                                &
                                     right_total_componentsmax,                          &
                                     right_density0,                                     &
                                     right_energy0,                                      &
                                     right_pressure,                                     &
                                     right_viscosity,                                    &
                                     right_soundspeed,                                   &
                                     right_density1,                                     &
                                     right_energy1,                                      &
                                     right_xvel0,                                        &
                                     right_yvel0,                                        &
                                     right_xvel1,                                        &
                                     right_yvel1,                                        &
                                     right_vol_flux_x,                                   &
                                     right_vol_flux_y,                                   &
                                     right_mass_flux_x,                                  &
                                     right_mass_flux_y,                                  &
                                     right_material0,                                    &
                                     right_material1,                                    &
                                     right_mmc_index0,                                   &
                                     right_mmc_j0,                                       &
                                     right_mmc_k0,                                       &
                                     right_mmc_component0,                               &
                                     right_mmc_material0,                                &
                                     right_mmc_density0,                                 &
                                     right_mmc_energy0,                                  &
                                     right_mmc_pressure,                                 &
                                     right_mmc_soundspeed,                               &
                                     right_mmc_viscosity,                                &
                                     right_mmc_volume0,                                  &
                                     right_mmc_index1,                                   &
                                     right_mmc_j1,                                       &
                                     right_mmc_k1,                                       &
                                     right_mmc_component1,                               &
                                     right_mmc_material1,                                &
                                     right_mmc_density1,                                 &
                                     right_mmc_energy1,                                  &
                                     right_mmc_volume1,                                  &
                                     fields,                                             &
                                     depth                                               )

  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER     , DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0,material1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume1
  INTEGER :: right_xmin, right_xmax, right_ymin, right_ymax
  INTEGER :: right_total_mmcs0,right_total_components0
  INTEGER :: right_total_mmcs1,right_total_components1
  INTEGER :: right_total_mmcsmax,right_total_componentsmax
  REAL(KIND=8), DIMENSION(right_xmin-2:right_xmax+2,right_ymin-2:right_ymax+2) :: right_density0,right_energy0
  REAL(KIND=8), DIMENSION(right_xmin-2:right_xmax+2,right_ymin-2:right_ymax+2) :: right_pressure,right_viscosity,right_soundspeed
  REAL(KIND=8), DIMENSION(right_xmin-2:right_xmax+2,right_ymin-2:right_ymax+2) :: right_density1,right_energy1
  REAL(KIND=8), DIMENSION(right_xmin-2:right_xmax+3,right_ymin-2:right_ymax+3) :: right_xvel0,right_yvel0
  REAL(KIND=8), DIMENSION(right_xmin-2:right_xmax+3,right_ymin-2:right_ymax+3) :: right_xvel1,right_yvel1
  REAL(KIND=8), DIMENSION(right_xmin-2:right_xmax+3,right_ymin-2:right_ymax+2) :: right_vol_flux_x,right_mass_flux_x
  REAL(KIND=8), DIMENSION(right_xmin-2:right_xmax+2,right_ymin-2:right_ymax+3) :: right_vol_flux_y,right_mass_flux_y
  INTEGER     , DIMENSION(right_xmin-2:right_xmax+2,right_ymin-2:right_ymax+2) :: right_material0,right_material1
  INTEGER     , DIMENSION(1:right_total_mmcsmax)                               :: right_mmc_index0
  INTEGER     , DIMENSION(1:right_total_mmcsmax)                               :: right_mmc_component0
  INTEGER     , DIMENSION(1:right_total_mmcsmax)                               :: right_mmc_j0,right_mmc_k0
  INTEGER     , DIMENSION(1:right_total_componentsmax)                         :: right_mmc_material0
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_density0
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_energy0
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_pressure
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_viscosity
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_volume0
  INTEGER     , DIMENSION(1:right_total_mmcsmax)                               :: right_mmc_index1
  INTEGER     , DIMENSION(1:right_total_mmcsmax)                               :: right_mmc_component1
  INTEGER     , DIMENSION(1:right_total_mmcsmax)                               :: right_mmc_j1,right_mmc_k1
  INTEGER     , DIMENSION(1:right_total_componentsmax)                         :: right_mmc_material1
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_density1
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_energy1
  REAL(KIND=8), DIMENSION(1:right_total_componentsmax)                         :: right_mmc_volume1

  INTEGER :: fields(:),depth
  INTEGER :: j,k,mmc,mmc_new,component,component_new

  INTEGER :: thread_number=1

  LOGICAL :: mmc_phase

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        material0(x_max+j,k)=right_material0(right_xmin-1+j,k)
        ! Perhaps I should have the mmc stuff outside the pure loop?
        IF(right_material0(right_xmin-1+j,k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*right_material0(right_xmin-1+j,k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component0(mmc_new)=right_mmc_component0(mmc)
          ! Set acceptor data
          material0(x_max+j,k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc_new)
            mmc_material0(mmc_index0(mmc_new)+component-1)=right_mmc_material0(right_mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=right_mmc_volume0(right_mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=x_max+j
          mmc_k0(mmc_new)=k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        material1(x_max+j,k)=right_material1(right_xmin-1+j,k)
        IF(right_material1(right_xmin-1+j,k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component1(mmc_new)=right_mmc_component1(mmc)
          ! Set acceptor data
          material1(x_max+j,k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc_new)
            mmc_material1(mmc_index1(mmc_new)+component-1)=right_mmc_material1(right_mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=right_mmc_volume1(right_mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=x_max+j
          mmc_k1(mmc_new)=k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density0(x_max+j,k)=right_density0(right_xmin-1+j,k)
        IF(mmc_phase.AND.right_material1(right_xmin-1+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_density0(mmc_index1(mmc_new)+component-1)=right_mmc_density0(right_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        density1(x_max+j,k)=right_density1(right_xmin-1+j,k)
        IF(mmc_phase.AND.right_material1(right_xmin-1+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_density1(mmc_index1(mmc_new)+component-1)=right_mmc_density1(right_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        energy0(x_max+j,k)=right_energy0(right_xmin-1+j,k)
        IF(mmc_phase.AND.right_material1(right_xmin-1+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=right_mmc_energy0(right_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        energy1(x_max+j,k)=right_energy1(right_xmin-1+j,k)
        IF(mmc_phase.AND.right_material1(right_xmin-1+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=right_mmc_energy1(right_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        pressure(x_max+j,k)=right_pressure(right_xmin-1+j,k)
        IF(mmc_phase.AND.right_material1(right_xmin-1+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=right_mmc_pressure(right_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        viscosity(x_max+j,k)=right_viscosity(right_xmin-1+j,k)
        IF(mmc_phase.AND.right_material1(right_xmin-1+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=right_mmc_viscosity(right_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        soundspeed(x_max+j,k)=right_soundspeed(right_xmin-1+j,k)
        IF(mmc_phase.AND.right_material1(right_xmin-1+j,k).LT.1) THEN
          ! Donor location
          mmc=-1*right_material1(right_xmin-1+j,k)
          ! Set acceptor data
          mmc_new=-1*material1(x_max+j,k)
          DO component=1,mmc_component1(mmc_new)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=right_mmc_soundspeed(right_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL0).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel0(x_max+1+j,k)=right_xvel0(right_xmin+1-1+j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL1).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        xvel1(x_max+1+j,k)=right_xvel1(right_xmin+1-1+j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL0).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel0(x_max+1+j,k)=right_yvel0(right_xmin+1-1+j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL1).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        yvel1(x_max+1+j,k)=right_yvel1(right_xmin+1-1+j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).GT.0) THEN
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        vol_flux_x(x_max+1+j,k)=right_vol_flux_x(right_xmin+1-1+j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).GT.0) THEN
    DO k=y_min-depth,y_max+depth
      DO j=1,depth
        mass_flux_x(x_max+1+j,k)=right_mass_flux_x(right_xmin+1-1+j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        vol_flux_y(x_max+j,k)=right_vol_flux_y(right_xmin-1+j,k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).GT.0) THEN
    DO k=y_min-depth,y_max+1+depth
      DO j=1,depth
        mass_flux_y(x_max+j,k)=right_mass_flux_y(right_xmin-1+j,k)
      ENDDO
    ENDDO
  ENDIF

END SUBROUTINE update_tile_halo_r_kernel

! Top and bottom only do xmin -> xmax
! This is because the corner ghosts will get communicated in the left right communication

SUBROUTINE update_tile_halo_t_kernel(x_min,x_max,y_min,y_max,                            &
                                     total_mmcs0,                                        &
                                     total_components0,                                  &
                                     total_mmcs1,                                        &
                                     total_components1,                                  &
                                     total_mmcsmax,                                      &
                                     total_componentsmax,                                &
                                     number_of_threads,                                  &
                                     mmc_counter_next,                                   &
                                     component_counter_next,                             &
                                     density0,                                           &
                                     energy0,                                            &
                                     pressure,                                           &
                                     viscosity,                                          &
                                     soundspeed,                                         &
                                     density1,                                           &
                                     energy1,                                            &
                                     xvel0,                                              &
                                     yvel0,                                              &
                                     xvel1,                                              &
                                     yvel1,                                              &
                                     vol_flux_x,                                         &
                                     vol_flux_y,                                         &
                                     mass_flux_x,                                        &
                                     mass_flux_y,                                        &
                                     material0,                                          &
                                     material1,                                          &
                                     mmc_index0,                                         &
                                     mmc_j0,                                             &
                                     mmc_k0,                                             &
                                     mmc_component0,                                     &
                                     mmc_material0,                                      &
                                     mmc_density0,                                       &
                                     mmc_energy0,                                        &
                                     mmc_pressure,                                       &
                                     mmc_soundspeed,                                     &
                                     mmc_viscosity,                                      &
                                     mmc_volume0,                                        &
                                     mmc_index1,                                         &
                                     mmc_j1,                                             &
                                     mmc_k1,                                             &
                                     mmc_component1,                                     &
                                     mmc_material1,                                      &
                                     mmc_density1,                                       &
                                     mmc_energy1,                                        &
                                     mmc_volume1,                                        &
                                     top_xmin, top_xmax, top_ymin, top_ymax,             &         
                                     top_total_mmcs0,                                    &
                                     top_total_components0,                              &
                                     top_total_mmcs1,                                    &
                                     top_total_components1,                              &
                                     top_total_mmcsmax,                                  &
                                     top_total_componentsmax,                            &
                                     top_density0,                                       &
                                     top_energy0,                                        &
                                     top_pressure,                                       &
                                     top_viscosity,                                      &
                                     top_soundspeed,                                     &
                                     top_density1,                                       &
                                     top_energy1,                                        &
                                     top_xvel0,                                          &
                                     top_yvel0,                                          &
                                     top_xvel1,                                          &
                                     top_yvel1,                                          &
                                     top_vol_flux_x,                                     &
                                     top_vol_flux_y,                                     &
                                     top_mass_flux_x,                                    &
                                     top_mass_flux_y,                                    &
                                     top_material0,                                      &
                                     top_material1,                                      &
                                     top_mmc_index0,                                     &
                                     top_mmc_j0,                                         &
                                     top_mmc_k0,                                         &
                                     top_mmc_component0,                                 &
                                     top_mmc_material0,                                  &
                                     top_mmc_density0,                                   &
                                     top_mmc_energy0,                                    &
                                     top_mmc_pressure,                                   &
                                     top_mmc_soundspeed,                                 &
                                     top_mmc_viscosity,                                  &
                                     top_mmc_volume0,                                    &
                                     top_mmc_index1,                                     &
                                     top_mmc_j1,                                         &
                                     top_mmc_k1,                                         &
                                     top_mmc_component1,                                 &
                                     top_mmc_material1,                                  &
                                     top_mmc_density1,                                   &
                                     top_mmc_energy1,                                    &
                                     top_mmc_volume1,                                    &
                                     fields,                                             &
                                     depth                                               )

  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER     , DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0,material1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume1
  INTEGER :: top_xmin, top_xmax, top_ymin, top_ymax
  INTEGER :: top_total_mmcs0,top_total_components0
  INTEGER :: top_total_mmcs1,top_total_components1
  INTEGER :: top_total_mmcsmax,top_total_componentsmax
  REAL(KIND=8), DIMENSION(top_xmin-2:top_xmax+2,top_ymin-2:top_ymax+2) :: top_density0,top_energy0
  REAL(KIND=8), DIMENSION(top_xmin-2:top_xmax+2,top_ymin-2:top_ymax+2) :: top_pressure,top_viscosity,top_soundspeed
  REAL(KIND=8), DIMENSION(top_xmin-2:top_xmax+2,top_ymin-2:top_ymax+2) :: top_density1,top_energy1
  REAL(KIND=8), DIMENSION(top_xmin-2:top_xmax+3,top_ymin-2:top_ymax+3) :: top_xvel0,top_yvel0
  REAL(KIND=8), DIMENSION(top_xmin-2:top_xmax+3,top_ymin-2:top_ymax+3) :: top_xvel1,top_yvel1
  REAL(KIND=8), DIMENSION(top_xmin-2:top_xmax+3,top_ymin-2:top_ymax+2) :: top_vol_flux_x,top_mass_flux_x
  REAL(KIND=8), DIMENSION(top_xmin-2:top_xmax+2,top_ymin-2:top_ymax+3) :: top_vol_flux_y,top_mass_flux_y
  INTEGER     , DIMENSION(top_xmin-2:top_xmax+2,top_ymin-2:top_ymax+2) :: top_material0,top_material1
  INTEGER     , DIMENSION(1:top_total_mmcsmax)                         :: top_mmc_index0
  INTEGER     , DIMENSION(1:top_total_mmcsmax)                         :: top_mmc_component0
  INTEGER     , DIMENSION(1:top_total_mmcsmax)                         :: top_mmc_j0,top_mmc_k0
  INTEGER     , DIMENSION(1:top_total_componentsmax)                   :: top_mmc_material0
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_density0
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_energy0
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_pressure
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_viscosity
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_volume0
  INTEGER     , DIMENSION(1:top_total_mmcsmax)                         :: top_mmc_index1
  INTEGER     , DIMENSION(1:top_total_mmcsmax)                         :: top_mmc_component1
  INTEGER     , DIMENSION(1:top_total_mmcsmax)                         :: top_mmc_j1,top_mmc_k1
  INTEGER     , DIMENSION(1:top_total_componentsmax)                   :: top_mmc_material1
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_density1
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_energy1
  REAL(KIND=8), DIMENSION(1:top_total_componentsmax)                   :: top_mmc_volume1

  INTEGER :: fields(:),depth
  INTEGER :: j,k,mmc,mmc_new,component,component_new

  INTEGER :: thread_number=1

  LOGICAL :: mmc_phase

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        material0(j,y_max+k)=top_material0(j,top_ymin-1+k)
        ! Perhaps I should have the mmc stuff outside the pure loop?
        IF(top_material0(j,top_ymin-1+k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*top_material0(j,top_ymin-1+k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component0(mmc_new)=top_mmc_component0(mmc)
          ! Set acceptor data
          material0(j,y_max+k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc_new)
            mmc_material0(mmc_index0(mmc_new)+component-1)=top_mmc_material0(top_mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=top_mmc_volume0(top_mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=j
          mmc_k0(mmc_new)=y_max+k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL1).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        material1(j,y_max+k)=top_material1(j,top_ymin-1+k)
        IF(top_material1(j,top_ymin-1+k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component1(mmc_new)=top_mmc_component1(mmc)
          ! Set acceptor data
          material1(j,y_max+k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc_new)
            mmc_material1(mmc_index1(mmc_new)+component-1)=top_mmc_material1(top_mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=top_mmc_volume1(top_mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=j
          mmc_k1(mmc_new)=y_max+k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        density0(j,y_max+k)=top_density0(j,top_ymin-1+k)
        IF(mmc_phase.AND.top_material1(j,top_ymin-1+k).LT.1) THEN
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          DO component=1,mmc_component1(mmc_new)
            mmc_density0(mmc_index1(mmc_new)+component-1)=top_mmc_density0(top_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        density1(j,y_max+k)=top_density1(j,top_ymin-1+k)
        IF(mmc_phase.AND.top_material1(j,top_ymin-1+k).LT.1) THEN
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          DO component=1,mmc_component1(mmc_new)
            mmc_density1(mmc_index1(mmc_new)+component-1)=top_mmc_density1(top_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        energy0(j,y_max+k)=top_energy0(j,top_ymin-1+k)
        IF(mmc_phase.AND.top_material1(j,top_ymin-1+k).LT.1) THEN
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          DO component=1,mmc_component1(mmc_new)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=top_mmc_energy0(top_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        energy1(j,y_max+k)=top_energy1(j,top_ymin-1+k)
        IF(mmc_phase.AND.top_material1(j,top_ymin-1+k).LT.1) THEN
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          DO component=1,mmc_component1(mmc_new)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=top_mmc_energy1(top_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        pressure(j,y_max+k)=top_pressure(j,top_ymin-1+k)
        IF(mmc_phase.AND.top_material1(j,top_ymin-1+k).LT.1) THEN
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          DO component=1,mmc_component1(mmc_new)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=top_mmc_pressure(top_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        viscosity(j,y_max+k)=top_viscosity(j,top_ymin-1+k)
        IF(mmc_phase.AND.top_material1(j,top_ymin-1+k).LT.1) THEN
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          DO component=1,mmc_component1(mmc_new)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=top_mmc_viscosity(top_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        soundspeed(j,y_max+k)=top_soundspeed(j,top_ymin-1+k)
        IF(mmc_phase.AND.top_material1(j,top_ymin-1+k).LT.1) THEN
          ! Donor location
          mmc=-1*top_material1(j,top_ymin-1+k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_max+k)
          DO component=1,mmc_component1(mmc_new)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=top_mmc_soundspeed(top_mmc_index0(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL0).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        xvel0(j,y_max+1+k)=top_xvel0(j,top_ymin+1-1+k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL1).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        xvel1(j,y_max+1+k)=top_xvel1(j,top_ymin+1-1+k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL0).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        yvel0(j,y_max+1+k)=top_yvel0(j,top_ymin+1-1+k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL1).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        yvel1(j,y_max+1+k)=top_yvel1(j,top_ymin+1-1+k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        vol_flux_x(j,y_max+k)=top_vol_flux_x(j,top_ymin-1+k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        mass_flux_x(j,y_max+k)=top_mass_flux_x(j,top_ymin-1+k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max
        vol_flux_y(j,y_max+1+k)=top_vol_flux_y(j,top_ymin+1-1+k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max
        mass_flux_y(j,y_max+1+k)=top_mass_flux_y(j,top_ymin+1-1+k)
      ENDDO
    ENDDO
  ENDIF

END SUBROUTINE update_tile_halo_t_kernel

SUBROUTINE update_tile_halo_b_kernel(x_min,x_max,y_min,y_max,                            &
                                     total_mmcs0,                                        &
                                     total_components0,                                  &
                                     total_mmcs1,                                        &
                                     total_components1,                                  &
                                     total_mmcsmax,                                      &
                                     total_componentsmax,                                &
                                     number_of_threads,                                  &
                                     mmc_counter_next,                                   &
                                     component_counter_next,                             &
                                     density0,                                           &
                                     energy0,                                            &
                                     pressure,                                           &
                                     viscosity,                                          &
                                     soundspeed,                                         &
                                     density1,                                           &
                                     energy1,                                            &
                                     xvel0,                                              &
                                     yvel0,                                              &
                                     xvel1,                                              &
                                     yvel1,                                              &
                                     vol_flux_x,                                         &
                                     vol_flux_y,                                         &
                                     mass_flux_x,                                        &
                                     mass_flux_y,                                        &
                                     material0,                                          &
                                     material1,                                          &
                                     mmc_index0,                                         &
                                     mmc_j0,                                             &
                                     mmc_k0,                                             &
                                     mmc_component0,                                     &
                                     mmc_material0,                                      &
                                     mmc_density0,                                       &
                                     mmc_energy0,                                        &
                                     mmc_pressure,                                       &
                                     mmc_soundspeed,                                     &
                                     mmc_viscosity,                                      &
                                     mmc_volume0,                                        &
                                     mmc_index1,                                         &
                                     mmc_j1,                                             &
                                     mmc_k1,                                             &
                                     mmc_component1,                                     &
                                     mmc_material1,                                      &
                                     mmc_density1,                                       &
                                     mmc_energy1,                                        &
                                     mmc_volume1,                                        &
                                     bottom_xmin, bottom_xmax, bottom_ymin, bottom_ymax, &         
                                     bottom_total_mmcs0,                                 &
                                     bottom_total_components0,                           &
                                     bottom_total_mmcs1,                                 &
                                     bottom_total_components1,                           &
                                     bottom_total_mmcsmax,                               &
                                     bottom_total_componentsmax,                         &
                                     bottom_density0,                                    &
                                     bottom_energy0,                                     &
                                     bottom_pressure,                                    &
                                     bottom_viscosity,                                   &
                                     bottom_soundspeed,                                  &
                                     bottom_density1,                                    &
                                     bottom_energy1,                                     &
                                     bottom_xvel0,                                       &
                                     bottom_yvel0,                                       &
                                     bottom_xvel1,                                       &
                                     bottom_yvel1,                                       &
                                     bottom_vol_flux_x,                                  &
                                     bottom_vol_flux_y,                                  &
                                     bottom_mass_flux_x,                                 &
                                     bottom_mass_flux_y,                                 &
                                     bottom_material0,                                   &
                                     bottom_material1,                                   &
                                     bottom_mmc_index0,                                  &
                                     bottom_mmc_j0,                                      &
                                     bottom_mmc_k0,                                      &
                                     bottom_mmc_component0,                              &
                                     bottom_mmc_material0,                               &
                                     bottom_mmc_density0,                                &
                                     bottom_mmc_energy0,                                 &
                                     bottom_mmc_pressure,                                &
                                     bottom_mmc_soundspeed,                              &
                                     bottom_mmc_viscosity,                               &
                                     bottom_mmc_volume0,                                 &
                                     bottom_mmc_index1,                                  &
                                     bottom_mmc_j1,                                      &
                                     bottom_mmc_k1,                                      &
                                     bottom_mmc_component1,                              &
                                     bottom_mmc_material1,                               &
                                     bottom_mmc_density1,                                &
                                     bottom_mmc_energy1,                                 &
                                     bottom_mmc_volume1,                                 &
                                     fields,                                             &
                                     depth                                               )

  IMPLICIT NONE
  
  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcs1,total_components1
  INTEGER :: total_mmcsmax,total_componentsmax
  INTEGER :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads) :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads) :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: pressure,viscosity,soundspeed
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3) :: xvel1,yvel1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2) :: vol_flux_x,mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3) :: vol_flux_y,mass_flux_y
  INTEGER     , DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material0,material1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j0,mmc_k0
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_viscosity
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume0
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_index1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_component1
  INTEGER     , DIMENSION(1:total_mmcsmax)                 :: mmc_j1,mmc_k1
  INTEGER     , DIMENSION(1:total_componentsmax)           :: mmc_material1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)           :: mmc_volume1
  INTEGER :: bottom_xmin, bottom_xmax, bottom_ymin, bottom_ymax
  INTEGER :: bottom_total_mmcs0,bottom_total_components0
  INTEGER :: bottom_total_mmcs1,bottom_total_components1
  INTEGER :: bottom_total_mmcsmax,bottom_total_componentsmax
  REAL(KIND=8), DIMENSION(bottom_xmin-2:bottom_xmax+2,bottom_ymin-2:bottom_ymax+2) :: bottom_density0,bottom_energy0
  REAL(KIND=8), DIMENSION(bottom_xmin-2:bottom_xmax+2,bottom_ymin-2:bottom_ymax+2) :: bottom_pressure,bottom_viscosity,bottom_soundspeed
  REAL(KIND=8), DIMENSION(bottom_xmin-2:bottom_xmax+2,bottom_ymin-2:bottom_ymax+2) :: bottom_density1,bottom_energy1
  REAL(KIND=8), DIMENSION(bottom_xmin-2:bottom_xmax+3,bottom_ymin-2:bottom_ymax+3) :: bottom_xvel0,bottom_yvel0
  REAL(KIND=8), DIMENSION(bottom_xmin-2:bottom_xmax+3,bottom_ymin-2:bottom_ymax+3) :: bottom_xvel1,bottom_yvel1
  REAL(KIND=8), DIMENSION(bottom_xmin-2:bottom_xmax+3,bottom_ymin-2:bottom_ymax+2) :: bottom_vol_flux_x,bottom_mass_flux_x
  REAL(KIND=8), DIMENSION(bottom_xmin-2:bottom_xmax+2,bottom_ymin-2:bottom_ymax+3) :: bottom_vol_flux_y,bottom_mass_flux_y
  INTEGER     , DIMENSION(bottom_xmin-2:bottom_xmax+2,bottom_ymin-2:bottom_ymax+2) :: bottom_material0,bottom_material1
  INTEGER     , DIMENSION(1:bottom_total_mmcsmax)                                  :: bottom_mmc_index0
  INTEGER     , DIMENSION(1:bottom_total_mmcsmax)                                  :: bottom_mmc_component0
  INTEGER     , DIMENSION(1:bottom_total_mmcsmax)                                  :: bottom_mmc_j0,bottom_mmc_k0
  INTEGER     , DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_material0
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_density0
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_energy0
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_pressure
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_soundspeed
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_viscosity
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_volume0
  INTEGER     , DIMENSION(1:bottom_total_mmcsmax)                                  :: bottom_mmc_index1
  INTEGER     , DIMENSION(1:bottom_total_mmcsmax)                                  :: bottom_mmc_component1
  INTEGER     , DIMENSION(1:bottom_total_mmcsmax)                                  :: bottom_mmc_j1,bottom_mmc_k1
  INTEGER     , DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_material1
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_density1
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_energy1
  REAL(KIND=8), DIMENSION(1:bottom_total_componentsmax)                            :: bottom_mmc_volume1

  INTEGER :: fields(:),depth
  INTEGER :: j,k,mmc,mmc_new,component,component_new

  INTEGER :: thread_number=1

  LOGICAL :: mmc_phase

  IF(fields(FIELD_MATERIAL0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL0).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        material0(j,y_min-k)=bottom_material0(j,bottom_ymax+1-k)
        ! Perhaps I should have the mmc stuff outside the pure loop?
        IF(bottom_material0(j,bottom_ymax+1-k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*bottom_material0(j,bottom_ymax+1-k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component0(mmc_new)=bottom_mmc_component0(mmc)
          ! Set acceptor data
          material0(j,y_min-k)=-mmc_new
          mmc_index0(mmc_new)=component_new
          DO component=1,mmc_component0(mmc_new)
            mmc_material0(mmc_index0(mmc_new)+component-1)=bottom_mmc_material0(bottom_mmc_index0(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume0(mmc_index0(mmc_new)+component-1)=bottom_mmc_volume0(bottom_mmc_index0(mmc)+component-1)
          ENDDO
          mmc_j0(mmc_new)=j
          mmc_k0(mmc_new)=y_min-k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component0(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MATERIAL1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_MATERIAL1).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        material1(j,y_min-k)=bottom_material1(j,bottom_ymax+1-k)
        IF(bottom_material1(j,bottom_ymax+1-k).LT.1) THEN ! Check if donor cell is MMC
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Acceptor
          mmc_new=mmc_counter_next(thread_number) ! This should be a thread local counter
          ! And component data
          component_new=component_counter_next(thread_number)
          mmc_component1(mmc_new)=bottom_mmc_component1(mmc)
          ! Set acceptor data
          material1(j,y_min-k)=-mmc_new
          mmc_index1(mmc_new)=component_new
          DO component=1,mmc_component1(mmc_new)
            mmc_material1(mmc_index1(mmc_new)+component-1)=bottom_mmc_material1(bottom_mmc_index1(mmc)+component-1)
            ! Do I need to make this a seperate update? Sometimes material number will not change but volume fraction will
            mmc_volume1(mmc_index1(mmc_new)+component-1)=bottom_mmc_volume1(bottom_mmc_index1(mmc)+component-1)
          ENDDO
          mmc_j1(mmc_new)=j
          mmc_k1(mmc_new)=y_min-k
          mmc_counter_next(thread_number)=mmc_counter_next(thread_number)+1
          component_counter_next(thread_number)=component_counter_next(thread_number)+mmc_component1(mmc_new)
        ENDIF
      ENDDO
    ENDDO
  ENDIF

  IF(fields(FIELD_DENSITY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY0).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        density0(j,y_min-k)=bottom_density0(j,bottom_ymax+1-k)
        IF(mmc_phase.AND.bottom_material1(j,bottom_ymax+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_min-k)
          DO component=1,mmc_component0(mmc_new)
            mmc_density0(mmc_index1(mmc_new)+component-1)=bottom_mmc_density0(bottom_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_DENSITY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_DENSITY1).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        density1(j,y_min-k)=bottom_density1(j,bottom_ymax+1-k)
        IF(mmc_phase.AND.bottom_material1(j,bottom_ymax+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_min-k)
          DO component=1,mmc_component1(mmc_new)
            mmc_density1(mmc_index1(mmc_new)+component-1)=bottom_mmc_density1(bottom_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY0).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY0).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        energy0(j,y_min-k)=bottom_energy0(j,bottom_ymax+1-k)
        IF(mmc_phase.AND.bottom_material1(j,bottom_ymax+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_min-k)
          DO component=1,mmc_component1(mmc_new)
            mmc_energy0(mmc_index1(mmc_new)+component-1)=bottom_mmc_energy0(bottom_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_ENERGY1).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_ENERGY1).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        energy1(j,y_min-k)=bottom_energy1(j,bottom_ymax+1-k)
        IF(mmc_phase.AND.bottom_material1(j,bottom_ymax+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_min-k)
          DO component=1,mmc_component1(mmc_new)
            mmc_energy1(mmc_index1(mmc_new)+component-1)=bottom_mmc_energy1(bottom_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_PRESSURE).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_PRESSURE).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        pressure(j,y_min-k)=bottom_pressure(j,bottom_ymax+1-k)
        IF(mmc_phase.AND.bottom_material1(j,bottom_ymax+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_min-k)
          DO component=1,mmc_component1(mmc_new)
            mmc_pressure(mmc_index1(mmc_new)+component-1)=bottom_mmc_pressure(bottom_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VISCOSITY).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_VISCOSITY).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        viscosity(j,y_min-k)=bottom_viscosity(j,bottom_ymax+1-k)
        IF(bottom_material1(j,bottom_ymax+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_min-k)
          DO component=1,mmc_component1(mmc_new)
            mmc_viscosity(mmc_index1(mmc_new)+component-1)=bottom_mmc_viscosity(bottom_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_SOUNDSPEED).GT.0) THEN
    mmc_phase=.FALSE.
    IF(fields(FIELD_SOUNDSPEED).GT.1) mmc_phase=.TRUE.
    DO k=1,depth
      DO j=x_min, x_max
        soundspeed(j,y_min-k)=bottom_soundspeed(j,bottom_ymax+1-k)
        IF(mmc_phase.AND.bottom_material1(j,bottom_ymax+1-k).LT.1) THEN
          ! Donor location
          mmc=-1*bottom_material1(j,bottom_ymax+1-k)
          ! Set acceptor data
          mmc_new=-1*material1(j,y_min-k)
          DO component=1,mmc_component1(mmc_new)
            mmc_soundspeed(mmc_index1(mmc_new)+component-1)=bottom_mmc_soundspeed(bottom_mmc_index1(mmc)+component-1)
          ENDDO
        ENDIF
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL0).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        xvel0(j,y_min-k)=bottom_xvel0(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_XVEL1).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        xvel1(j,y_min-k)=bottom_xvel1(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL0).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        yvel0(j,y_min-k)=bottom_yvel0(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_YVEL1).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        yvel1(j,y_min-k)=bottom_yvel1(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_X).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        vol_flux_x(j,y_min-k)=bottom_vol_flux_x(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_X).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max+1
        mass_flux_x(j,y_min-k)=bottom_mass_flux_x(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_VOL_FLUX_Y).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max
        vol_flux_y(j,y_min-k)=bottom_vol_flux_y(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF
  IF(fields(FIELD_MASS_FLUX_Y).GT.0) THEN
    DO k=1,depth
      DO j=x_min, x_max
        mass_flux_y(j,y_min-k)=bottom_mass_flux_y(j,bottom_ymax+1-k)
      ENDDO
    ENDDO
  ENDIF

END SUBROUTINE update_tile_halo_b_kernel

END  MODULE update_tile_halo_kernel_module
