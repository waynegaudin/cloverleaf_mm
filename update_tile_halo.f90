!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Driver for the halo updates
!>  @author Wayne Gaudin
!>  @details Invokes the kernels for the internal and external halo cells for
!>  the fields specified.

MODULE update_tile_halo_module

CONTAINS

SUBROUTINE update_tile_halo(fields,depth,left_right)

  USE clover_module
  USE update_tile_halo_kernel_module

  IMPLICIT NONE

  INTEGER :: tile,fields(NUM_FIELDS), depth
  INTEGER :: t_left, t_right, t_up, t_down
  LOGICAL :: left_right

! Update Top Bottom - Real to Real

!$OMN PARALLEL

  IF(.NOT.left_right) THEN

!$OMN DO PRIVATE(t_up, t_down)
    DO tile=1,tiles_per_chunk
      t_up   =chunk%tiles(tile)%tile_neighbours(TILE_TOP)
      t_down =chunk%tiles(tile)%tile_neighbours(TILE_BOTTOM)
    
      IF(t_up.GT.EXTERNAL_TILE) THEN
        CALL update_tile_halo_t_kernel(chunk%tiles(tile)%t_xmin,                           &
                                       chunk%tiles(tile)%t_xmax,                           &
                                       chunk%tiles(tile)%t_ymin,                           &
                                       chunk%tiles(tile)%t_ymax,                           &
                                       chunk%tiles(tile)%field%total_mmcs0,                &
                                       chunk%tiles(tile)%field%total_components0,          &
                                       chunk%tiles(tile)%field%total_mmcs1,                &
                                       chunk%tiles(tile)%field%total_components1,          &
                                       chunk%tiles(tile)%field%total_mmcsmax,              &
                                       chunk%tiles(tile)%field%total_componentsmax,        &
                                       number_of_threads,                                  &
                                       chunk%tiles(tile)%field%mmc_counter_next,           &
                                       chunk%tiles(tile)%field%component_counter_next,     &
                                       chunk%tiles(tile)%field%density0,                   &
                                       chunk%tiles(tile)%field%energy0,                    &
                                       chunk%tiles(tile)%field%pressure,                   &
                                       chunk%tiles(tile)%field%viscosity,                  &
                                       chunk%tiles(tile)%field%soundspeed,                 &
                                       chunk%tiles(tile)%field%density1,                   &
                                       chunk%tiles(tile)%field%energy1,                    &
                                       chunk%tiles(tile)%field%xvel0,                      &
                                       chunk%tiles(tile)%field%yvel0,                      &
                                       chunk%tiles(tile)%field%xvel1,                      &
                                       chunk%tiles(tile)%field%yvel1,                      &
                                       chunk%tiles(tile)%field%vol_flux_x,                 &
                                       chunk%tiles(tile)%field%vol_flux_y,                 &
                                       chunk%tiles(tile)%field%mass_flux_x,                &
                                       chunk%tiles(tile)%field%mass_flux_y,                &
                                       chunk%tiles(tile)%field%material0,                  &
                                       chunk%tiles(tile)%field%material1,                  &
                                       chunk%tiles(tile)%field%mmc_index0,                 &
                                       chunk%tiles(tile)%field%mmc_j0,                     &
                                       chunk%tiles(tile)%field%mmc_k0,                     &
                                       chunk%tiles(tile)%field%mmc_component0,             &
                                       chunk%tiles(tile)%field%mmc_material0,              &
                                       chunk%tiles(tile)%field%mmc_density0,               &
                                       chunk%tiles(tile)%field%mmc_energy0,                &
                                       chunk%tiles(tile)%field%mmc_pressure,               &
                                       chunk%tiles(tile)%field%mmc_soundspeed,             &
                                       chunk%tiles(tile)%field%mmc_viscosity,              &
                                       chunk%tiles(tile)%field%mmc_volume0,                &
                                       chunk%tiles(tile)%field%mmc_index1,                 &
                                       chunk%tiles(tile)%field%mmc_j1,                     &
                                       chunk%tiles(tile)%field%mmc_k1,                     &
                                       chunk%tiles(tile)%field%mmc_component1,             &
                                       chunk%tiles(tile)%field%mmc_material1,              &
                                       chunk%tiles(tile)%field%mmc_density1,               &
                                       chunk%tiles(tile)%field%mmc_energy1,                &
                                       chunk%tiles(tile)%field%mmc_volume1,                &
                                       chunk%tiles(t_up)%t_xmin,                           &
                                       chunk%tiles(t_up)%t_xmax,                           &
                                       chunk%tiles(t_up)%t_ymin,                           &
                                       chunk%tiles(t_up)%t_ymax,                           &
                                       chunk%tiles(t_up)%field%total_mmcs0,                &
                                       chunk%tiles(t_up)%field%total_components0,          &
                                       chunk%tiles(t_up)%field%total_mmcs1,                &
                                       chunk%tiles(t_up)%field%total_components1,          &
                                       chunk%tiles(t_up)%field%total_mmcsmax,              &
                                       chunk%tiles(t_up)%field%total_componentsmax,        &
                                       chunk%tiles(t_up)%field%density0,                   &
                                       chunk%tiles(t_up)%field%energy0,                    &
                                       chunk%tiles(t_up)%field%pressure,                   &
                                       chunk%tiles(t_up)%field%viscosity,                  &
                                       chunk%tiles(t_up)%field%soundspeed,                 &
                                       chunk%tiles(t_up)%field%density1,                   &
                                       chunk%tiles(t_up)%field%energy1,                    &
                                       chunk%tiles(t_up)%field%xvel0,                      &
                                       chunk%tiles(t_up)%field%yvel0,                      &
                                       chunk%tiles(t_up)%field%xvel1,                      &
                                       chunk%tiles(t_up)%field%yvel1,                      &
                                       chunk%tiles(t_up)%field%vol_flux_x,                 &
                                       chunk%tiles(t_up)%field%vol_flux_y,                 &
                                       chunk%tiles(t_up)%field%mass_flux_x,                &
                                       chunk%tiles(t_up)%field%mass_flux_y,                &
                                       chunk%tiles(t_up)%field%material0,                  &
                                       chunk%tiles(t_up)%field%material1,                  &
                                       chunk%tiles(t_up)%field%mmc_index0,                 &
                                       chunk%tiles(t_up)%field%mmc_j0,                     &
                                       chunk%tiles(t_up)%field%mmc_k0,                     &
                                       chunk%tiles(t_up)%field%mmc_component0,             &
                                       chunk%tiles(t_up)%field%mmc_material0,              &
                                       chunk%tiles(t_up)%field%mmc_density0,               &
                                       chunk%tiles(t_up)%field%mmc_energy0,                &
                                       chunk%tiles(t_up)%field%mmc_pressure,               &
                                       chunk%tiles(t_up)%field%mmc_soundspeed,             &
                                       chunk%tiles(t_up)%field%mmc_viscosity,              &
                                       chunk%tiles(t_up)%field%mmc_volume0,                &
                                       chunk%tiles(t_up)%field%mmc_index1,                 &
                                       chunk%tiles(t_up)%field%mmc_j1,                     &
                                       chunk%tiles(t_up)%field%mmc_k1,                     &
                                       chunk%tiles(t_up)%field%mmc_component1,             &
                                       chunk%tiles(t_up)%field%mmc_material1,              &
                                       chunk%tiles(t_up)%field%mmc_density1,               &
                                       chunk%tiles(t_up)%field%mmc_energy1,                &
                                       chunk%tiles(t_up)%field%mmc_volume1,                &
                                       fields,                                             &
                                       depth                                               )
      END IF
    
      IF(t_down.GT.EXTERNAL_TILE) THEN
        CALL update_tile_halo_b_kernel(chunk%tiles(tile)%t_xmin,                           &
                                       chunk%tiles(tile)%t_xmax,                           &
                                       chunk%tiles(tile)%t_ymin,                           &
                                       chunk%tiles(tile)%t_ymax,                           &
                                       chunk%tiles(tile)%field%total_mmcs0,                &
                                       chunk%tiles(tile)%field%total_components0,          &
                                       chunk%tiles(tile)%field%total_mmcs1,                &
                                       chunk%tiles(tile)%field%total_components1,          &
                                       chunk%tiles(tile)%field%total_mmcsmax,              &
                                       chunk%tiles(tile)%field%total_componentsmax,        &
                                       number_of_threads,                                  &
                                       chunk%tiles(tile)%field%mmc_counter_next,           &
                                       chunk%tiles(tile)%field%component_counter_next,     &
                                       chunk%tiles(tile)%field%density0,                   &
                                       chunk%tiles(tile)%field%energy0,                    &
                                       chunk%tiles(tile)%field%pressure,                   &
                                       chunk%tiles(tile)%field%viscosity,                  &
                                       chunk%tiles(tile)%field%soundspeed,                 &
                                       chunk%tiles(tile)%field%density1,                   &
                                       chunk%tiles(tile)%field%energy1,                    &
                                       chunk%tiles(tile)%field%xvel0,                      &
                                       chunk%tiles(tile)%field%yvel0,                      &
                                       chunk%tiles(tile)%field%xvel1,                      &
                                       chunk%tiles(tile)%field%yvel1,                      &
                                       chunk%tiles(tile)%field%vol_flux_x,                 &
                                       chunk%tiles(tile)%field%vol_flux_y,                 &
                                       chunk%tiles(tile)%field%mass_flux_x,                &
                                       chunk%tiles(tile)%field%mass_flux_y,                &
                                       chunk%tiles(tile)%field%material0,                  &
                                       chunk%tiles(tile)%field%material1,                  &
                                       chunk%tiles(tile)%field%mmc_index0,                 &
                                       chunk%tiles(tile)%field%mmc_j0,                     &
                                       chunk%tiles(tile)%field%mmc_k0,                     &
                                       chunk%tiles(tile)%field%mmc_component0,             &
                                       chunk%tiles(tile)%field%mmc_material0,              &
                                       chunk%tiles(tile)%field%mmc_density0,               &
                                       chunk%tiles(tile)%field%mmc_energy0,                &
                                       chunk%tiles(tile)%field%mmc_pressure,               &
                                       chunk%tiles(tile)%field%mmc_soundspeed,             &
                                       chunk%tiles(tile)%field%mmc_viscosity,              &
                                       chunk%tiles(tile)%field%mmc_volume0,                &
                                       chunk%tiles(tile)%field%mmc_index1,                 &
                                       chunk%tiles(tile)%field%mmc_j1,                     &
                                       chunk%tiles(tile)%field%mmc_k1,                     &
                                       chunk%tiles(tile)%field%mmc_component1,             &
                                       chunk%tiles(tile)%field%mmc_material1,              &
                                       chunk%tiles(tile)%field%mmc_density1,               &
                                       chunk%tiles(tile)%field%mmc_energy1,                &
                                       chunk%tiles(tile)%field%mmc_volume1,                &
                                       chunk%tiles(t_down)%t_xmin,                         &
                                       chunk%tiles(t_down)%t_xmax,                         &
                                       chunk%tiles(t_down)%t_ymin,                         &
                                       chunk%tiles(t_down)%t_ymax,                         &
                                       chunk%tiles(t_down)%field%total_mmcs0,              &
                                       chunk%tiles(t_down)%field%total_components0,        &
                                       chunk%tiles(t_down)%field%total_mmcs1,              &
                                       chunk%tiles(t_down)%field%total_components1,        &
                                       chunk%tiles(t_down)%field%total_mmcsmax,            &
                                       chunk%tiles(t_down)%field%total_componentsmax,      &
                                       chunk%tiles(t_down)%field%density0,                 &
                                       chunk%tiles(t_down)%field%energy0,                  &
                                       chunk%tiles(t_down)%field%pressure,                 &
                                       chunk%tiles(t_down)%field%viscosity,                &
                                       chunk%tiles(t_down)%field%soundspeed,               &
                                       chunk%tiles(t_down)%field%density1,                 &
                                       chunk%tiles(t_down)%field%energy1,                  &
                                       chunk%tiles(t_down)%field%xvel0,                    &
                                       chunk%tiles(t_down)%field%yvel0,                    &
                                       chunk%tiles(t_down)%field%xvel1,                    &
                                       chunk%tiles(t_down)%field%yvel1,                    &
                                       chunk%tiles(t_down)%field%vol_flux_x,               &
                                       chunk%tiles(t_down)%field%vol_flux_y,               &
                                       chunk%tiles(t_down)%field%mass_flux_x,              &
                                       chunk%tiles(t_down)%field%mass_flux_y,              &
                                       chunk%tiles(t_down)%field%material0,                &
                                       chunk%tiles(t_down)%field%material1,                &
                                       chunk%tiles(t_down)%field%mmc_index0,               &
                                       chunk%tiles(t_down)%field%mmc_j0,                   &
                                       chunk%tiles(t_down)%field%mmc_k0,                   &
                                       chunk%tiles(t_down)%field%mmc_component0,           &
                                       chunk%tiles(t_down)%field%mmc_material0,            &
                                       chunk%tiles(t_down)%field%mmc_density0,             &
                                       chunk%tiles(t_down)%field%mmc_energy0,              &
                                       chunk%tiles(t_down)%field%mmc_pressure,             &
                                       chunk%tiles(t_down)%field%mmc_soundspeed,           &
                                       chunk%tiles(t_down)%field%mmc_viscosity,            &
                                       chunk%tiles(t_down)%field%mmc_volume0,              &
                                       chunk%tiles(t_down)%field%mmc_index1,               &
                                       chunk%tiles(t_down)%field%mmc_j1,                   &
                                       chunk%tiles(t_down)%field%mmc_k1,                   &
                                       chunk%tiles(t_down)%field%mmc_component1,           &
                                       chunk%tiles(t_down)%field%mmc_material1,            &
                                       chunk%tiles(t_down)%field%mmc_density1,             &
                                       chunk%tiles(t_down)%field%mmc_energy1,              &
                                       chunk%tiles(t_down)%field%mmc_volume1,              &
                                       fields,                                             &
                                       depth                                               )
      ENDIF
      
    ENDDO
!$OMN ENDDO

  ELSE
  
  ! Update Left Right - Ghost, Real, Ghost - > Real
  
!$OMN DO PRIVATE(t_left, t_right)
    DO tile=1,tiles_per_chunk
      t_left   =chunk%tiles(tile)%tile_neighbours(TILE_LEFT)
      t_right  =chunk%tiles(tile)%tile_neighbours(TILE_RIGHT)
      
      IF(t_left.GT.EXTERNAL_TILE) THEN
        CALL update_tile_halo_l_kernel(chunk%tiles(tile)%t_xmin,                             &
                                       chunk%tiles(tile)%t_xmax,                             &
                                       chunk%tiles(tile)%t_ymin,                             &
                                       chunk%tiles(tile)%t_ymax,                             &
                                       chunk%tiles(tile)%field%total_mmcs0,                  &
                                       chunk%tiles(tile)%field%total_components0,            &
                                       chunk%tiles(tile)%field%total_mmcs1,                  &
                                       chunk%tiles(tile)%field%total_components1,            &
                                       chunk%tiles(tile)%field%total_mmcsmax,                &
                                       chunk%tiles(tile)%field%total_componentsmax,          &
                                       number_of_threads,                                    &
                                       chunk%tiles(tile)%field%mmc_counter_next,             &
                                       chunk%tiles(tile)%field%component_counter_next,       &
                                       chunk%tiles(tile)%field%density0,                     &
                                       chunk%tiles(tile)%field%energy0,                      &
                                       chunk%tiles(tile)%field%pressure,                     &
                                       chunk%tiles(tile)%field%viscosity,                    &
                                       chunk%tiles(tile)%field%soundspeed,                   &
                                       chunk%tiles(tile)%field%density1,                     &
                                       chunk%tiles(tile)%field%energy1,                      &
                                       chunk%tiles(tile)%field%xvel0,                        &
                                       chunk%tiles(tile)%field%yvel0,                        &
                                       chunk%tiles(tile)%field%xvel1,                        &
                                       chunk%tiles(tile)%field%yvel1,                        &
                                       chunk%tiles(tile)%field%vol_flux_x,                   &
                                       chunk%tiles(tile)%field%vol_flux_y,                   &
                                       chunk%tiles(tile)%field%mass_flux_x,                  &
                                       chunk%tiles(tile)%field%mass_flux_y,                  &
                                       chunk%tiles(tile)%field%material0,                    &
                                       chunk%tiles(tile)%field%material1,                    &
                                       chunk%tiles(tile)%field%mmc_index0,                   &
                                       chunk%tiles(tile)%field%mmc_j0,                       &
                                       chunk%tiles(tile)%field%mmc_k0,                       &
                                       chunk%tiles(tile)%field%mmc_component0,               &
                                       chunk%tiles(tile)%field%mmc_material0,                &
                                       chunk%tiles(tile)%field%mmc_density0,                 &
                                       chunk%tiles(tile)%field%mmc_energy0,                  &
                                       chunk%tiles(tile)%field%mmc_pressure,                 &
                                       chunk%tiles(tile)%field%mmc_soundspeed,               &
                                       chunk%tiles(tile)%field%mmc_viscosity,                &
                                       chunk%tiles(tile)%field%mmc_volume0,                  &
                                       chunk%tiles(tile)%field%mmc_index1,                   &
                                       chunk%tiles(tile)%field%mmc_j1,                       &
                                       chunk%tiles(tile)%field%mmc_k1,                       &
                                       chunk%tiles(tile)%field%mmc_component1,               &
                                       chunk%tiles(tile)%field%mmc_material1,                &
                                       chunk%tiles(tile)%field%mmc_density1,                 &
                                       chunk%tiles(tile)%field%mmc_energy1,                  &
                                       chunk%tiles(tile)%field%mmc_volume1,                  &
                                       chunk%tiles(t_left)%t_xmin,                           &
                                       chunk%tiles(t_left)%t_xmax,                           &
                                       chunk%tiles(t_left)%t_ymin,                           &
                                       chunk%tiles(t_left)%t_ymax,                           &
                                       chunk%tiles(t_left)%field%total_mmcs0,                &
                                       chunk%tiles(t_left)%field%total_components0,          &
                                       chunk%tiles(t_left)%field%total_mmcs1,                &
                                       chunk%tiles(t_left)%field%total_components1,          &
                                       chunk%tiles(t_left)%field%total_mmcsmax,              &
                                       chunk%tiles(t_left)%field%total_componentsmax,        &
                                       chunk%tiles(t_left)%field%density0,                   &
                                       chunk%tiles(t_left)%field%energy0,                    &
                                       chunk%tiles(t_left)%field%pressure,                   &
                                       chunk%tiles(t_left)%field%viscosity,                  &
                                       chunk%tiles(t_left)%field%soundspeed,                 &
                                       chunk%tiles(t_left)%field%density1,                   &
                                       chunk%tiles(t_left)%field%energy1,                    &
                                       chunk%tiles(t_left)%field%xvel0,                      &
                                       chunk%tiles(t_left)%field%yvel0,                      &
                                       chunk%tiles(t_left)%field%xvel1,                      &
                                       chunk%tiles(t_left)%field%yvel1,                      &
                                       chunk%tiles(t_left)%field%vol_flux_x,                 &
                                       chunk%tiles(t_left)%field%vol_flux_y,                 &
                                       chunk%tiles(t_left)%field%mass_flux_x,                &
                                       chunk%tiles(t_left)%field%mass_flux_y,                &
                                       chunk%tiles(t_left)%field%material0,                  &
                                       chunk%tiles(t_left)%field%material1,                  &
                                       chunk%tiles(t_left)%field%mmc_index0,                 &
                                       chunk%tiles(t_left)%field%mmc_j0,                     &
                                       chunk%tiles(t_left)%field%mmc_k0,                     &
                                       chunk%tiles(t_left)%field%mmc_component0,             &
                                       chunk%tiles(t_left)%field%mmc_material0,              &
                                       chunk%tiles(t_left)%field%mmc_density0,               &
                                       chunk%tiles(t_left)%field%mmc_energy0,                &
                                       chunk%tiles(t_left)%field%mmc_pressure,               &
                                       chunk%tiles(t_left)%field%mmc_soundspeed,             &
                                       chunk%tiles(t_left)%field%mmc_viscosity,              &
                                       chunk%tiles(t_left)%field%mmc_volume0,                &
                                       chunk%tiles(t_left)%field%mmc_index1,                 &
                                       chunk%tiles(t_left)%field%mmc_j1,                     &
                                       chunk%tiles(t_left)%field%mmc_k1,                     &
                                       chunk%tiles(t_left)%field%mmc_component1,             &
                                       chunk%tiles(t_left)%field%mmc_material1,              &
                                       chunk%tiles(t_left)%field%mmc_density1,               &
                                       chunk%tiles(t_left)%field%mmc_energy1,                &
                                       chunk%tiles(t_left)%field%mmc_volume1,                &
                                       fields,                                               &
                                       depth                                                 )
      END IF
    
      IF(t_right.GT.EXTERNAL_TILE) THEN
        CALL update_tile_halo_r_kernel(chunk%tiles(tile)%t_xmin,                              &
                                       chunk%tiles(tile)%t_xmax,                              &
                                       chunk%tiles(tile)%t_ymin,                              &
                                       chunk%tiles(tile)%t_ymax,                              &
                                       chunk%tiles(tile)%field%total_mmcs0,                   &
                                       chunk%tiles(tile)%field%total_components0,             &
                                       chunk%tiles(tile)%field%total_mmcs1,                   &
                                       chunk%tiles(tile)%field%total_components1,             &
                                       chunk%tiles(tile)%field%total_mmcsmax,                 &
                                       chunk%tiles(tile)%field%total_componentsmax,           &
                                       number_of_threads,                                     &
                                       chunk%tiles(tile)%field%mmc_counter_next,              &
                                       chunk%tiles(tile)%field%component_counter_next,        &
                                       chunk%tiles(tile)%field%density0,                      &
                                       chunk%tiles(tile)%field%energy0,                       &
                                       chunk%tiles(tile)%field%pressure,                      &
                                       chunk%tiles(tile)%field%viscosity,                     &
                                       chunk%tiles(tile)%field%soundspeed,                    &
                                       chunk%tiles(tile)%field%density1,                      &
                                       chunk%tiles(tile)%field%energy1,                       &
                                       chunk%tiles(tile)%field%xvel0,                         &
                                       chunk%tiles(tile)%field%yvel0,                         &
                                       chunk%tiles(tile)%field%xvel1,                         &
                                       chunk%tiles(tile)%field%yvel1,                         &
                                       chunk%tiles(tile)%field%vol_flux_x,                    &
                                       chunk%tiles(tile)%field%vol_flux_y,                    &
                                       chunk%tiles(tile)%field%mass_flux_x,                   &
                                       chunk%tiles(tile)%field%mass_flux_y,                   &
                                       chunk%tiles(tile)%field%material0,                     &
                                       chunk%tiles(tile)%field%material1,                     &
                                       chunk%tiles(tile)%field%mmc_index0,                    &
                                       chunk%tiles(tile)%field%mmc_j0,                        &
                                       chunk%tiles(tile)%field%mmc_k0,                        &
                                       chunk%tiles(tile)%field%mmc_component0,                &
                                       chunk%tiles(tile)%field%mmc_material0,                 &
                                       chunk%tiles(tile)%field%mmc_density0,                  &
                                       chunk%tiles(tile)%field%mmc_energy0,                   &
                                       chunk%tiles(tile)%field%mmc_pressure,                  &
                                       chunk%tiles(tile)%field%mmc_soundspeed,                &
                                       chunk%tiles(tile)%field%mmc_viscosity,                 &
                                       chunk%tiles(tile)%field%mmc_volume0,                   &
                                       chunk%tiles(tile)%field%mmc_index1,                    &
                                       chunk%tiles(tile)%field%mmc_j1,                        &
                                       chunk%tiles(tile)%field%mmc_k1,                        &
                                       chunk%tiles(tile)%field%mmc_component1,                &
                                       chunk%tiles(tile)%field%mmc_material1,                 &
                                       chunk%tiles(tile)%field%mmc_density1,                  &
                                       chunk%tiles(tile)%field%mmc_energy1,                   &
                                       chunk%tiles(tile)%field%mmc_volume1,                   &
                                       chunk%tiles(t_right)%t_xmin,                           &
                                       chunk%tiles(t_right)%t_xmax,                           &
                                       chunk%tiles(t_right)%t_ymin,                           &
                                       chunk%tiles(t_right)%t_ymax,                           &
                                       chunk%tiles(t_right)%field%total_mmcs0,                &
                                       chunk%tiles(t_right)%field%total_components0,          &
                                       chunk%tiles(t_right)%field%total_mmcs1,                &
                                       chunk%tiles(t_right)%field%total_components1,          &
                                       chunk%tiles(t_right)%field%total_mmcsmax,              &
                                       chunk%tiles(t_right)%field%total_componentsmax,        &
                                       chunk%tiles(t_right)%field%density0,                   &
                                       chunk%tiles(t_right)%field%energy0,                    &
                                       chunk%tiles(t_right)%field%pressure,                   &
                                       chunk%tiles(t_right)%field%viscosity,                  &
                                       chunk%tiles(t_right)%field%soundspeed,                 &
                                       chunk%tiles(t_right)%field%density1,                   &
                                       chunk%tiles(t_right)%field%energy1,                    &
                                       chunk%tiles(t_right)%field%xvel0,                      &
                                       chunk%tiles(t_right)%field%yvel0,                      &
                                       chunk%tiles(t_right)%field%xvel1,                      &
                                       chunk%tiles(t_right)%field%yvel1,                      &
                                       chunk%tiles(t_right)%field%vol_flux_x,                 &
                                       chunk%tiles(t_right)%field%vol_flux_y,                 &
                                       chunk%tiles(t_right)%field%mass_flux_x,                &
                                       chunk%tiles(t_right)%field%mass_flux_y,                &
                                       chunk%tiles(t_right)%field%material0,                  &
                                       chunk%tiles(t_right)%field%material1,                  &
                                       chunk%tiles(t_right)%field%mmc_index0,                 &
                                       chunk%tiles(t_right)%field%mmc_j0,                     &
                                       chunk%tiles(t_right)%field%mmc_k0,                     &
                                       chunk%tiles(t_right)%field%mmc_component0,             &
                                       chunk%tiles(t_right)%field%mmc_material0,              &
                                       chunk%tiles(t_right)%field%mmc_density0,               &
                                       chunk%tiles(t_right)%field%mmc_energy0,                &
                                       chunk%tiles(t_right)%field%mmc_pressure,               &
                                       chunk%tiles(t_right)%field%mmc_soundspeed,             &
                                       chunk%tiles(t_right)%field%mmc_viscosity,              &
                                       chunk%tiles(t_right)%field%mmc_volume0,                &
                                       chunk%tiles(t_right)%field%mmc_index1,                 &
                                       chunk%tiles(t_right)%field%mmc_j1,                     &
                                       chunk%tiles(t_right)%field%mmc_k1,                     &
                                       chunk%tiles(t_right)%field%mmc_component1,             &
                                       chunk%tiles(t_right)%field%mmc_material1,              &
                                       chunk%tiles(t_right)%field%mmc_density1,               &
                                       chunk%tiles(t_right)%field%mmc_energy1,                &
                                       chunk%tiles(t_right)%field%mmc_volume1,                &
                                       fields,                                                &
                                       depth                                                  )
      ENDIF
    ENDDO
!$OMN END DO
  ENDIF
!$OMN END PARALLEL

END SUBROUTINE update_tile_halo

END MODULE update_tile_halo_module
