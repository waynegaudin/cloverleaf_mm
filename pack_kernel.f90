!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran mpi buffer packing kernel
!>  @author Wayne Gaudin
!>  @details Packs/unpacks mpi send and receive buffers

MODULE pack_kernel_module

CONTAINS

SUBROUTINE clover_pack_message_left(x_min,x_max,y_min,y_max,field,                &
                                    left_snd_buffer,                              &
                                    CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                    depth,field_type,                             &
                                    buffer_offset,                                &
                                    mmc_comm,                                     &
                                    mmc_buffer_offset,                            &
                                    mmc_left_snd,                                 &
                                    component_left_snd,                           &
                                    mmc_field,                                    &
                                    left_index_comp_list,                         &
                                    left_component_comp_list                      )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: left_snd_buffer(:)

  INTEGER      :: CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_type,x_min,x_max,y_min,y_max
  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,index,mm_index,buffer_offset,buffer_index,comp_index
  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: mmc_buffer_offset
  INTEGER     , OPTIONAL :: mmc_left_snd(:),component_left_snd(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: left_index_comp_list(:)
  INTEGER     , OPTIONAL :: left_component_comp_list(:)

  ! Pack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  index=buffer_offset
!!$OMP PARALLEL DO LASTPRIVATE(index)
  DO k=y_min-depth,y_max+y_inc+depth
    DO j=1,depth
      index=index+1
      left_snd_buffer(index)=field(x_min+x_inc-1+j,k)
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is packed along with the cell data

  IF(PRESENT(mmc_comm)) THEN
    index=mmc_buffer_offset ! For tiles this will not be at the end of the average data, unless 1 tile per task
    IF(mmc_comm) THEN
      DO k=1,depth
        DO j=1,mmc_left_snd(k)
          mm_index=(k-1)*(mmc_left_snd(1))+j
          mmc=left_index_comp_list(mm_index)
          DO comp=1,left_component_comp_list(mm_index)
            buffer_index=index+mm_index+comp-1
            comp_index=mmc+comp-1
            left_snd_buffer(buffer_index)=mmc_field(comp_index)
          ENDDO
          index=index+left_component_comp_list(mm_index)-1
        ENDDO
      ENDDO
    ENDIF
  ENDIF

END SUBROUTINE clover_pack_message_left

SUBROUTINE clover_unpack_message_left(x_min,x_max,y_min,y_max,field,                &
                                      left_rcv_buffer,                              &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth,field_type,                             &
                                      buffer_offset,                                &
                                      mmc_comm,                                     &
                                      mmc_buffer_offset,                            &
                                      mmc_left_rcv,                                 &
                                      component_left_rcv,                           &
                                      mmc_field,                                    &
                                      left_component_rcv_comp_list                  )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: left_rcv_buffer(:)

  INTEGER      :: CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_type,x_min,x_max,y_min,y_max
  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,index,mm_index,buffer_offset,comp_index,buffer_index
  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: mmc_buffer_offset
  INTEGER     , OPTIONAL :: mmc_left_rcv(:),component_left_rcv(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: left_component_rcv_comp_list

  ! Unpack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  index=buffer_offset
!!$OMP PARALLEL DO LASTPRIVATE(index)
  DO k=y_min-depth,y_max+y_inc+depth
    DO j=1,depth
      index=index+1
      field(x_min-j,k)=left_rcv_buffer(index)
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is unpacked along with the cell data

  IF(PRESENT(mmc_comm)) THEN
    index=mmc_buffer_offset
    IF(mmc_comm) THEN
      ! Get the first free storage location for component
      comp=left_component_rcv_comp_list
        DO k=1,depth
      DO j=1,component_left_rcv(k)
          buffer_index=index+j
          comp_index=comp+(k-1)*(component_left_rcv(1))+j-1
          mmc_field(comp_index)=left_rcv_buffer(buffer_index)
        ENDDO
        index=index+component_left_rcv(1)
      ENDDO
    ENDIF
  ENDIF

END SUBROUTINE clover_unpack_message_left

SUBROUTINE clover_pack_message_right(x_min,x_max,y_min,y_max,field,                &
                                     right_snd_buffer,                             &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth,field_type,                             &
                                     buffer_offset,                                &
                                     mmc_comm,                                     &
                                     mmc_buffer_offset,                            &
                                     mmc_right_snd,                                &
                                     component_right_snd,                          &
                                     mmc_field,                                    &
                                     right_index_comp_list,                        &
                                     right_component_comp_list                     )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: right_snd_buffer(:)

  INTEGER      :: CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_type,x_min,x_max,y_min,y_max
  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,index,mm_index,buffer_offset,buffer_index,comp_index
  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: mmc_buffer_offset
  INTEGER     , OPTIONAL :: mmc_right_snd(:),component_right_snd(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: right_index_comp_list(:)
  INTEGER     , OPTIONAL :: right_component_comp_list(:)

  ! Pack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  index=buffer_offset
!!$OMP PARALLEL DO LASTPRIVATE(index)
  DO k=y_min-depth,y_max+y_inc+depth
    DO j=1,depth
      index=index+1
      right_snd_buffer(index)=field(x_max+1-j,k)
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is packed along with the cell data

  IF(PRESENT(mmc_comm)) THEN
    index=mmc_buffer_offset ! For tiles this will not be at the end of the average data, unless 1 tile per task
    IF(mmc_comm) THEN
        DO k=1,depth
      DO j=1,mmc_right_snd(k)
          mm_index=(k-1)*(mmc_right_snd(1))+j
          mmc=right_index_comp_list(mm_index)
          DO comp=1,right_component_comp_list(mm_index)
            buffer_index=index+mm_index+comp-1
            comp_index=mmc+comp-1
            right_snd_buffer(buffer_index)=mmc_field(comp_index)
          ENDDO
          index=index+right_component_comp_list(mm_index)-1
        ENDDO
      ENDDO
    ENDIF
  ENDIF

END SUBROUTINE clover_pack_message_right

SUBROUTINE clover_unpack_message_right(x_min,x_max,y_min,y_max,field,                &
                                       right_rcv_buffer,                             &
                                       CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                       depth,field_type,                             &
                                       buffer_offset,                                &
                                       mmc_comm,                                     &
                                       mmc_buffer_offset,                            &
                                       mmc_right_rcv,                                &
                                       component_right_rcv,                          &
                                       mmc_field,                                    &
                                       right_component_rcv_comp_list                 )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: right_rcv_buffer(:)

  INTEGER      :: CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_type,x_min,x_max,y_min,y_max
  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,index,mm_index,buffer_offset,comp_index,buffer_index
  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: mmc_buffer_offset
  INTEGER     , OPTIONAL :: mmc_right_rcv(:),component_right_rcv(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: right_component_rcv_comp_list

  ! Unpack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  index=buffer_offset
!!$OMP PARALLEL DO PRIVATE(index)
  DO k=y_min-depth,y_max+y_inc+depth
    DO j=1,depth
      index=index+1
      field(x_max+x_inc+j,k)=right_rcv_buffer(index)
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is unpacked along with the cell data

  IF(PRESENT(mmc_comm)) THEN
    index=mmc_buffer_offset
    IF(mmc_comm) THEN
      ! Get the first free storage location for component
      comp=right_component_rcv_comp_list
        DO k=1,depth
      DO j=1,component_right_rcv(k)
          buffer_index=index+j
          comp_index=comp+(k-1)*(component_right_rcv(1))+j-1
          mmc_field(comp_index)=right_rcv_buffer(buffer_index)
        ENDDO
        index=index+component_right_rcv(1)
      ENDDO
    ENDIF
  ENDIF

END SUBROUTINE clover_unpack_message_right

SUBROUTINE clover_pack_message_top(tile,                                         &
                                   x_min,x_max,y_min,y_max,                      &
                                   chunk_x_min,chunk_x_max,                      &
                                   field,                                        &
                                   top_snd_buffer,                               &
                                   CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                   depth,field_id,field_type,                    &
                                   buffer_offset,                                &
                                   mmc_comm,                                     &
                                   material,                                     &
                                   mmc_index,                                    &
                                   mmc_component,                                &
                                   mmc_field,                                    &
                                   component_top_offset,                         &
                                   mmc_count_top,                                &
                                   top_component_snd_comp_list,                  &
                                   top_component_snd_comp_count                  )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: top_snd_buffer(:)

  INTEGER      :: tile,CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_type,field_id,x_min,x_max,y_min,y_max
  INTEGER      :: chunk_x_min,chunk_x_max
  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,mm_index,buffer_offset,buffer_index,comp_index

  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: material(x_min-2:x_max+2,y_min-2:y_max+2)
  INTEGER     , OPTIONAL :: mmc_index(:)
  INTEGER     , OPTIONAL :: mmc_component(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: mmc_count_top(:,:)
  INTEGER     , OPTIONAL :: component_top_offset(0:,:,:)
  INTEGER     , OPTIONAL :: top_component_snd_comp_count(:,:)
  INTEGER     , OPTIONAL :: top_component_snd_comp_list(:,:)

  INTEGER                :: halo_offset(2),comp_prev,halo_depth

  ! This is called by a single tile.
  ! A tile only contributes to the buffer it is on the top boundary.
  ! Multiple tiles can contribute to the MPI buffer.
  ! The buffer is written in mesh chunk order so it is tile agnostic.
  ! This means that data for each strip that contributes to the buffer is not
  ! neccessarily continguous in memory.

  ! Offsets have already been calculated that tell the tile where to writes in SMC and MMC data into the buffer
  ! The SMC and MMC data is not adjacent in the buffer is there is more that one field and/or more than one tile.

  ! Pack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  ! Calculate the increment in the SMC data offset when the second data layer is accessed.
  ! This uses the MPI chunk size. This allows multiple tiles to interleave their data
  ! so the MPI buffer is written in a tile agnostic manner.
  halo_offset(1)=buffer_offset
  halo_offset(2)=buffer_offset+chunk_x_max-chunk_x_min+1+x_inc

!!$OMP PARALLEL DO PRIVATE(buffer_index)
  DO k=1,depth
    DO j=x_min,x_max+x_inc
      buffer_index=halo_offset(k)+j
      top_snd_buffer(buffer_index)=field(j,y_max+1-k)
    ENDDO
!!$OMP END PARALLEL DO
  ENDDO

  ! LOOP OVER ALL THE INDICES FOR A HALO DEPTH
  IF(PRESENT(mmc_comm)) THEN
    IF(mmc_comm) THEN
      comp_prev=0
      DO halo_depth=1,depth
        DO mmc=1,mmc_count_top(tile,halo_depth)
          DO comp=1,top_component_snd_comp_count(mmc,halo_depth)
            comp_index=top_component_snd_comp_list(mmc,halo_depth)+comp-1
            buffer_index=component_top_offset(tile,field_id,halo_depth)+comp+comp_prev
            top_snd_buffer(buffer_index)=mmc_field(comp_index)
          ENDDO
          comp_prev=comp_prev+top_component_snd_comp_count(mmc,halo_depth)
        ENDDO
        comp_prev=0
      ENDDO
    ENDIF
  ENDIF
  
END SUBROUTINE clover_pack_message_top

SUBROUTINE clover_unpack_message_top(x_min,x_max,y_min,y_max,                      &
                                     chunk_x_min,chunk_x_max,                      &
                                     field,                                        &
                                     top_rcv_buffer,                               &
                                     CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                     depth,field_type,                             &
                                     buffer_offset,                                &
                                     mmc_comm,                                     &
                                     smc_offset,                                   &
                                     mmc_offset,                                   &
                                     mmc_halo_offset,                              &
                                     component_counter,                            &
                                     material,                                     &
                                     mmc_index,                                    &
                                     mmc_component,                                &
                                     component_top_rcv,                            &
                                     mmc_field,                                    &
                                     top_component_rcv_comp_list                   )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: top_rcv_buffer(:)
  INTEGER      :: CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_type,x_min,x_max,y_min,y_max,buffer_offset
  INTEGER      :: chunk_x_min,chunk_x_max

  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: smc_offset,mmc_offset,component_counter,mmc_halo_offset
  INTEGER     , OPTIONAL :: material(x_min-2:x_max+2,y_min-2:y_max+2)
  INTEGER     , OPTIONAL :: mmc_index(:)
  INTEGER     , OPTIONAL :: mmc_component(:)
  INTEGER     , OPTIONAL :: component_top_rcv(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: top_component_rcv_comp_list

  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,buffer_index,mm_index,comp_index
  INTEGER      :: mmc_count,comp_count

  INTEGER      :: halo_offset(2),comp_prev

integer      :: ci,bi

  ! Unpack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  halo_offset(1)=buffer_offset
  halo_offset(2)=buffer_offset+chunk_x_max-chunk_x_min+1+x_inc
  buffer_index=buffer_offset ! Includes tile offset already
!!$OMP PARALLEL DO LASTPRIVATE(index)
  DO k=1,depth
    DO j=x_min,x_max+x_inc
      buffer_index=halo_offset(k)+j
      field(j,y_max+y_inc+k)=top_rcv_buffer(buffer_index)
    ENDDO
!!$OMP END PARALLEL DO
  ENDDO

  ! MMC data is unpacked along with the SMC data

  IF(PRESENT(mmc_comm)) THEN
    IF(mmc_comm) THEN
      mmc_count=1
      comp_prev=0
      comp_count=0
      halo_offset(1)=0
      halo_offset(2)=mmc_halo_offset
      DO k=1,depth ! THIS SHOULD BE A MESH LOOP, IN THE CORRECT ORDER
        DO j=x_min,x_max+x_inc
          IF(material(j,y_max+k).LT.1) THEN
            mm_index=-material(j,y_max+k)
            buffer_index=smc_offset+mmc_offset+comp_prev+halo_offset(k)
            comp_index=mmc_index(mm_index)-1
            DO comp=1,mmc_component(mm_index)
ci=comp_index+comp
bi=buffer_index+comp+mmc_halo_offset
              mmc_field(comp_index+comp)=top_rcv_buffer(buffer_index+comp)
            ENDDO
            comp_prev=comp_prev+mmc_component(mm_index)
            mmc_count=mmc_count+1
            comp_count=comp_count+mmc_component(mm_index)
          ENDIF
        ENDDO
      ENDDO
    ENDIF
  ENDIF

END SUBROUTINE clover_unpack_message_top

SUBROUTINE clover_pack_message_bottom(tile,                                         &
                                      x_min,x_max,y_min,y_max,                      &
                                      chunk_x_min,chunk_x_max,                      &
                                      field,                                        &
                                      bottom_snd_buffer,                            &
                                      CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                      depth,field_id,field_type,                    &
                                      buffer_offset,                                &
                                      mmc_comm,                                     &
                                      material,                                     &
                                      mmc_index,                                    &
                                      mmc_component,                                &
                                      mmc_field,                                    &
                                      component_bottom_offset,                      &
                                      mmc_count_bottom,                             &
                                      bottom_component_snd_comp_list,               &
                                      bottom_component_snd_comp_count               )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: bottom_snd_buffer(:)

  INTEGER      :: tile,CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_id,field_type,x_min,x_max,y_min,y_max
  INTEGER      :: chunk_x_min,chunk_x_max
  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,index,mm_index,buffer_offset,buffer_index,comp_index

  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: material(x_min-2:x_max+2,y_min-2:y_max+2)
  INTEGER     , OPTIONAL :: mmc_index(:)
  INTEGER     , OPTIONAL :: mmc_component(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: mmc_count_bottom(:,:)
  INTEGER     , OPTIONAL :: component_bottom_offset(0:,:,:)
  INTEGER     , OPTIONAL :: bottom_component_snd_comp_count(:,:)
  INTEGER     , OPTIONAL :: bottom_component_snd_comp_list(:,:)


  INTEGER      :: halo_offset(2),comp_prev,halo_depth=1

  ! Pack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  halo_offset(1)=buffer_offset
  halo_offset(2)=buffer_offset+chunk_x_max-chunk_x_min+1+x_inc

!!$OMP PARALLEL DO PRIVATE(buffer_index)
  DO k=1,depth
    DO j=x_min,x_max+x_inc
      buffer_index=halo_offset(k)+j
      bottom_snd_buffer(buffer_index)=field(j,y_min+y_inc-1+k)
    ENDDO
!!$OMP END PARALLEL DO
  ENDDO

  ! LOOP OVER ALL THE INDICES FOR A HALO DEPTH
  IF(PRESENT(mmc_comm)) THEN
    IF(mmc_comm) THEN
      comp_prev=0
      DO halo_depth=1,depth
        DO mmc=1,mmc_count_bottom(tile,halo_depth)
          DO comp=1,bottom_component_snd_comp_count(mmc,halo_depth)
            comp_index=bottom_component_snd_comp_list(mmc,halo_depth)+comp-1
            buffer_index=component_bottom_offset(tile,field_id,halo_depth)+comp+comp_prev
            bottom_snd_buffer(buffer_index)=mmc_field(comp_index)
          ENDDO
          comp_prev=comp_prev+bottom_component_snd_comp_count(mmc,halo_depth)
        ENDDO
        comp_prev=0
      ENDDO
    ENDIF
  ENDIF

END SUBROUTINE clover_pack_message_bottom

SUBROUTINE clover_unpack_message_bottom(x_min,x_max,y_min,y_max,                      &
                                        chunk_x_min,chunk_x_max,                      &
                                        field,                                        &
                                        bottom_rcv_buffer,                            &
                                        CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA,&
                                        depth,field_type,                             &
                                        buffer_offset,                                &
                                        mmc_comm,                                     &
                                        smc_offset,                                   &
                                        mmc_offset,                                   &
                                        mmc_halo_offset,                              &
                                        component_counter,                            &
                                        material,                                     &
                                        mmc_index,                                    &
                                        mmc_component,                                &
                                        component_bottom_rcv,                         &
                                        mmc_field,                                    &
                                        bottom_component_rcv_comp_list                )

  IMPLICIT NONE

  REAL(KIND=8) :: field(-1:,-1:) ! This seems to work for any type of mesh data
  REAL(KIND=8) :: bottom_rcv_buffer(:)
  INTEGER      :: CELL_DATA,VERTEX_DATA,X_FACE_DATA,Y_FACE_DATA
  INTEGER      :: depth,field_type,x_min,x_max,y_min,y_max,buffer_offset
  INTEGER      :: chunk_x_min,chunk_x_max

  LOGICAL,      OPTIONAL :: mmc_comm
  INTEGER     , OPTIONAL :: smc_offset,mmc_offset,component_counter,mmc_halo_offset
  INTEGER     , OPTIONAL :: material(x_min-2:x_max+2,y_min-2:y_max+2)
  INTEGER     , OPTIONAL :: mmc_index(:)
  INTEGER     , OPTIONAL :: mmc_component(:)
  INTEGER     , OPTIONAL :: component_bottom_rcv(:)
  REAL(KIND=8), OPTIONAL :: mmc_field(:)
  INTEGER     , OPTIONAL :: bottom_component_rcv_comp_list

  INTEGER      :: j,k,comp,mmc,x_inc,y_inc,buffer_index,mm_index,comp_index
  INTEGER      :: mmc_count,comp_prev,comp_count

  INTEGER      :: halo_offset(2)

  ! Unpack 

  ! These array modifications still need to be added on, plus the donor data location changes as in update_halo
  IF(field_type.EQ.CELL_DATA) THEN
    x_inc=0
    y_inc=0
  ENDIF
  IF(field_type.EQ.VERTEX_DATA) THEN
    x_inc=1
    y_inc=1
  ENDIF
  IF(field_type.EQ.X_FACE_DATA) THEN
    x_inc=1
    y_inc=0
  ENDIF
  IF(field_type.EQ.Y_FACE_DATA) THEN
    x_inc=0
    y_inc=1
  ENDIF

  halo_offset(1)=buffer_offset
  halo_offset(2)=buffer_offset+chunk_x_max-chunk_x_min+1+x_inc

!!$OMP PARALLEL DO LASTPRIVATE(index)
  DO k=1,depth
    DO j=x_min,x_max+x_inc
      buffer_index=halo_offset(k)+j
      field(j,y_min-k)=bottom_rcv_buffer(buffer_index)
    ENDDO
!!$OMP END PARALLEL DO
  ENDDO

  ! MMC data is unpacked along with the cell data

  IF(PRESENT(mmc_comm)) THEN
    IF(mmc_comm) THEN
      mmc_count=1
      comp_prev=0
      comp_count=0
      halo_offset(1)=0
      halo_offset(2)=mmc_halo_offset
      DO k=1,depth ! THIS SHOULD BE A MESH LOOP, IN THE CORRECT ORDER
        DO j=x_min,x_max+x_inc
          IF(material(j,y_min-k).LT.1) THEN
            mm_index=-material(j,y_min-k)
            buffer_index=smc_offset+mmc_offset+comp_prev+halo_offset(k)
            comp_index=mmc_index(mm_index)-1
            DO comp=1,mmc_component(mm_index)
              mmc_field(comp_index+comp)=bottom_rcv_buffer(buffer_index+comp)
            ENDDO
            comp_prev=comp_prev+mmc_component(mm_index)
            mmc_count=mmc_count+1
            comp_count=comp_count+mmc_component(mm_index)
          ENDIF
        ENDDO
      ENDDO
    ENDIF
  ENDIF

END SUBROUTINE clover_unpack_message_bottom

SUBROUTINE clover_pack_message_left_integer(x_min,x_max,y_min,y_max,                      &
                                            integer_field,                                &
                                            mmc_integer_field,                            &
                                            mmc_component,                                &
                                            left_snd_buffer_integer,                      &
                                            depth,                                        &
                                            mmc_left_snd,                                 &
                                            buffer_offset_integer,                        &
                                            mmc_buffer_offset_integer,                    &
                                            left_index_comp_list,                         &
                                            left_component_comp_list                      )

  IMPLICIT NONE

  INTEGER :: integer_field(-1:,-1:)
  INTEGER :: mmc_integer_field(:)
  INTEGER :: mmc_component(:)
  INTEGER :: left_snd_buffer_integer(:)

  INTEGER :: depth,x_min,x_max,y_min,y_max
  INTEGER :: j,k,index,buffer_offset_integer,mmc_buffer_offset_integer,buffer_index,comp_index,mm_index,mmc,comp
  INTEGER :: mmc_left_snd(:)
  INTEGER :: left_index_comp_list(:)
  INTEGER :: left_component_comp_list(:)

  ! Pack 

  index=buffer_offset_integer
!!$OMP PARALLEL DO PRIVATE(index)
  DO k=y_min-depth,y_max+depth
    DO j=1,depth
      index=index+1
      mmc=integer_field(x_min-1+j,k)
      IF(mmc.GT.0) THEN
        left_snd_buffer_integer(index)=integer_field(x_min-1+j,k)
      ELSE
        ! Pack component as a negative number
        mmc=-1*mmc
        left_snd_buffer_integer(index)=-1*mmc_component(mmc)
      ENDIF
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is packed along with the cell data

  index=mmc_buffer_offset_integer
    DO k=1,depth
  DO j=1,mmc_left_snd(k)
      mm_index=(k-1)*(mmc_left_snd(1))+j
      mmc=left_index_comp_list(mm_index)
      DO comp=1,left_component_comp_list(mm_index)
        buffer_index=index+mm_index+comp-1
        comp_index=mmc+comp-1
        left_snd_buffer_integer(buffer_index)=mmc_integer_field(comp_index)
      ENDDO
      index=index+left_component_comp_list(mm_index)-1
    ENDDO
  ENDDO

END SUBROUTINE clover_pack_message_left_integer

SUBROUTINE clover_unpack_message_left_integer(x_min,x_max,y_min,y_max,integer_field,        &
                                              left_rcv_buffer_integer,                      &
                                              depth,                                        &
                                              mmc_left_rcv,                                 &
                                              component_left_rcv,                           &
                                              buffer_offset_integer,                        &
                                              mmc_buffer_offset_integer,                    &
                                              mmc_integer_field,                            &
                                              mmc_index,                                    &
                                              mmc_component,                                &
                                              mmc_j,                                        &
                                              mmc_k,                                        &
                                              left_index_rcv_comp_list,                     &
                                              left_component_rcv_comp_list                  )

  IMPLICIT NONE

  INTEGER      :: integer_field(-1:,-1:) ! This seems to work for any type of mesh data
  INTEGER      :: left_rcv_buffer_integer(:)

  INTEGER      :: depth,x_min,x_max,y_min,y_max
  INTEGER      :: j,k,index,buffer_offset_integer,mmc_buffer_offset_integer,comp_index,buffer_index,comp,mmc_start,index_next
  INTEGER      :: mmc_left_rcv(:),component_left_rcv(:)
  INTEGER      :: mmc_integer_field(:),mmc_component(:),mmc_j(:),mmc_k(:),mmc_index(:)
  INTEGER      :: left_index_rcv_comp_list(:),left_component_rcv_comp_list

  ! Unpack 

  mmc_start=left_index_rcv_comp_list(1)
  index_next=left_component_rcv_comp_list

  index=buffer_offset_integer
!!$OMP PARALLEL DO LASTPRIVATE(index)
  DO k=y_min-depth,y_max+depth
    DO j=1,depth
      index=index+1
      IF(left_rcv_buffer_integer(index).GT.0) THEN
        integer_field(x_min-j,k)=left_rcv_buffer_integer(index)
      ELSE
        ! Unpack component as a negative number
        integer_field(x_min-j,k)=-mmc_start
        mmc_index(mmc_start)=index_next
        mmc_component(mmc_start)=-1*left_rcv_buffer_integer(index)
        mmc_j(mmc_start)=x_min-j
        mmc_k(mmc_start)=k
        index_next=index_next-left_rcv_buffer_integer(index)
        mmc_start=mmc_start+1
      ENDIF
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is unpacked along with the cell data

  index=mmc_buffer_offset_integer
  ! Get the first free storage location for component
  comp_index=left_component_rcv_comp_list
  comp=comp_index
    DO k=1,depth
  DO j=1,component_left_rcv(k)
      buffer_index=index+j
      comp_index=comp+(k-1)*(component_left_rcv(1))+j-1
      mmc_integer_field(comp_index)=left_rcv_buffer_integer(buffer_index)
    ENDDO
    index=index+component_left_rcv(1)
  ENDDO

END SUBROUTINE clover_unpack_message_left_integer

SUBROUTINE clover_pack_message_right_integer(x_min,x_max,y_min,y_max,                      &
                                             integer_field,                                &
                                             mmc_integer_field,                            &
                                             mmc_component,                                &
                                             right_snd_buffer_integer,                     &
                                             depth,                                        &
                                             mmc_right_snd,                                &
                                             buffer_offset_integer,                        &
                                             mmc_buffer_offset_integer,                    &
                                             right_index_comp_list,                        &
                                             right_component_comp_list                     )

  IMPLICIT NONE

  INTEGER :: integer_field(-1:,-1:)
  INTEGER :: mmc_integer_field(:)
  INTEGER :: mmc_component(:)
  INTEGER :: right_snd_buffer_integer(:)

  INTEGER :: depth,x_min,x_max,y_min,y_max
  INTEGER :: j,k,index,buffer_offset_integer,mmc_buffer_offset_integer,buffer_index,comp_index,mm_index,mmc,comp
  INTEGER :: mmc_right_snd(:)
  INTEGER :: right_index_comp_list(:)
  INTEGER :: right_component_comp_list(:)

  ! Pack 

  index=buffer_offset_integer
!!$OMP PARALLEL DO PRIVATE(index)
  DO k=y_min-depth,y_max+depth
    DO j=1,depth
      index=index+1
      mmc=integer_field(x_max+1-j,k)
      IF(mmc.GT.0) THEN
        right_snd_buffer_integer(index)=mmc
      ELSE
        ! Pack component as a negative number
        mmc=-1*mmc
        right_snd_buffer_integer(index)=-1*mmc_component(mmc)
      ENDIF
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is packed along with the cell data

  index=mmc_buffer_offset_integer
    DO k=1,depth
  DO j=1,mmc_right_snd(k)
      mm_index=(k-1)*(mmc_right_snd(1))+j
      mmc=right_index_comp_list(mm_index)
      DO comp=1,right_component_comp_list(mm_index)
        buffer_index=index+mm_index+comp-1
        comp_index=mmc+comp-1
        right_snd_buffer_integer(buffer_index)=mmc_integer_field(comp_index)
      ENDDO
      index=index+right_component_comp_list(mm_index)-1
    ENDDO
  ENDDO

END SUBROUTINE clover_pack_message_right_integer

SUBROUTINE clover_unpack_message_right_integer(x_min,x_max,y_min,y_max, integer_field,      &
                                               right_rcv_buffer_integer,                    &
                                               depth,                                       &
                                               mmc_right_rcv,                               &
                                               component_right_rcv,                         &
                                               buffer_offset_integer,                       &
                                               mmc_buffer_offset_integer,                   &
                                               mmc_integer_field,                           &
                                               mmc_index,                                   &
                                               mmc_component,                               &
                                               mmc_j,                                       &
                                               mmc_k,                                       &
                                               right_index_rcv_comp_list,                   &
                                               right_component_rcv_comp_list                )

  IMPLICIT NONE

  INTEGER :: integer_field(-1:,-1:)
  INTEGER :: right_rcv_buffer_integer(:)

  INTEGER :: depth,x_min,x_max,y_min,y_max
  INTEGER :: j,k,index,buffer_offset_integer,mmc_buffer_offset_integer,comp_index,buffer_index,comp,mmc_start
  INTEGER :: mmc_integer_field(:),mmc_component(:),mmc_j(:),mmc_k(:),mmc_index(:),index_next
  INTEGER :: mmc_right_rcv(:),component_right_rcv(:)
  INTEGER :: right_component_rcv_comp_list,right_index_rcv_comp_list(:)

  ! Unpack 

  mmc_start=right_index_rcv_comp_list(1)
  index_next=right_component_rcv_comp_list

  index=buffer_offset_integer
!!$OMP PARALLEL DO PRIVATE(index)
  DO k=y_min-depth,y_max+depth
    DO j=1,depth
      index=index+1
      IF(right_rcv_buffer_integer(index).GT.0) THEN
        integer_field(x_max+j,k)=right_rcv_buffer_integer(index)
      ELSE
        ! Unpack component as a negative number
        integer_field(x_max+j,k)=-mmc_start
        mmc_index(mmc_start)=index_next
        mmc_component(mmc_start)=-1*right_rcv_buffer_integer(index)
        mmc_j(mmc_start)=x_max+j
        mmc_k(mmc_start)=k
        index_next=index_next-right_rcv_buffer_integer(index)
        mmc_start=mmc_start+1
      ENDIF
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is unpacked along with the cell data

  ! Get the first free storage location for component because the structure has changed
  index=mmc_buffer_offset_integer
  comp_index=right_component_rcv_comp_list
  comp=comp_index
    DO k=1,depth
  DO j=1,component_right_rcv(k)
      buffer_index=index+j
      comp_index=comp+(k-1)*(component_right_rcv(1))+j-1
      mmc_integer_field(comp_index)=right_rcv_buffer_integer(buffer_index)
    ENDDO
    index=index+component_right_rcv(1)
  ENDDO

END SUBROUTINE clover_unpack_message_right_integer

SUBROUTINE clover_pack_message_top_integer(tile,                                         &
                                           x_min,x_max,y_min,y_max,                      &
                                           chunk_x_min,chunk_x_max,                      &
                                           integer_field,                                &
                                           mmc_integer_field,                            &
                                           mmc_component,                                &
                                           top_snd_buffer_integer,                       &
                                           depth,                                        &
                                           mmc_count_top,                                &
                                           buffer_offset_integer,                        &
                                           component_integer_top_offset,                 &
                                           top_component_snd_comp_list,                  &
                                           top_component_snd_comp_count                  )

  IMPLICIT NONE

  INTEGER :: tile,depth,x_min,x_max,y_min,y_max
  INTEGER :: chunk_x_min,chunk_x_max
  INTEGER :: integer_field(-1:,-1:)
  INTEGER :: mmc_integer_field(:)
  INTEGER :: mmc_component(:)
  INTEGER :: component_integer_top_offset(0:,:)
  INTEGER :: top_snd_buffer_integer(:)
  INTEGER :: top_component_snd_comp_list(:,:)
  INTEGER :: top_component_snd_comp_count(:,:)
  INTEGER :: mmc_count_top(:,:)

  INTEGER :: j,k,buffer_offset_integer,buffer_index,comp_index,mm_index,mmc,comp

  INTEGER :: halo_offset(2),halo_depth,comp_prev

  ! Pack 
  halo_offset(1)=buffer_offset_integer
  halo_offset(2)=buffer_offset_integer+chunk_x_max-chunk_x_min+1

!!$OMP PARALLEL DO PRIVATE(index)
  DO k=1,depth
    DO j=x_min,x_max
      buffer_index=halo_offset(k)+j
      mmc=integer_field(j,y_max+1-k)
      IF(mmc.GT.0) THEN
        top_snd_buffer_integer(buffer_index)=mmc
      ELSE
        ! Pack component as a negative number
        mmc=-1*mmc
        top_snd_buffer_integer(buffer_index)=-1*mmc_component(mmc)
      ENDIF
    ENDDO
  ENDDO
!!$OMP END PARALLEL DO

  ! MMC data is packed along with the cell data
  
  ! LOOP OVER ALL THE INDICES FOR A HALO DEPTH
  comp_prev=0
  DO halo_depth=1,depth
    DO mmc=1,mmc_count_top(tile,halo_depth)
      DO comp=1,top_component_snd_comp_count(mmc,halo_depth)
        comp_index=top_component_snd_comp_list(mmc,halo_depth)+comp-1
        buffer_index=component_integer_top_offset(tile,halo_depth)+comp+comp_prev
        top_snd_buffer_integer(buffer_index)=mmc_integer_field(comp_index)
      ENDDO
      comp_prev=comp_prev+top_component_snd_comp_count(mmc,halo_depth)
    ENDDO
    comp_prev=0
  ENDDO

END SUBROUTINE clover_pack_message_top_integer

SUBROUTINE clover_pack_message_bottom_integer(tile,                                      &
                                              x_min,x_max,y_min,y_max,                   &
                                              chunk_x_min,chunk_x_max,                   &
                                              integer_field,                             &
                                              mmc_integer_field,                         &
                                              mmc_component,                             &
                                              bottom_snd_buffer_integer,                 &
                                              depth,                                     &
                                              mmc_count_bottom,                          &
                                              buffer_offset_integer,                     &
                                              component_integer_bottom_offset,           &
                                              bottom_component_snd_comp_list,            &
                                              bottom_component_snd_comp_count            )

  IMPLICIT NONE

  INTEGER :: tile,depth,x_min,x_max,y_min,y_max
  INTEGER :: chunk_x_min,chunk_x_max
  INTEGER :: integer_field(-1:,-1:)
  INTEGER :: mmc_integer_field(:)
  INTEGER :: mmc_component(:)
  INTEGER :: component_integer_bottom_offset(0:,:)
  INTEGER :: bottom_snd_buffer_integer(:)
  INTEGER :: bottom_component_snd_comp_list(:,:)
  INTEGER :: bottom_component_snd_comp_count(:,:)
  INTEGER :: mmc_count_bottom(:,:)

  INTEGER :: j,k,buffer_offset_integer,buffer_index,comp_index,mm_index,mmc,comp

  INTEGER :: halo_offset(2),halo_depth,comp_prev

  ! Pack 
  halo_offset(1)=buffer_offset_integer
  halo_offset(2)=buffer_offset_integer+chunk_x_max-chunk_x_min+1

!!$OMP PARALLEL DO LASTPRIVATE(index)
  DO k=1,depth
    DO j=x_min,x_max
      buffer_index=halo_offset(k)+j
      mmc=integer_field(j,y_min-1+k)
      IF(mmc.GT.0) THEN
        bottom_snd_buffer_integer(buffer_index)=mmc
      ELSE
        ! Pack component as a negative number
        mmc=-1*mmc
        bottom_snd_buffer_integer(buffer_index)=-1*mmc_component(mmc)
      ENDIF
    ENDDO
!!$OMP END PARALLEL DO
  ENDDO

  ! MMC data is packed along with the cell data
  
  ! LOOP OVER ALL THE INDICES FOR A HALO DEPTH
  comp_prev=0
  DO halo_depth=1,depth
    DO mmc=1,mmc_count_bottom(tile,halo_depth)
      DO comp=1,bottom_component_snd_comp_count(mmc,halo_depth)
        comp_index=bottom_component_snd_comp_list(mmc,halo_depth)+comp-1
        buffer_index=component_integer_bottom_offset(tile,halo_depth)+comp+comp_prev
        bottom_snd_buffer_integer(buffer_index)=mmc_integer_field(comp_index)
      ENDDO
      comp_prev=comp_prev+bottom_component_snd_comp_count(mmc,halo_depth)
    ENDDO
    comp_prev=0
  ENDDO

END SUBROUTINE clover_pack_message_bottom_integer

END MODULE pack_kernel_module
