!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran ideal gas kernel.
!>  @author Wayne Gaudin
!>  @details Calculates the pressure and sound speed for the mesh chunk using
!>  the ideal gas equation of state, with a fixed gamma of 1.4.

! I am missing a material specific EOS. Started adding this. Change gamma based
! on material number perhaps?

MODULE ideal_gas_kernel_module

CONTAINS

SUBROUTINE ideal_gas_kernel(x_min,x_max,y_min,y_max, &
                            total_mmcs,              &
                            total_components,        &
                            total_mmcsmax,           &
                            total_componentsmax,     &
                            volume,                  &
                            density,                 &
                            energy,                  &
                            pressure,                &
                            soundspeed,              &
                            material,                &
                            mmc_component,           &
                            mmc_index,               &
                            mmc_j,                   &
                            mmc_k,                   &
                            mmc_material,            &
                            mmc_density,             &
                            mmc_energy,              &
                            mmc_pressure,            &
                            mmc_soundspeed,          &
                            mmc_volume,              &
                            pressure_average,        &
                            sound_speed_average      )


  IMPLICIT NONE

  ! These limits should be the max storage, not the max filled
  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs,total_components
  INTEGER :: total_mmcsmax,total_componentsmax
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: volume
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: density
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: energy
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: pressure
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: soundspeed
  INTEGER(KIND=4), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                 :: mmc_index
  INTEGER(KIND=4), DIMENSION(1:total_mmcsmax)                 :: mmc_component
  INTEGER(KIND=4),OPTIONAL, DIMENSION(1:total_mmcsmax)        :: mmc_j,mmc_k
  INTEGER(KIND=4), DIMENSION(1:total_componentsmax)           :: mmc_material
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_density
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_energy
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_soundspeed
  REAL(KIND=8),OPTIONAL, DIMENSION(1:total_componentsmax)     :: mmc_volume
  LOGICAl :: predict,pressure_average,sound_speed_average

  LOGICAL :: multi_material=.FALSE.

  INTEGER      :: j,k,mmc,component
  REAL(KIND=8) :: mmc_average

  REAL(KIND=8) :: sound_speed_squared,v,pressurebyenergy,pressurebyvolume

  IF (total_mmcs.NE.0) multi_material=.TRUE.

!$OMP TARGET ENTER DATA MAP(TO:volume,density,energy,pressure,soundspeed          )
!$OMP TARGET ENTER DATA MAP(TO:mmc_density,mmc_energy,mmc_pressure                )
!$OMP TARGET ENTER DATA MAP(TO:mmc_soundspeed,mmc_volume,material,mmc_index       )
!$OMP TARGET ENTER DATA MAP(TO:mmc_component,mmc_j,mmc_k,mmc_material             )
!$OMP TARGET TEAMS DISTRIBUTE                                                           &
!$OMP              PARALLEL DO                                                          &
!$OMP              PRIVATE(j,k,v,pressurebyenergy,pressurebyvolume,sound_speed_squared) &
!$OMP              COLLAPSE(2)
  DO k=y_min,y_max
    DO j=x_min,x_max
      IF(material(j,k).EQ.1) THEN
        v=1.0_8/density(j,k)
        pressure(j,k)=(1.4_8-1.0_8)*density(j,k)*energy(j,k)
        pressurebyenergy=(1.4_8-1.0_8)*density(j,k)
        pressurebyvolume=-density(j,k)*pressure(j,k)
        sound_speed_squared=v*v*(pressure(j,k)*pressurebyenergy-pressurebyvolume)
        soundspeed(j,k)=SQRT(sound_speed_squared)
      ELSE!IF(material(j,k).EQ.2) THEN
        v=1.0_8/density(j,k)
        pressure(j,k)=(1.4_8-1.0_8)*density(j,k)*energy(j,k)
        pressurebyenergy=(1.4_8-1.0_8)*density(j,k)
        pressurebyvolume=-density(j,k)*pressure(j,k)
        sound_speed_squared=v*v*(pressure(j,k)*pressurebyenergy-pressurebyvolume)
        soundspeed(j,k)=SQRT(sound_speed_squared)
      ENDIF
    ENDDO
  ENDDO
!$OMP END                       &
!$OMP TARGET TEAMS DISTRIBUTE   &
!$OMP PARALLEL DO

  IF(multi_material) THEN
!$OMP TARGET TEAMS DISTRIBUTE                                                                   &
!$OMP              PARALLEL DO                                                                  &
!$OMP              PRIVATE(mmc,component,v,pressurebyenergy,pressurebyvolume,sound_speed_squared)
    DO mmc=1,total_mmcs
      DO component=mmc_index(mmc),mmc_index(mmc)+mmc_component(mmc)-1
        IF(mmc_material(component).EQ.1) THEN
          v=1.0_8/mmc_density(component)
          mmc_pressure(component)=(1.4_8-1.0_8)*mmc_density(component)*mmc_energy(component)
          pressurebyenergy=(1.4_8-1.0_8)*mmc_density(component)
          pressurebyvolume=-mmc_density(component)*mmc_pressure(component)
          sound_speed_squared=v*v*(mmc_pressure(component)*pressurebyenergy-pressurebyvolume)
          mmc_soundspeed(component)=SQRT(sound_speed_squared)
        ELSE!IF(mmc_material(component).EQ.2) THEN
          v=1.0_8/mmc_density(component)
          mmc_pressure(component)=(1.4_8-1.0_8)*mmc_density(component)*mmc_energy(component)
          pressurebyenergy=(1.4_8-1.0_8)*mmc_density(component)
          pressurebyvolume=-mmc_density(component)*mmc_pressure(component)
          sound_speed_squared=v*v*(mmc_pressure(component)*pressurebyenergy-pressurebyvolume)
          mmc_soundspeed(component)=SQRT(sound_speed_squared)
        ENDIF
      ENDDO
    ENDDO
!$OMP END                       &
!$OMP TARGET TEAMS DISTRIBUTE   &
!$OMP PARALLEL DO
  ENDIF

  IF(pressure_average) THEN
!$OMP TARGET TEAMS DISTRIBUTE                       &
!$OMP              PARALLEL DO                      &
!$OMP              PRIVATE(mmc,mmc_average,component) 
    DO mmc=1,total_mmcs
      mmc_average=0.0
      DO component=mmc_index(mmc),mmc_index(mmc)+mmc_component(mmc)-1
        mmc_average=mmc_average+mmc_pressure(component)*mmc_volume(component)
      ENDDO
      pressure(mmc_j(mmc),mmc_k(mmc))=mmc_average
    ENDDO
!$OMP END                       &
!$OMP TARGET TEAMS DISTRIBUTE   &
!$OMP PARALLEL DO
  ENDIF
  IF(sound_speed_average) THEN
!$OMP TARGET TEAMS DISTRIBUTE                       &
!$OMP              PARALLEL DO                      &
!$OMP              PRIVATE(mmc,mmc_average,component) 
    DO mmc=1,total_mmcs
      mmc_average=0.0
      DO component=mmc_index(mmc),mmc_index(mmc)+mmc_component(mmc)-1
        mmc_average=mmc_average+mmc_soundspeed(component)**2.0_8*mmc_volume(component) &
                   *mmc_density(component)*volume(mmc_j(mmc),mmc_k(mmc))
      ENDDO
      soundspeed(mmc_j(mmc),mmc_k(mmc))=SQRT(mmc_average/(density(mmc_j(mmc),mmc_k(mmc))&
                                       *volume(mmc_j(mmc),mmc_k(mmc))))
    ENDDO
!$OMP END                       &
!$OMP TARGET TEAMS DISTRIBUTE   &
!$OMP PARALLEL DO
  ENDIF
!$OMP TARGET EXIT DATA MAP (FROM:pressure,soundspeed,mmc_pressure,mmc_soundspeed)

END SUBROUTINE ideal_gas_kernel

END MODULE ideal_gas_kernel_module
