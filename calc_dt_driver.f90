!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief standalone driver for the calc_dt kernels
!>  @author Wayne Gaudin
!>  @details Calls user requested kernel in standalone mode

PROGRAM calc_dt_driver

  USE set_data_module
  USE calc_dt_kernel_module
  USE calc_dt_kernel_single_mat_module

  IMPLICIT NONE

!$ INTEGER :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM

  INTEGER :: numargs,iargc,i

  CHARACTER (LEN=20)  :: command_line,temp

  INTEGER :: x_size,y_size

  REAL(KIND=8) :: kernel_time,timer,calc_dt_time

  LOGICAL :: use_fortran_kernels,multi_material
  INTEGER :: x_min,x_max,y_min,y_max,its,iteration
  REAL(KIND=8) :: dt,g_small,g_big,dtmin,dtc_safe,dtu_safe,dtv_safe,dtdiv_safe,xl_pos,yl_pos,local_dt
  INTEGER :: l_control,jldt,kldt,small,mm
  REAL(KIND=8)   ,ALLOCATABLE :: xarea(:,:),yarea(:,:),volume(:,:)
  REAL(KIND=8)   ,ALLOCATABLE :: cellx(:),celly(:)
  REAL(KIND=8)   ,ALLOCATABLE :: celldx(:),celldy(:)
  REAL(KIND=8)   ,ALLOCATABLE :: density0(:,:),energy0(:,:)
  REAL(KIND=8)   ,ALLOCATABLE :: pressure(:,:),soundspeed(:,:),viscosity(:,:)
  REAL(KIND=8)   ,ALLOCATABLE :: mmc_density0(:),mmc_energy0(:),mmc_volume0(:)
  REAL(KIND=8)   ,ALLOCATABLE :: mmc_pressure(:),mmc_soundspeed(:),mmc_viscosity(:)
  REAL(KIND=8)   ,ALLOCATABLE :: xvel0(:,:),yvel0(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_component0(:),mmc_material0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_j0(:),mmc_k0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_index0(:)
  INTEGER(KIND=4),ALLOCATABLE :: material0(:,:)
  INTEGER(KIND=4)             :: total_mmcs0,total_components0

!$OMP PARALLEL
!$  IF(OMP_GET_THREAD_NUM().EQ.0) THEN
!$    WRITE(*,'(a15,i5)') 'Thread Count: ',OMP_GET_NUM_THREADS()
!$  ENDIF

!$OMP END PARALLEL
  x_size=100
  y_size=100
  its=1
  use_fortran_kernels=.TRUE.
  multi_material=.FALSE.
  mm=0
  g_small    =1.0e-16
  g_big      =1.0e+21
  dtmin      =0.0000001_8
  dtc_safe   =0.7_8
  dtu_safe   =0.5_8
  dtv_safe   =0.5_8
  dtdiv_safe =0.7_8

  numargs = iargc()

  DO i=1,numargs,2
    CALL GETARG(i,command_line)
    SELECT CASE (command_line)
      CASE("-help")
        WRITE(*,*) "Usage -nx 100 -ny 100 -its 10 -multimat 0|1"
        STOP
      CASE("-nx")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") x_size
      CASE("-ny")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") y_size
      CASE("-its")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") its
      CASE("-multimat")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") mm
    END SELECT
  ENDDO

  x_min=1
  y_min=1
  x_max=x_size
  y_max=y_size

  IF(mm.NE.0)multi_material=.TRUE.

  WRITE(*,*) "Calc dt Kernel"
  WRITE(*,*) "Mesh size ",x_size,y_size
  WRITE(*,*) "Iterations ",its
  IF(multi_material) WRITE(*,*)"Multi material"
  IF(.NOT.multi_material) WRITE(*,*)"Single material"

  kernel_time=timer()

  CALL set_data(x_min,x_max,y_min,y_max,            &
                multi_material,                     &
                xarea=xarea,                        &
                yarea=yarea,                        &
                cellx=cellx,                        &
                celly=celly,                        &
                celldx=celldx,                      &
                celldy=celldy,                      &
                volume=volume,                      &
                density0=density0,                  &
                energy0=energy0,                    &
                pressure=pressure,                  &
                soundspeed=soundspeed,              &
                viscosity=viscosity,                &
                xvel0=xvel0,                        &
                yvel0=yvel0,                        &
                material0=material0,                &
                mmc_component0=mmc_component0,      &
                mmc_index0=mmc_index0,              &
                mmc_j0=mmc_j0,                      &
                mmc_k0=mmc_k0,                      &
                mmc_material0=mmc_material0,        &
                mmc_density0=mmc_density0,          &
                mmc_energy0=mmc_energy0,            &
                mmc_pressure=mmc_pressure,          &
                mmc_soundspeed=mmc_soundspeed,      &
                mmc_viscosity=mmc_viscosity,        &
                mmc_volume0=mmc_volume0,            &
                total_mmcs0=total_mmcs0,            &
                total_components0=total_components0,&
                dt=dt                               )

  WRITE(*,*) "Setup time ",timer()-kernel_time

  WRITE(*,*) "Data initialised"

  kernel_time=timer()

  DO iteration=1,its
    CALL calc_dt_kernel(x_min,             &
                        x_max,             &
                        y_min,             &
                        y_max,             &
                        total_mmcs0,       &
                        total_components0, &
                        g_small,           &
                        g_big,             &
                        dtmin,             &
                        dtc_safe,          &
                        dtu_safe,          &
                        dtv_safe,          &
                        dtdiv_safe,        &
                        xarea,             &
                        yarea,             &
                        cellx,             &
                        celly,             &
                        celldx,            &
                        celldy,            &
                        volume,            &
                        density0,          &
                        energy0,           &
                        pressure,          &
                        viscosity,         &
                        soundspeed,        &
                        xvel0,             &
                        yvel0,             &
                        material0,         &
                        mmc_j0,            &
                        mmc_k0,            &
                        mmc_component0,    &
                        mmc_index0,        &
                        mmc_material0,     &
                        mmc_volume0,       &
                        mmc_density0,      &
                        mmc_energy0,       &
                        mmc_pressure,      &
                        mmc_soundspeed,    &
                        mmc_viscosity,     &
                        local_dt,          &
                        xl_pos,            &
                        yl_pos,            &
                        jldt,              &
                        kldt,              &
                        small              )
  ENDDO

  calc_dt_time=(timer()-kernel_time)

  WRITE(*,*)
  WRITE(*,*) "Multi material calc dt time    ",calc_dt_time  
  WRITE(*,*) "local_dt                       ",local_dt

  kernel_time=timer()

  DO iteration=1,its
    CALL calc_dt_kernel_single_mat(x_min,      &
                                   x_max,      &
                                   y_min,      &
                                   y_max,      &
                                   g_small,    &
                                   g_big,      &
                                   dtmin,      &
                                   dtc_safe,   &
                                   dtu_safe,   &
                                   dtv_safe,   &
                                   dtdiv_safe, &
                                   xarea,      &
                                   yarea,      &
                                   cellx,      &
                                   celly,      &
                                   celldx,     &
                                   celldy,     &
                                   volume,     &
                                   density0,   &
                                   energy0,    &
                                   pressure,   &
                                   viscosity,  &
                                   soundspeed, &
                                   xvel0,      &
                                   yvel0,      &
                                   local_dt,   &
                                   l_control,  &
                                   xl_pos,     &
                                   yl_pos,     &
                                   jldt,       &
                                   kldt,       &
                                   small       )
  ENDDO

  calc_dt_time=(timer()-kernel_time)

  WRITE(*,*)
  WRITE(*,*) "Single material calc dt time   ",calc_dt_time  
  WRITE(*,*) "local_dt                       ",local_dt

  DEALLOCATE(xarea)
  DEALLOCATE(yarea)
  DEALLOCATE(cellx)
  DEALLOCATE(celly)
  DEALLOCATE(celldx)
  DEALLOCATE(celldy)
  DEALLOCATE(volume)
  DEALLOCATE(density0)
  DEALLOCATE(energy0)
  DEALLOCATE(pressure)
  DEALLOCATE(soundspeed)
  DEALLOCATE(viscosity)
  DEALLOCATE(xvel0)
  DEALLOCATE(yvel0)
  DEALLOCATE(material0)

END PROGRAM calc_dt_driver
