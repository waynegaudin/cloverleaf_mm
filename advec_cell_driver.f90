!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Cell centred advection driver.
!>  @author Wayne Gaudin
!>  @details Invokes the user selected advection kernel.

MODULE  advec_cell_driver_module

CONTAINS

SUBROUTINE advec_cell_driver(tile,sweep_number,dir)

  USE clover_module
  USE advec_mmc_x_cell_module
  USE advec_mmc_y_cell_module
  USE advec_cell_kernel_module

  IMPLICIT NONE

  INTEGER :: tile,sweep_number,dir

  LOGICAL :: reallocation_needed

  reallocation_needed=.TRUE.

  IF(dir.EQ.1.AND.chunk%tiles(tile)%field%max_mats.GT.1) THEN
    DO WHILE(reallocation_needed)
      ! Allocate dv,de,dm arrays
      CALL advec_mmc_x_cell_kernel(chunk%tiles(tile)%t_xmin,                      &
                                   chunk%tiles(tile)%t_xmax,                      &
                                   chunk%tiles(tile)%t_ymin,                      &
                                   chunk%tiles(tile)%t_ymax,                      &
                                   sweep_number,                                  &
                                   chunk%tiles(tile)%field%max_mats,              &
                                   chunk%tiles(tile)%field%max_mat_number,        &
                                   chunk%tiles(tile)%field%mat_list,              &
                                   chunk%tiles(tile)%field%reverse_mat_list,      &
                                   chunk%tiles(tile)%field%total_mmcs1,           &
                                   chunk%tiles(tile)%field%total_components1,     &
                                   chunk%tiles(tile)%field%total_mmcsmax,         &
                                   chunk%tiles(tile)%field%total_componentsmax,   &
                                   chunk%tiles(tile)%field%total_mmcs_halo,       &
                                   chunk%tiles(tile)%field%total_components_halo, &
                                   chunk%tiles(tile)%field%total_mmcsnew,         &
                                   chunk%tiles(tile)%field%total_componentsnew,   &
                                   chunk%tiles(tile)%field%max_comps,             &
                                   number_of_threads,                             &
                                   chunk%tiles(tile)%field%mmc_counter_next,      &
                                   chunk%tiles(tile)%field%component_counter_next,&
                                   chunk%tiles(tile)%field%celldx,                &
                                   chunk%tiles(tile)%field%celldy,                &
                                   chunk%tiles(tile)%field%vertexdx,              &
                                   chunk%tiles(tile)%field%vertexdy,              &
                                   chunk%tiles(tile)%field%volume,                &
                                   chunk%tiles(tile)%field%density1,              &
                                   chunk%tiles(tile)%field%densitynew,            &
                                   chunk%tiles(tile)%field%energy1,               &
                                   chunk%tiles(tile)%field%energynew,             &
                                   chunk%tiles(tile)%field%mass_flux_x,           &
                                   chunk%tiles(tile)%field%vol_flux_x,            &
                                   chunk%tiles(tile)%field%mass_flux_y,           &
                                   chunk%tiles(tile)%field%vol_flux_y,            &
                                   chunk%tiles(tile)%field%work_array1,           &
                                   chunk%tiles(tile)%field%work_array2,           &
                                   chunk%tiles(tile)%field%work_array3,           &
                                   chunk%tiles(tile)%field%dv,                    &
                                   chunk%tiles(tile)%field%dm,                    &
                                   chunk%tiles(tile)%field%de,                    &
                                   chunk%tiles(tile)%field%material1,             &
                                   chunk%tiles(tile)%field%materialnew,           &
                                   chunk%tiles(tile)%field%mmc_material1,         &
                                   chunk%tiles(tile)%field%mmc_materialnew,       &
                                   chunk%tiles(tile)%field%mmc_index1,            &
                                   chunk%tiles(tile)%field%mmc_indexnew,          &
                                   chunk%tiles(tile)%field%mmc_j1,                &
                                   chunk%tiles(tile)%field%mmc_k1,                &
                                   chunk%tiles(tile)%field%mmc_jnew,              &
                                   chunk%tiles(tile)%field%mmc_knew,              &
                                   chunk%tiles(tile)%field%mmc_component1,        &
                                   chunk%tiles(tile)%field%mmc_componentnew,      &
                                   chunk%tiles(tile)%field%mmc_density1,          &
                                   chunk%tiles(tile)%field%mmc_densitynew,        &
                                   chunk%tiles(tile)%field%mmc_energy1,           &
                                   chunk%tiles(tile)%field%mmc_energynew,         &
                                   chunk%tiles(tile)%field%mmc_volume1,           &
                                   chunk%tiles(tile)%field%mmc_volumenew,         &
                                   chunk%tiles(tile)%field%mmc_start_index,       &
                                   chunk%tiles(tile)%field%mmc_index,             &
                                   chunk%tiles(tile)%field%comp_start_index,      &
                                   chunk%tiles(tile)%field%comp_index,            &
                                   reallocation_needed                            )
      ! This is a failure of storage in the advection
      IF(reallocation_needed) CALL reallocate_mmc_storage(tile)
    ENDDO

    chunk%tiles(tile)%field%mmc_index=chunk%tiles(tile)%field%mmc_start_index
    chunk%tiles(tile)%field%comp_index=chunk%tiles(tile)%field%comp_start_index

    ! Now check if we are close to storage limits because halo updates will need free storage
    !IF(chunk%tiles(tile)%field%total_mmcs1*2.GT.chunk%tiles(tile)%field%total_mmcsmax) reallocation_needed=.TRUE.
    !IF(chunk%tiles(tile)%field%total_components1*2.GT.chunk%tiles(tile)%field%total_componentsmax) reallocation_needed=.TRUE.
    !IF(reallocation_needed) CALL reallocate_mmc_storage(tile)
    ! Deallocate dv,de,dm arrays

  ELSEIF(dir.EQ.2.AND.chunk%tiles(tile)%field%max_mats.GT.1) THEN
    DO WHILE(reallocation_needed)
      ! Allocate dv,de,dm arrays
      CALL advec_mmc_y_cell_kernel(chunk%tiles(tile)%t_xmin,                      &
                                   chunk%tiles(tile)%t_xmax,                      &
                                   chunk%tiles(tile)%t_ymin,                      &
                                   chunk%tiles(tile)%t_ymax,                      &
                                   sweep_number,                                  &
                                   chunk%tiles(tile)%field%max_mats,              &
                                   chunk%tiles(tile)%field%max_mat_number,        &
                                   chunk%tiles(tile)%field%mat_list,              &
                                   chunk%tiles(tile)%field%reverse_mat_list,      &
                                   chunk%tiles(tile)%field%total_mmcs1,           &
                                   chunk%tiles(tile)%field%total_components1,     &
                                   chunk%tiles(tile)%field%total_mmcsmax,         &
                                   chunk%tiles(tile)%field%total_componentsmax,   &
                                   chunk%tiles(tile)%field%total_mmcs_halo,       &
                                   chunk%tiles(tile)%field%total_components_halo, &
                                   chunk%tiles(tile)%field%total_mmcsnew,         &
                                   chunk%tiles(tile)%field%total_componentsnew,   &
                                   chunk%tiles(tile)%field%max_comps,             &
                                   number_of_threads,                             &
                                   chunk%tiles(tile)%field%mmc_counter_next,      &
                                   chunk%tiles(tile)%field%component_counter_next,&
                                   chunk%tiles(tile)%field%celldx,                &
                                   chunk%tiles(tile)%field%celldy,                &
                                   chunk%tiles(tile)%field%vertexdx,              &
                                   chunk%tiles(tile)%field%vertexdy,              &
                                   chunk%tiles(tile)%field%volume,                &
                                   chunk%tiles(tile)%field%density1,              &
                                   chunk%tiles(tile)%field%densitynew,            &
                                   chunk%tiles(tile)%field%energy1,               &
                                   chunk%tiles(tile)%field%energynew,             &
                                   chunk%tiles(tile)%field%mass_flux_x,           &
                                   chunk%tiles(tile)%field%vol_flux_x,            &
                                   chunk%tiles(tile)%field%mass_flux_y,           &
                                   chunk%tiles(tile)%field%vol_flux_y,            &
                                   chunk%tiles(tile)%field%work_array1,           &
                                   chunk%tiles(tile)%field%work_array2,           &
                                   chunk%tiles(tile)%field%work_array3,           &
                                   chunk%tiles(tile)%field%dv,                    &
                                   chunk%tiles(tile)%field%dm,                    &
                                   chunk%tiles(tile)%field%de,                    &
                                   chunk%tiles(tile)%field%material1,             &
                                   chunk%tiles(tile)%field%materialnew,           &
                                   chunk%tiles(tile)%field%mmc_material1,         &
                                   chunk%tiles(tile)%field%mmc_materialnew,       &
                                   chunk%tiles(tile)%field%mmc_index1,            &
                                   chunk%tiles(tile)%field%mmc_indexnew,          &
                                   chunk%tiles(tile)%field%mmc_j1,                &
                                   chunk%tiles(tile)%field%mmc_k1,                &
                                   chunk%tiles(tile)%field%mmc_jnew,              &
                                   chunk%tiles(tile)%field%mmc_knew,              &
                                   chunk%tiles(tile)%field%mmc_component1,        &
                                   chunk%tiles(tile)%field%mmc_componentnew,      &
                                   chunk%tiles(tile)%field%mmc_density1,          &
                                   chunk%tiles(tile)%field%mmc_densitynew,        &
                                   chunk%tiles(tile)%field%mmc_energy1,           &
                                   chunk%tiles(tile)%field%mmc_energynew,         &
                                   chunk%tiles(tile)%field%mmc_volume1,           &
                                   chunk%tiles(tile)%field%mmc_volumenew,         &
                                   chunk%tiles(tile)%field%mmc_start_index,       &
                                   chunk%tiles(tile)%field%mmc_index,             &
                                   chunk%tiles(tile)%field%comp_start_index,      &
                                   chunk%tiles(tile)%field%comp_index,            &
                                   reallocation_needed                            )
      ! This is a failure of storage in the advection
      IF(reallocation_needed) CALL reallocate_mmc_storage(tile)
    ENDDO

    chunk%tiles(tile)%field%mmc_index=chunk%tiles(tile)%field%mmc_start_index
    chunk%tiles(tile)%field%comp_index=chunk%tiles(tile)%field%comp_start_index

    ! Now check if we are close to storage limits because halo updates will need free storage
    !IF(chunk%tiles(tile)%field%total_mmcs1*2.GT.chunk%tiles(tile)%field%total_mmcsmax) reallocation_needed=.TRUE.
    !IF(chunk%tiles(tile)%field%total_components1*2.GT.chunk%tiles(tile)%field%total_componentsmax) reallocation_needed=.TRUE.
    !IF(reallocation_needed) CALL reallocate_mmc_storage(tile)
    ! Deallocate dv,de,dm arrays
  ELSE
    ! Allow this to kick in for single material tiles
    CALL advec_cell_kernel(chunk%tiles(tile)%t_xmin,                      &
                           chunk%tiles(tile)%t_xmax,                      &
                           chunk%tiles(tile)%t_ymin,                      &
                           chunk%tiles(tile)%t_ymax,                      &
                           dir,                                           &
                           sweep_number,                                  &
                           chunk%tiles(tile)%field%vertexdx,              &
                           chunk%tiles(tile)%field%vertexdy,              &
                           chunk%tiles(tile)%field%volume,                &
                           chunk%tiles(tile)%field%density1,              &
                           chunk%tiles(tile)%field%energy1,               &
                           chunk%tiles(tile)%field%mass_flux_x,           &
                           chunk%tiles(tile)%field%vol_flux_x,            &
                           chunk%tiles(tile)%field%mass_flux_y,           &
                           chunk%tiles(tile)%field%vol_flux_y,            &
                           chunk%tiles(tile)%field%work_array1,           &
                           chunk%tiles(tile)%field%work_array2,           &
                           chunk%tiles(tile)%field%work_array3            )
  ENDIF

END SUBROUTINE advec_cell_driver

SUBROUTINE reallocate_mmc_storage(tile)

  !This should be a proper kernel so it can be threaded etc

  USE clover_module

  IMPLICIT NONE

  INTEGER :: tile

  INTEGER :: mmc_expansion_factor,comp_expansion_factor,thread
  INTEGER :: total_mmcsmax_old,total_componentsmax_old

  INTEGER,ALLOCATABLE, DIMENSION(:)         :: mmc_length,comp_length

  mmc_expansion_factor=2
  comp_expansion_factor=2

  ALLOCATE(mmc_length(0:number_of_threads))
  ALLOCATE(comp_length(0:number_of_threads))

  ! Allocate new arrays THIS NEED DOING ON THE TARGET
  DEALLOCATE(chunk%tiles(tile)%field%mmc_jnew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_knew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_indexnew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_componentnew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_volumenew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_materialnew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_densitynew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_energynew)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_pressure)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_soundspeed)
  total_mmcsmax_old=chunk%tiles(tile)%field%total_mmcsmax
  total_componentsmax_old=chunk%tiles(tile)%field%total_componentsmax
  chunk%tiles(tile)%field%total_mmcsmax=chunk%tiles(tile)%field%total_mmcsmax*mmc_expansion_factor
  chunk%tiles(tile)%field%total_componentsmax=chunk%tiles(tile)%field%total_componentsmax*comp_expansion_factor
  ALLOCATE(chunk%tiles(tile)%field%mmc_jnew(1:chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_knew(1:chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_indexnew(1:chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_componentnew(1:chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_volumenew(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_materialnew(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_densitynew(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_energynew(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_pressure(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_soundspeed(1:chunk%tiles(tile)%field%total_componentsmax))
  ! Copy data THIS NEEDS THREADING
  chunk%tiles(tile)%field%mmc_jnew(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_j1(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_knew(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_k1(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_indexnew(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_index1(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_componentnew(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_component1(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_volumenew(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_volume1(1:chunk%tiles(tile)%field%total_components1)
  chunk%tiles(tile)%field%mmc_materialnew(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_material1(1:chunk%tiles(tile)%field%total_components1)
  chunk%tiles(tile)%field%mmc_densitynew(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_density1(1:chunk%tiles(tile)%field%total_components1)
  chunk%tiles(tile)%field%mmc_energynew(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_energy1(1:chunk%tiles(tile)%field%total_components1)
  ! Deallocate old arrays and allocate with new storage limits
  DEALLOCATE(chunk%tiles(tile)%field%mmc_j1)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_k1)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_index1)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_component1)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_volume1)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_material1)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_density1)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_energy1)
  ALLOCATE(chunk%tiles(tile)%field%mmc_j1(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_k1(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_index1(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_component1(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_volume1(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_material1(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_density1(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_energy1(1:chunk%tiles(tile)%field%total_componentsmax))
  DEALLOCATE(chunk%tiles(tile)%field%mmc_j0)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_k0)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_index0)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_component0)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_volume0)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_material0)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_density0)
  DEALLOCATE(chunk%tiles(tile)%field%mmc_energy0)
  ALLOCATE(chunk%tiles(tile)%field%mmc_j0(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_k0(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_index0(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_component0(chunk%tiles(tile)%field%total_mmcsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_volume0(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_material0(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_density0(1:chunk%tiles(tile)%field%total_componentsmax))
  ALLOCATE(chunk%tiles(tile)%field%mmc_energy0(1:chunk%tiles(tile)%field%total_componentsmax))
  chunk%tiles(tile)%field%mmc_j1(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_jnew(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_k1(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_knew(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_index1(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_indexnew(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_component1(1:chunk%tiles(tile)%field%total_mmcs1)=chunk%tiles(tile)%field%mmc_componentnew(1:chunk%tiles(tile)%field%total_mmcs1)
  chunk%tiles(tile)%field%mmc_volume1(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_volumenew(1:chunk%tiles(tile)%field%total_components1)
  chunk%tiles(tile)%field%mmc_material1(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_materialnew(1:chunk%tiles(tile)%field%total_components1)
  chunk%tiles(tile)%field%mmc_density1(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_densitynew(1:chunk%tiles(tile)%field%total_components1)
  chunk%tiles(tile)%field%mmc_energy1(1:chunk%tiles(tile)%field%total_components1)=chunk%tiles(tile)%field%mmc_energynew(1:chunk%tiles(tile)%field%total_components1)
  ! Shift MMC thread data pointers in proportion to their current storage
  DO thread=1,number_of_threads-1
    mmc_length(thread)=chunk%tiles(tile)%field%mmc_start_index(thread+1)-chunk%tiles(tile)%field%mmc_start_index(thread)
    comp_length(thread)=chunk%tiles(tile)%field%comp_start_index(thread+1)-chunk%tiles(tile)%field%comp_start_index(thread)
  ENDDO
  ! Last thread
  mmc_length(number_of_threads)=MAX(0,total_mmcsmax_old-chunk%tiles(tile)%field%mmc_start_index(number_of_threads))
  comp_length(number_of_threads)=MAX(0,total_componentsmax_old-chunk%tiles(tile)%field%comp_start_index(number_of_threads))
! the 0.95 below is just to prevent a round off issue I can't quite squash
  DO thread=1,number_of_threads
    chunk%tiles(tile)%field%mmc_start_index(thread)=1+0.95*mmc_expansion_factor*SUM(mmc_length(1:thread))
    chunk%tiles(tile)%field%comp_start_index(thread)=1+0.95*comp_expansion_factor*SUM(comp_length(1:thread))
  ENDDO
  chunk%tiles(tile)%field%mmc_index=chunk%tiles(tile)%field%mmc_start_index
  chunk%tiles(tile)%field%comp_index=chunk%tiles(tile)%field%comp_start_index

END SUBROUTINE reallocate_mmc_storage

END MODULE  advec_cell_driver_module

