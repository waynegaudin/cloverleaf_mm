!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Top level advection driver
!>  @author Wayne Gaudin
!>  @details Controls the advection step and invokes required communications.

MODULE advection_module

CONTAINS

SUBROUTINE advection()

  USE clover_module
  USE advec_cell_driver_module
  USE advec_mom_driver_module
  USE update_halo_module

  IMPLICIT NONE

  INTEGER :: sweep_number,direction,tile

  INTEGER :: xvel,yvel

  INTEGER :: fields(NUM_FIELDS)

  REAL(KIND=8) :: kernel_time,timer

  INTEGER :: j,k,mat,component

  INTEGER,ALLOCATABLE, DIMENSION(:)         :: material
  LOGICAL,ALLOCATABLE, DIMENSION(:)         :: unique_material

  ! I don't need keep allocating these
  ALLOCATE(unique_material(MAXVAL(states(:)%material)))
  ALLOCATE(material(MAXVAL(states(:)%material)))
  material(states(:)%material)=states(:)%material
  
  sweep_number=1
  IF(advect_x)      direction=g_xdir
  IF(.not.advect_x) direction=g_ydir
  xvel=g_xdir
  yvel=g_ydir

  fields=0
  fields(FIELD_MATERIAL1)=3
  fields(FIELD_ENERGY1)=3
  fields(FIELD_DENSITY1)=3
  fields(FIELD_VOL_FLUX_X)=1
  fields(FIELD_VOL_FLUX_Y)=1
  IF(profiler_on) kernel_time=timer()
  CALL update_halo(fields,2)
  IF(profiler_on) profiler%halo_exchange=profiler%halo_exchange+(timer()-kernel_time)
  unique_material=.FALSE.
!$OMN PARALLEL
!$OMN DO FIRSTPRIVATE(unique_material)
  DO tile=1,tiles_per_chunk
    DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+2
      DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+2
        IF(chunk%tiles(tile)%field%material1(j,k).GT.0) THEN
          unique_material(chunk%tiles(tile)%field%material1(j,k))=.TRUE.
        ENDIF
      ENDDO
    ENDDO
    ! Now check MMC materials
    DO component=1,chunk%tiles(tile)%field%total_components_halo
      unique_material(chunk%tiles(tile)%field%mmc_material1(component))=.TRUE.
    ENDDO

    IF(chunk%tiles(tile)%field%max_mats.NE.COUNT(unique_material)) THEN
      ! Only needs reallocating if max-mats has changed
      chunk%tiles(tile)%field%max_mats=COUNT(unique_material)
      DEALLOCATE(chunk%tiles(tile)%field%dv,chunk%tiles(tile)%field%de,chunk%tiles(tile)%field%dm)
      DEALLOCATE(chunk%tiles(tile)%field%mat_list)
      ALLOCATE(chunk%tiles(tile)%field%dv(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
      ALLOCATE(chunk%tiles(tile)%field%dm(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
      ALLOCATE(chunk%tiles(tile)%field%de(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
  
      ALLOCATE(chunk%tiles(tile)%field%mat_list(chunk%tiles(tile)%field%max_mats))
    ENDIF
    chunk%tiles(tile)%field%mat_list=PACK(material,unique_material)
    IF(chunk%tiles(tile)%field%max_mat_number.NE.MAXVAL(chunk%tiles(tile)%field%mat_list)) THEN
      chunk%tiles(tile)%field%max_mat_number=MAXVAL(chunk%tiles(tile)%field%mat_list)
      DEALLOCATE(chunk%tiles(tile)%field%reverse_mat_list)
      ALLOCATE(chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%max_mat_number))
    ENDIF
    chunk%tiles(tile)%field%reverse_mat_list=0
    DO mat=1,chunk%tiles(tile)%field%max_mats
      chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%mat_list(mat))=mat
    ENDDO

  ENDDO
!$OMN END DO
!$OMN END PARALLEL

  IF(profiler_on) kernel_time=timer()
!$OMN PARALLEL
!$OMN DO
  DO tile=1,tiles_per_chunk
    CALL advec_cell_driver(tile,sweep_number,direction)
  ENDDO
!$OMN END DO
!$OMN END PARALLEL

  IF(profiler_on) profiler%cell_advection=profiler%cell_advection+(timer()-kernel_time)

  fields=0
  fields(FIELD_MATERIAL1)=3 ! This means the structure needs updating
  fields(FIELD_DENSITY1)=3
  fields(FIELD_ENERGY1)=3
  fields(FIELD_VOLUME1)=3
  fields(FIELD_XVEL1)=1
  fields(FIELD_YVEL1)=1
  fields(FIELD_MASS_FLUX_X)=1
  fields(FIELD_MASS_FLUX_y)=1
  IF(profiler_on) kernel_time=timer()
  CALL update_halo(fields,2)

  ! I now need to update max_mats and the compacted material list
  ! This should be in a kernel so I can thread at the loop level as well
  ! Should also detect changes so I can reallocate de,dv etc
  unique_material=.FALSE.
!$OMN PARALLEL
!$OMN DO FIRSTPRIVATE(unique_material)
  DO tile=1,tiles_per_chunk
    DO k=chunk%tiles(tile)%t_ymin-2,chunk%tiles(tile)%t_ymax+2
      DO j=chunk%tiles(tile)%t_xmin-2,chunk%tiles(tile)%t_xmax+2
        IF(chunk%tiles(tile)%field%material1(j,k).GT.0) THEN
          unique_material(chunk%tiles(tile)%field%material1(j,k))=.TRUE.
        ENDIF
      ENDDO
    ENDDO
    ! Now check MMC materials
    DO component=1,chunk%tiles(tile)%field%total_components_halo
      unique_material(chunk%tiles(tile)%field%mmc_material1(component))=.TRUE.
    ENDDO

    IF(chunk%tiles(tile)%field%max_mats.NE.COUNT(unique_material)) THEN
      ! Only needs reallocating if max-mats has changed
      chunk%tiles(tile)%field%max_mats=COUNT(unique_material)
      DEALLOCATE(chunk%tiles(tile)%field%dv,chunk%tiles(tile)%field%de,chunk%tiles(tile)%field%dm)
      DEALLOCATE(chunk%tiles(tile)%field%mat_list)
      ALLOCATE(chunk%tiles(tile)%field%dv(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
      ALLOCATE(chunk%tiles(tile)%field%dm(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
      ALLOCATE(chunk%tiles(tile)%field%de(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
  
      ALLOCATE(chunk%tiles(tile)%field%mat_list(chunk%tiles(tile)%field%max_mats))
    ENDIF
    chunk%tiles(tile)%field%mat_list=PACK(material,unique_material)
    IF(chunk%tiles(tile)%field%max_mat_number.NE.MAXVAL(chunk%tiles(tile)%field%mat_list)) THEN
      chunk%tiles(tile)%field%max_mat_number=MAXVAL(chunk%tiles(tile)%field%mat_list)
      DEALLOCATE(chunk%tiles(tile)%field%reverse_mat_list)
      ALLOCATE(chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%max_mat_number))
    ENDIF
    chunk%tiles(tile)%field%reverse_mat_list=0
    DO mat=1,chunk%tiles(tile)%field%max_mats
      chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%mat_list(mat))=mat
    ENDDO

  ENDDO
!$OMN END DO
!$OMN END PARALLEL

  IF(profiler_on) profiler%halo_exchange=profiler%halo_exchange+(timer()-kernel_time)

  IF(profiler_on) kernel_time=timer()

  
!$OMN PARALLEL
!$OMN DO
  DO tile=1,tiles_per_chunk
    CALL advec_mom_driver(tile,xvel,direction,sweep_number) 
    CALL advec_mom_driver(tile,yvel,direction,sweep_number) 
  ENDDO
!$OMN END DO
!$OMN END PARALLEL
  IF(profiler_on) profiler%mom_advection=profiler%mom_advection+(timer()-kernel_time)

  sweep_number=2
  IF(advect_x)      direction=g_ydir
  IF(.not.advect_x) direction=g_xdir

  IF(profiler_on) kernel_time=timer()
!$OMN PARALLEL
!$OMN DO
  DO tile=1,tiles_per_chunk
    CALL advec_cell_driver(tile,sweep_number,direction)
  ENDDO
!$OMN END DO
!$OMN END PARALLEL

  IF(profiler_on) profiler%cell_advection=profiler%cell_advection+(timer()-kernel_time)

  fields=0
  fields(FIELD_MATERIAL1)=3
  fields(FIELD_DENSITY1)=3
  fields(FIELD_ENERGY1)=3
  fields(FIELD_VOLUME1)=3
  fields(FIELD_XVEL1)=1
  fields(FIELD_YVEL1)=1
  fields(FIELD_MASS_FLUX_X)=1
  fields(FIELD_MASS_FLUX_y)=1
  IF(profiler_on) kernel_time=timer()
  CALL update_halo(fields,2)

  ! I now need to update max_mats and the compacted material list
  ! This should be in a kernel so I can thread at the loop level as well
  unique_material=.FALSE.
!$OMN PARALLEL
!$OMN DO FIRSTPRIVATE(unique_material)
  DO tile=1,tiles_per_chunk
    DO k=chunk%tiles(tile)%t_ymin,chunk%tiles(tile)%t_ymax
      DO j=chunk%tiles(tile)%t_xmin,chunk%tiles(tile)%t_xmax
        IF(chunk%tiles(tile)%field%material1(j,k).GT.0) THEN
          unique_material(chunk%tiles(tile)%field%material1(j,k))=.TRUE.
        ENDIF
      ENDDO
    ENDDO
    ! Now check MMC materials. I MUST MAKE SURE THIS INCLUDES HALO
    DO component=1,chunk%tiles(tile)%field%total_components_halo
      unique_material(chunk%tiles(tile)%field%mmc_material1(component))=.TRUE.
    ENDDO

    ! These arrays should be at the tile level and have a thread index so temporary storage
    ! can be shared in scalar loops over tiles
    IF(chunk%tiles(tile)%field%max_mats.NE.COUNT(unique_material)) THEN
      chunk%tiles(tile)%field%max_mats=COUNT(unique_material)
      DEALLOCATE(chunk%tiles(tile)%field%dv,chunk%tiles(tile)%field%de,chunk%tiles(tile)%field%dm)
      DEALLOCATE(chunk%tiles(tile)%field%mat_list)
      ALLOCATE(chunk%tiles(tile)%field%dv(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
      ALLOCATE(chunk%tiles(tile)%field%dm(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
      ALLOCATE(chunk%tiles(tile)%field%de(chunk%tiles(tile)%field%max_mats,                      &
                                          chunk%tiles(tile)%t_xmin-2:chunk%tiles(tile)%t_xmax+2, &
                                          chunk%tiles(tile)%t_ymin-2:chunk%tiles(tile)%t_ymax+2))
      ALLOCATE(chunk%tiles(tile)%field%mat_list(chunk%tiles(tile)%field%max_mats))
    ENDIF
    chunk%tiles(tile)%field%mat_list=PACK(material,unique_material)
    IF(chunk%tiles(tile)%field%max_mat_number.NE.MAXVAL(chunk%tiles(tile)%field%mat_list)) THEN
      chunk%tiles(tile)%field%max_mat_number=MAXVAL(chunk%tiles(tile)%field%mat_list)
      DEALLOCATE(chunk%tiles(tile)%field%reverse_mat_list)
      ALLOCATE(chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%max_mat_number))
    ENDIF
    chunk%tiles(tile)%field%reverse_mat_list=0
    DO mat=1,chunk%tiles(tile)%field%max_mats
      chunk%tiles(tile)%field%reverse_mat_list(chunk%tiles(tile)%field%mat_list(mat))=mat
    ENDDO
  ENDDO
!$OMN END DO
!$OMN END PARALLEL

  IF(profiler_on) profiler%halo_exchange=profiler%halo_exchange+(timer()-kernel_time)

  IF(profiler_on) kernel_time=timer()
!$OMN PARALLEL
!$OMN DO
  DO tile=1,tiles_per_chunk
    CALL advec_mom_driver(tile,xvel,direction,sweep_number) 
    CALL advec_mom_driver(tile,yvel,direction,sweep_number) 
  ENDDO
!$OMN END DO
!$OMN END PARALLEL
  IF(profiler_on) profiler%mom_advection=profiler%mom_advection+(timer()-kernel_time)

  DEALLOCATE(unique_material)
  DEALLOCATE(material)

END SUBROUTINE advection

END MODULE advection_module
