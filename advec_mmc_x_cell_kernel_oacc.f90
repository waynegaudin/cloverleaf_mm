!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran multi-material cell advection kernel.
!>  @author Wayne Gaudin
!>  @details Performs a second order advective remap using van-Leer limiting
!>  with directional splitting.

MODULE advec_mmc_x_cell_module

CONTAINS

SUBROUTINE advec_mmc_x_cell_kernel(x_min,x_max,y_min,y_max,      &
                                   sweep_number,                 &
                                   max_mats,                     &
                                   max_mat_number,               &
                                   mat_list,                     &
                                   reverse_mat_list,             &
                                   total_mmcs1,                  &
                                   total_components1,            &
                                   total_mmcsmax,                &
                                   total_componentsmax,          &
                                   total_mmcs_halo,              &
                                   total_components_halo,        &
                                   total_mmcsnew,                &
                                   total_componentsnew,          &
                                   max_comps,                    &
                                   number_of_threads,            &
                                   mmc_counter_next,             &
                                   component_counter_next,       &
                                   celldx,                       &
                                   celldy,                       &
                                   vertexdx,                     &
                                   vertexdy,                     &
                                   volume,                       &
                                   density1,                     &
                                   densitynew,                   &
                                   energy1,                      &
                                   energynew,                    &
                                   mass_flux_x,                  &
                                   vol_flux_x,                   &
                                   mass_flux_y,                  &
                                   vol_flux_y,                   &
                                   pre_vol,                      &
                                   post_vol,                     &
                                   ener_flux,                    &
                                   dv,                           &
                                   dm,                           &
                                   de,                           &
                                   material1,                    &
                                   materialnew,                  &
                                   mmc_material1,                &
                                   mmc_materialnew,              &
                                   mmc_index1,                   &
                                   mmc_indexnew,                 &
                                   mmc_j1,                       &
                                   mmc_k1,                       &
                                   mmc_jnew,                     &
                                   mmc_knew,                     &
                                   mmc_component1,               &
                                   mmc_componentnew,             &
                                   mmc_density1,                 &
                                   mmc_densitynew,               &
                                   mmc_energy1,                  &
                                   mmc_energynew,                &
                                   mmc_volume1,                  &
                                   mmc_volumenew,                &
                                   mmc_start_index,              &
                                   mmc_index,                    &
                                   comp_start_index,             &
                                   comp_index,                   &
                                   reallocate_needed             )

  USE interface_module

  IMPLICIT NONE

  INTEGER                                                          :: x_min,x_max,y_min,y_max
  INTEGER                                                          :: dir,sweep_number
  INTEGER                                                          :: total_mmcs1,total_components1,max_mats,max_mat_number
  INTEGER, DIMENSION(1:max_mats)                                   :: mat_list
  INTEGER, DIMENSION(1:max_mat_number)                             :: reverse_mat_list
  INTEGER                                                          :: total_mmcsnew,total_componentsnew
  INTEGER                                                          :: total_mmcsmax,total_componentsmax
  INTEGER                                                          :: total_mmcs_halo,total_components_halo
  INTEGER                                                          :: max_comps
  INTEGER                                                          :: number_of_threads
  INTEGER, DIMENSION(1:number_of_threads)                          :: mmc_counter_next
  INTEGER, DIMENSION(1:number_of_threads)                          :: component_counter_next
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2)                         :: celldx
  REAL(KIND=8), DIMENSION(y_min-2:y_max+2)                         :: celldy
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3)                         :: vertexdx
  REAL(KIND=8), DIMENSION(y_min-2:y_max+3)                         :: vertexdy
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)         :: volume
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)         :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)         :: densitynew,energynew
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2)         :: vol_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3)         :: vol_flux_y
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)         :: pre_vol
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)         :: post_vol
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)         :: ener_flux
  REAL(KIND=8), DIMENSION(max_mats,x_min-2:x_max+2,y_min-2:y_max+2):: dv
  REAL(KIND=8), DIMENSION(max_mats,x_min-2:x_max+2,y_min-2:y_max+2):: dm
  REAL(KIND=8), DIMENSION(max_mats,x_min-2:x_max+2,y_min-2:y_max+2):: de
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2)         :: mass_flux_x
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3)         :: mass_flux_y
  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)              :: material1
  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)              :: materialnew
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_index1
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_indexnew
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_component1
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_componentnew
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_j1
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_k1
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_jnew
  INTEGER, DIMENSION(1:total_mmcsmax)                              :: mmc_knew
  INTEGER, DIMENSION(1:total_componentsmax)                        :: mmc_material1
  INTEGER, DIMENSION(1:total_componentsmax)                        :: mmc_materialnew
  REAL(KIND=8), DIMENSION(1:total_componentsmax)                   :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)                   :: mmc_densitynew
  REAL(KIND=8), DIMENSION(1:total_componentsmax)                   :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)                   :: mmc_energynew
  REAL(KIND=8), DIMENSION(1:total_componentsmax)                   :: mmc_volume1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)                   :: mmc_volumenew
  INTEGER , DIMENSION(0:number_of_threads)                         :: mmc_start_index
  INTEGER , DIMENSION(0:number_of_threads)                         :: mmc_index
  INTEGER , DIMENSION(0:number_of_threads)                         :: comp_start_index
  INTEGER , DIMENSION(0:number_of_threads)                         :: comp_index
  LOGICAL                                                          :: reallocate_needed

  INTEGER :: j,upwind,donor,downwind,dif,k
  INTEGER :: mmc,mmca,mmcb,m,mm,ma,mb,mc,m0,m1,m2,ml,mr,comp
  INTEGER :: n,dirn
  INTEGER :: g_xdir=1
  INTEGER :: mat(max_mats)
  INTEGER :: component_delta,mmc_delta,thread_number,thread,cc

  REAL(KIND=8) :: eps,dd,d1,d2,ed,e1,e2
  REAL(KIND=8) :: epst,epsv,epsm,eps3,eps4,dd1,dd2,dld,dmin,fmin
  REAL(KIND=8) :: ftot,cdt,cet,ddm,dde,cmass0,cmass1,cvol1
  REAL(KIND=8), PARAMETER :: c_sixth=1.0_8/6.0_8, c_one=1.0_8
  REAL(KIND=8) :: cvm(max_mats),cmm(max_mats),cem(max_mats),fdv(max_mats+1)

  LOGICAL :: z2ndord,complete,mmc_overwrite_detected,comp_overwrite_detected

  INTEGER :: mmc_storage_left,component_storage_left,team_number
  INTEGER :: read_index,write_index
  INTEGER :: mmc_free,comp_free

  INTEGER, DIMENSION(number_of_threads) :: mmc_length,comp_length

!!$ INTEGER :: omp_get_team_num,omp_get_thread_num

  write(0,*)"ADV X Number of threads ",number_of_threads

  reallocate_needed=.FALSE.
  complete=.FALSE.

  mr=0
  ma=0
  mb=0

  ! The dv, dm and de arrays below are mesh wide to thread
  ! I should just be able to get away with the volume advected per material through each face

  dmin=0.00000001_8
  fmin=0.00000001_8

!$ACC ENTER DATA COPYIN(pre_vol,post_vol,volume,vol_flux_x,vol_flux_y,                      &
!$ACC                   material1,reverse_mat_list,mmc_material1,mmc_index1,mmc_component1, &
!$ACC                   dv,dm,de,mmc_density1,mmc_energy1,density1,energy1,mass_flux_x,     &
!$ACC                   ener_flux,mmc_densitynew,mmc_energynew,mmc_volumenew,               &
!$ACC                   mmc_counter_next,component_counter_next,                            &
!$ACC                   mmc_j1,mmc_jnew,mmc_k1,mmc_knew,mat_list,mmc_materialnew            )

!-----Estimate semi-Lagrangian cell volumes.

 IF(sweep_number.EQ.1)THEN
!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
    DO k=y_min,y_max ! main k loop over real cells
!$ACC LOOP INDEPENDENT 
      DO j=x_min-2,x_max+2
        pre_vol(j,k)=volume(j,k)+vol_flux_x(j+1,k)-vol_flux_x(j,k)+vol_flux_y(j,k+1)-vol_flux_y(j,k)
        post_vol(j,k)=pre_vol(j,k)-vol_flux_x(j+1,k)+vol_flux_x(j,k)
      ENDDO
    ENDDO
!$ACC END KERNELS
  ELSE
!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
    DO k=y_min,y_max ! main k loop over real cells
!$ACC LOOP INDEPENDENT 
      DO j=x_min-2,x_max+2
        pre_vol(j,k)=volume(j,k)+vol_flux_x(j+1,k)-vol_flux_x(j,k)
        post_vol(j,k)=volume(j,k)
      ENDDO
    ENDDO
!$ACC END KERNELS
  ENDIF

  ! Apply reverse material mapping, including halo cell data
!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
  DO k=y_min-2,y_max+2
!$ACC LOOP INDEPENDENT 
    DO j=x_min-2,x_max+2
      IF(material1(j,k).GT.0)material1(j,k)=reverse_mat_list(material1(j,k))
    ENDDO
  ENDDO
!$ACC END KERNELS
!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
  DO comp=1,total_components_halo
    mmc_material1(comp)=reverse_mat_list(mmc_material1(comp))
  ENDDO
!$ACC END KERNELS

!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
  DO k=y_min,y_max ! main k loop over real cells
!$ACC LOOP INDEPENDENT 
    DO j=x_min,x_max+1 ! main j loop, needs to go to x_max+1 for crossings

      !-----Set indices for upwind, donor and downwind cells.

      IF(vol_flux_x(j,k).GT.0.0)THEN
         upwind   =j-2
         donor    =j-1
         downwind =j
         dif      =donor
         dirn     =1
      ELSE
         upwind   =MIN(j+1,x_max+2)
         donor    =j
         downwind =j-1
         dif      =upwind
         dirn     =-1
      ENDIF

      !-----Evalute the volume crossing from donor to acceptor

      mmc=material1(donor,k)
      IF(mmc.LE.0) THEN

        !-----Crossings procedure - Donor cell mixed

        eps=ABS(vol_flux_x(j,k))/pre_vol(donor,k)

        ! fdv holds the volume fraction of each material crossing the face

        CALL interface(max_mats,max_mat_number,donor,k,g_xdir,dirn,material1,mmc_index1, &
                       mmc_component1,mmc_material1,mmc_volume1,celldx,celldy,x_min,x_max,y_min,y_max,eps,fdv)

        !---Calc total crossings at current interface

        !---Initialise loop over donor cell materials

        mass_flux_x(j,k)=0.0    !total mass     in cell
        ener_flux(j,k)=0.0    !total energy   in cell

        DO m=1,max_mats
          dv(m,j,k)=0.0    !volume of component 
          dm(m,j,k)=0.0    !mass     of component
          de(m,j,k)=0.0    !energy   of component
        ENDDO

        ma=0
        mmc=-mmc

        !---Loop over donor cell materials

        DO m=mmc_index1(mmc),mmc_index1(mmc)+mmc_component1(mmc)-1

          !---Set donor values for current donor material

          mm=mmc_material1(m)
          IF(fdv(mm).EQ.0.0) CYCLE
          dv(mm,j,k)=vol_flux_x(j,k)*fdv(mm)
          dd=mmc_density1(m)
          ed=mmc_energy1(m)

          z2ndord=.FALSE.
          DO ! Why is this do here? It cuts down on further tests once 2nd order can't be performed
            IF(dd.LT.dmin) EXIT ! Try and remove

            !---Test for current donor material in upstream cell

            m1=material1(upwind,k)
            IF(m1.EQ.mm) THEN

            !-Present (pure).Set upstream values for current donor material

              d1=density1(upwind,k)
              e1=energy1(upwind,k)

            ELSE

              IF(m1.GE.0) EXIT ! This is if it is a pure cell that doesn't match
              mmca=-m1
              m0=0
              DO mc=mmc_index1(mmca),mmc_index1(mmca)+mmc_component1(mmca)-1
                IF(mmc_material1(mc).EQ.mm) THEN
                  m0=mc
                  EXIT
                ENDIF
              ENDDO
              IF(m0.EQ.0) EXIT ! No matching material found

              !-Present (mixed).Set upstream values for current donor material

              d1=mmc_density1(m0)
              e1=mmc_energy1(m0)

            ENDIF

            !---Test for current donor material in acceptor cell

            m2=material1(downwind,k)
            IF(m2.EQ.mm) THEN

              !-Present (pure).Set acceptor values for current donor material

              d2=density1(downwind,k)
              e2=energy1(downwind,k)
            ELSE
              IF(m2.GE.0) EXIT
              mmcb=-m2
              m0=0
              DO mb=mmc_index1(mmcb),mmc_index1(mmcb)+mmc_component1(mmcb)-1
                IF(mmc_material1(mb).EQ.mm) THEN
                  m0=mb
                  EXIT
                ENDIF
              ENDDO
              IF(m0.EQ.0) EXIT

              !-Present (mixed).Set acceptor values for current donor material

              d2=mmc_density1(m0)
              e2=mmc_energy1(m0)
            ENDIF

            !-Set interp. intervals and total volume fraction for crossings

            epst=ABS(vol_flux_x(j,k))/pre_vol(donor,k)
            eps3=(1.0_8+epst)*(vertexdx(j)/vertexdx(dif))
            eps4=2.0_8-epst

            !-Donor cell/Lax Wendroff for current donor material crossings

            !---Mass

            epsv=ABS(dv(mm,j,k))/(mmc_volume1(m)*pre_vol(donor,k))
            IF(epsv.GT.1.001_8) THEN
              !PRINT *,'Advecx cell (',j,',',k,') ',&
              !        'partial volume fraction crossing GT 1',epsv
              epsv=1.001_8
            ENDIF
            dd1=dd-d1
            dd2=d2-dd
            IF(dd1*dd2.GT.0.0)THEN
               dld=(1.0_8-epsv)*SIGN(c_one,dd2)*MIN(ABS(dd1),ABS(dd2)&
                   ,c_sixth*(eps3*ABS(dd1)+eps4*ABS(dd2)))
            ELSE
               dld=0.0
            ENDIF
            dm(mm,j,k)=dv(mm,j,k)*(dd+dld)

            !---Internal energy

            epsm=ABS(dm(mm,j,k))/(dd*(mmc_volume1(m)*pre_vol(donor,k)))
            ! Remove/move error check
            IF(epsm.GT.1.001_8) THEN
              !PRINT *,'Advecx cell (',j,',',k,') ',&
              !        'partial mass fraction crossing GT 1'
              epsm=1.001_8
            ENDIF
            dd1=ed-e1
            dd2=e2-ed
            IF(dd1*dd2.GT.0.0)THEN
               dld=(1.0_8-epsm)*SIGN(c_one,dd2)*MIN(ABS(dd1),ABS(dd2)&
                   ,c_sixth*(eps3*ABS(dd1)+eps4*ABS(dd2)))
            ELSE
               dld=0.0
            ENDIF
            de(mm,j,k)=dm(mm,j,k)*(ed+dld)

            z2ndord=.TRUE.
            EXIT
          ENDDO

          IF (.NOT.z2ndord) THEN
            ! Materials did not all match so drop to first order
            dm(mm,j,k)=dv(mm,j,k)*dd
            de(mm,j,k)=dm(mm,j,k)*ed
          ENDIF

          !---Accumulate partial crossings for donor materials

          mass_flux_x(j,k)=mass_flux_x(j,k)+dm(mm,j,k)
          ener_flux(j,k)=ener_flux(j,k)+de(mm,j,k)

        ENDDO !m
      ELSE

        !-----Crossings procedure - Donor cell pure

        !---Calc total crossings at current interface

        ma=mmc

        !---Set donor values of donor materials

        dd=density1(donor,k)
        ed=energy1(donor,k)
        z2ndord=.FALSE.
        DO ! Is this DO loop strictly necessary?
          IF(dd.LT.dmin) EXIT

          !---Test for donor materials in upstream cell

          m1=material1(upwind,k)
          IF(m1.EQ.ma) THEN

            !-Present (pure).Set upstream values for current donor material

            d1=density1(upwind,k)
            e1=energy1(upwind,k)
          ELSE
            IF(m1.GE.0) EXIT
            mmc=-m1
            m0=0
            DO m=mmc_index1(mmc),mmc_index1(mmc)+mmc_component1(mmc)-1
              IF(mmc_material1(m).EQ.ma) THEN
                m0=m
                EXIT
              ENDIF
            ENDDO
            IF(m0.EQ.0) EXIT

            !-Present (mixed).Set upstream values for current donor material

            d1=mmc_density1(m0)
            e1=mmc_energy1(m0)
          ENDIF

          !---Test for current donor material in acceptor cell

          m2=material1(downwind,k)
          IF(m2.EQ.ma) THEN

            !-Present (pure).Set acceptor values for current donor material

            d2=density1(downwind,k)
            e2=energy1(downwind,k)
          ELSE
            IF(m2.GE.0) EXIT
            mmc=-m2
            m0=0
            DO m=mmc_index1(mmc),mmc_index1(mmc)+mmc_component1(mmc)-1
              IF(mmc_material1(m).EQ.ma) THEN
                m0=m
                EXIT
              ENDIF
            ENDDO
            IF(m0.EQ.0) EXIT

            !-Present (mixed).Set acceptor values for current donor material

            d2=mmc_density1(m0)
            e2=mmc_energy1(m0)
          ENDIF

          !-Set interp. intervals and total volume fraction for crossings
          epst=ABS(vol_flux_x(j,k))/pre_vol(donor,k)
          eps3=(1.0_8+epst)*(vertexdx(j)/vertexdx(dif))
          eps4=2.0_8-epst

          !-Donor cell/Lax Wendroff for current donor matl crossings

          !---Mass

          eps=epst
          epsv=epst

          dd1=dd-d1
          dd2=d2-dd
          IF(dd1*dd2.GT.0.0)THEN
             dld=(1.0_8-epsv)*SIGN(c_one,dd2)*MIN(ABS(dd1),ABS(dd2)&
                 ,c_sixth*(eps3*ABS(dd1)+eps4*ABS(dd2)))
          ELSE
             dld=0.0
          ENDIF
          mass_flux_x(j,k)=vol_flux_x(j,k)*(dd+dld)
          !---Internal energy

          epsm=ABS(mass_flux_x(j,k))/(density1(donor,k)*pre_vol(donor,k))
          dd1=ed-e1
          dd2=e2-ed
          IF(dd1*dd2.GT.0.0)THEN
             dld=(1.0_8-epsm)*SIGN(c_one,dd2)*MIN(ABS(dd1),ABS(dd2)&
                 ,c_sixth*(eps3*ABS(dd1)+eps4*ABS(dd2)))
          ELSE
             dld=0.0
          ENDIF
          ener_flux(j,k)=mass_flux_x(j,k)*(ed+dld)
          z2ndord=.TRUE.
          EXIT
        ENDDO

        !---Set donor material crossings in 1st order cases

        IF (.NOT.z2ndord) THEN
          mass_flux_x(j,k)=vol_flux_x(j,k)*dd
          ener_flux(j,k)=mass_flux_x(j,k)*ed
        ENDIF
      ENDIF

    ENDDO ! main j loop
  ENDDO ! main k loop
!$ACC END KERNELS

! All crossings have been calculated now, so the mmc data structure
! needs constructing and filled based on these crossings

! I can perhaps use the non-zeroes in the dv(m,j,k) array to construct upper bounds on mixed cell and component count
! With the current storage scheme, there is not a limit on components per cell but only total component count

  DO WHILE(.NOT.complete)

!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
    DO k=y_min,y_max ! main k loop over real cells
!$ACC LOOP INDEPENDENT 
      DO j=x_min,x_max ! main j loop

!!$      thread_number=OMP_GET_THREAD_NUM()+1

        ! To drop out of a threaded loop if MMC limits are passed, I cannot EXIT
        ! So I need a thread provide logical that I can reduce on after the loop has finished
        IF(reallocate_needed) CYCLE 

        ! Make sure the ingoing/outgoing material is taken from the correct cell
        mc=material1(j,k)
        ! Left face
        IF(vol_flux_x(j,k).GT.0.0)THEN
          ml=material1(j-1,k)
        ELSE
          ml=material1(j,k)
        ENDIF
        ! Right face
        IF(vol_flux_x(j+1,k).GT.0.0)THEN
          mr=material1(j,k)
        ELSE
          mr=material1(j+1,k)
        ENDIF

        IF(.NOT.(ml.EQ.mc).OR..NOT.(mr.EQ.mc).OR.mc.LT.0) THEN

          !---Multimaterial advection

          !---Set pre-advection values
          DO m=1,max_mats
            cvm(m)=0.0
            cmm(m)=0.0
            cem(m)=0.0
          ENDDO

          IF(mc.LE.0) THEN

            !---Cell already mixed
            !   These are the current values
            mc=-mc
            DO m=mmc_index1(mc),mmc_index1(mc)+mmc_component1(mc)-1
              mm=mmc_material1(m) ! Is this material number or index to the mth material?
              cvm(mm)=mmc_volume1(m)*pre_vol(j,k)
              cmm(mm)=mmc_density1(m)*cvm(mm) ! This converts to mass and total internal energy
              cem(mm)=mmc_energy1(m)*cmm(mm)
            ENDDO
          ELSE
            !---Cell not already mixed
            !   These are the current values
            cvm(mc)=pre_vol(j,k)
            cmm(mc)=density1(j,k)*cvm(mc) ! This converts to mass and total internal energy
            cem(mc)=energy1(j,k)*cmm(mc)
          ENDIF

          ! These are now the additions/substractions of the two cell faces
          !---Left hand side crossings
          IF(ml.LE.0) THEN
            DO m=1,max_mats
              cvm(m)=cvm(m)+dv(m,j,k)
              cmm(m)=cmm(m)+dm(m,j,k)
              cem(m)=cem(m)+de(m,j,k)
            ENDDO
          ELSE
            cvm(ml)=cvm(ml)+vol_flux_x(j,k)
            cmm(ml)=cmm(ml)+mass_flux_x(j,k)
            cem(ml)=cem(ml)+ener_flux(j,k)
          ENDIF

          !---Right hand side crossings
          IF(mr.LE.0) THEN
            DO m=1,max_mats
              cvm(m)=cvm(m)-dv(m,j+1,k)
              cmm(m)=cmm(m)-dm(m,j+1,k)
              cem(m)=cem(m)-de(m,j+1,k)
            ENDDO
          ELSE
            cvm(mr)=cvm(mr)-vol_flux_x(j+1,k)
            cmm(mr)=cmm(mr)-mass_flux_x(j+1,k)
            cem(mr)=cem(mr)-ener_flux(j+1,k)
          ENDIF

          !---Test all materials and convert mass to density etc
          mm=0
          DO m=1,max_mats
            IF(cvm(m).LT.fmin*post_vol(j,k)) CYCLE
            IF(cvm(m).LT.fmin*volume(j,k)) CYCLE
            mm=mm+1 ! Key line to decide if the updated cell is mixed
            mat(mm)=m
            IF(cmm(m).LT.dmin*cvm(m)) THEN
              cem(m)=0.0
            ELSE
              cem(m)=cem(m)/cmm(m)
              cmm(m)=cmm(m)/cvm(m)
            ENDIF
            cvm(m)=cvm(m)/post_vol(j,k)
          ENDDO
          mc=material1(j,k) ! What is this for?
          IF(mm.EQ.1) THEN

            !---Single material after advection
            materialnew(j,k)=mat(1)
            densitynew(j,k)=cmm(mat(1))
            energynew(j,k)=cem(mat(1))

          ELSE

            !---MMC material after advection
            !---Pick up next vacant directory location
            ! Store counter in thread locally
            ! This will require a thread local MMC and component storage.
            ! So I need to have an mmc_index that is thread local that has an initial value based on thread number
!$          team_number=omp_get_team_num()
!$          thread_number=omp_get_thread_num()+1
          ! Update the thread counters
! GANGS ARE TEAMS? So gangs of threads
!!$ACC ATOMIC CAPTURE
            mmc=mmc_index(thread_number)
            mmc_index(thread_number)=mmc_index(thread_number)+1
!!$ACC END ATOMIC
!!$ACC ATOMIC CAPTURE
            cc=comp_index(thread_number)
            comp_index(thread_number)=comp_index(thread_number)+mm
!!$ACC END ATOMIC

            !---No vacancies so increase mixed store (this should be per thread limit, not array limit
            IF (mmc+1.GT.total_mmcsmax) THEN
!$ACC ATOMIC WRITE
              reallocate_needed=.TRUE.
              CYCLE
            ENDIF
            IF (cc+mm.GT.total_componentsmax) THEN ! Component counter
!$ACC ATOMIC WRITE
              reallocate_needed=.TRUE.
              CYCLE
            ENDIF

            !---Set material specifcation
            materialnew(j,k)=-mmc

            mmc_indexnew(mmc)=cc
            mmc_jnew(mmc)=j
            mmc_knew(mmc)=k
            mmc_componentnew(mmc)=mm

            !---Set volume fraction scaling
            ftot=0.0
            DO m=1,mm
              ftot=ftot+cvm(mat(m))
            ENDDO
            ftot=1.0/ftot

            !---Set partial values for each material and accumulate

            cdt=0.0
            cet=0.0
            comp=mmc_indexnew(mmc)
            DO m=1,mm                              ! This is the component loop
              mmc_materialnew(comp+m-1)=mat(m)
              mmc_volumenew(comp+m-1)=cvm(mat(m))*ftot
              mmc_densitynew(comp+m-1)=cmm(mat(m))
              mmc_energynew(comp+m-1)=cem(mat(m))
              ddm=mmc_densitynew(comp+m-1)*mmc_volumenew(comp+m-1)
              dde=mmc_energynew(comp+m-1)*ddm
              cdt=cdt+ddm ! Cell averages
              cet=cet+dde ! Cell averages
            ENDDO

            !---Set mean values for cell (not S.D.S)

            densitynew(j,k)=cdt
            IF (cdt.GT.0.0) THEN
              energynew(j,k)=cet/cdt
            ELSE
              energynew(j,k)=cet !0.0 in old version
            ENDIF

          ENDIF

        ELSE
          !---Single material advection
          materialnew(j,k)=material1(j,k)
          cmass0=density1(j,k)*pre_vol(j,k)
          cmass1=cmass0+mass_flux_x(j,k)-mass_flux_x(j+1,k)
          cvol1=pre_vol(j,k)+vol_flux_x(j,k)-vol_flux_x(j+1,k)
          IF (cvol1.LE.0.0) THEN
            densitynew(j,k)=cmass1
          ELSE
            ! Volume weighted
            densitynew(j,k)=cmass1/cvol1
          ENDIF
          IF(cmass1.LE.0.0) THEN
            energynew(j,k)=0.0
          ELSE
            ! Mass weighted
            energynew(j,k)=(energy1(j,k)*cmass0+ener_flux(j,k)-ener_flux(j+1,k))/cmass1
          ENDIF
        ENDIF

      ENDDO ! main j update loop
    ENDDO ! main k update loop
!$ACC END KERNELS

    IF(reallocate_needed) THEN
      WRITE(0,*)"X reallocate mmc storage"
      WRITE(0,*)"Exiting and restarting with more storage"
      WRITE(0,*)
      total_mmcsnew=total_mmcs1
      total_componentsnew=total_components1
      RETURN
    ENDIF

  ! Check if a thread storage counter crossed a thread boundary.
    mmc_overwrite_detected=.FALSE.
    comp_overwrite_detected=.FALSE.
!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
    DO thread=1,number_of_threads-1
      IF(mmc_index(thread).GE.mmc_start_index(thread+1)) THEN
        mmc_overwrite_detected=.TRUE.
      ENDIF
      IF(comp_index(thread).GE.comp_start_index(thread+1)) THEN
        comp_overwrite_detected=.TRUE.
      ENDIF
    ENDDO
!$ACC END KERNELS

    complete=.TRUE.
    IF(mmc_overwrite_detected.OR.comp_overwrite_detected) THEN
      IF(mmc_overwrite_detected) WRITE(0,*)"X-thread accessed neighbour MMC storage"
      IF(comp_overwrite_detected) WRITE(0,*)"X-thread accessed neighbour Comp storage"
      complete=.FALSE.
      ! Split free storage between threads.
      total_mmcsnew=SUM(mmc_index-mmc_start_index)
      total_componentsnew=SUM(comp_index-comp_start_index)
      mmc_free=(total_mmcsmax-total_mmcsnew)/number_of_threads
      comp_free=(total_componentsmax-total_componentsnew)/number_of_threads
      IF(mmc_free.GT.0.AND.comp_free.GT.0) THEN ! Check to see if total storage would be exceeded
write(0,*)"1- MMC thread storage adequate ",total_mmcsnew,total_componentsnew,mmc_free,comp_free
        DO thread=1,number_of_threads
          mmc_length(thread)=mmc_free+mmc_index(thread)-mmc_start_index(thread)
          comp_length(thread)=comp_free+comp_index(thread)-comp_start_index(thread)
        ENDDO
        DO thread=2,number_of_threads
          mmc_start_index(thread)=SUM(mmc_length(1:thread-1))+1
          comp_start_index(thread)=SUM(comp_length(1:thread-1))+1
        ENDDO
        mmc_index=mmc_start_index
        comp_index=comp_start_index
      ELSE
write(0,*)"thread violation and reallocate needed ",mmc_free,comp_free,total_mmcsnew,total_componentsnew
        DO thread=1,number_of_threads
          mmc_length(thread)=mmc_index(thread)-mmc_start_index(thread)
          comp_length(thread)=comp_index(thread)-comp_start_index(thread)
        ENDDO
        DO thread=2,number_of_threads
          mmc_start_index(thread)=SUM(mmc_length(1:thread-1))+1
          comp_start_index(thread)=SUM(comp_length(1:thread-1))+1
        ENDDO
        mmc_index=mmc_start_index
        comp_index=comp_start_index
        reallocate_needed=.TRUE.
        RETURN
      ENDIF
    ENDIF
    mmc_overwrite_detected=.FALSE.
    comp_overwrite_detected=.FALSE.
  ENDDO

write(0,*)
write(0,*)"MMC thread storage adequate ",total_mmcsnew,total_componentsnew
write(0,*)

  ! The MMC data is seperated in memory at the thread level. I should be able compact it in the copy to level 1.
  ! This maximum is the max of the last thread.

!$ACC PARALLEL COPY(total_mmcsnew,total_componentsnew)
  total_mmcsnew=mmc_index(number_of_threads)-1
  total_componentsnew=comp_index(number_of_threads)-1
!$ACC END PARALLEL
  IF(total_mmcsnew.GT.total_mmcsmax)THEN
    WRITE(0,*)"Exhausted mixed cell count, this shouldn't happen"
  ENDIF
  IF(total_componentsnew.GT.total_componentsmax)THEN
    WRITE(0,*)"Exhausted component count, this shouldn't happen"
  ENDIF

  ! Compact the new MMC data. The first thread's data is already in the correct place.
  ! Update to permanent data structures
!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
  DO thread=1,number_of_threads
    DO comp=1,comp_index(thread)-comp_start_index(thread)
      read_index=comp+comp_start_index(thread)-1
      write_index=SUM(comp_index(0:thread-1)-comp_start_index(0:thread-1))+comp
      mmc_material1(write_index)=mat_list(mmc_materialnew(read_index))
      mmc_density1(write_index)=mmc_densitynew(read_index)
      mmc_energy1(write_index)=mmc_energynew(read_index)
      mmc_volume1(write_index)=mmc_volumenew(read_index)
    ENDDO
    DO mmc=1,mmc_index(thread)-mmc_start_index(thread)
      read_index=mmc+mmc_start_index(thread)-1
      write_index=SUM(mmc_index(0:thread-1)-mmc_start_index(0:thread-1))+mmc
      material1(mmc_jnew(read_index),mmc_knew(read_index))=-write_index
      mmc_index1(write_index)=mmc_indexnew(read_index)-comp_start_index(thread) &
                             +SUM(comp_index(0:thread-1)-comp_start_index(0:thread-1))+1
      mmc_component1(write_index)=mmc_componentnew(read_index)
      mmc_j1(write_index)=mmc_jnew(read_index)
      mmc_k1(write_index)=mmc_knew(read_index)
    ENDDO
  ENDDO
!$ACC END KERNELS

  total_mmcsnew=0
  total_componentsnew=0
  DO thread=1,number_of_threads
    total_mmcsnew=total_mmcsnew+mmc_index(thread)-mmc_start_index(thread)
    total_componentsnew=total_componentsnew+comp_index(thread)-comp_start_index(thread)
  ENDDO
  total_mmcs1=total_mmcsnew
  total_components1=total_componentsnew

!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
    DO k=y_min,y_max
!$ACC LOOP INDEPENDENT 
      DO j=x_min,x_max
        IF(materialnew(j,k).GT.0) THEN
          material1(j,k)=mat_list(materialnew(j,k))
        ENDIF
        density1(j,k)=densitynew(j,k)
        energy1(j,k)=energynew(j,k)
      ENDDO
    ENDDO
!$ACC END KERNELS

!$ACC PARALLEL COPY(max_comps,mmc_delta,component_delta)

  max_comps=MAXVAL(mmc_component1(1:total_mmcs1))

  ! Now store starting mmc index and component points to be used in halo exchange so
  ! there is no need to search the list for the end every time an exchange occurs
  ! This is just assuming 1 thread but when it is threaded it will need a loop
  ! over thread number and the starting point for each thread calculated.
  ! There should also be parameter to make sure 1 thread does get as far as another threads storage

  ! Deltas should be exported for error detection outside of this routine
  mmc_storage_left=total_mmcsmax-total_mmcs1
  component_storage_left=total_componentsmax-total_components1
  mmc_delta=mmc_storage_left/number_of_threads
  component_delta=component_storage_left/number_of_threads
!$ACC END PARALLEL

!$ACC KERNELS
!$ACC LOOP INDEPENDENT 
  DO thread_number=1,number_of_threads
    mmc_counter_next(thread_number)=total_mmcsnew+1+(thread_number-1)*mmc_delta
    component_counter_next(thread_number)=total_componentsnew+1+(thread_number-1)*component_delta
  ENDDO
!$ACC END KERNELS

!$ACC EXIT DATA COPYOUT(material1,mmc_material1,mmc_index1,mmc_component1,         &
!$ACC                   mmc_density1,mmc_energy1,density1,energy1,mass_flux_x,     &
!$ACC                   mmc_counter_next,component_counter_next,                   &
!$ACC                   mmc_j1,mmc_k1,mat_list                                     )

END SUBROUTINE advec_mmc_x_cell_kernel

END MODULE advec_mmc_x_cell_module
