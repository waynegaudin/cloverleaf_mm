!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Holds the high level Fortran data types
!>  @author Wayne Gaudin
!>  @details The high level data types used to store the mesh and field data
!>  are defined here.
!>
!>  Also the global variables used for defining the input and controlling the
!>  scheme are defined here.

MODULE definitions_module

   USE data_module
   
   IMPLICIT NONE

   TYPE state_type
      LOGICAL            :: defined

      REAL(KIND=8)       :: density          &
                           ,energy           &
                           ,xvel             &
                           ,yvel

      INTEGER            :: geometry

      INTEGER            :: material

      REAL(KIND=8)       :: xmin             &
                           ,xmax             &
                           ,ymin             &
                           ,ymax             &
                           ,radius
   END TYPE state_type

   TYPE(state_type), ALLOCATABLE             :: states(:)
   INTEGER                                   :: number_of_states
   INTEGER                                   :: number_of_materials

   TYPE grid_type
     REAL(KIND=8)       :: xmin              &
                          ,ymin              &
                          ,xmax              &
                          ,ymax
                     
     INTEGER            :: x_cells           &
                          ,y_cells
   END TYPE grid_type

   INTEGER      :: step
   INTEGER      :: number_of_threads

   LOGICAL      :: advect_x

   INTEGER  :: tiles_per_chunk

   INTEGER      :: error_condition

   INTEGER      :: test_problem
   LOGICAL      :: complete

   LOGICAL      :: profiler_on ! Internal code profiler to make comparisons across systems easier

   TYPE profiler_type
     REAL(KIND=8)       :: timestep        &
                          ,acceleration    &
                          ,PdV             &
                          ,cell_advection  &
                          ,mom_advection   &
                          ,viscosity       &
                          ,ideal_gas       &
                          ,visit           &
                          ,summary         &
                          ,reset           &
                          ,revert          &
                          ,flux            &
                          ,halo_exchange
                     
   END TYPE profiler_type
   TYPE(profiler_type)  :: profiler

   REAL(KIND=8) :: end_time

   INTEGER      :: end_step

   REAL(KIND=8) :: dtold          &
                  ,dt             &
                  ,time           &
                  ,dtinit         &
                  ,dtmin          &
                  ,dtmax          &
                  ,dtrise         &
                  ,dtu_safe       &
                  ,dtv_safe       &
                  ,dtc_safe       &
                  ,dtdiv_safe     &
                  ,dtc            &
                  ,dtu            &
                  ,dtv            &
                  ,dtdiv

   INTEGER      :: visit_frequency   &
                  ,summary_frequency

   INTEGER      :: jdt,kdt

   TYPE field_list

     REAL(KIND=8), POINTER :: smc_data(:,:)
     REAL(KIND=8), POINTER :: mmc_data(:)

   END TYPE field_list

   TYPE field_type

     INTEGER(KIND=4), DIMENSION(:,:), ALLOCATABLE :: material0,material1,materialnew ! Positive is material number, negative is index into mmc data arrays
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: density0,density1,densitynew
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: energy0,energy1,energynew
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: pressure
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: viscosity
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: soundspeed
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: xvel0,xvel1
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: yvel0,yvel1
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: vol_flux_x,mass_flux_x
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: vol_flux_y,mass_flux_y
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: work_array1 !node_flux, stepbymass, volume_change, pre_vol
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: work_array2 !node_mass_post, post_vol
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: work_array3 !node_mass_pre,pre_mass
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: work_array4 !advec_vel, post_mass
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: work_array5 !mom_flux, advec_vol
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: work_array6 !pre_vol, post_ener
     REAL(KIND=8),    DIMENSION(:,:),  ALLOCATABLE :: work_array7 !post_vol, ener_flux
     REAL(KIND=8),    DIMENSION(:,:,:),ALLOCATABLE :: dv
     REAL(KIND=8),    DIMENSION(:,:,:),ALLOCATABLE :: dm
     REAL(KIND=8),    DIMENSION(:,:,:),ALLOCATABLE :: de
     REAL(KIND=8),    DIMENSION(:)  ,  ALLOCATABLE :: mmc_density0,mmc_density1,mmc_densitynew
     REAL(KIND=8),    DIMENSION(:)  ,  ALLOCATABLE :: mmc_energy0,mmc_energy1,mmc_energynew
     REAL(KIND=8),    DIMENSION(:)  ,  ALLOCATABLE :: mmc_pressure
     REAL(KIND=8),    DIMENSION(:)  ,  ALLOCATABLE :: mmc_viscosity
     REAL(KIND=8),    DIMENSION(:)  ,  ALLOCATABLE :: mmc_soundspeed
     REAL(KIND=8),    DIMENSION(:)  ,  ALLOCATABLE :: mmc_volume0,mmc_volume1,mmc_volumenew
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_index0 ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_index1 ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_indexnew ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_component0 ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_component1 ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_componentnew ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_material0 ! Allocated to the total number of components
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_material1 ! Allocated to the total number of components
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_materialnew ! Allocated to the total number of components
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_j0,mmc_k0 ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_j1,mmc_k1 ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_jnew,mmc_knew ! Allocated to number of multi-material cells
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_start_index
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: mmc_index
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: comp_start_index
     INTEGER(KIND=4), DIMENSION(:)  ,  ALLOCATABLE :: comp_index

     TYPE(field_list) :: field_list(15)

     REAL(KIND=8),    DIMENSION(:)  ,  ALLOCATABLE :: cellx    &
                                                     ,celly    &
                                                     ,vertexx  &
                                                     ,vertexy  &
                                                     ,celldx   &
                                                     ,celldy   &
                                                     ,vertexdx &
                                                     ,vertexdy

     REAL(KIND=8),   DIMENSION(:,:) ,  ALLOCATABLE :: volume   &
                                                     ,xarea    &
                                                     ,yarea

     INTEGER                               :: total_components0,total_mmcs0
     INTEGER                               :: total_components1,total_mmcs1
     INTEGER                               :: total_componentsnew,total_mmcsnew
     INTEGER                               :: total_componentsmax,total_mmcsmax
     INTEGER                               :: total_components_halo,total_mmcs_halo
     INTEGER                               :: max_mats
     INTEGER                               :: max_mat_number
     INTEGER                               :: max_comps
     INTEGER, DIMENSION(:)  ,  ALLOCATABLE :: mmc_counter_next, component_counter_next
     INTEGER, DIMENSION(:)  ,  ALLOCATABLE :: mat_list(:)
     INTEGER, DIMENSION(:)  ,  ALLOCATABLE :: reverse_mat_list(:)

     INTEGER         :: mmc_left_rcv(2)
     INTEGER         :: mmc_right_rcv(2)
     INTEGER         :: mmc_bottom_rcv(2)
     INTEGER         :: mmc_top_rcv(2)
     INTEGER         :: mmc_left_snd(2)
     INTEGER         :: mmc_right_snd(2)
     INTEGER         :: mmc_bottom_snd(2)
     INTEGER         :: mmc_top_snd(2)
     INTEGER         :: component_left_rcv(2)
     INTEGER         :: component_right_rcv(2)
     INTEGER         :: component_bottom_rcv(2)
     INTEGER         :: component_top_rcv(2)
     INTEGER         :: component_left_snd(2)
     INTEGER         :: component_right_snd(2)
     INTEGER         :: component_bottom_snd(2)
     INTEGER         :: component_top_snd(2)

     INTEGER, DIMENSION(:)   , ALLOCATABLE :: left_index_snd_comp_list
     INTEGER, DIMENSION(:)   , ALLOCATABLE :: left_component_snd_comp_list
     INTEGER, DIMENSION(:)   , ALLOCATABLE :: left_index_rcv_comp_list
     INTEGER, DIMENSION(:,:) , ALLOCATABLE :: left_component_snd_comp_count
     INTEGER                               :: left_component_rcv_comp_list

     INTEGER, DIMENSION(:)   , ALLOCATABLE :: right_index_snd_comp_list
     INTEGER, DIMENSION(:)   , ALLOCATABLE :: right_component_snd_comp_list
     INTEGER, DIMENSION(:)   , ALLOCATABLE :: right_index_rcv_comp_list
     INTEGER, DIMENSION(:,:) , ALLOCATABLE :: right_component_snd_comp_count
     INTEGER                               :: right_component_rcv_comp_list

     INTEGER, DIMENSION(:,:) , ALLOCATABLE :: bottom_component_snd_comp_list
     INTEGER, DIMENSION(:,:) , ALLOCATABLE :: bottom_component_snd_comp_count
     INTEGER, DIMENSION(:)   , ALLOCATABLE :: bottom_index_rcv_comp_list
     INTEGER                               :: bottom_component_rcv_comp_list

     INTEGER, DIMENSION(:,:) , ALLOCATABLE :: top_component_snd_comp_list
     INTEGER, DIMENSION(:,:) , ALLOCATABLE :: top_component_snd_comp_count
     INTEGER, DIMENSION(:)   , ALLOCATABLE :: top_index_rcv_comp_list
     INTEGER                               :: top_component_rcv_comp_list

   END TYPE field_type

    TYPE tile_type

        TYPE(field_type):: field
        INTEGER :: tile_neighbours(4)

        INTEGER :: t_xmin, t_xmax, t_ymin, t_ymax

        INTEGER :: t_left, t_right, t_bottom, t_top

    END TYPE tile_type

    TYPE chunk_type

        INTEGER         :: task   !mpi task

        INTEGER         :: chunk_neighbours(4) ! Chunks, not tasks, so we can overload in the future

        ! Idealy, create an array to hold the buffers for each field so a commuincation only needs
        !  one send and one receive per face, rather than per field.
        ! If chunks are overloaded, i.e. more chunks than tasks, might need to pack for a task to task comm 
        !  rather than a chunk to chunk comm. See how performance is at high core counts before deciding
        REAL(KIND=8),ALLOCATABLE:: left_rcv_buffer(:),right_rcv_buffer(:),bottom_rcv_buffer(:),top_rcv_buffer(:)
        REAL(KIND=8),ALLOCATABLE:: left_snd_buffer(:),right_snd_buffer(:),bottom_snd_buffer(:),top_snd_buffer(:)
        INTEGER,ALLOCATABLE:: integer_left_rcv_buffer(:),integer_right_rcv_buffer(:)
        INTEGER,ALLOCATABLE:: integer_bottom_rcv_buffer(:),integer_top_rcv_buffer(:)
        INTEGER,ALLOCATABLE:: integer_left_snd_buffer(:),integer_right_snd_buffer(:)
        INTEGER,ALLOCATABLE:: integer_bottom_snd_buffer(:),integer_top_snd_buffer(:)

        TYPE(tile_type), DIMENSION(:), ALLOCATABLE :: tiles

        INTEGER         :: x_min  &
                          ,y_min  &
                          ,x_max  &
                          ,y_max

        INTEGER         :: left            &
                          ,right           &
                          ,bottom          &
                          ,top             &
                          ,left_boundary   &
                          ,right_boundary  &
                          ,bottom_boundary &
                          ,top_boundary

        INTEGER         :: left_buffer_size,  &
                           right_buffer_size, &
                           bottom_buffer_size,&
                           top_buffer_size

    END TYPE chunk_type

  TYPE(chunk_type)       :: chunk
  INTEGER                :: number_of_chunks

  TYPE(grid_type)        :: grid

END MODULE definitions_module
