!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran PdV kernel.
!>  @author Wayne Gaudin
!>  @details Calculates the change in energy and density in a cell using the
!>  change on cell volume due to the velocity gradients in a cell. The time
!>  level of the velocity data depends on whether it is invoked as the
!>  predictor or corrector.

! To Do
! Play around with how to deal with Multi material to get best performance
! when both only 1 material present and more than one present

! Note that the material data structure doesn't change, only the valiue so
! the time level 1 data structure is not passed

MODULE PdV_kernel_module

CONTAINS

SUBROUTINE PdV_kernel(predict,                                          &
                      x_min,x_max,y_min,y_max,dt,                       &
                      total_mmcs0,                                      &
                      total_components0,                                &
                      total_mmcsmax,                                    &
                      total_componentsmax,                              &
                      xarea,yarea,volume,                               &
                      density0,                                         &
                      density1,                                         &
                      energy0,                                          &
                      energy1,                                          &
                      pressure,                                         &
                      viscosity,                                        &
                      xvel0,                                            &
                      xvel1,                                            &
                      yvel0,                                            &
                      yvel1,                                            &
                      material0,                                        &
                      mmc_index0,                                       &
                      mmc_j0,                                           &
                      mmc_k0,                                           &
                      mmc_component0,                                   &
                      mmc_density0,                                     &
                      mmc_density1,                                     &
                      mmc_energy0,                                      &
                      mmc_energy1,                                      &
                      mmc_pressure,                                     &
                      mmc_viscosity                                     )

  IMPLICIT NONE

  LOGICAL :: predict

  INTEGER :: x_min,x_max,y_min,y_max
  INTEGER :: total_mmcs0,total_components0
  INTEGER :: total_mmcsmax,total_componentsmax
  REAL(KIND=8)  :: dt
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+2)    :: xarea
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+3)    :: yarea
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: volume
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: density0,energy0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: pressure
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: density1,energy1
  REAL(KIND=8), DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)    :: viscosity
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)    :: xvel0,yvel0
  REAL(KIND=8), DIMENSION(x_min-2:x_max+3,y_min-2:y_max+3)    :: xvel1,yvel1
  INTEGER, DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2)         :: material0
  INTEGER, DIMENSION(1:total_mmcsmax)                         :: mmc_index0
  INTEGER,OPTIONAL, DIMENSION(1:total_mmcsmax)                :: mmc_j0,mmc_k0
  INTEGER, DIMENSION(1:total_mmcsmax)                         :: mmc_component0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_density0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_density1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_energy0
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_energy1
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_pressure
  REAL(KIND=8), DIMENSION(1:total_componentsmax)              :: mmc_viscosity

  INTEGER :: j,k,component,mmc

  REAL(KIND=8)  :: recip_volume,energy_change,min_cell_volume
  REAL(KIND=8)  :: right_flux,left_flux,top_flux,bottom_flux,total_flux
  REAL(KIND=8)  :: volume_change
  REAL(KIND=8)  :: quarter_dt,half_dt

  ! These should use some sort of scratch arrays to prevent allocates and/or stack overflows
  REAL(KIND=8)  :: mmc_volume_change(total_mmcs0)
  REAL(KIND=8)  :: mmc_recip_volume(total_mmcs0)
  REAL(KIND=8)  :: mmc_denom(total_mmcs0)

  LOGICAL :: multi_material=.FALSE.

  quarter_dt=0.25_8*dt*0.5_8
  half_dt=0.25_8*dt

  ! Try and avoid MM updates if none on the chunk
  IF (total_mmcs0.NE.0) multi_material=.TRUE.

!$OMP PARALLEL

  IF(predict)THEN

! This loop will always get done, single or multi material
!$OMP DO                                                                               &
!$OMP              PRIVATE(j,k,right_flux,left_flux,top_flux,bottom_flux,total_flux, &
!$OMP              min_cell_volume,energy_change,recip_volume,volume_change,mmc)
    DO k=y_min,y_max
      DO j=x_min,x_max

        left_flux=  (xarea(j  ,k  )*(xvel0(j  ,k  )+xvel0(j  ,k+1)                     &
                                    +xvel0(j  ,k  )+xvel0(j  ,k+1)))*quarter_dt
        right_flux= (xarea(j+1,k  )*(xvel0(j+1,k  )+xvel0(j+1,k+1)                     &
                                    +xvel0(j+1,k  )+xvel0(j+1,k+1)))*quarter_dt
        bottom_flux=(yarea(j  ,k  )*(yvel0(j  ,k  )+yvel0(j+1,k  )                     &
                                    +yvel0(j  ,k  )+yvel0(j+1,k  )))*quarter_dt
        top_flux=   (yarea(j  ,k+1)*(yvel0(j  ,k+1)+yvel0(j+1,k+1)                     &
                                    +yvel0(j  ,k+1)+yvel0(j+1,k+1)))*quarter_dt
        total_flux=right_flux-left_flux+top_flux-bottom_flux

        volume_change=volume(j,k)/(volume(j,k)+total_flux)

        min_cell_volume=MIN(volume(j,k)+right_flux-left_flux+top_flux-bottom_flux &
                           ,volume(j,k)+right_flux-left_flux                      &
                           ,volume(j,k)+top_flux-bottom_flux)
 
        recip_volume=1.0_8/volume(j,k) 

        energy_change=((pressure(j,k)/density0(j,k))+(viscosity(j,k)/density0(j,k)))*total_flux*recip_volume

        energy1(j,k)=energy0(j,k)-energy_change

        density1(j,k)=density0(j,k)*volume_change

        ! I only need to test this if I don't store the volume change
        ! Make sure this doesn't hinder vectorisation or optimisation
        ! If it does, then I can take the global flag outside of the loop and
        ! duplicate the loop for single and multi-material chunks
        IF(material0(j,k).LT.0) THEN
          mmc=-material0(j,k)
          mmc_volume_change(mmc)=volume_change
          mmc_recip_volume(mmc)=recip_volume
          mmc_denom(mmc)=total_flux*recip_volume
        ENDIF

      ENDDO
    ENDDO
!$OMP END DO

    IF(multi_material) THEN

!$OMP DO                                  &
!$OMP              PRIVATE(mmc,component)
      ! mmc_components is the number of components in a cell
      DO mmc=1,total_mmcs0
        DO component=mmc_index0(mmc),mmc_index0(mmc)+mmc_component0(mmc)-1
          ! This should have stride 1 through memory, but will the compiler realise?
          mmc_energy1(component)=mmc_energy0(component)                           &
                              -((mmc_pressure(component)/mmc_density0(component)  &
                                +viscosity(mmc_j0(mmc),mmc_k0(mmc))/density0(mmc_j0(mmc),mmc_k0(mmc)))&
                                *mmc_denom(mmc))
          mmc_density1(component)=mmc_density0(component)*mmc_volume_change(mmc)
        ENDDO
      ENDDO
!$OMP END DO
    ENDIF

  ELSE

!$OMP DO                                                                             &
!$OMP              PRIVATE(j,k,right_flux,left_flux,top_flux,bottom_flux,total_flux, &
!$OMP              min_cell_volume,energy_change,recip_volume,volume_change,mmc)
    DO k=y_min,y_max
      DO j=x_min,x_max

        left_flux=  (xarea(j  ,k  )*(xvel0(j  ,k  )+xvel0(j  ,k+1)                     &
                                    +xvel1(j  ,k  )+xvel1(j  ,k+1)))*half_dt
        right_flux= (xarea(j+1,k  )*(xvel0(j+1,k  )+xvel0(j+1,k+1)                     &
                                    +xvel1(j+1,k  )+xvel1(j+1,k+1)))*half_dt
        bottom_flux=(yarea(j  ,k  )*(yvel0(j  ,k  )+yvel0(j+1,k  )                     &
                                    +yvel1(j  ,k  )+yvel1(j+1,k  )))*half_dt
        top_flux=   (yarea(j  ,k+1)*(yvel0(j  ,k+1)+yvel0(j+1,k+1)                     &
                                    +yvel1(j  ,k+1)+yvel1(j+1,k+1)))*half_dt
        total_flux=right_flux-left_flux+top_flux-bottom_flux

        volume_change=volume(j,k)/(volume(j,k)+total_flux)

        min_cell_volume=MIN(volume(j,k)+right_flux-left_flux+top_flux-bottom_flux &
                           ,volume(j,k)+right_flux-left_flux                      &
                           ,volume(j,k)+top_flux-bottom_flux)
 
        recip_volume=1.0/volume(j,k) 

        energy_change=(pressure(j,k)/density0(j,k)+viscosity(j,k)/density0(j,k))*total_flux*recip_volume

        energy1(j,k)=energy0(j,k)-energy_change

        density1(j,k)=density0(j,k)*volume_change

        IF(material0(j,k).LT.0) THEN
          mmc=-material0(j,k)
          mmc_volume_change(mmc)=volume_change
          mmc_recip_volume(mmc)=recip_volume
          mmc_denom(mmc)=total_flux*recip_volume
        ENDIF

      ENDDO
    ENDDO
!$OMP END DO

    IF(multi_material) THEN

!$OMP DO PRIVATE(mmc,component)
      ! mmc_components is the number of components in a cell
      DO mmc=1,total_mmcs0
        DO component=mmc_index0(mmc),mmc_index0(mmc)+mmc_component0(mmc)-1
          ! This should have stride 1 through memory, but will the compiler realise?
          mmc_energy1(component)=mmc_energy0(component)                           &
                              -((mmc_pressure(component)/mmc_density0(component)  &
                                +viscosity(mmc_j0(mmc),mmc_k0(mmc))/density0(mmc_j0(mmc),mmc_k0(mmc)))&
                                *mmc_denom(mmc))
          mmc_density1(component)=mmc_density0(component)*mmc_volume_change(mmc)
        ENDDO
      ENDDO
!$OMP END DO
    ENDIF

  ENDIF

!$OMP END PARALLEL

END SUBROUTINE PdV_kernel

END MODULE PdV_kernel_module

