!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief interface reconstruction for multi-material advection
!>  @author Wayne Gaudin
!>  @details interface reconstruction for multi-material advection

MODULE interface_module

REAL(KIND=8), PARAMETER :: fvtol=1.0e-10_8

CONTAINS

! Comments : Calculates side fraction for two cells of size d1,d2
!            with volume fractions f1,f2
!
FUNCTION fside(f1,f2,d1,d2) RESULT (f_side)

  IMPLICIT NONE
  
  REAL (KIND=8) :: f1,f2,d1,d2 
  REAL (KIND=8) :: r,fmax,fmin,fr,f_side

!!$OMP DECLARE TARGET
  
  IF (f2.GT.f1) THEN
    fmax=f2
    fmin=f1
    r=d2/d1
  ELSE     
    fmax=f1
    fmin=f2
    r=d1/d2
  END IF
  
  fmax=MIN(fmax,0.999999_8)
  fmin=MAX(fmin,0.000001_8)
  
  ! Purpose of this do loop rather than an if then else?
  DO
    IF(fmax-fmin.GE.0.5_8) THEN
      IF((1.0_8-fmax)*(fmin/r+fmax).LE.0.25_8) THEN
        IF(fmin*(1.0_8+r-fmin-r*fmax).LE.0.25_8) THEN
          f_side=1.0_8/(1.0_8+SQRT(r*(1.0_8-fmax)/fmin))
          EXIT
        ENDIF
      ENDIF
    ENDIF
  
    IF(fmax.GE.(2.0_8+r)*fmin) THEN
      fr=fmin/r
      f_side=2.0_8*(SQRT(fr*(fr+fmax))-fr)
    ELSEIF((r+r+1.0_8)*fmax-r*fmin.GE.1.0_8+r) THEN
      fr=(1.0_8-fmax)*r
      f_side=1.0_8-2.0*(SQRT(fr*(fr+1.0_8-fmin))-fr)
    ELSE
      f_side=(r*fmin+fmax)/(1.0_8+r)
    ENDIF
    EXIT
  END DO

END FUNCTION fside

SUBROUTINE fvol(fva,eps,fdva,dx,dy,r)

  IMPLICIT NONE
  
  REAL (KIND=8)             :: fva(3,3),dx(3),dy(3),eps,r
  REAL (KIND=8)             :: fdva,f1,f8,fa,fb,fc,fd
  REAL (KIND=8)             :: ar,d1,d2,fv,p,q,fp,fdv
  REAL (KIND=8), PARAMETER  :: eee=0.025_8, ee1=1.0E-2_8, ee8=1.0E-1_8
  LOGICAL                   :: za,zb,z1,z2,z
  INTEGER                   :: mixedc(3,3),mixednum
!!$OMP DECLARE TARGET
  
  f1=fva(2,2) !centre cell
  
  DO ! do with an exit again, best practice?
  !
  ! Extract out exceptional cases (small vol fracs or all cells mixed)
  !
    IF(eps.LT.1.0E-10_8.OR.f1.EQ.0.0_8) THEN
      fdva=f1
      EXIT
    ELSEIF (f1.LT.ee1.OR.f1.GT.1.0_8-ee1 ) THEN
      f8=SUM(fva)-f1
      za=(f8.LT.ee8).AND.(f1.LT.ee1)
      zb=(f8.GT.8.0_8-ee8).AND.(f1.GT.1.0_8-ee1)
      IF(za.OR.zb) THEN
        fp=f1
        IF(zb) fp=1.0_8-f1
        fdv=0.0
        IF(r.LE.eps) fdv=MIN(1.0_8,fp/eps)
        fdva=fdv
        IF(zb) fdva=1.0_8-fdv
        EXIT
      ELSEIF(f1.LT.1.0E-4_8.OR. f1> 0.9999_8) THEN
        fdva=MIN(fva(3,2),f1/eps)
        fdva=MAX(fdva,(f1-1.0_8+eps)/eps)
        EXIT
      END IF
    END IF
  
    ! Array syntax
    WHERE (fva.GT.eee.AND.fva.LT.1.0_8-eee)
      mixedc=1
    ELSEWHERE
      mixedc=0
    END WHERE
  
    mixednum=SUM(mixedc)-mixedc(2,2)
  
    IF (mixednum.EQ.8) THEN
      fdva=MIN(fva(3,2),f1/eps)
      fdva=MAX(fdva,(f1-1.0_8+eps)/eps)
      EXIT
    END IF
  
  !
  ! Calculate side fractions.FA=East side fraction,FB=North,FC=West,FD=South
  ! 
  
    mixednum=mixedc(2,1)+mixedc(3,1)+mixedc(2,3)+mixedc(3,3)
    z1=mixednum.GE.3
    z2=fva(3,2).LT.fva(1,2)
    IF (mixednum.GE.3.AND.fva(3,2).LT.fva(1,2)) fa=0.0_8
    IF (mixednum.GE.3.AND..NOT.fva(3,2).LT.fva(1,2)) fa=1.0_8
    IF (.NOT.z1) fa=fside(f1,fva(3,2),dx(2),dx(3))

    mixednum=mixedc(3,2)+mixedc(3,3)+mixedc(1,2)+mixedc(1,3)
    z1=mixednum.GE.3
    z2=fva(2,3).LT.fva(2,1)
    IF (mixednum.GE.3.AND.fva(2,3).LT.fva(2,1)) fb=0.0_8
    IF (mixednum.GE.3.AND..NOT.fva(2,3).LT.fva(2,1)) fb=1.0_8
    IF (.NOT.z1) fb=fside(f1,fva(2,3),dy(2),dy(3))
  
    mixednum=mixedc(2,3)+mixedc(1,3)+mixedc(1,1)+mixedc(2,1)
    z1=mixednum.GE.3
    z2=fva(1,2).LT.fva(3,2)
    IF (mixednum.GE.3.AND.fva(1,2).LT.fva(3,2)) fc=0.0_8
    IF (mixednum.GE.3.AND..NOT.fva(1,2).LT.fva(3,2)) fc=1.0_8
    IF (.NOT.z1) fc=fside(f1,fva(1,2),dx(2),dx(1))

    mixednum=mixedc(1,1)+mixedc(1,2)+mixedc(3,1)+mixedc(3,2)
    Z1=mixednum.GE.3
    Z2=fva(2,1).LT.fva(2,3)
    IF (mixednum.GE.3.AND.fva(2,1).LT.fva(2,3)) fd=0.0_8
    IF (mixednum.GE.3.AND..NOT.fva(2,1).LT.fva(2,3)) fd=1.0_8
    IF (.NOT.z1) fd=fside(f1,fva(2,1),dy(2),dy(1))
    d1=ABS(fb-fd)+1.0E-10_8
    d2=ABS(fa-fc)+1.0E-10_8
  !
  !Scheme based on the approximation (V=volume fraction)
  !         dV/dx=fEast-fWest       (D2=|dV/dx|)
  !         dV/dy=fNorth-fSouth     (D1=|dV/dy|)
  !dirn of normal=dirn of max change in V => tan(theta) = (dV/dy) / (dV/dx)
  !This is then translated to get the right area.
  !AR=area of triangle formed when interface intersects lower corner of cell
  ! 
  
    z=fa.GT.fc
    IF (fa.gt.fc)      fv=fva(2,2)
    IF (.NOT.z) fv=1.0_8-fva(2,2)
    IF (d1.GE.d2) THEN 
      ar=0.5_8*d2/d1
      IF (fv.LE.ar ) THEN
        IF (fv.LE.eps**2*ar) THEN
          fdv=fv/eps
        ELSE
          p=SQRT(fv/ar)
          q=p*d2/d1
          fdv=q*(1.0_8-0.5_8*eps/p)
        ENDIF
      ELSE IF (fv.LE.1.0_8-ar) THEN
        fdv=fv+(1.0_8-eps)*ar
      ELSE IF ((1.0_8-fv).GE.(1.0_8-eps)**2*ar) THEN
        P=SQRT(MAX(0.0_8,(1.0_8-fv))/ar)
        fdv=((p+eps-1.0_8)/P)**2*(1.0_8-fv)/eps
        fdv=1.0_8-fdv
      ELSE
        fdv=1.0_8
      ENDIF
    ELSE
      ar=0.5_8*d1/d2
      IF (fv.LE.ar ) THEN
        IF (d1**2*fv <= (d2*eps)**2*ar) THEN
          fdv=fv/eps
        ELSE
          q=SQRT(fv/ar)
          p=q*d1/d2
          fdv=q*(1.0_8-0.5_8*eps/p)
        ENDIF
      ELSEIF (fv.LE.1.0_8-AR) THEN
        p=fv-ar
        q=fv+ar
        IF (eps.LE.p) THEN
          fdv=1.0_8
        ELSEIF(eps.LE.Q) THEN 
          fdv=(eps-p)**2/(4.0_8*ar*eps)
          fdv=1.0_8-fdv
        ELSE
          fdv=fv/eps
        ENDIF
      ELSEIF(d1**2*(1.0_8-fv).GE.((1.0_8-eps)*d2)**2*ar) THEN
        p=d1/d2*SQRT(MAX(0.0_8,(1.0_8-fv))/ar)
        fdv=((p+eps-1.0_8)/p)**2*(1.0_8-fv)/eps
        fdv=1.0_8-fdv
      ELSE
        fdv=1.0_8
      ENDIF
  
    ENDIF
  
    IF(z)      fdva=fdv
    IF(.not.Z) fdva=1.0_8-fdv   
    EXIT
  
  ENDDO

END SUBROUTINE fvol

SUBROUTINE setstencil_x(j0,k0,dirn,material1,mmc_component1,mmc_index1,mmc_material1,mmc_volume,cdx,cdy,&
                      x_min,x_max,y_min,y_max,max_mats,max_mat_number,ns,fa,scdx,scdy)

  IMPLICIT NONE

  INTEGER(KIND=4),INTENT(IN) :: j0,k0,dirn,x_min,x_max,y_min,y_max,max_mats,max_mat_number,ns
  INTEGER(KIND=4),INTENT(IN) :: material1(x_min-2:x_max+2,y_min-2:y_max+2),mmc_index1(:),mmc_material1(:),mmc_component1(:)
  REAL(KIND=8),   INTENT(IN) :: mmc_volume(:),cdx(x_min-2:x_max+2),cdy(y_min-2:y_max+2)
  REAL(KIND=8),  INTENT(OUT) :: fa(ns,ns,max_mats),scdx(ns),scdy(ns)
  INTEGER(KIND=4) :: cc,j,jj,k,kk,ks(ns),mmc,m,mm,m1
!!$OMP DECLARE TARGET

  cc=(1+ns)/2 !index of centre cell
  DO kk=1,ns
    k=k0+kk-cc
    ks(kk)=k
    scdy(kk)=cdy(k)
  ENDDO

  fa=0.0_8

  DO jj=1,ns
    j=j0+(jj-cc)*dirn
    scdx(jj)=cdx(j)
    DO kk=1,ns
      k=ks(kk)
      mmc=material1(j,k)
      IF(mmc.LT.0)THEN
        mmc=-mmc
        DO m=mmc_index1(mmc),mmc_index1(mmc)+mmc_component1(mmc)-1
          mm=mmc_material1(m)
          m1=mm !priority(mm) priority list just uses material number
          fa(jj,kk,1:m1)=fa(jj,kk,1:m1)+mmc_volume(m)
        ENDDO ! m
      ELSE
        m1=mmc ! priority(mat)
        fa(jj,kk,1:m1)=1.0_8
      ENDIF
    ENDDO ! k
  ENDDO ! j

END SUBROUTINE setstencil_x

SUBROUTINE setstencil_y(j0,k0,dirn,material1,mmc_component1,mmc_index1,mmc_material1,mmc_volume,cdx,cdy,&
                      x_min,x_max,y_min,y_max,max_mats,max_mat_number,ns,fa,scdx,scdy)

  IMPLICIT NONE

  INTEGER(KIND=4),INTENT(IN) :: j0,k0,dirn,x_min,x_max,y_min,y_max,max_mats,max_mat_number,ns
  INTEGER(KIND=4),INTENT(IN) :: material1(x_min-2:x_max+2,y_min-2:y_max+2),mmc_index1(:),mmc_material1(:),mmc_component1(:)
  REAL(KIND=8),   INTENT(IN) :: mmc_volume(:),cdx(x_min-2:x_max+2),cdy(y_min-2:y_max+2)
  REAL(KIND=8),  INTENT(OUT) :: fa(ns,ns,max_mats),scdx(ns),scdy(ns)
  INTEGER(KIND=4) :: cc,j,jj,k,kk,js(ns),mmc,m,mm,m1
!!$OMP DECLARE TARGET

  cc=(1+ns)/2 !index of centre cell
  DO jj=1,ns
    j=j0+jj-cc
    js(jj)=j
    scdx(jj)=cdx(j)
  ENDDO

  fa=0.0_8

  DO kk=1,ns
    k=k0+(kk-cc)*dirn
    scdy(kk)=cdy(k)
    DO jj=1,ns
      j=js(jj)
      mmc=material1(j,k)
      IF(mmc.LT.0)THEN
        mmc=-mmc
        DO m=mmc_index1(mmc),mmc_index1(mmc)+mmc_component1(mmc)-1
          mm=mmc_material1(m)
          m1=mm !priority(mm) priority list just uses material number
          fa(kk,jj,1:m1)=fa(kk,jj,1:m1)+mmc_volume(m)
        ENDDO ! m
      ELSE
        m1=mmc ! priority(mat)
        fa(kk,jj,1:m1)=1.0_8
      ENDIF
    ENDDO ! k
  ENDDO ! j

END SUBROUTINE setstencil_y

SUBROUTINE interface(max_mats,max_mat_number,j,k,dir,dirn,material1,mmc_index1, &
                     mmc_component1,mmc_material1,mmc_volume1,cdx,cdy,x_min,x_max,y_min,y_max,eps,fdv)

  IMPLICIT NONE

!ADD EXPLICIT LIMITS
  INTEGER      :: j,k,dir,dirn,x_min,x_max,y_min,y_max
  INTEGER      :: material1(x_min-2:x_max+2,y_min-2:y_max+2),mmc_index1(:),mmc_material1(:),mmc_component1(:)
  REAL(KIND=8) :: mmc_volume1(:),cdx(x_min-2:x_max+2),cdy(y_min-2:y_max+2)
  REAL(KIND=8) :: eps

  REAL(KIND=8) :: fa(3,3,max_mats),fdvs(max_mats+1),fdv(max_mats+1)
  REAL(KIND=8) :: scdx(3),scdy(3)
  REAL(KIND=8) :: fdva,fade,r
  INTEGER      :: mmc,m,m1,mm,max_mats,max_mat_number
!!$OMP DECLARE TARGET

  IF(dir.EQ.1) THEN
    CALL setstencil_x(j,k,dirn,material1,mmc_component1,mmc_index1,mmc_material1,mmc_volume1,cdx,cdy,&
                      x_min,x_max,y_min,y_max,max_mats,max_mat_number,3,fa,scdx,scdy)
  ELSE
    CALL setstencil_y(j,k,dirn,material1,mmc_component1,mmc_index1,mmc_material1,mmc_volume1,cdx,cdy,&
                      x_min,x_max,y_min,y_max,max_mats,max_mat_number,3,fa,scdx,scdy)
  ENDIF

  !---fdvs is quantity of each region rhs of x=eps line ie thats advected out
  fdvs=1.0_8
  DO m=2,max_mats
    m1=m-1
    IF (fa(2,2,m).GE.fa(2,2,m1)-fvtol) THEN
      fdvs(m)=fdvs(m1)
      CYCLE
    ENDIF
    r=0.5
    CALL fvol(fa(:,:,m),eps,fdva,scdx,scdy,r) ! This actually calculates face volumes
    ! Print out the values that come out of fvol, i.e. fdva
    fdvs(m)=MIN(fdva,fdvs(m1)) ! This will put a ceiling of 1.0 on any crossing
    IF (ABS(eps).GE.fvtol) THEN
      fade=(fa(2,2,m1)-fa(2,2,m))/eps
      IF (fdvs(m1)-fdvs(m).GT.fade) fdvs(m)=fdvs(m1)-fade
    ENDIF
  ENDDO ! m

  fdvs(max_mats+1)=0.0_8 ! Does this need zeroing here?
  mmc=-material1(j,k)

  DO m=mmc_index1(mmc),mmc_index1(mmc)+mmc_component1(mmc)-1
    mm=mmc_material1(m)
    m1=mm !priority(mm)
    fdv(mm)=fdvs(m1)-fdvs(m1+1)
  ENDDO ! m

END SUBROUTINE interface

END MODULE interface_module

