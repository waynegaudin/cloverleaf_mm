!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief standalone driver for the cell multi-material advection kernels
!>  @author Wayne Gaudin
!>  @details Calls user requested kernel in standalone mode

PROGRAM hydro_driver

  USE data_module
  USE set_data_module
  USE update_halo_kernel_module
  USE ideal_gas_kernel_module
  USE viscosity_kernel_module
  USE calc_dt_kernel_module
  USE revert_kernel_module
  USE accelerate_kernel_module
  USE PdV_kernel_module
  USE flux_calc_kernel_module
  USE advec_mmc_x_cell_module
  USE advec_mmc_y_cell_module
  USE advec_mom_kernel_mod
  USE reset_field_kernel_module
  USE field_summary_kernel_module

  IMPLICIT NONE

!$ INTEGER :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM
!!$ INTEGER :: OMP_GET_NUM_TEAMS,OMP_GET_TEAM_NUM

  INTEGER :: numargs,iargc,i
  CHARACTER (LEN=20)  :: command_line,temp

  INTEGER :: x_size,y_size,mm,vis
  LOGICAL :: multi_material,visualise

  LOGICAL :: reallocation_needed,predict,pressure_average,sound_speed_average

  REAL(KIND=8) :: kernel_timer,timer,time,wall_clock_time,total_time

  REAL(KIND=8) :: timestep_timer,ideal_gas_timer,viscosity_timer,PdV_timer,revert_timer,summary_timer,boundary_timer
  REAL(KIND=8) :: acceleration_timer,flux_timer,cell_advection_timer,mom_advection_timer,reset_timer,visit_timer

  INTEGER :: x_min,x_max,y_min,y_max,its,iteration,direction,sweep_number,m,which_vel,mat
  REAL(KIND=8),ALLOCATABLE :: vertexdx(:),vertexdy(:)
  REAL(KIND=8),ALLOCATABLE :: vertexx(:),vertexy(:)
  REAL(KIND=8),ALLOCATABLE :: celldx(:),celldy(:)
  REAL(KIND=8),ALLOCATABLE :: xarea(:,:),yarea(:,:)
  REAL(KIND=8),ALLOCATABLE :: volume(:,:)
  REAL(KIND=8),ALLOCATABLE :: density0(:,:),energy0(:,:)
  REAL(KIND=8),ALLOCATABLE :: density1(:,:),energy1(:,:)
  REAL(KIND=8),ALLOCATABLE :: densitynew(:,:),energynew(:,:)
  REAL(KIND=8),ALLOCATABLE :: pressure(:,:),soundspeed(:,:),viscosity(:,:)
  REAL(KIND=8),ALLOCATABLE :: mmc_density0(:),mmc_energy0(:),mmc_volume0(:)
  REAL(KIND=8),ALLOCATABLE :: mmc_density1(:),mmc_energy1(:),mmc_volume1(:)
  REAL(KIND=8),ALLOCATABLE :: mmc_densitynew(:),mmc_energynew(:),mmc_volumenew(:)
  REAL(KIND=8),ALLOCATABLE :: mmc_pressure(:),mmc_soundspeed(:),mmc_viscosity(:)
  REAL(KIND=8),ALLOCATABLE :: xvel0(:,:),yvel0(:,:),xvel1(:,:),yvel1(:,:)
  REAL(KIND=8),ALLOCATABLE :: vol_flux_x(:,:),vol_flux_y(:,:),mass_flux_x(:,:),mass_flux_y(:,:)
  REAL(KIND=8),ALLOCATABLE :: pre_vol(:,:),post_vol(:,:)
  REAL(KIND=8),POINTER     :: advec_vel(:,:)
  REAL(KIND=8),ALLOCATABLE,TARGET :: ener_flux(:,:)
  REAL(KIND=8),ALLOCATABLE :: node_flux(:,:),node_mass_post(:,:),node_mass_pre(:,:)
  REAL(KIND=8),ALLOCATABLE :: mom_flux(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: material0(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: material1(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: materialnew(:,:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_component0(:),mmc_material0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_component1(:),mmc_material1(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_componentnew(:),mmc_materialnew(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_j0(:),mmc_k0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_j1(:),mmc_k1(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_jnew(:),mmc_knew(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_index0(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_index1(:)
  INTEGER(KIND=4),ALLOCATABLE :: mmc_indexnew(:)
  INTEGER(KIND=4)             :: total_mmcs0,total_components0
  INTEGER(KIND=4)             :: total_mmcs1,total_components1
  INTEGER(KIND=4)             :: total_mmcsnew,total_componentsnew
  INTEGER(KIND=4)             :: total_mmcsmax,total_componentsmax
  INTEGER(KIND=4)             :: total_mmcsmax_old,total_componentsmax_old
  INTEGER(KIND=4)             :: total_mmcs_halo,total_components_halo
  INTEGER(KIND=4)             :: max_mats,max_comps
  INTEGER(KIND=4)             :: x_first

  REAL(KIND=8),ALLOCATABLE, DIMENSION(:,:,:):: dv
  REAL(KIND=8),ALLOCATABLE, DIMENSION(:,:,:):: dm
  REAL(KIND=8),ALLOCATABLE, DIMENSION(:,:,:):: de
  INTEGER                                   :: number_of_threads,max_mat_number
  INTEGER                                   :: number_of_threads_gpu
  INTEGER                                   :: number_of_teams
  INTEGER                                   :: team_number
  INTEGER,ALLOCATABLE, DIMENSION(:)         :: mmc_counter_next
  INTEGER,ALLOCATABLE, DIMENSION(:)         :: component_counter_next
  INTEGER,ALLOCATABLE, DIMENSION(:)         :: mmc_start_index,mmc_index,mmc_length
  INTEGER,ALLOCATABLE, DIMENSION(:)         :: comp_start_index,comp_index,comp_length
  INTEGER,ALLOCATABLE, DIMENSION(:)         :: mat_list
  INTEGER,ALLOCATABLE, DIMENSION(:)         :: reverse_mat_list
  INTEGER,PARAMETER                         :: number_of_materials=2
  REAL(KIND=8), DIMENSION(number_of_materials):: vol,mass,ie,ke,press
  INTEGER :: l_control,jldt,kldt,small,dtl_control
  REAL(KIND=8) :: dtmin,dtc_safe,dtu_safe,dtv_safe,dtdiv_safe,dt_min_val,xl_pos,yl_pos
  REAL(KIND=8),ALLOCATABLE, DIMENSION(:) :: cellx,celly

  INTEGER :: j,k,component,thread
  REAL(KIND=8) :: mmc_delta,component_delta
  INTEGER :: mmc_expansion_factor,comp_expansion_factor

  INTEGER,ALLOCATABLE, DIMENSION(:)         :: material
  LOGICAL,ALLOCATABLE, DIMENSION(:)         :: unique_material

  INTEGER :: fields(NUM_FIELDS),depth

  ALLOCATE(unique_material(2))
  ALLOCATE(material(2))
  material(1)=1
  material(2)=2

  number_of_threads=1
  number_of_threads_gpu=0
  number_of_teams=0

  mmc_expansion_factor=2
  comp_expansion_factor=2

  time=0.0

!$OMP PARALLEL
!$  number_of_threads=OMP_GET_NUM_THREADS()
!$OMP END PARALLEL

!!$ number_of_teams=omp_get_num_teams()
!$ number_of_threads_gpu=omp_get_num_threads()

  write(0,*)"Number of teams ",number_of_teams
  write(0,*)"Number of gpu threads",number_of_threads_gpu
  write(0,*)"Number of threads",number_of_threads

  x_size=100
  y_size=100
  its=1
  multi_material=.FALSE.
  visualise=.FALSE.
  vis=0
  mm=0

  dtmin      =0.0000001_8
  dtc_safe   =0.7_8
  dtu_safe   =0.5_8
  dtv_safe   =0.5_8
  dtdiv_safe =0.7_8

  numargs = iargc()

  DO i=1,numargs,2
    CALL GETARG(i,command_line)
    SELECT CASE (command_line)
      CASE("-help")
        WRITE(*,*) "Usage -nx 100 -ny 100 -its 10 -multimat 0|1"
        STOP
      CASE("-nx")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") x_size
      CASE("-ny")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") y_size
      CASE("-its")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") its
      CASE("-multimat")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") mm
      CASE("-vis")
        CALL GETARG(i+1,temp)
        READ(UNIT=temp,FMT="(I20)") vis
    END SELECT
  ENDDO

  x_min=1
  y_min=1
  x_max=x_size
  y_max=y_size

  IF(mm.NE.0) THEN
    multi_material=.TRUE.
    max_mats=2
    max_mat_number=2
  ELSE
    max_mats=1
    max_mat_number=1
  ENDIF

  IF(vis.NE.0) visualise=.TRUE.

  ALLOCATE(mmc_counter_next(number_of_threads))
  ALLOCATE(component_counter_next(number_of_threads))
  ALLOCATE(mmc_start_index(0:number_of_threads))
  ALLOCATE(mmc_index(0:number_of_threads))
  ALLOCATE(mmc_length(0:number_of_threads))
  ALLOCATE(comp_start_index(0:number_of_threads))
  ALLOCATE(comp_index(0:number_of_threads))
  ALLOCATE(comp_length(0:number_of_threads))

  ALLOCATE(mat_list(max_mats))
  ALLOCATE(reverse_mat_list(max_mat_number))
  DO m=1,max_mats
    mat_list(m)=m
  ENDDO
  DO m=1,max_mat_number
    reverse_mat_list(m)=m
  ENDDO

  WRITE(*,*) "Hydro MMC Driver"
  WRITE(*,*) "Mesh size ",x_size,y_size
  WRITE(*,*) "Iterations ",its
  IF(multi_material) WRITE(*,*)"Multi material"
  IF(.NOT.multi_material) WRITE(*,*)"Single material"
  WRITE(*,*)"Number of threads ",number_of_threads

  timestep_timer=0.0
  ideal_gas_timer=0.0
  viscosity_timer=0.0
  PdV_timer=0.0
  revert_timer=0.0
  summary_timer=0.0
  acceleration_timer=0.0
  flux_timer=0.0
  cell_advection_timer=0.0
  mom_advection_timer=0.0
  visit_timer=0.0
  reset_timer=0.0
  boundary_timer=0.0

  kernel_timer=timer()

  CALL set_data(x_min,x_max,y_min,y_max,                &
                multi_material,                         &
                cellx=cellx,                            &
                celly=celly,                            &
                vertexdx=vertexdx,                      &
                vertexdy=vertexdy,                      &
                vertexx=vertexx,                        &
                vertexy=vertexy,                        &
                celldx=celldx,                          &
                celldy=celldy,                          &
                xarea=xarea,                            &
                yarea=yarea,                            &
                volume=volume,                          &
                energy0=energy0,                        &
                energy1=energy1,                        &
                energynew=energynew,                    &
                density0=density0,                      &
                density1=density1,                      &
                densitynew=densitynew,                  &
                pressure=pressure,                      &
                soundspeed=soundspeed,                  &
                viscosity=viscosity,                    &
                xvel0=xvel0,                            &
                xvel1=xvel1,                            &
                yvel0=yvel0,                            &
                yvel1=yvel1,                            &
                vol_flux_x=vol_flux_x,                  &
                vol_flux_y=vol_flux_y,                  &
                mass_flux_x=mass_flux_x,                &
                mass_flux_y=mass_flux_y,                &
                work_array1=pre_vol,                    &
                work_array2=post_vol,                   &
                work_array3=node_mass_pre,              &
                work_array4=mom_flux,                   &
                work_array5=node_mass_post,             &
                work_array6=node_flux,                  &
                work_array7=ener_flux,                  &
                material0=material0,                    &
                material1=material1,                    &
                materialnew=materialnew,                &
                mmc_component0=mmc_component0,          &
                mmc_component1=mmc_component1,          &
                mmc_componentnew=mmc_componentnew,      &
                mmc_index0=mmc_index0,                  &
                mmc_index1=mmc_index1,                  &
                mmc_indexnew=mmc_indexnew,              &
                mmc_j0=mmc_j0,                          &
                mmc_k0=mmc_k0,                          &
                mmc_j1=mmc_j1,                          &
                mmc_k1=mmc_k1,                          &
                mmc_jnew=mmc_jnew,                      &
                mmc_knew=mmc_knew,                      &
                mmc_material0=mmc_material0,            &
                mmc_material1=mmc_material1,            &
                mmc_materialnew=mmc_materialnew,        &
                mmc_density0=mmc_density0,              &
                mmc_density1=mmc_density1,              &
                mmc_densitynew=mmc_densitynew,          &
                mmc_energy0=mmc_energy0,                &
                mmc_energy1=mmc_energy1,                &
                mmc_energynew=mmc_energynew,            &
                mmc_pressure=mmc_pressure,              &
                mmc_soundspeed=mmc_soundspeed,          &
                mmc_viscosity=mmc_viscosity,            &
                mmc_volume0=mmc_volume0,                &
                mmc_volume1=mmc_volume1,                &
                mmc_volumenew=mmc_volumenew,            &
                total_mmcs0=total_mmcs0,                &
                total_components0=total_components0,    &
                total_mmcs1=total_mmcs1,                &
                total_components1=total_components1,    &
                total_mmcsmax=total_mmcsmax,            &
                total_componentsmax=total_componentsmax,&
                dt=dt_min_val                           )

                fields=0
                fields(FIELD_MATERIAL0)=3
                fields(FIELD_DENSITY0)=3
                fields(FIELD_ENERGY0)=3
                fields(FIELD_PRESSURE)=3

  CALL update_ext_halo(x_min,x_max,y_min,y_max,total_mmcs0,total_components0,total_mmcs1,total_components1,&
                       total_mmcsmax,total_componentsmax,total_mmcs_halo,total_components_halo,            &
                       number_of_threads,mmc_counter_next,component_counter_next,                          &
                       density0,energy0,pressure,viscosity,soundspeed, density1,energy1,                   &
                       xvel0,yvel0,xvel1,yvel1,vol_flux_x,vol_flux_y,mass_flux_x,mass_flux_y,              &
                       material0,mmc_index0,mmc_j0,mmc_k0,mmc_component0,mmc_material0,                    &
                       mmc_density0,mmc_energy0,mmc_pressure,mmc_soundspeed,mmc_viscosity,mmc_volume0,     &
                       material1,mmc_index1,mmc_j1,mmc_k1,mmc_component1,mmc_material1,                    &
                       mmc_density1,mmc_energy1,mmc_volume1,fields,depth                                   )

  ! I NEED TO UPDATE THESE CORRECTLY NOW
  total_mmcs_halo=total_mmcs1
  total_components_halo=total_components1

  ! Calculate an initial partition of the MMC data storage for each thread
  component_delta=REAL(total_componentsmax,8)/REAL(number_of_threads,8)
  mmc_delta=REAL(total_mmcsmax,8)/REAL(number_of_threads,8)

  mmc_index(0)=1
  mmc_start_index(0)=1
  comp_index(0)=1
  comp_start_index(0)=1

  DO thread=1,number_of_threads
    mmc_index(thread)=1+(thread-1)*mmc_delta
    mmc_start_index(thread)=1+(thread-1)*mmc_delta
    comp_index(thread)=1+(thread-1)*component_delta
    comp_start_index(thread)=1+(thread-1)*component_delta
  ENDDO

  CALL field_summary_kernel(x_min,x_max,y_min,y_max, &
                            total_mmcs0,             &
                            total_components0,       &
                            total_mmcsmax,           &
                            total_componentsmax,     &
                            max_mats,                &
                            volume,                  &
                            density0,                &
                            energy0,                 &
                            pressure,                &
                            xvel0,                   &
                            yvel0,                   &
                            material0,               &
                            mmc_component0,          &
                            mmc_index0,              &
                            mmc_material0,           &
                            mmc_volume0,             &
                            mmc_density0,            &
                            mmc_energy0,             &
                            mmc_pressure,            &
                            vol,mass,ie,ke,press     )
  WRITE(*,*)
  WRITE(*,*) 'Time ',time
  WRITE(*,'(a13,8a16)')'           ','Material','Volume','Mass','Density','Pressure' &
                                    ,'Internal Energy','Kinetic Energy','Total Energy'
  DO mat=1,max_mats
    WRITE(*,'(a6,i7,i16,7e16.4)')' step:',0,mat,vol(mat),mass(mat),mass(mat)/vol(mat), &
                                          press(mat)/vol(mat),ie(mat),ke(mat),ie(mat)+ke(mat)
  ENDDO
  WRITE(*,'(a29,7e16.4)')'Totals',SUM(vol),SUM(mass),SUM(mass)/SUM(vol), &
                                        SUM(press)/SUM(vol),SUM(ie),SUM(ke),SUM(ie)+SUM(ke)
  CALL visit_kernel(0,                                &
                    x_min,                            &
                    x_max,                            &
                    y_min,                            &
                    y_max,                            &
                    vertexx,                          &
                    vertexy,                          &
                    material0,                        &
                    density0,                         &
                    energy0,                          &
                    pressure,                         &
                    viscosity,                        &
                    xvel0,                            &
                    yvel0                             )

  advec_vel => ener_flux

  ALLOCATE(dv(max_mats,x_min-2:x_max+2,y_min-2:y_max+2))
  ALLOCATE(dm(max_mats,x_min-2:x_max+2,y_min-2:y_max+2))
  ALLOCATE(de(max_mats,x_min-2:x_max+2,y_min-2:y_max+2))

  WRITE(*,*) "Setup time ",timer()-kernel_timer

  WRITE(*,*) "Data initialised"

  vol_flux_x=0.1_8
  vol_flux_y=0.1_8

  pressure_average=.TRUE.
  sound_speed_average=.TRUE.

  wall_clock_time=timer()

  WRITE(0,*)"total_mmcs1 ",total_mmcs1
  WRITE(0,*)"total_components1 ",total_components1
  WRITE(0,*)"thread number     ",number_of_threads
  DO iteration=1,its
    x_first=MOD(iteration+1,2)
    WRITE(0,*)"Iteration count ", iteration, x_first

    kernel_timer=timer()

    CALL ideal_gas_kernel(x_min,x_max,y_min,y_max, &
                          total_mmcs0,             &
                          total_components0,       &
                          total_mmcsmax,           &
                          total_componentsmax,     &
                          volume,                  &
                          density0,                &
                          energy0,                 &
                          pressure,                &
                          soundspeed,              &
                          material0,               &
                          mmc_component0,          &
                          mmc_index0,              &
                          mmc_j0,                  &
                          mmc_k0,                  &
                          mmc_material0,           &
                          mmc_density0,            &
                          mmc_energy0,             &
                          mmc_pressure,            &
                          mmc_soundspeed,          &
                          mmc_volume0,             &
                          pressure_average,        &
                          sound_speed_average      )

    ideal_gas_timer=ideal_gas_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL viscosity_kernel(x_min,x_max,y_min,y_max,    &
                          celldx,celldy,              &
                          density0,                   &
                          pressure,                   &
                          viscosity,                  &
                          xvel0,                      &
                          yvel0                       )

    viscosity_timer=viscosity_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL calc_dt_kernel(x_min,x_max,y_min,y_max,             &
                        g_small,g_big,dtmin,                 &
                        dtc_safe,                            &
                        dtu_safe,                            &
                        dtv_safe,                            &
                        dtdiv_safe,                          &
                        xarea,                               &
                        yarea,                               &
                        cellx,                               &
                        celly,                               &
                        celldx,                              &
                        celldy,                              &
                        volume,                              &
                        density0,                            &
                        energy0,                             &
                        pressure,                            &
                        viscosity,                           &
                        soundspeed,                          &
                        xvel0,yvel0,                         &
                        dt_min_val,                          &
                        dtl_control,                         &
                        xl_pos,                              &
                        yl_pos,                              &
                        jldt,                                &
                        kldt,                                &
                        small                                )

    WRITE(*,*) "Dt                       ",dt_min_val
    time=time+dt_min_val

    timestep_timer=timestep_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    predict=.TRUE.

    CALL PdV_kernel(predict,            &
                    x_min,              &
                    x_max,              &
                    y_min,              &
                    y_max,              &
                    dt_min_val,         &
                    total_mmcs0,        &
                    total_components0,  &
                    total_mmcsmax,      &
                    total_componentsmax,&
                    xarea,              &
                    yarea,              &
                    volume ,            &
                    density0,           &
                    density1,           &
                    energy0,            &
                    energy1,            &
                    pressure,           &
                    viscosity,          &
                    xvel0,              &
                    xvel1,              &
                    yvel0,              &
                    yvel1,              &
                    material0,          &
                    mmc_index0,         &
                    mmc_j0,             &
                    mmc_k0,             &
                    mmc_component0,     &
                    mmc_density0,       &
                    mmc_density1,       &
                    mmc_energy0,        &
                    mmc_energy1,        &
                    mmc_pressure,       &
                    mmc_viscosity       )

    PdV_timer=PdV_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL ideal_gas_kernel(x_min,x_max,y_min,y_max, &
                          total_mmcs0,             &
                          total_components0,       &
                          total_mmcsmax,           &
                          total_componentsmax,     &
                          volume,                  &
                          density1,                &
                          energy1,                 &
                          pressure,                &
                          soundspeed,              &
                          material0,               &
                          mmc_component0,          &
                          mmc_index0,              &
                          mmc_j0,                  &
                          mmc_k0,                  &
                          mmc_material0,           &
                          mmc_density1,            &
                          mmc_energy1,             &
                          mmc_pressure,            &
                          mmc_soundspeed,          &
                          mmc_volume0,             &
                          pressure_average,        &
                          sound_speed_average      )

    ideal_gas_timer=ideal_gas_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL revert_kernel(x_min,                        &
                       x_max,                        &
                       y_min,                        &
                       y_max,                        &
                       total_components0,            &
                       total_componentsmax,          &
                       density0,                     &
                       density1,                     &
                       energy0,                      &
                       energy1,                      &
                       mmc_density0,                 &
                       mmc_density1,                 &
                       mmc_energy0,                  &
                       mmc_energy1                   )

    revert_timer=revert_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL accelerate_kernel(x_min,                  &
                           x_max,                  &
                           y_min,                  &
                           y_max,                  &
                           dt_min_val,             &
                           xarea,                  &
                           yarea,                  &
                           volume,                 &
                           density0,               &
                           pressure,               &
                           viscosity,              &
                           xvel0,                  &
                           yvel0,                  &
                           xvel1,                  &
                           yvel1                   )

    acceleration_timer=acceleration_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    predict=.FALSE.

    CALL PdV_kernel(predict,            &
                    x_min,              &
                    x_max,              &
                    y_min,              &
                    y_max,              &
                    dt_min_val,         &
                    total_mmcs0,        &
                    total_components0,  &
                    total_mmcsmax,      &
                    total_componentsmax,&
                    xarea,              &
                    yarea,              &
                    volume ,            &
                    density0,           &
                    density1,           &
                    energy0,            &
                    energy1,            &
                    pressure,           &
                    viscosity,          &
                    xvel0,              &
                    xvel1,              &
                    yvel0,              &
                    yvel1,              &
                    material0,          &
                    mmc_index0,         &
                    mmc_j0,             &
                    mmc_k0,             &
                    mmc_component0,     &
                    mmc_density0,       &
                    mmc_density1,       &
                    mmc_energy0,        &
                    mmc_energy1,        &
                    mmc_pressure,       &
                    mmc_viscosity       )

    PdV_timer=PdV_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL flux_calc_kernel(x_min,x_max,y_min,y_max,dt_min_val,&
                          xarea,                             &
                          yarea,                             &
                          xvel0,                             &
                          yvel0,                             &
                          xvel1,                             &
                          yvel1,                             &
                          vol_flux_x,                        &
                          vol_flux_y                         )

    flux_timer=flux_timer+(timer()-kernel_timer)

    IF( x_first.EQ.1) THEN

      kernel_timer=timer()

      DO k=y_min-2,y_max+2
        DO j=x_min-2,x_max+2
          IF(material1(j,k).GT.0) THEN
            unique_material(material1(j,k))=.TRUE.
          ENDIF
        ENDDO
      ENDDO
      DO component=1,total_components_halo
        unique_material(mmc_material1(component))=.TRUE.
      ENDDO

      direction=1
      sweep_number=1
      reallocation_needed=.TRUE.
      DO WHILE(reallocation_needed)
        CALL advec_mmc_x_cell_kernel(x_min,x_max,y_min,y_max,    &
                                     sweep_number,               &
                                     max_mats,                   &
                                     max_mat_number,             &
                                     mat_list,                   &
                                     reverse_mat_list,           &
                                     total_mmcs1,                &
                                     total_components1,          &
                                     total_mmcsmax,              &
                                     total_componentsmax,        &
                                     total_mmcs_halo,            &
                                     total_components_halo,      &
                                     total_mmcsnew,              &
                                     total_componentsnew,        &
                                     max_comps,                  &
                                     number_of_threads,          &
                                     mmc_counter_next,           &
                                     component_counter_next,     &
                                     celldx,                     &
                                     celldy,                     &
                                     vertexdx,                   &
                                     vertexdy,                   &
                                     volume,                     &
                                     density1,                   &
                                     densitynew,                 &
                                     energy1,                    &
                                     energynew,                  &
                                     mass_flux_x,                &
                                     vol_flux_x,                 &
                                     mass_flux_y,                &
                                     vol_flux_y,                 &
                                     pre_vol,                    &
                                     post_vol,                   &
                                     ener_flux,                  &
                                     dv,                         &
                                     dm,                         &
                                     de,                         &
                                     material1,                  &
                                     materialnew,                &
                                     mmc_material1,              &
                                     mmc_materialnew,            &
                                     mmc_index1,                 &
                                     mmc_indexnew,               &
                                     mmc_j1,                     &
                                     mmc_k1,                     &
                                     mmc_jnew,                   &
                                     mmc_knew,                   &
                                     mmc_component1,             &
                                     mmc_componentnew,           &
                                     mmc_density1,               &
                                     mmc_densitynew,             &
                                     mmc_energy1,                &
                                     mmc_energynew,              &
                                     mmc_volume1,                &
                                     mmc_volumenew,              &
                                     mmc_start_index,            &
                                     mmc_index,                  &
                                     comp_start_index,           &
                                     comp_index,                 &
                                     reallocation_needed         )
        write(0,*)"total_mmcs1 ",total_mmcsnew
        write(0,*)"total_components1 ",total_componentsnew
        write(0,*)"total_mmcsmax ",total_mmcsmax
        write(0,*)"total_componentsmax ",total_componentsmax
        IF(reallocation_needed) THEN
          ! Allocate new arrays
          DEALLOCATE(mmc_jnew)
          DEALLOCATE(mmc_knew)
          DEALLOCATE(mmc_indexnew)
          DEALLOCATE(mmc_componentnew)
          DEALLOCATE(mmc_volumenew)
          DEALLOCATE(mmc_materialnew)
          DEALLOCATE(mmc_densitynew)
          DEALLOCATE(mmc_energynew)
          DEALLOCATE(mmc_pressure)
          DEALLOCATE(mmc_soundspeed)
          total_mmcsmax_old=total_mmcsmax
          total_componentsmax_old=total_componentsmax
          total_mmcsmax=total_mmcsmax*mmc_expansion_factor
          total_componentsmax=total_componentsmax*comp_expansion_factor
          ALLOCATE(mmc_jnew(1:total_mmcsmax))
          ALLOCATE(mmc_knew(1:total_mmcsmax))
          ALLOCATE(mmc_indexnew(1:total_mmcsmax))
          ALLOCATE(mmc_componentnew(1:total_mmcsmax))
          ALLOCATE(mmc_volumenew(1:total_componentsmax))
          ALLOCATE(mmc_materialnew(1:total_componentsmax))
          ALLOCATE(mmc_densitynew(1:total_componentsmax))
          ALLOCATE(mmc_energynew(1:total_componentsmax))
          ALLOCATE(mmc_pressure(1:total_componentsmax))
          ALLOCATE(mmc_soundspeed(1:total_componentsmax))
          ! Copy data
          mmc_jnew(1:total_mmcs1)=mmc_j1(1:total_mmcs1)
          mmc_knew(1:total_mmcs1)=mmc_k1(1:total_mmcs1)
          mmc_indexnew(1:total_mmcs1)=mmc_index1(1:total_mmcs1)
          mmc_componentnew(1:total_mmcs1)=mmc_component1(1:total_mmcs1)
          mmc_volumenew(1:total_components1)=mmc_volume1(1:total_components1)
          mmc_materialnew(1:total_components1)=mmc_material1(1:total_components1)
          mmc_densitynew(1:total_components1)=mmc_density1(1:total_components1)
          mmc_energynew(1:total_components1)=mmc_energy1(1:total_components1)
          ! Deallocate old arrays and allocate with new storage limits
          DEALLOCATE(mmc_j1)
          DEALLOCATE(mmc_k1)
          DEALLOCATE(mmc_index1)
          DEALLOCATE(mmc_component1)
          DEALLOCATE(mmc_volume1)
          DEALLOCATE(mmc_material1)
          DEALLOCATE(mmc_density1)
          DEALLOCATE(mmc_energy1)
          ALLOCATE(mmc_j1(total_mmcsmax))
          ALLOCATE(mmc_k1(total_mmcsmax))
          ALLOCATE(mmc_index1(total_mmcsmax))
          ALLOCATE(mmc_component1(total_mmcsmax))
          ALLOCATE(mmc_volume1(1:total_componentsmax))
          ALLOCATE(mmc_material1(1:total_componentsmax))
          ALLOCATE(mmc_density1(1:total_componentsmax))
          ALLOCATE(mmc_energy1(1:total_componentsmax))
          DEALLOCATE(mmc_j0)
          DEALLOCATE(mmc_k0)
          DEALLOCATE(mmc_index0)
          DEALLOCATE(mmc_component0)
          DEALLOCATE(mmc_volume0)
          DEALLOCATE(mmc_material0)
          DEALLOCATE(mmc_density0)
          DEALLOCATE(mmc_energy0)
          ALLOCATE(mmc_j0(total_mmcsmax))
          ALLOCATE(mmc_k0(total_mmcsmax))
          ALLOCATE(mmc_index0(total_mmcsmax))
          ALLOCATE(mmc_component0(total_mmcsmax))
          ALLOCATE(mmc_volume0(1:total_componentsmax))
          ALLOCATE(mmc_material0(1:total_componentsmax))
          ALLOCATE(mmc_density0(1:total_componentsmax))
          ALLOCATE(mmc_energy0(1:total_componentsmax))
          mmc_j1(1:total_mmcs1)=mmc_jnew(1:total_mmcs1)
          mmc_k1(1:total_mmcs1)=mmc_knew(1:total_mmcs1)
          mmc_index1(1:total_mmcs1)=mmc_indexnew(1:total_mmcs1)
          mmc_component1(1:total_mmcs1)=mmc_componentnew(1:total_mmcs1)
          mmc_volume1(1:total_components1)=mmc_volumenew(1:total_components1)
          mmc_material1(1:total_components1)=mmc_materialnew(1:total_components1)
          mmc_density1(1:total_components1)=mmc_densitynew(1:total_components1)
          mmc_energy1(1:total_components1)=mmc_energynew(1:total_components1)
          ! Shift MMC thread data pointers in proportion to their current storage
          DO thread=1,number_of_threads-1
            mmc_length(thread)=mmc_start_index(thread+1)-mmc_start_index(thread)
            comp_length(thread)=comp_start_index(thread+1)-comp_start_index(thread)
          ENDDO
          ! Last thread
          mmc_length(number_of_threads)=MAX(0,total_mmcsmax_old-mmc_start_index(number_of_threads))
          comp_length(number_of_threads)=MAX(0,total_componentsmax_old-comp_start_index(number_of_threads))
! the 0.95 below is just to prevent a round off issue I can't quite squash
          DO thread=1,number_of_threads
            mmc_start_index(thread)=1+0.95*mmc_expansion_factor*SUM(mmc_length(1:thread))
            comp_start_index(thread)=1+0.95*comp_expansion_factor*SUM(comp_length(1:thread))
          ENDDO
          mmc_index=mmc_start_index
          comp_index=comp_start_index
        ENDIF
      ENDDO

      mmc_index=mmc_start_index
      comp_index=comp_start_index

      total_mmcs_halo=total_mmcs1
      total_components_halo=total_components1

      DO k=y_min-2,y_max+2
        DO j=x_min-2,x_max+2
          IF(material1(j,k).GT.0) THEN
            unique_material(material1(j,k))=.TRUE.
          ENDIF
        ENDDO
      ENDDO
      DO component=1,total_components_halo
        unique_material(mmc_material1(component))=.TRUE.
      ENDDO

      cell_advection_timer=cell_advection_timer+(timer()-kernel_timer)

      kernel_timer=timer()

      which_vel=1
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )
      !WRITE(*,*)
      !WRITE(*,*)"Y mom advection"
      which_vel=2
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )

      mom_advection_timer=mom_advection_timer+(timer()-kernel_timer)

      kernel_timer=timer()

      direction=2
      sweep_number=2

!   Note that the 2nd sweep uses *new as the initial data and *1 and as the final data, to prevent excessive copies
      reallocation_needed=.TRUE.
      DO WHILE(reallocation_needed)
        CALL advec_mmc_y_cell_kernel(x_min,x_max,y_min,y_max,    &
                                     sweep_number,               &
                                     max_mats,                   &
                                     max_mat_number,             &
                                     mat_list,                   &
                                     reverse_mat_list,           &
                                     total_mmcs1,                &
                                     total_components1,          &
                                     total_mmcsmax,              &
                                     total_componentsmax,        &
                                     total_mmcs_halo,            &
                                     total_components_halo,      &
                                     total_mmcsnew,              &
                                     total_componentsnew,        &
                                     max_comps,                  &
                                     number_of_threads,          &
                                     mmc_counter_next,           &
                                     component_counter_next,     &
                                     celldx,                     &
                                     celldy,                     &
                                     vertexdx,                   &
                                     vertexdy,                   &
                                     volume,                     &
                                     density1,                   &
                                     densitynew,                 &
                                     energy1,                    &
                                     energynew,                  &
                                     mass_flux_x,                &
                                     vol_flux_x,                 &
                                     mass_flux_y,                &
                                     vol_flux_y,                 &
                                     pre_vol,                    &
                                     post_vol,                   &
                                     ener_flux,                  &
                                     dv,                         &
                                     dm,                         &
                                     de,                         &
                                     material1,                  &
                                     materialnew,                &
                                     mmc_material1,              &
                                     mmc_materialnew,            &
                                     mmc_index1,                 &
                                     mmc_indexnew,               &
                                     mmc_j1,                     &
                                     mmc_k1,                     &
                                     mmc_jnew,                   &
                                     mmc_knew,                   &
                                     mmc_component1,             &
                                     mmc_componentnew,           &
                                     mmc_density1,               &
                                     mmc_densitynew,             &
                                     mmc_energy1,                &
                                     mmc_energynew,              &
                                     mmc_volume1,                &
                                     mmc_volumenew,              &
                                     mmc_start_index,            &
                                     mmc_index,                  &
                                     comp_start_index,           &
                                     comp_index,                 &
                                     reallocation_needed         )
        write(0,*)"total_mmcs1 ",total_mmcsnew
        write(0,*)"total_components1 ",total_componentsnew
        write(0,*)"total_mmcsmax ",total_mmcsmax
        write(0,*)"total_componentsmax ",total_componentsmax
        IF(reallocation_needed) THEN
          ! Allocate new arrays
          DEALLOCATE(mmc_jnew)
          DEALLOCATE(mmc_knew)
          DEALLOCATE(mmc_indexnew)
          DEALLOCATE(mmc_componentnew)
          DEALLOCATE(mmc_volumenew)
          DEALLOCATE(mmc_materialnew)
          DEALLOCATE(mmc_densitynew)
          DEALLOCATE(mmc_energynew)
          DEALLOCATE(mmc_pressure)
          DEALLOCATE(mmc_soundspeed)
          total_mmcsmax_old=total_mmcsmax
          total_componentsmax_old=total_componentsmax
          total_mmcsmax=total_mmcsmax*mmc_expansion_factor
          total_componentsmax=total_componentsmax*comp_expansion_factor
          ALLOCATE(mmc_jnew(1:total_mmcsmax))
          ALLOCATE(mmc_knew(1:total_mmcsmax))
          ALLOCATE(mmc_indexnew(1:total_mmcsmax))
          ALLOCATE(mmc_componentnew(1:total_mmcsmax))
          ALLOCATE(mmc_volumenew(1:total_componentsmax))
          ALLOCATE(mmc_materialnew(1:total_componentsmax))
          ALLOCATE(mmc_densitynew(1:total_componentsmax))
          ALLOCATE(mmc_energynew(1:total_componentsmax))
          ALLOCATE(mmc_pressure(1:total_componentsmax))
          ALLOCATE(mmc_soundspeed(1:total_componentsmax))
          ! Copy data
          mmc_jnew(1:total_mmcs1)=mmc_j1(1:total_mmcs1)
          mmc_knew(1:total_mmcs1)=mmc_k1(1:total_mmcs1)
          mmc_indexnew(1:total_mmcs1)=mmc_index1(1:total_mmcs1)
          mmc_componentnew(1:total_mmcs1)=mmc_component1(1:total_mmcs1)
          mmc_volumenew(1:total_components1)=mmc_volume1(1:total_components1)
          mmc_materialnew(1:total_components1)=mmc_material1(1:total_components1)
          mmc_densitynew(1:total_components1)=mmc_density1(1:total_components1)
          mmc_energynew(1:total_components1)=mmc_energy1(1:total_components1)
          ! Deallocate old arrays and allocate with new storage limits
          DEALLOCATE(mmc_j1)
          DEALLOCATE(mmc_k1)
          DEALLOCATE(mmc_index1)
          DEALLOCATE(mmc_component1)
          DEALLOCATE(mmc_volume1)
          DEALLOCATE(mmc_material1)
          DEALLOCATE(mmc_density1)
          DEALLOCATE(mmc_energy1)
          ALLOCATE(mmc_j1(total_mmcsmax))
          ALLOCATE(mmc_k1(total_mmcsmax))
          ALLOCATE(mmc_index1(total_mmcsmax))
          ALLOCATE(mmc_component1(total_mmcsmax))
          ALLOCATE(mmc_volume1(1:total_componentsmax))
          ALLOCATE(mmc_material1(1:total_componentsmax))
          ALLOCATE(mmc_density1(1:total_componentsmax))
          ALLOCATE(mmc_energy1(1:total_componentsmax))
          DEALLOCATE(mmc_j0)
          DEALLOCATE(mmc_k0)
          DEALLOCATE(mmc_index0)
          DEALLOCATE(mmc_component0)
          DEALLOCATE(mmc_volume0)
          DEALLOCATE(mmc_material0)
          DEALLOCATE(mmc_density0)
          DEALLOCATE(mmc_energy0)
          ALLOCATE(mmc_j0(total_mmcsmax))
          ALLOCATE(mmc_k0(total_mmcsmax))
          ALLOCATE(mmc_index0(total_mmcsmax))
          ALLOCATE(mmc_component0(total_mmcsmax))
          ALLOCATE(mmc_volume0(1:total_componentsmax))
          ALLOCATE(mmc_material0(1:total_componentsmax))
          ALLOCATE(mmc_density0(1:total_componentsmax))
          ALLOCATE(mmc_energy0(1:total_componentsmax))
          mmc_j1(1:total_mmcs1)=mmc_jnew(1:total_mmcs1)
          mmc_k1(1:total_mmcs1)=mmc_knew(1:total_mmcs1)
          mmc_index1(1:total_mmcs1)=mmc_indexnew(1:total_mmcs1)
          mmc_component1(1:total_mmcs1)=mmc_componentnew(1:total_mmcs1)
          mmc_volume1(1:total_components1)=mmc_volumenew(1:total_components1)
          mmc_material1(1:total_components1)=mmc_materialnew(1:total_components1)
          mmc_density1(1:total_components1)=mmc_densitynew(1:total_components1)
          mmc_energy1(1:total_components1)=mmc_energynew(1:total_components1)
          ! Shift MMC thread data pointers in proportion to their current storage
          DO thread=1,number_of_threads-1
            mmc_length(thread)=mmc_start_index(thread+1)-mmc_start_index(thread)
            comp_length(thread)=comp_start_index(thread+1)-comp_start_index(thread)
          ENDDO
          ! Last thread
          mmc_length(number_of_threads)=MAX(0,total_mmcsmax_old-mmc_start_index(number_of_threads))
          comp_length(number_of_threads)=MAX(0,total_componentsmax_old-comp_start_index(number_of_threads))
          DO thread=1,number_of_threads
            mmc_start_index(thread)=1+0.95*mmc_expansion_factor*SUM(mmc_length(1:thread))
            comp_start_index(thread)=1+0.95*comp_expansion_factor*SUM(comp_length(1:thread))
          ENDDO
          mmc_index=mmc_start_index
          comp_index=comp_start_index
        ENDIF
      ENDDO

      mmc_index=mmc_start_index
      comp_index=comp_start_index

      total_mmcs_halo=total_mmcs1
      total_components_halo=total_components1

      DO k=y_min-2,y_max+2
        DO j=x_min-2,x_max+2
          IF(material1(j,k).GT.0) THEN
            unique_material(material1(j,k))=.TRUE.
          ENDIF
        ENDDO
      ENDDO
      DO component=1,total_components_halo
        unique_material(mmc_material1(component))=.TRUE.
      ENDDO

      cell_advection_timer=cell_advection_timer+(timer()-kernel_timer)

      kernel_timer=timer()

      which_vel=1
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )
      which_vel=2
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )

      mom_advection_timer=mom_advection_timer+(timer()-kernel_timer)

    ELSE

      kernel_timer=timer()

      direction=2
      sweep_number=1
      reallocation_needed=.TRUE.
      DO WHILE(reallocation_needed)
        CALL advec_mmc_y_cell_kernel(x_min,x_max,y_min,y_max,    &
                                     sweep_number,               &
                                     max_mats,                   &
                                     max_mat_number,             &
                                     mat_list,                   &
                                     reverse_mat_list,           &
                                     total_mmcs1,                &
                                     total_components1,          &
                                     total_mmcsmax,              &
                                     total_componentsmax,        &
                                     total_mmcs_halo,            &
                                     total_components_halo,      &
                                     total_mmcsnew,              &
                                     total_componentsnew,        &
                                     max_comps,                  &
                                     number_of_threads,          &
                                     mmc_counter_next,           &
                                     component_counter_next,     &
                                     celldx,                     &
                                     celldy,                     &
                                     vertexdx,                   &
                                     vertexdy,                   &
                                     volume,                     &
                                     density1,                   &
                                     densitynew,                 &
                                     energy1,                    &
                                     energynew,                  &
                                     mass_flux_x,                &
                                     vol_flux_x,                 &
                                     mass_flux_y,                &
                                     vol_flux_y,                 &
                                     pre_vol,                    &
                                     post_vol,                   &
                                     ener_flux,                  &
                                     dv,                         &
                                     dm,                         &
                                     de,                         &
                                     material1,                  &
                                     materialnew,                &
                                     mmc_material1,              &
                                     mmc_materialnew,            &
                                     mmc_index1,                 &
                                     mmc_indexnew,               &
                                     mmc_j1,                     &
                                     mmc_k1,                     &
                                     mmc_jnew,                   &
                                     mmc_knew,                   &
                                     mmc_component1,             &
                                     mmc_componentnew,           &
                                     mmc_density1,               &
                                     mmc_densitynew,             &
                                     mmc_energy1,                &
                                     mmc_energynew,              &
                                     mmc_volume1,                &
                                     mmc_volumenew,              &
                                     mmc_start_index,            &
                                     mmc_index,                  &
                                     comp_start_index,           &
                                     comp_index,                 &
                                     reallocation_needed         )
        write(0,*)"total_mmcs1 ",total_mmcsnew
        write(0,*)"total_components1 ",total_componentsnew
        write(0,*)"total_mmcsmax ",total_mmcsmax
        write(0,*)"total_componentsmax ",total_componentsmax
        IF(reallocation_needed) THEN
          ! Allocate new arrays
          DEALLOCATE(mmc_jnew)
          DEALLOCATE(mmc_knew)
          DEALLOCATE(mmc_indexnew)
          DEALLOCATE(mmc_componentnew)
          DEALLOCATE(mmc_volumenew)
          DEALLOCATE(mmc_materialnew)
          DEALLOCATE(mmc_densitynew)
          DEALLOCATE(mmc_energynew)
          DEALLOCATE(mmc_pressure)
          DEALLOCATE(mmc_soundspeed)
          total_mmcsmax_old=total_mmcsnew
          total_componentsmax_old=total_componentsnew
          total_mmcsmax=total_mmcsnew*mmc_expansion_factor
          total_componentsmax=total_componentsnew*comp_expansion_factor
          write(0,*)"reallocate total_mmcsmax ",total_mmcsmax
          write(0,*)"reallocate total_componentsmax ",total_componentsmax
          ALLOCATE(mmc_jnew(1:total_mmcsmax))
          ALLOCATE(mmc_knew(1:total_mmcsmax))
          ALLOCATE(mmc_indexnew(1:total_mmcsmax))
          ALLOCATE(mmc_componentnew(1:total_mmcsmax))
          ALLOCATE(mmc_volumenew(1:total_componentsmax))
          ALLOCATE(mmc_materialnew(1:total_componentsmax))
          ALLOCATE(mmc_densitynew(1:total_componentsmax))
          ALLOCATE(mmc_energynew(1:total_componentsmax))
          ALLOCATE(mmc_pressure(1:total_componentsmax))
          ALLOCATE(mmc_soundspeed(1:total_componentsmax))
          ! Copy data
          mmc_jnew(1:total_mmcs1)=mmc_j1(1:total_mmcs1)
          mmc_knew(1:total_mmcs1)=mmc_k1(1:total_mmcs1)
          mmc_indexnew(1:total_mmcs1)=mmc_index1(1:total_mmcs1)
          mmc_componentnew(1:total_mmcs1)=mmc_component1(1:total_mmcs1)
          mmc_volumenew(1:total_components1)=mmc_volume1(1:total_components1)
          mmc_materialnew(1:total_components1)=mmc_material1(1:total_components1)
          mmc_densitynew(1:total_components1)=mmc_density1(1:total_components1)
          mmc_energynew(1:total_components1)=mmc_energy1(1:total_components1)
          ! Deallocate old arrays and allocate with new storage limits
          DEALLOCATE(mmc_j1)
          DEALLOCATE(mmc_k1)
          DEALLOCATE(mmc_index1)
          DEALLOCATE(mmc_component1)
          DEALLOCATE(mmc_volume1)
          DEALLOCATE(mmc_material1)
          DEALLOCATE(mmc_density1)
          DEALLOCATE(mmc_energy1)
          ALLOCATE(mmc_j1(total_mmcsmax))
          ALLOCATE(mmc_k1(total_mmcsmax))
          ALLOCATE(mmc_index1(total_mmcsmax))
          ALLOCATE(mmc_component1(total_mmcsmax))
          ALLOCATE(mmc_volume1(1:total_componentsmax))
          ALLOCATE(mmc_material1(1:total_componentsmax))
          ALLOCATE(mmc_density1(1:total_componentsmax))
          ALLOCATE(mmc_energy1(1:total_componentsmax))
          DEALLOCATE(mmc_j0)
          DEALLOCATE(mmc_k0)
          DEALLOCATE(mmc_index0)
          DEALLOCATE(mmc_component0)
          DEALLOCATE(mmc_volume0)
          DEALLOCATE(mmc_material0)
          DEALLOCATE(mmc_density0)
          DEALLOCATE(mmc_energy0)
          ALLOCATE(mmc_j0(total_mmcsmax))
          ALLOCATE(mmc_k0(total_mmcsmax))
          ALLOCATE(mmc_index0(total_mmcsmax))
          ALLOCATE(mmc_component0(total_mmcsmax))
          ALLOCATE(mmc_volume0(1:total_componentsmax))
          ALLOCATE(mmc_material0(1:total_componentsmax))
          ALLOCATE(mmc_density0(1:total_componentsmax))
          ALLOCATE(mmc_energy0(1:total_componentsmax))
          mmc_j1(1:total_mmcs1)=mmc_jnew(1:total_mmcs1)
          mmc_k1(1:total_mmcs1)=mmc_knew(1:total_mmcs1)
          mmc_index1(1:total_mmcs1)=mmc_indexnew(1:total_mmcs1)
          mmc_component1(1:total_mmcs1)=mmc_componentnew(1:total_mmcs1)
          mmc_volume1(1:total_components1)=mmc_volumenew(1:total_components1)
          mmc_material1(1:total_components1)=mmc_materialnew(1:total_components1)
          mmc_density1(1:total_components1)=mmc_densitynew(1:total_components1)
          mmc_energy1(1:total_components1)=mmc_energynew(1:total_components1)
          ! Shift MMC thread data pointers in proportion to their current storage
          DO thread=1,number_of_threads-1
            mmc_length(thread)=mmc_start_index(thread+1)-mmc_start_index(thread)
            comp_length(thread)=comp_start_index(thread+1)-comp_start_index(thread)
          ENDDO
          ! Last thread
          mmc_length(number_of_threads)=MAX(0,total_mmcsmax_old-mmc_start_index(number_of_threads))
          comp_length(number_of_threads)=MAX(0,total_componentsmax_old-comp_start_index(number_of_threads))
          DO thread=1,number_of_threads
            mmc_start_index(thread)=1+0.95*mmc_expansion_factor*SUM(mmc_length(1:thread))
            comp_start_index(thread)=1+0.95*comp_expansion_factor*SUM(comp_length(1:thread))
          ENDDO
          mmc_index=mmc_start_index
          comp_index=comp_start_index
        ENDIF
      ENDDO

      mmc_index=mmc_start_index
      comp_index=comp_start_index

      total_mmcs_halo=total_mmcs1
      total_components_halo=total_components1

      DO k=y_min-2,y_max+2
        DO j=x_min-2,x_max+2
          IF(material1(j,k).GT.0) THEN
            unique_material(material1(j,k))=.TRUE.
          ENDIF
        ENDDO
      ENDDO
      DO component=1,total_components_halo
        unique_material(mmc_material1(component))=.TRUE.
      ENDDO

      cell_advection_timer=cell_advection_timer+(timer()-kernel_timer)

      kernel_timer=timer()

      which_vel=1
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )
      which_vel=2
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )

      mom_advection_timer=mom_advection_timer+(timer()-kernel_timer)

      kernel_timer=timer()

      direction=1
      sweep_number=2
      reallocation_needed=.TRUE.
      DO WHILE(reallocation_needed)
!   Note that the 2nd sweep uses *new as the initial data and *1 and as the final data, to prevent excessive copies
        CALL advec_mmc_x_cell_kernel(x_min,x_max,y_min,y_max,    &
                                     sweep_number,               &
                                     max_mats,                   &
                                     max_mat_number,             &
                                     mat_list,                   &
                                     reverse_mat_list,           &
                                     total_mmcs1,                &
                                     total_components1,          &
                                     total_mmcsmax,              &
                                     total_componentsmax,        &
                                     total_mmcs_halo,            &
                                     total_components_halo,      &
                                     total_mmcsnew,              &
                                     total_componentsnew,        &
                                     max_comps,                  &
                                     number_of_threads,          &
                                     mmc_counter_next,           &
                                     component_counter_next,     &
                                     celldx,                     &
                                     celldy,                     &
                                     vertexdx,                   &
                                     vertexdy,                   &
                                     volume,                     &
                                     density1,                   &
                                     densitynew,                 &
                                     energy1,                    &
                                     energynew,                  &
                                     mass_flux_x,                &
                                     vol_flux_x,                 &
                                     mass_flux_y,                &
                                     vol_flux_y,                 &
                                     pre_vol,                    &
                                     post_vol,                   &
                                     ener_flux,                  &
                                     dv,                         &
                                     dm,                         &
                                     de,                         &
                                     material1,                  &
                                     materialnew,                &
                                     mmc_material1,              &
                                     mmc_materialnew,            &
                                     mmc_index1,                 &
                                     mmc_indexnew,               &
                                     mmc_j1,                     &
                                     mmc_k1,                     &
                                     mmc_jnew,                   &
                                     mmc_knew,                   &
                                     mmc_component1,             &
                                     mmc_componentnew,           &
                                     mmc_density1,               &
                                     mmc_densitynew,             &
                                     mmc_energy1,                &
                                     mmc_energynew,              &
                                     mmc_volume1,                &
                                     mmc_volumenew,              &
                                     mmc_start_index,            &
                                     mmc_index,                  &
                                     comp_start_index,           &
                                     comp_index,                 &
                                     reallocation_needed         )
        write(0,*)"total_mmcs1 ",total_mmcsnew
        write(0,*)"total_components1 ",total_componentsnew
        write(0,*)"total_mmcsmax ",total_mmcsmax
        write(0,*)"total_componentsmax ",total_componentsmax
        IF(reallocation_needed) THEN
          ! Allocate new arrays
          DEALLOCATE(mmc_jnew)
          DEALLOCATE(mmc_knew)
          DEALLOCATE(mmc_indexnew)
          DEALLOCATE(mmc_componentnew)
          DEALLOCATE(mmc_volumenew)
          DEALLOCATE(mmc_materialnew)
          DEALLOCATE(mmc_densitynew)
          DEALLOCATE(mmc_energynew)
          DEALLOCATE(mmc_pressure)
          DEALLOCATE(mmc_soundspeed)
          total_mmcsmax_old=total_mmcsmax
          total_componentsmax_old=total_componentsmax
          total_mmcsmax=total_mmcsmax*mmc_expansion_factor
          total_componentsmax=total_componentsmax*comp_expansion_factor
          ALLOCATE(mmc_jnew(1:total_mmcsmax))
          ALLOCATE(mmc_knew(1:total_mmcsmax))
          ALLOCATE(mmc_indexnew(1:total_mmcsmax))
          ALLOCATE(mmc_componentnew(1:total_mmcsmax))
          ALLOCATE(mmc_volumenew(1:total_componentsmax))
          ALLOCATE(mmc_materialnew(1:total_componentsmax))
          ALLOCATE(mmc_densitynew(1:total_componentsmax))
          ALLOCATE(mmc_energynew(1:total_componentsmax))
          ALLOCATE(mmc_pressure(1:total_componentsmax))
          ALLOCATE(mmc_soundspeed(1:total_componentsmax))
          ! Copy data
          mmc_jnew(1:total_mmcs1)=mmc_j1(1:total_mmcs1)
          mmc_knew(1:total_mmcs1)=mmc_k1(1:total_mmcs1)
          mmc_indexnew(1:total_mmcs1)=mmc_index1(1:total_mmcs1)
          mmc_componentnew(1:total_mmcs1)=mmc_component1(1:total_mmcs1)
          mmc_volumenew(1:total_components1)=mmc_volume1(1:total_components1)
          mmc_materialnew(1:total_components1)=mmc_material1(1:total_components1)
          mmc_densitynew(1:total_components1)=mmc_density1(1:total_components1)
          mmc_energynew(1:total_components1)=mmc_energy1(1:total_components1)
          ! Deallocate old arrays and allocate with new storage limits
          DEALLOCATE(mmc_j1)
          DEALLOCATE(mmc_k1)
          DEALLOCATE(mmc_index1)
          DEALLOCATE(mmc_component1)
          DEALLOCATE(mmc_volume1)
          DEALLOCATE(mmc_material1)
          DEALLOCATE(mmc_density1)
          DEALLOCATE(mmc_energy1)
          ALLOCATE(mmc_j1(total_mmcsmax))
          ALLOCATE(mmc_k1(total_mmcsmax))
          ALLOCATE(mmc_index1(total_mmcsmax))
          ALLOCATE(mmc_component1(total_mmcsmax))
          ALLOCATE(mmc_volume1(1:total_componentsmax))
          ALLOCATE(mmc_material1(1:total_componentsmax))
          ALLOCATE(mmc_density1(1:total_componentsmax))
          ALLOCATE(mmc_energy1(1:total_componentsmax))
          DEALLOCATE(mmc_j0)
          DEALLOCATE(mmc_k0)
          DEALLOCATE(mmc_index0)
          DEALLOCATE(mmc_component0)
          DEALLOCATE(mmc_volume0)
          DEALLOCATE(mmc_material0)
          DEALLOCATE(mmc_density0)
          DEALLOCATE(mmc_energy0)
          ALLOCATE(mmc_j0(total_mmcsmax))
          ALLOCATE(mmc_k0(total_mmcsmax))
          ALLOCATE(mmc_index0(total_mmcsmax))
          ALLOCATE(mmc_component0(total_mmcsmax))
          ALLOCATE(mmc_volume0(1:total_componentsmax))
          ALLOCATE(mmc_material0(1:total_componentsmax))
          ALLOCATE(mmc_density0(1:total_componentsmax))
          ALLOCATE(mmc_energy0(1:total_componentsmax))
          mmc_j1(1:total_mmcs1)=mmc_jnew(1:total_mmcs1)
          mmc_k1(1:total_mmcs1)=mmc_knew(1:total_mmcs1)
          mmc_index1(1:total_mmcs1)=mmc_indexnew(1:total_mmcs1)
          mmc_component1(1:total_mmcs1)=mmc_componentnew(1:total_mmcs1)
          mmc_volume1(1:total_components1)=mmc_volumenew(1:total_components1)
          mmc_material1(1:total_components1)=mmc_materialnew(1:total_components1)
          mmc_density1(1:total_components1)=mmc_densitynew(1:total_components1)
          mmc_energy1(1:total_components1)=mmc_energynew(1:total_components1)
          ! Shift MMC thread data pointers in proportion to their current storage
          DO thread=1,number_of_threads-1
            mmc_length(thread)=mmc_start_index(thread+1)-mmc_start_index(thread)
            comp_length(thread)=comp_start_index(thread+1)-comp_start_index(thread)
          ENDDO
          ! Last thread
          mmc_length(number_of_threads)=MAX(0,total_mmcsmax_old-mmc_start_index(number_of_threads))
          comp_length(number_of_threads)=MAX(0,total_componentsmax_old-comp_start_index(number_of_threads))
          DO thread=1,number_of_threads
            mmc_start_index(thread)=1+0.95*mmc_expansion_factor*SUM(mmc_length(1:thread))
            comp_start_index(thread)=1+0.95*comp_expansion_factor*SUM(comp_length(1:thread))
          ENDDO
          mmc_index=mmc_start_index
          comp_index=comp_start_index
        ENDIF
      ENDDO

      mmc_index=mmc_start_index
      comp_index=comp_start_index

      total_components_halo=total_components1
      total_mmcs_halo=total_mmcs1

      DO k=y_min-2,y_max+2
        DO j=x_min-2,x_max+2
          IF(material1(j,k).GT.0) THEN
            unique_material(material1(j,k))=.TRUE.
          ENDIF
        ENDDO
      ENDDO
      DO component=1,total_components_halo
      unique_material(mmc_material1(component))=.TRUE.
      ENDDO

      cell_advection_timer=cell_advection_timer+(timer()-kernel_timer)

      kernel_timer=timer()

      which_vel=1
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )
      which_vel=2
      CALL advec_mom_kernel(x_min,x_max,y_min,y_max,   &
                            xvel1,                     &
                            yvel1,                     &
                            mass_flux_x,               &
                            vol_flux_x,                &
                            mass_flux_y,               &
                            vol_flux_y,                &
                            volume,                    &
                            density1,                  &
                            node_flux,                 &
                            node_mass_post,            &
                            node_mass_pre,             &
                            advec_vel,                 &
                            mom_flux,                  &
                            pre_vol,                   &
                            post_vol,                  &
                            celldx,                    &
                            celldy,                    &
                            which_vel,                 &
                            sweep_number,              &
                            direction                  )

      mom_advection_timer=mom_advection_timer+(timer()-kernel_timer)

    ENDIF

    kernel_timer=timer()

    DO k=y_min-2,y_max+2
      DO j=x_min-2,x_max+2
        IF(material1(j,k).GT.0) THEN
          unique_material(material1(j,k))=.TRUE.
        ENDIF
      ENDDO
    ENDDO
    DO component=1,total_components_halo
      unique_material(mmc_material1(component))=.TRUE.
    ENDDO

    CALL reset_field_kernel(x_min,x_max,y_min,y_max,      &
                            total_mmcs0,                  &
                            total_components0,            &
                            total_mmcs1,                  &
                            total_components1,            &
                            total_mmcsmax,                &
                            total_componentsmax,          &
                            density0,                     &
                            density1,                     &
                            energy0,                      &
                            energy1,                      &
                            xvel0,                        &
                            xvel1,                        &
                            yvel0,                        &
                            yvel1,                        &
                            material0,                    &
                            material1,                    &
                            mmc_material0,                &
                            mmc_material1,                &
                            mmc_index0,                   &
                            mmc_index1,                   &
                            mmc_j0,                       &
                            mmc_k0,                       &
                            mmc_j1,                       &
                            mmc_k1,                       &
                            mmc_component0,               &
                            mmc_component1,               &
                            mmc_density0,                 &
                            mmc_density1,                 &
                            mmc_energy0,                  &
                            mmc_energy1,                  &
                            mmc_volume0,                  &
                            mmc_volume1                   )

    reset_timer=reset_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL ideal_gas_kernel(x_min,x_max,y_min,y_max, &
                          total_mmcs0,             &
                          total_components0,       &
                          total_mmcsmax,           &
                          total_componentsmax,     &
                          volume,                  &
                          density0,                &
                          energy0,                 &
                          pressure,                &
                          soundspeed,              &
                          material0,               &
                          mmc_component0,          &
                          mmc_index0,              &
                          mmc_j0,                  &
                          mmc_k0,                  &
                          mmc_material0,           &
                          mmc_density0,            &
                          mmc_energy0,             &
                          mmc_pressure,            &
                          mmc_soundspeed,          &
                          mmc_volume0,             &
                          pressure_average,        &
                          sound_speed_average      )

    ideal_gas_timer=ideal_gas_timer+(timer()-kernel_timer)

    kernel_timer=timer()

    CALL field_summary_kernel(x_min,x_max,y_min,y_max, &
                              total_mmcs0,             &
                              total_components0,       &
                              total_mmcsmax,           &
                              total_componentsmax,     &
                              max_mats,                &
                              volume,                  &
                              density0,                &
                              energy0,                 &
                              pressure,                &
                              xvel0,                   &
                              yvel0,                   &
                              material0,               &
                              mmc_component0,          &
                              mmc_index0,              &
                              mmc_material0,           &
                              mmc_volume0,             &
                              mmc_density0,            &
                              mmc_energy0,             &
                              mmc_pressure,            &
                              vol,mass,ie,ke,press     )
    WRITE(*,*)
    WRITE(*,*) 'Time ',time
    WRITE(*,'(a13,8a16)')'           ','Material','Volume','Mass','Density','Pressure' &
                                      ,'Internal Energy','Kinetic Energy','Total Energy'
    DO mat=1,max_mats
      WRITE(*,'(a6,i7,i16,7e16.4)')' step:',iteration,mat,vol(mat),mass(mat),mass(mat)/vol(mat), &
                                            press(mat)/vol(mat),ie(mat),ke(mat),ie(mat)+ke(mat)
    ENDDO
    WRITE(*,'(a29,7e16.4)')'Totals',SUM(vol),SUM(mass),SUM(mass)/SUM(vol), &
                                        SUM(press)/SUM(vol),SUM(ie),SUM(ke),SUM(ie)+SUM(ke)

    summary_timer=summary_timer+(timer()-kernel_timer)

    IF(visualise) THEN

      kernel_timer=timer()

      CALL viscosity_kernel(x_min,x_max,y_min,y_max,    &
                            celldx,celldy,              &
                            density0,                   &
                            pressure,                   &
                            viscosity,                  &
                            xvel0,                      &
                            yvel0                       )

      viscosity_timer=viscosity_timer+(timer()-kernel_timer)

      kernel_timer=timer()

      CALL visit_kernel(iteration,                        &
                        x_min,                            &
                        x_max,                            &
                        y_min,                            &
                        y_max,                            &
                        vertexx,                          &
                        vertexy,                          &
                        material0,                        &
                        density0,                         &
                        energy0,                          &
                        pressure,                         &
                        viscosity,                        &
                        xvel0,                            &
                        yvel0                             )

      visit_timer=visit_timer+(timer()-kernel_timer)
    ENDIF

    WRITE(*,*)"Wall Clock ",(timer()-wall_clock_time)

  ENDDO

  wall_clock_time=(timer()-wall_clock_time)
  total_time=timestep_timer+ideal_gas_timer+viscosity_timer+PdV_timer+revert_timer+summary_timer &
                     +acceleration_timer+flux_timer+cell_advection_timer+mom_advection_timer+reset_timer+visit_timer

  WRITE(*,*)
  WRITE(*,*)"                Kernel              Time            Percentage"
  WRITE(*,*)"--------------------------------------------------------------"
  WRITE(*,'(a27,2f16.4)') " Timestep           ",timestep_timer,(timestep_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Ideal Gas          ",ideal_gas_timer,(ideal_gas_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Viscosity          ",viscosity_timer,(viscosity_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " PdV                ",PdV_timer,(PdV_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Revert             ",revert_timer,(revert_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Acceleration       ",acceleration_timer,(acceleration_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Flux               ",flux_timer,(flux_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Cell Advection     ",cell_advection_timer,(cell_advection_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Momentum Advection ",mom_advection_timer,(mom_advection_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Reset              ",reset_timer,(reset_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Halo Exchange      ",boundary_timer,(boundary_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Summary            ",summary_timer,(summary_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Visit              ",visit_timer,(visit_timer/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " Total              ",total_time,(total_time/wall_clock_time)*100.0
  WRITE(*,'(a27,2f16.4)') " The Rest           ",wall_clock_time-total_time,((wall_clock_time-total_time)/wall_clock_time)*100.0

  DEALLOCATE(vertexdx)
  DEALLOCATE(vertexdy)
  DEALLOCATE(xarea)
  DEALLOCATE(yarea)
  DEALLOCATE(volume)
  DEALLOCATE(soundspeed)
  DEALLOCATE(density1)
  DEALLOCATE(xvel0)
  DEALLOCATE(xvel1)
  DEALLOCATE(yvel0)
  DEALLOCATE(yvel1)
  DEALLOCATE(vol_flux_x)
  DEALLOCATE(vol_flux_y)
  DEALLOCATE(mass_flux_x)
  DEALLOCATE(mass_flux_y)
  DEALLOCATE(pre_vol)
  DEALLOCATE(post_vol)
  DEALLOCATE(ener_flux)

END PROGRAM hydro_driver

SUBROUTINE update_ext_halo(x_min,                     &
                           x_max,                     &
                           y_min,                     &
                           y_max,                     &
                           total_mmcs0,               &
                           total_components0,         &
                           total_mmcs1,               &
                           total_components1,         &
                           total_mmcsmax,             &
                           total_componentsmax,       &
                           total_mmcs_halo,           &
                           total_components_halo,     &
                           number_of_threads,         &
                           mmc_counter_next,          &
                           component_counter_next,    &
                           density0,                  &
                           energy0,                   &
                           pressure,                  &
                           viscosity,                 &
                           soundspeed,                &
                           density1,                  &
                           energy1,                   &
                           xvel0,                     &
                           yvel0,                     &
                           xvel1,                     &
                           yvel1,                     &
                           vol_flux_x,                &
                           vol_flux_y,                &
                           mass_flux_x,               &
                           mass_flux_y,               &
                           material0,                 &
                           mmc_index0,                &
                           mmc_j0,                    &
                           mmc_k0,                    &
                           mmc_component0,            &
                           mmc_material0,             &
                           mmc_density0,              &
                           mmc_energy0,               &
                           mmc_pressure,              &
                           mmc_soundspeed,            &
                           mmc_viscosity,             &
                           mmc_volume0,               &
                           material1,                 &
                           mmc_index1,                &
                           mmc_j1,                    &
                           mmc_k1,                    &
                           mmc_component1,            &
                           mmc_material1,             &
                           mmc_density1,              &
                           mmc_energy1,               &
                           mmc_volume1,               &
                           fields,                    &
                           depth                      )

  USE data_module
  USE update_halo_kernel_module

  IMPLICIT NONE

  INTEGER         :: x_min,x_max,y_min,y_max
  INTEGER         :: total_mmcs0,total_components0
  INTEGER         :: total_mmcs1,total_components1
  INTEGER         :: total_mmcsmax,total_componentsmax
  INTEGER         :: total_mmcs_halo,total_components_halo
  INTEGER         :: number_of_threads
  INTEGER         :: mmc_counter_next(number_of_threads)
  INTEGER         :: component_counter_next(number_of_threads)
  REAL(KIND=8)    :: volume(x_min-2:x_max+2,y_min-2:y_max+2)
  REAL(KIND=8)    :: density0(x_min-2:x_max+2,y_min-2:y_max+2),energy0(x_min-2:x_max+2,y_min-2:y_max+2)
  REAL(KIND=8)    :: density1(x_min-2:x_max+2,y_min-2:y_max+2),energy1(x_min-2:x_max+2,y_min-2:y_max+2)
  REAL(KIND=8)    :: pressure(x_min-2:x_max+2,y_min-2:y_max+2),soundspeed(x_min-2:x_max+2,y_min-2:y_max+2)
  REAL(KIND=8)    :: viscosity(x_min-2:x_max+2,y_min-2:y_max+2)
  REAL(KIND=8)    :: mmc_density0(1:total_componentsmax),mmc_energy0(1:total_componentsmax)
  REAL(KIND=8)    :: mmc_volume0(1:total_componentsmax)
  REAL(KIND=8)    :: mmc_density1(1:total_componentsmax),mmc_energy1(1:total_componentsmax)
  REAL(KIND=8)    :: mmc_volume1(1:total_componentsmax)
  REAL(KIND=8)    :: mmc_pressure(1:total_componentsmax),mmc_soundspeed(1:total_componentsmax)
  REAL(KIND=8)    :: mmc_viscosity(1:total_componentsmax)
  REAL(KIND=8)    :: xvel0(x_min-2:x_max+3,y_min-2:y_max+3),yvel0(x_min-2:x_max+3,y_min-2:y_max+3)
  REAL(KIND=8)    :: xvel1(x_min-2:x_max+3,y_min-2:y_max+3),yvel1(x_min-2:x_max+3,y_min-2:y_max+3)
  REAL(KIND=8)    :: vol_flux_x(x_min-2:x_max+3,y_min-2:y_max+2),vol_flux_y(x_min-2:x_max+2,y_min-2:y_max+3)
  REAL(KIND=8)    :: mass_flux_x(x_min-2:x_max+3,y_min-2:y_max+2),mass_flux_y(x_min-2:x_max+2,y_min-2:y_max+3)
  INTEGER         :: material0(x_min-2:x_max+2,y_min-2:y_max+2)
  INTEGER         :: material1(x_min-2:x_max+2,y_min-2:y_max+2)
  INTEGER         :: mmc_component0(1:total_mmcsmax),mmc_material0(1:total_mmcsmax)
  INTEGER         :: mmc_component1(1:total_mmcsmax),mmc_material1(1:total_mmcsmax)
  INTEGER         :: mmc_j0(1:total_mmcsmax),mmc_k0(1:total_mmcsmax)
  INTEGER         :: mmc_j1(1:total_mmcsmax),mmc_k1(1:total_mmcsmax)
  INTEGER         :: mmc_index0(1:total_mmcsmax)
  INTEGER         :: mmc_index1(1:total_mmcsmax)
  INTEGER         :: fields(NUM_FIELDS),depth

  CALL update_halo_kernel_t(x_min,x_max,y_min,y_max,    &
                            total_mmcs0,                &
                            total_components0,          &
                            total_mmcs1,                &
                            total_components1,          &
                            total_mmcsmax,              &
                            total_componentsmax,        &
                            total_mmcs_halo,            &
                            total_components_halo,      &
                            number_of_threads,          &
                            mmc_counter_next,           &
                            component_counter_next,     &
                            density0,                   &
                            energy0,                    &
                            pressure,                   &
                            viscosity,                  &
                            soundspeed,                 &
                            density1,                   &
                            energy1,                    &
                            xvel0,                      &
                            yvel0,                      &
                            xvel1,                      &
                            yvel1,                      &
                            vol_flux_x,                 &
                            vol_flux_y,                 &
                            mass_flux_x,                &
                            mass_flux_y,                &
                            material0,                  &
                            mmc_index0,                 &
                            mmc_j0,                     &
                            mmc_k0,                     &
                            mmc_component0,             &
                            mmc_material0,              &
                            mmc_density0,               &
                            mmc_energy0,                &
                            mmc_pressure,               &
                            mmc_soundspeed,             &
                            mmc_viscosity,              &
                            mmc_volume0,                &
                            material1,                  &
                            mmc_index1,                 &
                            mmc_j1,                     &
                            mmc_k1,                     &
                            mmc_component1,             &
                            mmc_material1,              &
                            mmc_density1,               &
                            mmc_energy1,                &
                            mmc_volume1,                &
                            fields,                     &
                            depth                       )


  CALL update_halo_kernel_b(x_min,x_max,y_min,y_max,    &
                            total_mmcs0,                &
                            total_components0,          &
                            total_mmcs1,                &
                            total_components1,          &
                            total_mmcsmax,              &
                            total_componentsmax,        &
                            total_mmcs_halo,            &
                            total_components_halo,      &
                            number_of_threads,          &
                            mmc_counter_next,           &
                            component_counter_next,     &
                            density0,                   &
                            energy0,                    &
                            pressure,                   &
                            viscosity,                  &
                            soundspeed,                 &
                            density1,                   &
                            energy1,                    &
                            xvel0,                      &
                            yvel0,                      &
                            xvel1,                      &
                            yvel1,                      &
                            vol_flux_x,                 &
                            vol_flux_y,                 &
                            mass_flux_x,                &
                            mass_flux_y,                &
                            material0,                  &
                            mmc_index0,                 &
                            mmc_j0,                     &
                            mmc_k0,                     &
                            mmc_component0,             &
                            mmc_material0,              &
                            mmc_density0,               &
                            mmc_energy0,                &
                            mmc_pressure,               &
                            mmc_soundspeed,             &
                            mmc_viscosity,              &
                            mmc_volume0,                &
                            material1,                  &
                            mmc_index1,                 &
                            mmc_j1,                     &
                            mmc_k1,                     &
                            mmc_component1,             &
                            mmc_material1,              &
                            mmc_density1,               &
                            mmc_energy1,                &
                            mmc_volume1,                &
                            fields,                     &
                            depth                       )

  CALL update_halo_kernel_r(x_min,x_max,y_min,y_max,    &
                            total_mmcs0,                &
                            total_components0,          &
                            total_mmcs1,                &
                            total_components1,          &
                            total_mmcsmax,              &
                            total_componentsmax,        &
                            total_mmcs_halo,            &
                            total_components_halo,      &
                            number_of_threads,          &
                            mmc_counter_next,           &
                            component_counter_next,     &
                            density0,                   &
                            energy0,                    &
                            pressure,                   &
                            viscosity,                  &
                            soundspeed,                 &
                            density1,                   &
                            energy1,                    &
                            xvel0,                      &
                            yvel0,                      &
                            xvel1,                      &
                            yvel1,                      &
                            vol_flux_x,                 &
                            vol_flux_y,                 &
                            mass_flux_x,                &
                            mass_flux_y,                &
                            material0,                  &
                            mmc_index0,                 &
                            mmc_j0,                     &
                            mmc_k0,                     &
                            mmc_component0,             &
                            mmc_material0,              &
                            mmc_density0,               &
                            mmc_energy0,                &
                            mmc_pressure,               &
                            mmc_soundspeed,             &
                            mmc_viscosity,              &
                            mmc_volume0,                &
                            material1,                  &
                            mmc_index1,                 &
                            mmc_j1,                     &
                            mmc_k1,                     &
                            mmc_component1,             &
                            mmc_material1,              &
                            mmc_density1,               &
                            mmc_energy1,                &
                            mmc_volume1,                &
                            fields,                     &
                            depth                       )

  CALL update_halo_kernel_l(x_min,x_max,y_min,y_max,    &
                            total_mmcs0,                &
                            total_components0,          &
                            total_mmcs1,                &
                            total_components1,          &
                            total_mmcsmax,              &
                            total_componentsmax,        &
                            total_mmcs_halo,            &
                            total_components_halo,      &
                            number_of_threads,          &
                            mmc_counter_next,           &
                            component_counter_next,     &
                            density0,                   &
                            energy0,                    &
                            pressure,                   &
                            viscosity,                  &
                            soundspeed,                 &
                            density1,                   &
                            energy1,                    &
                            xvel0,                      &
                            yvel0,                      &
                            xvel1,                      &
                            yvel1,                      &
                            vol_flux_x,                 &
                            vol_flux_y,                 &
                            mass_flux_x,                &
                            mass_flux_y,                &
                            material0,                  &
                            mmc_index0,                 &
                            mmc_j0,                     &
                            mmc_k0,                     &
                            mmc_component0,             &
                            mmc_material0,              &
                            mmc_density0,               &
                            mmc_energy0,                &
                            mmc_pressure,               &
                            mmc_soundspeed,             &
                            mmc_viscosity,              &
                            mmc_volume0,                &
                            material1,                  &
                            mmc_index1,                 &
                            mmc_j1,                     &
                            mmc_k1,                     &
                            mmc_component1,             &
                            mmc_material1,              &
                            mmc_density1,               &
                            mmc_energy1,                &
                            mmc_volume1,                &
                            fields,                     &
                            depth                       )

END SUBROUTINE update_ext_halo
