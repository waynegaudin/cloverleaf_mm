!Crown Copyright 2012 AWE.
!
! This file is part of CloverLeaf.
!
! CloverLeaf is free software: you can redistribute it and/or modify it under 
! the terms of the GNU General Public License as published by the 
! Free Software Foundation, either version 3 of the License, or (at your option) 
! any later version.
!
! CloverLeaf is distributed in the hope that it will be useful, but 
! WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
! FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
! details.
!
! You should have received a copy of the GNU General Public License along with 
! CloverLeaf. If not, see http://www.gnu.org/licenses/.

!>  @brief Fortran Mixed cell packing kernel
!>  @author Wayne Gaudin
!>  @details Packs mixed cell data for best strided access for loops over the j,k grid

MODULE mmc_pack_kernel_module

CONTAINS

SUBROUTINE mmc_pack_kernel(x_min,x_max,y_min,y_max,                         &
                           total_mmcs,                                      &
                           total_components,                                &
                           max_mmcs,                                        &
                           max_components,                                  &
                           material,                                        &
                           mmc_index,                                       &
                           mmc_indexnew,                                    &
                           mmc_j,                                           &
                           mmc_k,                                           &
                           mmc_jnew,                                        &
                           mmc_knew,                                        &
                           mmc_component,                                   &
                           mmc_componentnew,                                &
                           mmc_material,                                    &
                           mmc_materialnew,                                 &
                           mmc_density,                                     &
                           mmc_densitynew,                                  &
                           mmc_energy,                                      &
                           mmc_energynew,                                   &
                           mmc_volume,                                      &
                           mmc_volumenew                                    )
  IMPLICIT NONE

  INTEGER :: x_min,x_max,y_min,y_max,total_mmcs,total_components,max_mmcs,max_components

  INTEGER(KIND=4),DIMENSION(x_min-2:x_max+2,y_min-2:y_max+2) :: material
  INTEGER(KIND=4),DIMENSION(1:max_mmcs)                    :: mmc_index
  INTEGER(KIND=4),DIMENSION(1:max_mmcs)                    :: mmc_indexnew
  INTEGER(KIND=4),DIMENSION(1:max_mmcs)                    :: mmc_component
  INTEGER(KIND=4),DIMENSION(1:max_mmcs)                    :: mmc_componentnew
  INTEGER(KIND=4),DIMENSION(1:max_mmcs)                    :: mmc_j,mmc_k
  INTEGER(KIND=4),DIMENSION(1:max_mmcs)                    :: mmc_jnew,mmc_knew
  INTEGER(KIND=4),DIMENSION(1:max_components)              :: mmc_material
  INTEGER(KIND=4),DIMENSION(1:max_components)              :: mmc_materialnew
  REAL(KIND=8),DIMENSION(1:max_components)                 :: mmc_density
  REAL(KIND=8),DIMENSION(1:max_components)                 :: mmc_densitynew
  REAL(KIND=8),DIMENSION(1:max_components)                 :: mmc_energy
  REAL(KIND=8),DIMENSION(1:max_components)                 :: mmc_energynew
  REAL(KIND=8),DIMENSION(1:max_components)                 :: mmc_volume
  REAL(KIND=8),DIMENSION(1:max_components)                 :: mmc_volumenew

  INTEGER :: mmc,mmc_new,component,component_new,j,k,mmc_counter,component_counter

  ! Loop of j,k grid and if it is an mmc, move it to the packed location
  ! Hard to thread without a critical section, even hard with it
  ! I think I will have to precalculate the index for each cell before I pack

  ! I loop over halo cells to make sure they are also packed in a decent position
  ! This means that halo exchange should be carried out before the packing takes place

  mmc_counter=1
  component_counter=1

!!$OMP PARALLEL 
!!$OMP DO PRIVATE(mmc,mmc_new,j,component,component_new)
  DO k=y_min-2,y_max+2
    DO j=x_min-2,x_max+2
      IF(material(j,k).LT.1) THEN
!!$OMP CRITICAL
        mmc_new=mmc_counter
        component_new=component_counter
!!$OMP END CRITICAL
!!$OMP ATOMIC
        mmc_counter=mmc_counter+1
!!$OMP ATOMIC
        component_counter=component_counter+1
!!$OMP END ATOMIC
        mmc=-1*material(j,k)
        material(j,k)=-mmc_new
        mmc_index(mmc_new)=component_new
        DO component=(mmc),mmc_index(mmc)+mmc_component(mmc)-1
          mmc_densitynew(mmc_indexnew(mmc_new)+component)=mmc_density(mmc_index(mmc)+component)
          mmc_energynew(mmc_indexnew(mmc_new)+component)=mmc_energy(mmc_index(mmc)+component)
          mmc_volumenew(mmc_indexnew(mmc_new)+component)=mmc_volume(mmc_index(mmc)+component)
        ENDDO
        mmc_j(mmc_new)=j
        mmc_k(mmc_new)=k
      ENDIF
    ENDDO
  ENDDO
!!$OMP END DO

!!$OMP END PARALLEL

END SUBROUTINE mmc_pack_kernel

END MODULE mmc_pack_kernel_module

