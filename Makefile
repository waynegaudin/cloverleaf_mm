#Crown Copyright 2012 AWE.
#
# This file is part of CloverLeaf.
#
# CloverLeaf is free software: you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your option) 
# any later version.
#
# CloverLeaf is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
# details.
#
# You should have received a copy of the GNU General Public License along with 
# CloverLeaf. If not, see http://www.gnu.org/licenses/.

#  @brief Makefile for CloverLeaf
#  @author Wayne Gaudin, Andy Herdman
#  @details Agnostic, platform independent makefile for the Clover Leaf benchmark code.

# It is not meant to be clever in anyway, just a simple build out of the box script.
# Just make sure mpif90 is in your path. It uses mpif90 even for all builds because this abstracts the base
#  name of the compiler. If you are on a system that doesn't use mpif90, just replace mpif90 with the compiler name
#  of choice. The only mpi dependencies in this non-MPI version are mpi_wtime in timer.f90.

# There is no single way of turning OpenMP compilation on with all compilers.
# The known compilers have been added as a variable. By default the make
#  will use no options, which will work on Cray for example, but not on other
#  compilers.
# To select a OpenMP compiler option, do this in the shell before typing make:-
#
#  export COMPILER=INTEL       # to select the Intel flags
#  export COMPILER=SUN         # to select the Sun flags
#  export COMPILER=GNU         # to select the Gnu flags
#  export COMPILER=CRAY        # to select the Cray flags
#  export COMPILER=PGI         # to select the PGI flags
#  export COMPILER=PATHSCALE   # to select the Pathscale flags
#  export COMPILER=XL          # to select the IBM Xlf flags

# or this works as well:-
#
# make COMPILER=INTEL
# make COMPILER=SUN
# make COMPILER=GNU
# make COMPILER=CRAY
# make COMPILER=PGI
# make COMPILER=PATHSCALE
# make COMPILER=XL
#

# Don't forget to set the number of threads you want to use, like so
# export OMP_NUM_THREADS=4

# usage: make                     # Will make the binary
#        make clean               # Will clean up the directory
#        make DEBUG=1             # Will select debug options. If a compiler is selected, it will use compiler specific debug options
#        make IEEE=1              # Will select debug options as long as a compiler is selected as well
# e.g. make COMPILER=INTEL MPI_COMPILER=mpiifort C_MPI_COMPILER=mpiicc DEBUG=1 IEEE=1 # will compile with the intel compiler with intel debug and ieee flags included

ifndef COMPILER
  MESSAGE=select a compiler to compile in OpenMP, e.g. make COMPILER=INTEL
endif

OMP_INTEL     = -qopenmp
#OMP_INTEL     = 
OMP_SUN       = -xopenmp=parallel -vpara
OMP_GNU       = -fopenmp
OMP_GNU       =
OMP_CRAY      = -hlist=a
OMP_PGI       = -mp=nonuma
OMP_PGI       =
OMP_PATHSCALE = -mp
OMP_XL        = -qsmp=omp -qthreaded
OMP_CLANG     = -fopenmp=libomp -fopenmp-targets=nvptx64-nvidia-cuda --cuda-path=/nfs/modules/cuda/8.0.44/
OMP=$(OMP_$(COMPILER))

FLAGS_INTEL     = -O3 -no-prec-div
FLAGS_SUN       = -fast -xipo=2 -Xlistv4
FLAGS_GNU       = -O3 -march=native -funroll-loops
FLAGS_CRAY      = -em -ra -h acc_model=fast_addr:no_deep_copy:auto_async_all
FLAGS_PGI       = -fastsse -Mipa=fast -Mlist
FLAGS_PGI       =
FLAGS_PATHSCALE = -O3
FLAGS_XL       = -O5 -qipa=partition=large -g -qfullpath -Q -qsigtrap -qextname=flush:ideal_gas_kernel_c:viscosity_kernel_c:pdv_kernel_c:revert_kernel_c:accelerate_kernel_c:flux_calc_kernel_c:advec_cell_kernel_c:advec_mom_kernel_c:reset_field_kernel_c:timer_c:unpack_top_bottom_buffers_c:pack_top_bottom_buffers_c:unpack_left_right_buffers_c:pack_left_right_buffers_c:field_summary_kernel_c:update_halo_kernel_c:generate_chunk_kernel_c:initialise_chunk_kernel_c:calc_dt_kernel_c -qlistopt -qattr=full -qlist -qreport -qxref=full -qsource -qsuppress=1506-224:1500-036
FLAGS_          = -O3
CFLAGS_INTEL     = -O3 -no-prec-div -restrict -fno-alias
CFLAGS_SUN       = -fast -xipo=2
CFLAGS_GNU       = -O3 -march=native -funroll-loops
CFLAGS_CRAY      = -em -h list=a
CFLAGS_PGI       = -fastsse -Mipa=fast -Mlist
CFLAGS_PGI       = 
CFLAGS_PATHSCALE = -O3
CFLAGS_XL       = -O5 -qipa=partition=large -g -qfullpath -Q -qlistopt -qattr=full -qlist -qreport -qxref=full -qsource -qsuppress=1506-224:1500-036 -qsrcmsg
CFLAGS_          = -O3

ifdef DEBUG
  FLAGS_INTEL     = -O0 -g -debug all -check all -traceback -check noarg_temp_created
  FLAGS_SUN       = -g -xopenmp=noopt -stackvar -u -fpover=yes -C -ftrap=common
  FLAGS_GNU       = -O0 -g -O -Wall -Wextra -fbounds-check
  FLAGS_CRAY      = -O0 -g -em -eD
  FLAGS_PGI       = -O0 -g -C -Mchkstk -Ktrap=fp -Mchkfpstk -Mchkptr
  FLAGS_PATHSCALE = -O0 -g
  FLAGS_XL       = -O0 -g -qfullpath -qcheck -qflttrap=ov:zero:invalid:en -qsource -qinitauto=FF -qmaxmem=-1 -qinit=f90ptr -qsigtrap -qextname=flush:ideal_gas_kernel_c:viscosity_kernel_c:pdv_kernel_c:revert_kernel_c:accelerate_kernel_c:flux_calc_kernel_c:advec_cell_kernel_c:advec_mom_kernel_c:reset_field_kernel_c:timer_c:unpack_top_bottom_buffers_c:pack_top_bottom_buffers_c:unpack_left_right_buffers_c:pack_left_right_buffers_c:field_summary_kernel_c:update_halo_kernel_c:generate_chunk_kernel_c:initialise_chunk_kernel_c:calc_dt_kernel_c
  FLAGS_          = -O0 -g
  CFLAGS_INTEL    = -O0 -g -debug all -traceback
  CFLAGS_SUN      = -g -O0 -xopenmp=noopt -stackvar -u -fpover=yes -C -ftrap=common
  CFLAGS_GNU       = -O0 -g -O -Wall -Wextra -fbounds-check
  CFLAGS_CRAY     = -O0 -g -em -eD
  CFLAGS_PGI      = -O0 -g -C -Mchkstk -Ktrap=fp -Mchkfpstk
  CFLAGS_PATHSCALE= -O0 -g
  CFLAGS_XL      = -O0 -g -qfullpath -qcheck -qflttrap=ov:zero:invalid:en -qsource -qinitauto=FF -qmaxmem=-1 -qsrcmsg
  CFLAGS_          = -O0 -g
endif

ifdef IEEE
  I3E_INTEL     = -fp-model strict -fp-model source -prec-div -prec-sqrt
  I3E_SUN       = -fsimple=0 -fns=no
  I3E_GNU       = -ffloat-store
  I3E_CRAY      = -hflex_mp=intolerant
  I3E_PGI       = -Kieee
  I3E_PATHSCALE = -mieee-fp
  I3E_XL       = -qfloat=nomaf
  I3E=$(I3E_$(COMPILER))
endif

# This allows the selection of alternative OMP kernels, e.g. _omp3 or _omp4
OMP_KERNEL=

FLAGS=$(FLAGS_$(COMPILER)) $(OMP) $(I3E) $(OPTIONS)
CFLAGS=$(CFLAGS_$(COMPILER)) $(OMP) $(I3E) $(C_OPTIONS) -c
MPI_COMPILER=mpif90
C_MPI_COMPILER=mpicc

ideal_gas_driver:  cell_average_kernel.f90 ideal_gas_driver.f90 set_data.f90 ideal_gas_kernel_single_mat.f90 ideal_gas_kernel.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) cell_average_kernel.f90 set_data.f90 ideal_gas_kernel.f90 ideal_gas_kernel_single_mat.f90 timer.f90 ideal_gas_driver.f90
	$(MPI_COMPILER) $(FLAGS) timer_c.o cell_average_kernel.o set_data.o ideal_gas_kernel.o ideal_gas_kernel_single_mat.o timer.o ideal_gas_driver.o -o ideal_gas_driver ; echo $(MESSAGE)

reset_field_driver:  cell_average_kernel.f90 set_data.f90 reset_field_kernel_omp4.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) cell_average_kernel.f90 set_data.f90 reset_field_kernel_omp4.f90 timer.f90 reset_field_driver.f90
	$(MPI_COMPILER) $(FLAGS) timer_c.o cell_average_kernel.o set_data.o reset_field_kernel_omp4.o imer.o reset_field_driver.o -o reset_field_driver ; echo $(MESSAGE)

PdV_driver:  cell_average_kernel.f90 PdV_driver.f90 set_data.f90 PdV_kernel_single_mat.f90 PdV_kernel_omp4.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) cell_average_kernel.f90 set_data.f90 PdV_kernel_omp4.f90 PdV_kernel_single_mat.f90 timer.f90 PdV_driver.f90
	$(MPI_COMPILER) $(FLAGS) timer_c.o cell_average_kernel.o set_data.o PdV_kernel_omp4.o PdV_kernel_single_mat.o timer.o PdV_driver.o -o PdV_driver ; echo $(MESSAGE)

accelerate_driver:  cell_average_kernel.f90 accelerate_driver.f90 set_data.f90 accelerate_kernel_omp4.f90 timer_c.c accelerate_kernel_omp4_c.c timer.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c accelerate_kernel_omp4_c.c
	$(MPI_COMPILER) -c $(FLAGS) cell_average_kernel.f90 set_data.f90 accelerate_kernel_omp4.f90 accelerate_driver.f90 timer.f90
	$(MPI_COMPILER) $(FLAGS) timer_c.o timer.o accelerate_kernel_omp_c.o cell_average_kernel.o set_data.o accelerate_kernel_omp4.o accelerate_driver.o -o accelerate_driver ; echo $(MESSAGE)

calc_dt_driver:  cell_average_kernel.f90 calc_dt_driver.f90 set_data.f90 calc_dt_kernel_single_mat.f90 calc_dt_kernel.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) cell_average_kernel.f90 set_data.f90 calc_dt_kernel.f90 calc_dt_kernel_single_mat.f90 timer.f90 calc_dt_driver.f90
	$(MPI_COMPILER) $(FLAGS) timer_c.o cell_average_kernel.o set_data.o calc_dt_kernel.o calc_dt_kernel_single_mat.o timer.o calc_dt_driver.o -o calc_dt_driver ; echo $(MESSAGE)

mmc_cell_driver:  timer.f90 mmc_pack_kernel.f90 cell_average_kernel.f90 mmc_cell_driver.f90 set_data.f90 interface.f90 advec_mmc_x_cell_kernel_omp4.f90 advec_cell_kernel.f90 advec_mmc_y_cell_kernel_omp4.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) timer.f90 mmc_pack_kernel.f90 cell_average_kernel.f90 set_data.f90 interface.f90 advec_mmc_x_cell_kernel_omp4.f90 advec_cell_kernel.f90 advec_mmc_y_cell_kernel_omp4.f90 mmc_cell_driver.f90
	$(MPI_COMPILER) $(FLAGS) timer.o timer_c.o mmc_pack_kernel.o cell_average_kernel.o set_data.o mmc_cell_driver.o interface.o advec_mmc_x_cell_kernel_omp4.o advec_mmc_y_cell_kernel_omp4.o advec_cell_kernel.o -o mmc_cell_driver ; echo $(MESSAGE)

hydro_driver:  timer.f90 data.f90 update_halo_kernel.f90 cell_average_kernel.f90 set_data.f90 viscosity_kernel.f90 field_summary_kernel.f90 calc_dt_kernel.f90 revert_kernel.f90 ideal_gas_kernel.f90 accelerate_kernel.f90 interface.f90 PdV_kernel.f90 flux_calc_kernel.f90 advec_mmc_x_cell_kernel.f90 advec_mmc_y_cell_kernel.f90 advec_mom_kernel.f90 reset_field_kernel.f90 visit_kernel.f90 hydro_driver.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) timer.f90 data.f90 update_halo_kernel.f90 cell_average_kernel.f90 set_data.f90  viscosity_kernel.f90 field_summary_kernel.f90 calc_dt_kernel.f90 revert_kernel.f90 ideal_gas_kernel.f90 accelerate_kernel.f90 interface.f90 PdV_kernel.f90 flux_calc_kernel.f90 advec_mmc_x_cell_kernel.f90 advec_mmc_y_cell_kernel.f90 advec_mom_kernel.f90 reset_field_kernel.f90 visit_kernel.f90 hydro_driver.f90 
	$(MPI_COMPILER) $(FLAGS) timer.o timer_c.o data.o update_halo_kernel.o cell_average_kernel.o set_data.o   viscosity_kernel.o field_summary_kernel.o calc_dt_kernel.o hydro_driver.o revert_kernel.f90 ideal_gas_kernel.o accelerate_kernel.o interface.o PdV_kernel.o flux_calc_kernel.o advec_mmc_x_cell_kernel.o advec_mmc_y_cell_kernel.o advec_mom_kernel.f90 reset_field_kernel.o visit_kernel.o -o hydro_driver ; echo $(MESSAGE)

hydro_driver_omp4:  timer.f90 data.f90 cell_average_kernel.f90 set_data.f90 update_halo_kernel_omp4.f90 viscosity_kernel_omp4.f90 field_summary_kernel_omp4.f90 calc_dt_kernel_omp4.f90 revert_kernel_omp4.f90 ideal_gas_kernel_omp4.f90 accelerate_kernel_omp4.f90 interface_omp4.f90 PdV_kernel_omp4.f90 flux_calc_kernel_omp4.f90 advec_mmc_x_cell_kernel_omp4.f90 advec_mmc_y_cell_kernel_omp4.f90 advec_mom_kernel_omp4.f90 reset_field_kernel_omp4.f90 visit_kernel.f90 hydro_driver_omp4.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) timer.f90 data.f90 cell_average_kernel.f90 set_data.f90 update_halo_kernel_omp4.f90 viscosity_kernel_omp4.f90 field_summary_kernel_omp4.f90 calc_dt_kernel_omp4.f90 revert_kernel_omp4.f90 ideal_gas_kernel_omp4.f90 accelerate_kernel_omp4.f90 interface_omp4.f90 PdV_kernel_omp4.f90 flux_calc_kernel_omp4.f90 advec_mmc_x_cell_kernel_omp4.f90 advec_mmc_y_cell_kernel_omp4.f90 advec_mom_kernel_omp4.f90 reset_field_kernel_omp4.f90 visit_kernel.f90 hydro_driver_omp4.f90
	$(MPI_COMPILER) $(FLAGS) timer.o timer_c.o data.o cell_average_kernel.o set_data.o update_halo_kernel_omp4.o viscosity_kernel_omp4.o field_summary_kernel_omp4.o calc_dt_kernel_omp4.o hydro_driver_omp4.o revert_kernel_omp4.f90 ideal_gas_kernel_omp4.o accelerate_kernel_omp4.o interface_omp4.o PdV_kernel_omp4.o flux_calc_kernel_omp4.o advec_mmc_x_cell_kernel_omp4.o advec_mmc_y_cell_kernel_omp4.o advec_mom_kernel_omp4.f90 reset_field_kernel_omp4.o visit_kernel.o -o hydro_driver_omp4 ; echo $(MESSAGE)

hydro_driver_omp3:  timer.f90 data.f90 update_halo_kernel.f90 cell_average_kernel.f90 set_data.f90 viscosity_kernel_omp3.f90 field_summary_kernel_omp3.f90  calc_dt_kernel_omp3.f90 revert_kernel_omp3.f90 ideal_gas_kernel_omp3.f90 accelerate_kernel_omp3.f90 interface.f90 PdV_kernel_omp3.f90 flux_calc_kernel_omp3.f90 advec_mmc_x_cell_kernel_omp3.f90 advec_mmc_y_cell_kernel_omp3.f90 advec_mom_kernel_omp3.f90 reset_field_kernel_omp3.f90 visit_kernel.f90 hydro_driver_omp3.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) timer.f90 data.f90 update_halo_kernel.f90 cell_average_kernel.f90 set_data.f90 viscosity_kernel_omp3.f90 field_summary_kernel_omp3.f90 calc_dt_kernel_omp3.f90 revert_kernel_omp3.f90 ideal_gas_kernel_omp3.f90 accelerate_kernel_omp3.f90 interface.f90 PdV_kernel_omp3.f90 flux_calc_kernel_omp3.f90 advec_mmc_x_cell_kernel_omp3.f90 advec_mmc_y_cell_kernel_omp3.f90 advec_mom_kernel_omp3.f90 reset_field_kernel_omp3.f90 visit_kernel.f90 hydro_driver_omp3.f90
	$(MPI_COMPILER) $(FLAGS) timer.o timer_c.o data.o update_halo_kernel.o cell_average_kernel.o set_data.o viscosity_kernel_omp3.o field_summary_kernel_omp3.o calc_dt_kernel_omp3.o hydro_driver_omp3.o revert_kernel_omp3.f90 ideal_gas_kernel_omp3.o accelerate_kernel_omp3.o interface.o PdV_kernel_omp3.o flux_calc_kernel_omp3.o advec_mmc_x_cell_kernel_omp3.o advec_mmc_y_cell_kernel_omp3.o advec_mom_kernel_omp3.f90 reset_field_kernel_omp3.o visit_kernel.o -o hydro_driver_omp3 ; echo $(MESSAGE)

# make hydro_driver_oacc COMPILER=PGI OPTIONS="-ta=nvidia,cc35" MPI_COMPILER=pgf90
# plus the PGI flags need some options removing on IBM
# or make clean; make hydro_driver_oacc COMPILER=PGI OPTIONS="-ta=multicore" MPI_COMPILER=pgf90 # to target the host

hydro_driver_oacc:  timer.f90 cell_average_kernel.f90 set_data.f90 viscosity_kernel_oacc.f90 field_summary_kernel_oacc.f90  calc_dt_kernel_oacc.f90 revert_kernel_oacc.f90 ideal_gas_kernel_oacc.f90 accelerate_kernel_oacc.f90 interface_oacc.f90 PdV_kernel_oacc.f90 flux_calc_kernel_oacc.f90 advec_mmc_x_cell_kernel_oacc.f90 advec_mmc_y_cell_kernel_oacc.f90 advec_mom_kernel_oacc.f90 reset_field_kernel_oacc.f90 visit_kernel_oacc.f90 hydro_driver_oacc.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) timer.f90 cell_average_kernel.f90 set_data.f90 viscosity_kernel_oacc.f90 field_summary_kernel_oacc.f90 calc_dt_kernel_oacc.f90 revert_kernel_oacc.f90 ideal_gas_kernel_oacc.f90 accelerate_kernel_oacc.f90 interface_oacc.f90 PdV_kernel_oacc.f90 flux_calc_kernel_oacc.f90 advec_mmc_x_cell_kernel_oacc.f90 advec_mmc_y_cell_kernel_oacc.f90 advec_mom_kernel_oacc.f90 reset_field_kernel_oacc.f90 visit_kernel_oacc.f90 hydro_driver_oacc.f90
	$(MPI_COMPILER) $(FLAGS) timer.o timer_c.o cell_average_kernel.o set_data.o viscosity_kernel_oacc.o field_summary_kernel_oacc.o calc_dt_kernel_oacc.o hydro_driver_oacc.o revert_kernel_oacc.f90 ideal_gas_kernel_oacc.o accelerate_kernel_oacc.o interface_oacc.o PdV_kernel_oacc.o flux_calc_kernel_oacc.o advec_mmc_x_cell_kernel_oacc.o advec_mmc_y_cell_kernel_oacc.o advec_mom_kernel_oacc.f90 reset_field_kernel_oacc.o visit_kernel_oacc.o -o hydro_driver_oacc ; echo $(MESSAGE)

cell_driver_omp4:  timer.f90 cell_average_kernel.f90 cell_driver_omp4.f90 set_data.f90 interface.f90 advec_mmc_x_cell_kernel_omp4.f90
	$(C_MPI_COMPILER) $(CFLAGS) timer_c.c
	$(MPI_COMPILER) -c $(FLAGS) timer.f90 cell_average_kernel.f90 set_data.f90 interface.f90 advec_mmc_x_cell_kernel_omp4.f90 cell_driver_omp4.f90
	$(MPI_COMPILER) $(FLAGS) timer.o timer_c.o cell_average_kernel.o set_data.o cell_driver_omp4.o interface.o advec_mmc_x_cell_kernel_omp4.o -o cell_driver_omp4 ; echo $(MESSAGE)

# clang example command line clang -fopenmp=libomp -fopenmp-targets=nvptx64-nvidia-cuda --cuda-path=/nfs/modules/cuda/8.0.44/ -c revert_kernel_c.c

clover_leaf: c_lover *.f90 Makefile
	$(MPI_COMPILER) $(FLAGS)		\
	data.f90				\
	definitions.f90				\
	pack_kernel.f90				\
	clover.f90				\
	report.f90				\
	timer.f90				\
	parse.f90				\
	read_input.f90				\
	initialise_chunk_kernel.f90		\
	initialise_chunk.f90			\
	build_field.f90				\
	update_tile_halo_kernel.f90		\
	update_tile_halo.f90			\
	update_halo_kernel.f90			\
	update_halo.f90				\
	ideal_gas_kernel$(OMPKERNEL).f90	\
	ideal_gas_kernel_single_mat.f90		\
	ideal_gas.f90				\
	generate_chunk_kernel.f90		\
	generate_chunk.f90			\
	start.f90				\
	initialise.f90				\
	field_summary_kernel$(OMPKERNEL).f90	\
	field_summary.f90			\
	viscosity_kernel$(OMPKERNEL).f90	\
	viscosity.f90				\
	calc_dt_kernel$(OMPKERNEL).f90		\
	calc_dt.f90				\
	timestep.f90				\
	accelerate_kernel$(OMPKERNEL).f90	\
	accelerate.f90				\
	revert_kernel$(OMPKERNEL).f90		\
	revert.f90				\
	PdV_kernel$(OMPKERNEL).f90		\
	PdV_kernel_single_mat.f90		\
	PdV.f90					\
	flux_calc_kernel$(OMPKERNEL).f90	\
	flux_calc.f90				\
	interface.f90				\
	advec_cell_kernel.f90			\
	advec_mmc_x_cell_kernel$(OMPKERNEL).f90	\
	advec_mmc_y_cell_kernel$(OMPKERNEL).f90	\
	advec_cell_driver.f90			\
	advec_mom_kernel$(OMPKERNEL).f90	\
	advec_mom_driver.f90			\
	advection.f90				\
	reset_field_kernel$(OMPKERNEL).f90	\
	reset_field.f90				\
	hydro.f90				\
	visit.f90				\
	print_cell.f90				\
	clover_leaf.f90				\
	timer_c.o                       	\
	-o clover_leaf; echo $(MESSAGE)

c_lover: *.c Makefile
	$(C_MPI_COMPILER) $(CFLAGS)     \
	timer_c.c


clean:
	rm -f *.o *.mod *genmod* *cuda* *hmd* *.cu *.oo *.hmf *.lst *.cub *.ptx *.cl clover_leaf *_driver *_driver_omp4 *driver_omp3 *driver_oacc *.exe *.modmic

very_clean: clean
	rm -f *.vtk *.visit *~
